/*
 * leaderboard.js
 *
 * Nathan Cooper
 *
 * Main workhorse Leaderboard JS class.
 */

function Leaderboard(scope, options) {
    this.scope = scope;
    this.options = $.extend(true, {}, Leaderboard.DEFAULT_OPTIONS, options);

    this.isUsingTeamID = (typeof this.options.teamID != 'undefined' && this.options.teamID != null) ? true : false;
    this.lastUpdateAt = new Date();

    this.updateState = {
        isUpdating : false,             //Whether an update is currently in progress.
        updateCallback : null,          //The callback to run once the update is finished.
        categoriesToUpdate : null,      //How many categories have to be updated this update.
        categoriesUpdated : null        //How many categories have finished updating this update.
    };

    if (this.options.mode == Leaderboard.constants.PERIODIC){
        this.nextUpdateAt = new Date(this.lastUpdateAt.getTime() + this.options.updateInterval);
    }

    if (this.options.allowPlayback) {
        this.playbackState = {
            captureIndex: 0,
            playWasPressed: false
        };
    }

    this.setUpdateOptions();
}

Leaderboard.CATEGORY_DISPLAY_MODES = {};

Leaderboard.constants = {
    LIVE : 1,
    STATIC : 2,
    PERIODIC : 3,
    GOOD_BACKGROUND_COLOR : '#CCFFCC',
    BAD_BACKGROUND_COLOR : '#FFCCCC',
    AMBIGUOUS_BACKGROUND_COLOR : '#CCCCFF'
};

Leaderboard.DEFAULT_OPTIONS = {
    basePath: null,                                     //The base path for links.
    teamID : null,                                      //Which team to filter on.
    makeTeamLinks : true,                               //Whether to turn each entry into a link to the team leaderboard.
    console : null,                                     //The DOM element to use as the console.
    mode : Leaderboard.constants.STATIC,                //One of the update modes defined above.
    updateInterval : 1000 * 30,                         //How often to update the PERIODIC boar.
    rankingsURL : null,                                 //The path to obtain static rankings.
    websocketPath : null,                               //Which websocket path to connect to.
    websocketSubscription : null,                       //Which websocket event to subscribe to.
    masonryTarget : '.category-grouping',               //The selector container for masonry behavior.
    linkContributingCaptures : false,                   //Whether to make links to each rank's contributing captures.
    capturesList : null                                 //Where to look for contributing captures.
};

Leaderboard.prototype.start = function(/*Function*/ callback) {

    //Update updateOptions;
    this.setupUI();

    //Fetch the initial data.
    var leaderboard = this;
    this.fetchUpdateData(
        function(data){
            leaderboard.update(data,false,function(){
                //Run our callback;
                $(leaderboard.scope).find('table').each(function(n,table){
                    leaderboard.onFirstTableUpdate(table); //This is guaranteed to the be the first time we are fetching an update.
                });
                leaderboard.startUpdates();
                callback();
            });
        }
    );
};

Leaderboard.prototype.startUpdates = function(){
    var leaderboard = this;
    switch (this.options.mode){
        case Leaderboard.constants.LIVE:
            //Bind to the websocket.
            this.setupWebsocket(false); //But don't immediately refresh.
            break;
        case Leaderboard.constants.STATIC:
            //Nothing to do, the leaderboard is already populated.
            break;
        case Leaderboard.constants.PERIODIC:
            //Setup polling interval.
            window.setInterval(function(){
                var now = (new Date()).getTime();
                if (now < leaderboard.nextUpdateAt.getTime()){
                    //Update time to next update.
                    var diff = Math.floor((leaderboard.nextUpdateAt.getTime() - now)/1000);
                    $(leaderboard.options.console).find('.next-update').html('Next Update In ' + Leaderboard.padNumber(diff,2) + ' Seconds');
                }
                else {
                    //Perform the update.
                    $(leaderboard.options.console).find('.next-update').html('Updating...');
                    leaderboard.nextUpdateAt = new Date((new Date()).getTime() + leaderboard.options.updateInterval + 2000);
                    leaderboard.fetchUpdateData(function(data) {
                        leaderboard.update(data, true, null);
                    });
                }
            },1000);
            break;
    }
};

Leaderboard.prototype.onFirstTableUpdate = function(table){
    this.initMarquee(table);
    this.initMason();
    if ($(table).find('.rank').length == 0) {
        $(table).find('.empty-category-row').show();
    }
    else{
        $(table).find('.empty-category-row').hide();
    }
};

Leaderboard.prototype.updateConsoleTime = function(){
    var diff = Math.round(((new Date()).getTime() - leaderboard.lastUpdateAt.getTime())/1000);
    $(leaderboard.options.console).find(".last-update").html("(" + Leaderboard.fromNow(diff) + ")");
};

Leaderboard.prototype.getLiveStatusLight = function(dispatcher){
    var clazz = 'pending';
    if (dispatcher != null){
        clazz = dispatcher.state
    }
    return '<div class="live-status-indicator"><div class="bulb ' + clazz + '"></div></div>';
};

Leaderboard.prototype.updateLiveStatus = function(){
    if (this.options.mode == Leaderboard.constants.LIVE) {
        var status = 'Connecting...';
        if (this.dispatcher != null){
            switch (this.dispatcher.state){
                case 'connected':
                    status = 'Live';
                    break;
                case 'connecting':
                    status = 'Connecting...';
                    break;
                case 'disconnected':
                    status = 'Disconnected';
                    break;
                default:
                    break;
            }
        }
        $(leaderboard.options.console).find(".live-status").html(status + '&nbsp;' + this.getLiveStatusLight(this.dispatcher));
    }
};

Leaderboard.prototype.showMessage = function(/*String*/ message, /*String*/ flashColor, /*boolean*/ updateTimestamp){
    //console.log(message);
    if (this.options.console){
        $(this.options.console).find(".message").html(message);
        if (updateTimestamp) {
            this.lastUpdateAt = new Date();
        }
        this.updateConsoleTime();
        this.updateLiveStatus();
        if (flashColor) {
            //An update was triggered, so flash the console divs.
            Leaderboard.flashElement($(this.options.console).find('.message').first(), 'color', null, flashColor, 2500, 12);
            Leaderboard.flashElement($(this.options.console).find('.last-update').first(), 'color', null, flashColor, 2500, 12);
            Leaderboard.flashElement($(this.options.console).find('.next-update').first(), 'color', null, flashColor, 2500, 12);
        }
    }
};

Leaderboard.prototype.fetchUpdateData = function(/*Function(data)*/ successCallback){
    var leaderboard = this;
    $.ajax({
        url : leaderboard.options.rankingsURL,
        type : 'GET',
        dataType : 'json',
        success : function(data){
            successCallback(data);
        },
        error : function(){
            leaderboard.showMessage('Could not fetch update.','red',true);
        }
    });
};

//HELPERS

Leaderboard.prototype.setupUI = function(){
    var leaderboard = this;
    if (this.options.allowPlayback){
        //Setup playback controls.
        $(this.scope).find('.fa-play').click(function() {
            if (leaderboard.playbackState.playWasPressed) {
                leaderboard.resumePlayback();
            } else {
                leaderboard.startPlayback();
            }
        });
        $(this.scope).find('.fa-pause').click(function() {
            leaderboard.pausePlayback();
        });
        $(this.scope).find('.fa-times').click(function() {
            leaderboard.stopPlayback();
        });
    }
    if (this.options.console){
        //Show or hide the live status icon.
        if (this.options.mode == Leaderboard.constants.LIVE){
            $(this.options.console).find('.live-status').show();
            $(this.options.console).find('.next-update').hide();
            window.setInterval(function(){
                leaderboard.updateConsoleTime();
                leaderboard.updateLiveStatus();
            },1000);
        }
        else if (this.options.mode == Leaderboard.constants.STATIC){
            $(this.options.console).find('.last-update').hide();
            $(this.options.console).find('.live-status').hide();
            $(this.options.console).find('.next-update').hide();
        }
        else if (this.options.mode == Leaderboard.constants.PERIODIC){
            $(this.options.console).find('.last-update').hide();
            $(this.options.console).find('.live-status').hide();
            $(this.options.console).find('.next-update').show();
        }
    }
    if (this.options.capturesList){
        this.setupReverseCaptureLookup();
    }
};

Leaderboard.prototype.setUpdateOptions = function(){
    var leaderboard = this;
    this.updateOptions = {
        duration: [1000,200,700,200,1000],
        animationSettings: {
            up: { backgroundColor: Leaderboard.constants.GOOD_BACKGROUND_COLOR, left: 0 },
            down: { backgroundColor: Leaderboard.constants.BAD_BACKGROUND_COLOR, left: 0  },
            fresh: { backgroundColor: Leaderboard.constants.GOOD_BACKGROUND_COLOR, left: 0 },
            drop: { backgroundColor: Leaderboard.constants.BAD_BACKGROUND_COLOR, left: 0 }
        },
        tableChangedFunction: function (newTable, oldTable) {

            //Kill this table's marquee since we know it will change.
            leaderboard.removeMarquee(newTable);
            leaderboard.removeMarquee(oldTable);

            //Save us some time.
            newTable = $(newTable);
            oldTable = $(oldTable);

            //Clear out the classes.
            newTable.removeClass("unaltered-table");
            newTable.addClass("altered-table");
            oldTable.removeClass("unaltered-table");
            oldTable.addClass("altered-table");
            Leaderboard.removeIndicatorClassesForTable(newTable);
            Leaderboard.removeIndicatorClassesForTable(oldTable);

            //Go ahead and remove indicators.
            oldTable.find('.ranking-indicator .text').html('&nbsp;');
            newTable.find('.ranking-indicator .text').html('&nbsp;');

            //Show updating icons.
            oldTable.closest('.category').find('.updating-icon').show();
        },
        tableUnchangedFunction: function (newTable, oldTable) {
            Leaderboard.updateIndicatorsForTable(newTable);
            Leaderboard.updateIndicatorsForTable(oldTable);
            $(oldTable).removeClass("altered-table");
            $(oldTable).addClass("unaltered-table");
            $(newTable).removeClass("altered-table");
            $(newTable).addClass("unaltered-table");
        },
        getCustomState: function (newRow, oldRow) {
            //Save us some time.
            oldRow = $(oldRow);
            newRow = $(newRow);

            //Checks for a custom change if the row is otherwise unchanged.
            var originalID = oldRow.find('.ranking-hash').html();
            var cloneID = newRow.find('.ranking-hash').html();

            if (originalID == cloneID) {
                var originalScore = oldRow.find('.ranking-score').html();
                var originalPosition = oldRow.find('.ranking-ordinal').html();
                var cloneScore = newRow.find('.ranking-score').html();
                var clonePosition = newRow.find('.ranking-ordinal').html();

                if (originalPosition == clonePosition && (originalScore != cloneScore)) {
                    //Something changed, but the positions are the same.
                    return 'score-updated';
                }
                else if (leaderboard.isUsingTeamID) {
                    var originalTeamPosition = oldRow.find('.ranking-team-position').html();
                    var cloneTeamPosition = newRow.find('.ranking-team-position').html();
                    if (originalID == cloneID && originalTeamPosition == cloneTeamPosition) {
                        //The team positions are the same, yes, but their overall positions might have changed.
                        var oldPosition = parseInt(oldRow.find('.ranking-position').html());
                        var newPosition = parseInt(newRow.find('.ranking-position').html());
                        if (oldPosition < newPosition) {
                            return 'virtual-down';
                        }
                        else if (oldPosition > newPosition) {
                            return 'virtual-up';
                        }
                    }
                }
            }
            //No custom state.
            return null;
        },
        rowChangedFunction: function (newRow, oldRow, state) {
            //Runs anytime a row is decided to be changed.
            $(oldRow).addClass('ranking-changed-' + state);
            $(newRow).addClass('ranking-changed-' + state);
        },
        rowUnchangedFunction: function (newRow, oldRow) {
            //Runs anytime a row is decided to be unchanged.
            $(oldRow).addClass('ranking-unchanged');
            $(newRow).addClass('ranking-unchanged');
        },
        beforeAnimate: function (newTable, oldTable) {
            //Flash things that were updated but didn't move.
            oldTable = $(oldTable);
            oldTable.find('.ranking-changed-score-updated').each(function () {
                Leaderboard.flashElement($(this).find('td'), 'background-color', Leaderboard.constants.AMBIGUOUS_BACKGROUND_COLOR, null, 2000, 10);
            });
            oldTable.find('.ranking-changed-virtual-up').each(function () {
                Leaderboard.flashElement($(this).find('td'), 'background-color', Leaderboard.constants.GOOD_BACKGROUND_COLOR, null, 2000, 10);
            });
            oldTable.find('.ranking-changed-virtual-down').each(function () {
                Leaderboard.flashElement($(this).find('td'), 'background-color', Leaderboard.constants.BAD_BACKGROUND_COLOR, null, 2000, 10);
            });
        },
        afterAnimate: function (newTable, oldTable) {
            //Add the proper text to the indicators.
            leaderboard.initMarquee(newTable);
            Leaderboard.updateIndicatorsForTable(newTable);
            $(newTable).closest('.category').find('.updating-icon').hide();
            if ($(newTable).find('.rank').length == 0) {
                $(newTable).find('.empty-category-row').show();
            }
            else{
                $(newTable).find('.empty-category-row').hide();
            }
        },
        onComplete: function () {
            leaderboard.updateState.categoriesUpdated++;
            if (leaderboard.updateState.categoriesUpdated == leaderboard.updateState.categoriesToUpdate) {
                leaderboard.finishUpdating();
            }
        }
    };
};

Leaderboard.prototype.finishUpdating = function(){
    leaderboard.updateState.isUpdating = false;
    leaderboard.initMason();
    leaderboard.showUpdateMessage(false,false);
    //console.log("Update complete.");
    leaderboard.updateState.updateCallback();
};

//WEB SOCKETS (FOR LIVE LEADERBOARDS)

Leaderboard.prototype.setupWebsocket = function(/*boolean*/ runRefreshOnConnect){
    if (this.options.mode == Leaderboard.constants.LIVE) {
        if (this.dispatcher != null){
            this.dispatcher.disconnect();
        }
        this.dispatcher = new WebSocketRails(this.options.websocketPath);
        var leaderboard = this;
        this.dispatcher.on_open = function(){
            //console.log("Websocket connection established.");
            leaderboard.updateLiveStatus();
            if (runRefreshOnConnect){
                leaderboard.fetchUpdateData(
                    function(data){
                        leaderboard.update(data,true,null);
                    }
                );
            }
        };
        this.dispatcher.bind('connection_closed', function(){
            //console.log("Websocket connection disconnected. Attempting to reconnect...");
            leaderboard.updateLiveStatus();
            leaderboard.setupWebsocket(true);
        });
        this.channel = null;
        this.bindSocket();
    }
};

Leaderboard.prototype.bindSocket = function() {
    if (this.options.mode == Leaderboard.constants.LIVE) {
        var leaderboard = this;
        this.channel = this.dispatcher.subscribe(this.options.websocketSubscription);
        this.channel.bind('update', function (data) {
            leaderboard.update(JSON.parse(data),true,null);
        });
    }
};

Leaderboard.prototype.unbindSocket = function() {
    if (this.options.mode == Leaderboard.constants.LIVE) {
        this.dispatcher.unsubscribe(this.options.websocketSubscription);
        this.channel.destroy();
        this.channel = null;
    }
};

//UPDATING

Leaderboard.prototype.showUpdateMessage = function(/*boolean*/ animate, /*boolean*/ updateTimestamp){
    var updateTime = Leaderboard.formatTime(this.lastUpdateAt);
    switch (this.options.mode){
        case Leaderboard.constants.LIVE:
            if (this.updateState.isUpdating){
                var message = 'Updating...';
            }
            else {
                var message = "Updated " + updateTime;
            }
            break;
        case Leaderboard.constants.STATIC:
            var message = "Standings As of " + updateTime + '<br/>(Refresh Page To Update)';
            break;
        default:
            var message = "Updated " + updateTime;
            break;
    }
    this.showMessage(message,(animate ? 'lightgreen' : null),updateTimestamp);
};

Leaderboard.prototype.update = function(/*Object*/ json, /*boolean*/ animate, /*Function*/ callback) {

    var leaderboard = this;

    //Make sure we have our data.
    if (json == null){
        return;
    }

    if (callback == null || typeof callback == 'undefined'){
        callback = function() {};
    }

    if (this.updateState.isUpdating){
        //We are already updating. Queue this update so it occurs after this one.
        var oldCallback = this.updateState.updateCallback;
        this.updateState.updateCallback = function(){
            oldCallback();
            leaderboard.update(json,animate,callback);
        };
        //console.log("Updated queued.");
        return;
    }

    //Get ready to update.
    this.updateState.isUpdating = true;
    this.updateState.categoriesToUpdate = 0;
    this.updateState.categoriesUpdated = 0;
    this.updateState.updateCallback = callback;

    this.showUpdateMessage(animate,true);
    this.clearSelection(); //We clear the selection every time we update.
    var $table = null; var $clone = null;
    var currentCategoryID = null;

    //Iterate through every ranking and update the appropriate category.
    for (var i = 0; i < json.rankings.length; i++) {

        var rank = json.rankings[i];
        if (rank.category_id != currentCategoryID) {
            //Start a new category than what we were on before.
            if (currentCategoryID != null) {
                //We are finished with the previous category. Update it using the API.
                this.updateState.categoriesToUpdate++;
                $table.rankingTableUpdate($clone, this.updateOptions);
            }
            currentCategoryID = rank.category_id;
            $table = $(this.scope).find('.category-' + rank.category_id + ' table');
            if ($table.length == 0) {
                //Category was not found (i.e. not in this page). Skip this record.
                $table = null;
                currentCategoryID = null;
                continue;
            }
            //If it's the first update, we simply treat the original as the clone.
            $clone = animate ? $table.clone() : $table;
        }

        if (this.isUsingTeamID && rank.team_id != this.options.teamID) {
            //We are not interested in other teams for the team leaderboard.
        }
        else {
            //Overwrite the cell contents in the clone.
            var activePosition = (this.isUsingTeamID ? rank.team_position : rank.position);
            var $clone_row = $clone.find('.rank-' + activePosition);
            if ($clone_row.length < 1) {
                //We don't have any such place yet. Make it.
                $clone_row = $("<tr class=\"rank position-" + rank.position + " rank-" + activePosition + "\"></tr>");
                $clone.find('tbody').append($clone_row);
            }
            $clone_row.html(leaderboard.HTMLForRank(rank));
            $clone_row.attr('title',leaderboard.titleForRank(rank));
            $clone_row[0].rankData = rank;

            if (this.options.linkContributingCaptures) {
                this.bindContributingCaptures($clone_row,rank.contributing_captures);
            }
        }
    }
    if ($table != null) {
        //Update the last category that wouldn't have been caught above.
        this.updateState.categoriesToUpdate++;
        $table.rankingTableUpdate($clone, this.updateOptions);
    }

    if (this.updateState.categoriesToUpdate == 0){
        //There were no categories to update, so rankingTablesUpdate was never called.
        this.finishUpdating();
    }
};

//RANKINGS

Leaderboard.prototype.titleForRank = function(/*Object*/ rank){
    if (rank.is_empty){
        return '';
    }
    else if (rank.last_capture_at == rank.first_capture_at){
        return 'Entered At:\n' + Leaderboard.formatTimestamp(rank.first_capture_at);
    }
    else {
        return 'Entered Between:\n' + Leaderboard.formatTimestamp(rank.first_capture_at) + '\nand\n' + Leaderboard.formatTimestamp(rank.last_capture_at);
    }
};

Leaderboard.prototype.HTMLForRank = function(/*Object*/ rank){
    var s = "";
    s += '<td class="ranking-hash" style="display:none"><div class="text">' + rank.entity_hash + '</div></td>';
    s += '<td class="ranking-position" style="display:none"><div class="text">' + rank.position + '</div></td>';
    s += '<td class="ranking-team-position" style="display:none"><div class="text">' + rank.team_position + '</div></td>';
    s += '<td class="' + (rank.is_empty ? 'ranking-indicator empty' : 'ranking-indicator') + '"><div class="text">&nbsp;</div></td>';
    s += '<td class="ranking-ordinal reward-icon-money">';
        s += '<div class="text">' + Leaderboard.makeOrdinalLabel(rank.position_label) + '&nbsp;</div>';
    s += '</td>';
    if (rank.is_empty){
        //Empty placeholder row.
        s += '<td class="ranking-label empty"><div class="text entity">(Empty)</div></td>';
        s += '<td class="ranking-score"><div class="text">&nbsp;</div></td>';
    }
    else {
        //Regular row.
        s += '<td class="ranking-label"><div class="text entity">' + this.makeEntityLabel(rank) + '</div><div class="text contributed-points" style="display:none;"></div></td>';
        s += '<td class="ranking-score"><div class="text">' + Leaderboard.makeScore(rank,Leaderboard.CATEGORY_DISPLAY_MODES[rank.category_id]) + '&nbsp;&nbsp;</div></td>';
    }
    return s;
};

Leaderboard.prototype.makeEntityLabel = function(rank){
    if (this.options.makeTeamLinks && rank.team_id != null){
        return '<a class="team-link" href="' + this.options.basePath + '/teams/' + rank.team_id + '">' + rank.entity_label + '</a>'
    }
    else {
        return rank.entity_label;
    }
};

//CORRESPONDING CAPTURE LIST

Leaderboard.prototype.clearSelection = function(){
    //Clear contributing captures lookup.
    $(this.scope).find(".selected").removeClass("selected");
    if (this.options.capturesList != null){
        $(this.options.capturesList).find(".highlighted").removeClass("highlighted");
    }
    //Clear reverse captures lookup.
    $(this.scope).find(".highlighted").removeClass("highlighted");
    if (this.options.capturesList != null){
        $(this.options.capturesList).find(".selected").removeClass("selected");
    }
    //Remove contributed points list.
    $(this.scope).find('.contributed-points').hide();
    $(this.scope).find('.entity').show();
};

Leaderboard.prototype.bindContributingCaptures = function(row){
    var leaderboard = this;
    $(row).addClass('selectable');
    $(row).on('click', function() {
        leaderboard.selectRank(this);
    });
};

Leaderboard.prototype.selectRank = function(/*Element*/ rank){
    var leaderboard = this;
    if ($(rank).hasClass("selected")){
        this.clearSelection();
    }
    else {
        this.clearSelection();
        $(rank).addClass("selected");
        //Highlight all contributing ranks.
        var contributingRanks = $(rank)[0].rankData.contributing_ranks;
        if (contributingRanks != null) {
            for (var i = 0; i < contributingRanks.length; i++) {
                var contributingRank = $(leaderboard.scope).find(".category-" + contributingRanks[i][0] + " .rank.position-" + contributingRanks[i][1]);
                contributingRank.find(".contributed-points").html(Leaderboard.makeUnitSpan(contributingRanks[i][2],' Points'));
                contributingRank.find(".entity").hide();
                contributingRank.find(".contributed-points").show();
                contributingRank.addClass("highlighted");
            }
        }
        //Highlight all contributing captures.
        var contributingCaptures = $(rank)[0].rankData.contributing_captures;
        if (contributingCaptures != null) {
            for (var i = 0; i < contributingCaptures.length; i++) {
                var capture = $(leaderboard.options.capturesList).find(".capture-" + contributingCaptures[i]);
                capture.addClass("highlighted");
            }
            $('html,body').animate({
                scrollTop: (parseInt($(leaderboard.options.capturesList).add(leaderboard.scope).find(".highlighted:first").offset().top,10)) - 10 + "px"
            },'slow');
        }
    }
};

Leaderboard.prototype.setupReverseCaptureLookup = function() {
    var leaderboard = this;
    $(this.options.capturesList).find('.capture').each(function(){
        $(this).addClass("selectable");
        $(this).on('click',function() {
            leaderboard.selectCapture(this);
        });
    });
};

Leaderboard.prototype.selectCapture = function(/*Element*/ capture){
    var leaderboard = this;
    if ($(capture).hasClass("selected")){
        this.clearSelection();
    }
    else {
        //Highlight all ranks with this capture included as a corresponding capture.
        this.clearSelection();
        $(capture).addClass("selected");
        var id = parseInt($(capture).attr('capture-id'));
        $(this.scope).find('.rank').each(function(){
            var contributingCaptures = $(this)[0].rankData.contributing_captures;
            if (contributingCaptures != null && $.inArray(id,contributingCaptures) > -1){
                $(this).addClass("highlighted");
            }
        });
        $('html,body').animate({
            scrollTop: (parseInt($(leaderboard.scope).find(".highlighted:first").offset().top,10)) - 10 + "px"
        },'slow');
    }
};

//OTHER UI HELPERS

Leaderboard.prototype.initMarquee = function(table) {
    $(table).find('.ranking-label .text').each(function() {
        var element = $(this)[0];
        if($(element).width() < element.scrollWidth){
            $(this).addClass('lb-marquee');
            $(this).marquee({
                delayBeforeStart: 1000,
                duration: 5000,
                pauseOnCycle: true,
                duplicated: true
            });
        }
    });
};

Leaderboard.prototype.removeMarquee = function(table) {
    $(table).find('.ranking-label div').each(function() {
        $(this).marquee('destroy');
        $(this).removeClass('lb-marquee');
    });
};

Leaderboard.prototype.initMason = function(){
    if ($(this.scope).find('.super-category ' + this.options.masonryTarget).length > 0) {
        $(this.scope).find('.super-category ' + this.options.masonryTarget).masonry({
            itemSelector: '.category',
            isFitWidth: true,
            gutterWidth: 5,
            containerStyle: {margin: "0 auto"}
        });
    }
};

Leaderboard.prototype.removeMason = function() {
    $(this.scope).find('.super-category ' + this.options.masonryTarget).masonry('destroy');
};