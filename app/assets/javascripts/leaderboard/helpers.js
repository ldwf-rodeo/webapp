//UI HELPERS

Leaderboard.updateIndicatorsForTable = function(table){
    table = $(table);
    table.find('.ranking-changed-score-updated .ranking-indicator .text').html('<span class="arrow-updated">↻</span>');
    table.find('.ranking-changed-fresh .ranking-indicator .text').html('<span class="arrow-new">+</span>');
    table.find('.ranking-changed-up .ranking-indicator .text').html('<span class="arrow-up">▲</span>');
    table.find('.ranking-changed-virtual-up .ranking-indicator .text').html('<span class="arrow-up">▲</span>');
    table.find('.ranking-changed-down .ranking-indicator .text').html('<span class="arrow-down">▼</span>');
    table.find('.ranking-changed-virtual-down .ranking-indicator .text').html('<span class="arrow-down">▼</span>');
    table.find('.ranking-indicator.empty .text').html('');
};

Leaderboard.removeIndicatorClassesForTable = function(table){
    table = $(table);
    table.find('.ranking-unchanged').removeClass('ranking-unchanged');
    table.find('.ranking-changed-down').removeClass('ranking-changed-down');
    table.find('.ranking-changed-up').removeClass('ranking-changed-up');
    table.find('.ranking-changed-fresh').removeClass('ranking-changed-fresh');
    table.find('.ranking-changed-drop').removeClass('ranking-changed-drop');
    table.find('.ranking-changed-score-updated').removeClass('ranking-changed-score-updated');
    table.find('.ranking-changed-virtual-up').removeClass('ranking-changed-virtual-up');
    table.find('.ranking-changed-virtual-down').removeClass('ranking-changed-virtual-down');
};

Leaderboard.flashElement = function(element, attribute, color1, color2, duration, times){
    var original = $(element).css(attribute);
    if (color1 == null) color1 = original;
    if (color2 == null) color2 = original;
    var state = 0;
    var interval = window.setInterval(function(){
        $(element).css(attribute, state == 0 ? color1 : color2);
        state = (state+1) % 2;
    },duration/times);
    window.setTimeout(function(){
        clearInterval(interval);
        $(element).css(attribute,original);
    },duration+1);
};

Leaderboard.makeOrdinalLabel = function(/*String*/ positionLabel){
    return positionLabel.substring(0,positionLabel.length-2) + '<div class="letters">' + positionLabel.substring(positionLabel.length-2,positionLabel.length) + '</div>';
};

Leaderboard.formatTimestamp = function(text){
    return Date.create(text).format('{Weekday} {Mon} {dd}, {yyyy} @ {HH}:{mm}:{ss} {tz}');
};

Leaderboard.makeScore = function(/*Object*/ rank, /*String*/ displayMode){
    switch(displayMode){
        case 'score_and_unit':
            return Leaderboard.makeUnitSpan(rank.score_label,rank.unit_label);
            break;
        case 'smallest_and_largest':
            return Leaderboard.makeUnitSpan(rank.largest_subscore_label,rank.unit_label) + ',' + Leaderboard.makeUnitSpan(rank.smallest_subscore_label,rank.unit_label);
            break;
        case 'score_and_unit_and_count':
            return Leaderboard.makeUnitSpan(rank.score_label,rank.unit_label) + '<span class="capture-count"> (' + rank.capture_count + ')</span>';
            break;
        case 'count':
            return Leaderboard.makeUnitSpan(rank.capture_count,'ct.');
            break;
        case 'none':
            return '';
            break;
        default:
            return 'n/a';
            break;
    }
};

Leaderboard.makeUnitSpan = function(value,unit){
    return (value + ' ' + '<span class="unit">' + unit + '</span>');
};

Leaderboard.padNumber = function(n, width){
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
};

Leaderboard.formatTime = function(date){
    return date.getHours() + ":" + Leaderboard.padNumber(date.getMinutes(),2) + ":" + Leaderboard.padNumber(date.getSeconds(),2);
};

Leaderboard.fromNow = function(diff){
    var MEASURES = [['second',1,'s'],['minute',60,'m'],['hour',60*60,'h'],['day',60*60*24,'d']];
    if (diff == 0){
        return "just now";
    }
    else {
        var remainder = diff;
        var s = "";
        for (var i = MEASURES.length - 1; i >= 0; i--) {
            measure = MEASURES[i];
            number = Math.floor(remainder / measure[1]);
            if (number > 0) {
                s += " " + number + " " + measure[0] + (number == 1 ? "" : "s");
                remainder -= number * measure[1];
            }
        }
        return s.trim() + " ago";
    }
};