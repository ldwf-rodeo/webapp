/*
 * leaderboard_playback.js
 *
 * Nathan Cooper, Daniel Ward
 *
 * This file contains additional prototype definitions for the leaderboard class
 * relating to playback.
 */

//Playback

Leaderboard.prototype.getNextState = function(){
    if(this.playbackState.captureIndex < this.options.playbackCaptures.length){
        $.getJSON(this.options.playbackURL, { capture_id : this.options.playbackCaptures[this.playbackState.captureIndex] }, function(json){
            $('.current-capture-time').text(Date.create(json['capture_date']).format('{Mon} {d}, {yyyy} {h}:{mm} {TT}'));
            this.update(JSON.stringify(json['data']));
            this.playbackState.captureIndex++;
        });
    }
};

Leaderboard.prototype.startPlayback = function() {
    var leaderboard = this;
    this.playbackState.playWasPresed = true;
    $('.fa-play').hide('fade', 'slow', function() {
        $('.fa-circle-o-notch').show('fade', 'slow');
    });
    $(document).on(LEADERBOARD_ANIMATION_FINISHED_EVENT, function(){
        leaderboard.getNextState();
    });
    this.getNextState();
};

Leaderboard.prototype.resumePlayback = function() {
    $('.fa-play').hide('fade', 'slow', function() {
        $('.fa-circle-o-notch').show('fade', 'slow');
    });
    $(document).on(LEADERBOARD_ANIMATION_FINISHED_EVENT, function(){
        this.getNextState();
    });
    this.getNextState();
};

Leaderboard.prototype.pausePlayback = function() {
    $('.fa-circle-o-notch').hide('fade', 'slow', function() {
        $('.fa-play').show('fade', 'slow');
    });
    $(document).unbind(LEADERBOARD_ANIMATION_FINISHED_EVENT);
};

Leaderboard.prototype.stopPlayback = function(){
    this.playbackState.playWasPressed = false;
    this.hidePlaybackControls();
    this.bindSocket();
    this.bindMarqueeOnAnimationFinish();
    this.reinitialize();
};

Leaderboard.prototype.resetBoardToEmpty = function(callback){
    var leaderboard = this;
    $.get(leaderboard_html_url, function(){
        leaderboard.initMason();
        leaderboard.reveal();
        callback();
    }, 'script');
};

Leaderboard.prototype.reinitialize = function(){
    var leaderboard = this;
    $.get(leaderboard_html_url, function(){
        // ask for the current state
        leaderboard.isFirstUpdate = true;
        leaderboard.refresh();
    },'script');
};

Leaderboard.prototype.showPlaybackControls = function(){

    this.isFirstUpdate = true;
    this.unbindSocket();
    $('.current-capture-time').text('');

    // unbind the old animation listener
    $(document).unbind(LEADERBOARD_ANIMATION_FINISHED_EVENT);

    $('.timestamps-container').hide('fade', 'slow', function() {
        $('.playback-controls').show('fade', 'slow');
        $('.current-capture-time').show('fade', 'slow');
    });

    $('.options').hide('fade', 'slow');

    hidePopovers();

    this.resetBoardToEmpty(function(){
        this.getNextState();
    });
};

Leaderboard.prototype.hidePlaybackControls = function(){
    $('.playback-controls').hide('fade', 'slow', function() {
        $('.timestamps-container').show('fade', 'slow');
        $('.options').show('fade', 'slow');
    });
    $('.current-capture-time').hide('fade', 'slow');
};

//POPOVERS

Leaderboard.prototype.initPopover = function() {
    var content = $('#popover_template').find('.popover-stuff').clone(true, true);

    // create the popover for the gear item
    $('.popover-dismiss').popover({
        html: true,
        placement: 'left',
        content: content. html()
    });

    // remove the popover when the body is clicked
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
};

Leaderboard.prototype.hidePopovers = function() {
    $('[data-toggle="popover"]').each(function () {
        $(this).popover('hide');
    });
}