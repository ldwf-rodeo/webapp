$(document).on('ready page:load', function(){
    var containerSelector = $('.events-sms-index');

    var completeSMSSend = function(n){
        //We will disable all SMS forms while the submission is happening, then re-enable them whenever it finishes.
        alert("Successfully sent " + n + " SMS messages!");
        setTimeout(function() {
            containerSelector.find('form').find('input:submit').removeAttr('disabled');
        },1000);
    };

    var errorSMSSend = function() {
        alert("ERROR: Some messages may not have sent.");
        setTimeout(function () {
            containerSelector.find('form').find('input:submit').removeAttr('disabled');
        }, 1000);
    };

    containerSelector.find('form').submit(
        function(){
            containerSelector.find('form').find('input:submit').attr('disabled','disabled');
            alert("Your SMS request is in. Messages will be sent ASAP.");

            return true;
        }
    );

    containerSelector.find('.substitution-button').click(function(){
        var textArea = $(this).closest('form').find('#custom_message_text_area');
        var cursorPos = textArea.prop('selectionStart');
        var v = textArea.val();
        var textBefore = v.substring(0,  cursorPos);
        var textAfter  = v.substring(cursorPos, v.length);
        textArea.val(textBefore + $(this).data('value') + textAfter);
    });
});