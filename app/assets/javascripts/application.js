// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.cookie
//= require jstz
//= require browser_timezone_rails/set_time_zone
//= require websocket_rails/main
//= require autocomplete-rails
//= require_tree ./premium_category_grouping

// used to refresh a jquery cached selector
$.fn.refresh = function() {
    var elems = $(this.selector);
    this.splice(0, this.length);
    this.push.apply( this, elems );
    return this;
};

$(document).ready(function(){

    // Prepare
    var History = window.History;
    if ( !History.enabled ) {
        return false;
    }

    // Bind to StateChange Event
    History.Adapter.bind(window,'statechange',function(){
        var State = History.getState();
        $.ajax({
            url: State.url,
            headers: { 'ajax-load': 'true' },
            cache: false,
            success: function(html){
                $(".inner-content").empty();
                $(".inner-content").append(html);
                commonInit();
            }
        });
    });


    $("a[data-load]").click(function(){

        var path = $(this).attr('data-load');

        $.ajax({
            url: path,
            headers: { 'ajax-load': 'true' },
            cache: false,
            success: function(html){
                if ( History.enabled ) {
                    History.pushState({state:1},'',path);
                }

                $(".inner-content").empty();
                $(".inner-content").append(html);
                commonInit();
            }
        });
    });

    initHeader();

//    initMason();

    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

    var MONTH_NAMES = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    $('.time-show').each(function () {
        var date = $(this).text().trim();
        date = date.split(/[\s]+/);
        date.remove("UTC");
        date  = date[0] + "T" + date[1];
        var formattedDate = new Date(date);
        var minutes = formattedDate.getMinutes();
        if(minutes < 10){
            minutes = '0'.concat(minutes);
        }
        var time = (formattedDate.getHours()%12) + ':' + minutes;
        time.concat(formattedDate.getHours() > 12 ? ' PM' : ' AM');
        formattedDate = MONTH_NAMES[formattedDate.getMonth()] + ' ' + formattedDate.getDate() + ', ' + formattedDate.getFullYear() + ' ' + time;
        $(this).text(formattedDate);
    });

    $(".color-picker").change(function () {
        console.log($(this).val());
        $(this).closest('div.color-fields').find('.color-field').val($(this).val().toUpperCase());
    });

    $(".color-field").change(function () {
        $(this).closest('div.color-fields').find('.color-picker').val($(this).val().toUpperCase());
    });

    $('[data-url]').click(function() {
        window.location = $(this).data('url');
    });

    initDatePickers();
});


function initDatePickers() {

    if ($('.date-wrapper').length == 0){
        //Don't do anything if there are no dates.
        return;
    }

    $('.date-input').datetimepicker('destroy');
    $('.datetime-input').datetimepicker('destroy');

    $('.date-input').each(function() {


        var timeValue = $(this).closest('.date-wrapper').find('.date-value').val();
        var timeString = null;

        if (timeValue) {
            timeString  = Date.create($(this).closest('.date-wrapper').find('.date-value').val() || null).format('{Mon} {d}, {yyyy}');
        }

        $(this).datetimepicker({
            value: timeString,
            timepicker: false,
            format: 'M j, Y',
            formatDate: 'M j, Y',
            scrollInput: false,
            onChangeDateTime:function(dp, $input) {
                var val = $input.val();

                if (val.length > 0) {
                    timeString = Date.create(val).toISOString();
                    $input.closest('.date-wrapper').find('.date-value').val(timeString);
                }
            }
        });

        $(this).focusout(function() {
            if ($(this).val() == '') {
                $(this).closest('.date-wrapper').find('.date-value').val('');
            } else {
                timeString = Date.create($(this).val()).toISOString();
                $(this).closest('.date-wrapper').find('.date-value').val(timeString);
            }

        });
    });

    $('.datetime-input').each(function() {

        var timeValue = $(this).closest('.date-wrapper').find('.date-value').val();

        var timeString = '';

        if (timeValue) {
            timeString  = Date.create(timeValue).format('{Mon} {d}, {yyyy} {h}:{mm} {TT}');
        }

        $(this).datetimepicker({
            value: timeString,
            timepicker: true,
            format: 'M j, Y h:i A',
            formatDate: 'M j, Y',
            formatTime: 'h:i A',
            scrollInput: false,
            onChangeDateTime:function(dp, $input){
                var val = $input.val();

                if (val.length > 0) {
                    timeString = Date.create(val).toISOString();
                    $input.closest('.date-wrapper').find('.date-value').val(timeString);
                }
            }
        });

        $(this).focusout(function() {
            if ($(this).val() == '') {
                $(this).closest('.date-wrapper').find('.date-value').val('');
            } else {
                timeString = Date.create($(this).val()).toISOString();
                $(this).closest('.date-wrapper').find('.date-value').val(timeString);
            }
        });
    });
}

function commonInit(){
    $('.phone').inputmask('999-999-9999');
    $('.zip').inputmask('99999');
}

var headerHeight;
var oldScrollPosition = 0;

function initHeader() {

    $(window).scroll(function(){
        oldScrollPosition = $(document).scrollTop()
        $('#big-header').data('size','big');

        if($(document).scrollTop() > 101)
        {
            $('.nav-event-title').show('fade');
        }
        else
        {
            $('.nav-event-title').hide('fade');
        }
    });
}
