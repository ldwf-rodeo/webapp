var initRan = false;
var species = null;
var participants = null;

$(document).ready(function(){
    if(!initRan){
        init();
    }
});

var d;

function init(){
    initRan = true;

    $('#participant_name').bind('railsAutocomplete.select', function(event, data){
        updateParticipantDisplay();
    });

    $('form').validationEngine('detach');
    $('form').validationEngine('attach',{prettySelect: true});


    $("input[data-autocomplete]").railsAutocomplete();
    $("#dialog").dialog({autoOpen : false, modal : true});
    $("#dialog2").dialog({ buttons: [ { text: "Ok", click: function() { $( this ).dialog( "close" ); } } ], autoOpen : false, modal : true});


    $('form').submit(function () {
        $("#submit-captures-button").attr("disabled","disabled");
        $('#dialog').dialog('open');
    });

    var capture_count = $('.capture').length;
    $('.capture').each(function(){

    });

    $('.add-captures-button').click(function() {
        var orig = $('.capture').last();
        var cap =  orig.clone(true);
        cap.find('.exterminate-button').html("Remove");
        cap.find('.exterminate-button').removeClass("inactive");
        cap.find('.exterminate-button').addClass('active');

        cap.find('.measurement').find('select, input').val('');

        cap.find('input, select').each(function(){
             if (($(this).hasClass('preserved-attribute'))) {
                 $(this).val(orig.find('#' + $(this).attr('id')).val());
             }

             var oldId = $(this).attr('id');
             if (typeof oldId != 'undefined') {
                 var newId = oldId.replace(new RegExp(/_[0-9]+_/), "_" + capture_count + "_");
                 $(this).attr('id', newId);
             }
             var oldName = $(this).attr('name');
             if (typeof oldName != 'undefined'){
                 var newName = oldName.replace(new RegExp(/\[[0-9]+\]/), "[" + capture_count + "]");
                 $(this).attr('name', newName);
             }

             if ($(this).attr('type') == 'checkbox'){
                 $(this).attr('checked',null);
             }

         });

        // Remove the datetimepickers so initDatePickers can give both the original
        // and the clone their own datetimepickers
        cap.find('.date-value').val('');
        cap.find('.datetime-input').val('');
        capture_count++;

        cap.hide().insertAfter($('.capture').last()).slideDown('fast');
        refreshCaptureElements();

        initDatePickers();
    });

//    $('form').bind('ajax:success',function(){
//        resetCapturesForm();
//    });

    $('form').bind('ajax:failure',function(){
        $('#dialog').dialog('close');
        alert('There was a connection error. Please check your connection and try again.');
        $("#submit-captures-button").removeAttr("disabled");
    });
//    $('form').bind('ajax:error',function(){
//        $('#dialog').dialog('close');
//        alert('There was a connection error. Please check your connection and try again.');
//    });

//    initDatePickers();

}

function resetCapturesForm(){
    $("#enter-to-enable").show();
    $("#submit-captures-button").attr("disabled","disabled");
    $("#participant_id").val(null);
    $("#participant_name").val(null);
    $("#suppress_sms_value").attr("checked",false);
    $(".participant-capture-info li span").html("n/a");
    $(".capture").each(function(index,element){
        if (index > 0){
            $(this).remove();
        }
    });
    $(".capture").find('input').each(function(){
        if (!$(this).hasClass('preserved-attribute')) {
            $(this).val("");
        }
        if ($(this).attr('type') =='checkbox'){
            $(this).attr('checked',null);
        }
    });
    $(".capture").find('select').each(function(){
        $(this).val($(this).find('option:first').val())
    });
}

function blockWhileAnimating(){
   $(".capture").each(function(index,element){
       var cap = $(element);
       $(element).find('.exterminate-button').unbind('click');
       cap.find('.exterminate-button').addClass("inactive");
       cap.find('.exterminate-button').removeClass("active");
   });
}

function refreshCaptureElements(){

    //Allow the form to be submitted if an angler is set.
    var angler_id =  $("#participant_id").val();
    if (angler_id){
        $("#enter-to-enable").hide();
        $("#submit-captures-button").removeAttr("disabled");
    }
    else{
        $("#enter-to-enable").show();
        $("#submit-captures-button").attr("disabled","disabled");
    }

    //For each capture, check if we can disable it and update its title.
    var limited = $('.capture').length <= 1 ? true : false;
    $(".capture").each(function(index,element){
        $(element).find(".display-name").html("Capture #" + (index+1));
        var cap = $(element);
        cap.find('.exterminate-button').unbind('click');
        if (limited){
            cap.find('.exterminate-button').addClass("inactive");
            cap.find('.exterminate-button').removeClass("active");
        }
        else{
            cap.find('.exterminate-button').click(function(){
                blockWhileAnimating();
                cap.hide("fade","slow",function(){cap.remove(); refreshCaptureElements(); });
            });
            cap.find('.exterminate-button').removeClass("inactive");
            cap.find('.exterminate-button').addClass("active");
        }
    });
}

function autoFisher9000(){
    $.getJSON('/test_new?event_id=' + $('.event-id').val(), function(data){
        species = data.species;
        participants = data.participants;
        testStuff();
    });
}

function updateParticipantDisplay() {
    if ($("#participant_id").val()) {
        $.getJSON(INFO_URL, { id: $('#participant_id').val() }, function (data) {
            $('#participant_team').html(data.team.name);

            //Format the values accordingly.
            var name = data.participant.first_name.trim() + " " + data.participant.last_name.trim();
            var gender = data.participant.gender == null ? "n/a" : data.participant.gender.charAt(0).toUpperCase() + data.participant.gender.slice(1);
            var number = data.number == null ? "n/a" : data.number;
            var email = data.participant.email == null ? "n/a" : data.participant.email;
            var address = data.participant.city == null ? "n/a" : data.participant.city;

            //Put them into the appropriate elements.
            $('#participant_name').val(name);
            $("#label_participant_name").html(name);
            $("#label_participant_number").html(number);
            $("#label_team_name").html(data.team.name);
            $("#label_team_number").html(data.team.number);
            $("#label_participant_gender").html(gender);
            $("#label-participant_email").html(email);
            $("#label_participant_address").html(address);

            refreshCaptureElements();

        });
    }
}

function testStuff(){
    $('#capture_participant_id').val(participants[Math.floor(Math.random() * participants.length )].id);
    $('#capture_species').val(species[Math.floor(Math.random() * species.length )].id);
    $('#capture_weight').val(Math.floor((Math.random() * 100) + 1));
    $('form').submit();
    setTimeout("testStuff()", 3000);
}

commonInit();
updateParticipantDisplay();
refreshCaptureElements();

if(!initRan){
    init();
}
//uncomment for awesome stress testing action!!
//setTimeout("autoFisher9000()", 7000);
