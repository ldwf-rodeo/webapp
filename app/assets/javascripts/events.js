var initRan = false;

$(document).ready(function(){
   if (!initRan) {
       init();
   }
});

function init() {
    initRan = true;

    $('form').validationEngine('attach',{promptPosition : "centerRight",  prettySelect: true});

    initDatePickers();
}

commonInit();

if(!initRan){
    init();
}
