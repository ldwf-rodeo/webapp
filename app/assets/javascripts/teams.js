var lastParticipant = null;

$(function () {
    $('.date-picker').datepicker();
    $('.super-slider-div').hide();

    $('.slide').unbind('click');
    $('.add-participant-button').unbind('click');
    $('add-vessel-button').unbind('click');

    $('.slide').click(function(){
        $('.super-un-slider').fadeOut(1000,function(){
            $('.super-slider-div').fadeIn(1000);
        });
    });

    lastParticipant = $('.dropzone').last();

    $('#add-participant').click(function(){
        $('.phone').inputmask('remove');
        $('.zip').inputmask('remove');

        var clone = lastParticipant.clone();
        $(clone).removeClass('captain');
        $(clone).addClass('participant-dropzone');
        $(clone).find('input[data-autocomplete]').railsAutocomplete();

        var participant = clone.find('.participant');
        participant.find(".formError").remove();

        participant.attr('id', currentParticipantPrefix + currentParticipantNumber);
        currentParticipantNumber++;

        var currIndex = $('.participant').length;

        participant.find('input, select').each(function(){

            // these are the participant number model fields
            if ($(this).closest('.participant-numbers').length > 0) {
                // skip some stuff
                if (!$(this).hasClass('event-id')){
                    $(this).val("");
                    $(this).attr('value', '');
                }
            }
            else if ( $(this).closest('.notification-fields').length > 0){
                //Don't clear out the notification checkbox fields.
            }
            else {
               // we are looking at the participant model fields
               $(this).val("");
            }
            //Clear all fields except for the


            var oldId = $(this).attr('id');

            /*
                Can ignore adjusting the specific participant numbers since a participant
                will only have one number, which is always index at 0
             */

            // if id is present, update. Else ignore
            if (typeof oldId != 'undefined' ) {
                var newId = oldId.replace(
                    new RegExp(/team_participations_attributes_[0-9]+/),
                        "team_participations_attributes_" + currIndex);
                $(this).attr('id', newId);
            }

             var oldName = $(this).attr('name');
             var newName = oldName.replace(
                 new RegExp(/\[participations_attributes\]\[[0-9]+\]/),
                     "[participations_attributes][" + currIndex + "]");
             $(this).attr('name', newName);

            $(this).find('.header .display-name').text('');

         });

        participant.find('.header .display-name').text('');
        participant.removeClass('new').removeClass('known').addClass('new');

        clone.appendTo($('#participants'));
        clone.show();

        lastParticipant = clone;

        $('.phone').inputmask('999-999-9999');
        $('.zip').inputmask('99999');

        $('.exterminate-button:not(:first)').show();
        rebindRemoveButton();
        rebindClearButton();
        rebindAutoCompleteJsonLookup();
    });


    $('form').validationEngine('attach',{ prettySelect: true });

    commonInit();

    initCaptain();

    $('.phone').inputmask('999-999-9999');
    $('.zip').inputmask('99999');

    $('.participant .header .chevron').click(participantHeaderClicked);

    initDragAndDrop();
    check_on_waiting_badges();

    rebindAutoCompleteJsonLookup();
    rebindRemoveButton();
    rebindClearButton();

});


function participantHeaderClicked() {
    var participantDiv = $(this).closest('.participant');

    participantDiv.find('.display-name').hide('fade');

    if (!participantDiv.find('.body').is(':visible')) {
        participantDiv.find('.header').removeClass('rounded-border');
    }


    participantDiv.find('.body').toggle('blind', function () {

        // if the body is no longer visible, then show the display name
        if (!participantDiv.find('.body').is(':visible')) {
            participantDiv.find('.header').addClass('rounded-border');

            firstName = participantDiv.find('.first-name-field').val();
            lastName  = participantDiv.find('.last-name-field').val();

            participantDiv.find('.display-name').text(firstName + " " + lastName);

            participantDiv.find('.display-name').show('fade');
        } else {
//            participantDiv.find('.header').removeClass('rounded-border');
        }
    });

    participantDiv.find('.chevron').toggleClass('rotate-up');
}

var m;

function initCaptain () {
    var count = 0;
    $('.participant').each(function(){
        var part = $(this);

        if (count == 0 && parseInt(part.find('.is-captain-field').val()) ==  1) {
            var dropzone = part.closest('.dropzone');
            part.detach().appendTo($('.captain-dropzone'));
            dropzone.remove();
            count++;
        } else {
            part.find('.is-captain-field').val(0);
        }
    });

    $('.participant:not(:first)').find('.is-captain-field').val(0);
}

///==================================================================================================================

function initDragAndDrop() {
    /* The dragging code for '.draggable' from the demo above
     * applies to this demo as well so it doesn't have to be repeated. */

// enable draggables to be dropped into this
    interact('.dropzone').dropzone({
        // only accept elements matching this CSS selector
        accept: '.draggable',
        // Require a 75% element overlap for a drop to be possible
        overlap: 0.50,

        // listen for drop related events:

        ondropactivate: function (event) {
            // add active dropzone feedback
            event.target.classList.add('drop-active');
        },

        ondragenter: function (event) {
            var draggableElement = event.relatedTarget,
                dropzoneElement = event.target;

            // feedback the possibility of a drop
            dropzoneElement.classList.add('drop-target');
            draggableElement.classList.add('can-drop');
        },

        ondragleave: function (event) {
            // remove the drop feedback style
            event.target.classList.remove('drop-target');
            event.relatedTarget.classList.remove('can-drop');
        },

        ondrop: function (event) {
            setTimeout(function(){
                var source = $(event.relatedTarget);
                var target = $(event.target).find('.participant');

                var sourceDropZone = source.closest('.dropzone');
                var targetDropZone = target.closest('.dropzone');

                source.appendTo(targetDropZone);
                target.appendTo(sourceDropZone);

                source.removeAttr('data-x').removeAttr('data-y').removeAttr('style');

                // update the is_captain_field
                $('.captain-dropzone').find('.is-captain-field').val(1);
                $('#participants').find('.is-captain-field').val(0);

            }, 100);

        },

        ondropdeactivate: function (event) {
            // remove active dropzone feedback
            event.target.classList.remove('drop-active');
            event.target.classList.remove('drop-target');
        }
    });

    // target elements with the "draggable" class
    interact('.draggable').draggable({
            // enable inertial throwing
            inertia: true,
            allowFrom: '.header',
            // call this function on every dragmove event
            onmove: dragMoveListener,
            // call this function on every dragend event
            onend: function (event) {
                // left empty
            }
        });

    function dragMoveListener (event) {
        var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        // translate the element
        target.style.webkitTransform =
            target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';

        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
    }

    // this is used later in the resizing demo
    window.dragMoveListener = dragMoveListener;
}

//--------------------------------------------
function check_on_waiting_badges() {

    var selectors = $('[data-id].print-buttons');

    if (selectors.length == 0) {
        return;
    }

    var ids  = selectors.map(function(){ return $(this).data('id') }).get();
    var params = { ids: ids };

    $.getJSON(TEAM_BADGE_CHECK_STATUS_PATH, params, function (data) {console.log(data);

        togglePrintButtons(selectors, data.ready, true);

        //--------------------------------------------------------------

        togglePrintButtons(selectors, data.not_ready, false);

        setTimeout(check_on_waiting_badges, 3000);
    });
}

function togglePrintButtons(selectors, ids, shouldBeReady) {
    selectors.filter(function(){ return $.inArray($(this).data('id'),ids) == 0 }).each(function() {
        var printButton = $(this).find('a.print-button');
        var waitingButton = $(this).find('a.waiting-button');

        if (shouldBeReady) {

            if (!waitingButton.hasClass('hidden')) {
                waitingButton.addClass('hidden');
            }

            printButton.removeClass('hidden');

        } else {

            if (!printButton.hasClass('hidden')) {
                printButton.addClass('hidden');
            }

            waitingButton.removeClass('hidden');

        }
    });
}

function rebindRemoveButton() {
    $('.exterminate-button').unbind('click').click(function(){
        if (parseInt($(this).closest('.dropzone').find('.is-captain-field').val()) == 1){
            //Don't allow us to delete the captain.
            return;
        }
        var participant = $(this).closest('.participant-dropzone');
        var id = participant.find('.participant-id-field').val();
        if (id) {
            // get the persis field for the rails server to use
            var persistField = participant.find('.participant-persist-field');
            // flip the value of the field
            persistField.val(1 ^ parseInt(persistField.val()));

            // hide this participant, but leave them in the DOM
            participant.hide("fade", "slow");
        }

        else{
            var e = participant.closest('.participant-dropzone');
            e.hide('fade','slow', function(){e.remove();});
        }
    });
}

function rebindClearButton() {
    $('.clear-button').unbind('click').click(function(){
        $(this)
            .closest('.participant')
            .removeClass('known')
            .removeClass('new')
            .addClass('new')
            .find(':input')
            .not(':button')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });
}

function rebindAutoCompleteJsonLookup() {
    $('.first-name-field').unbind('railsAutocomplete.select').bind('railsAutocomplete.select', function(event, data){
        var self = $(this);
        $.getJSON(INFO_URL, {id: data.item.id}, function(data) {
            var participant = data.participant;

            var participantContainer = self.closest('.participant');
            participantContainer.find('.participant-id-field').val(participant.id);
            participantContainer.find('.first-name-field').val(participant.first_name);
            participantContainer.find('.last-name-field').val(participant.last_name);
            participantContainer.find('.email-input').val(participant.email);
            participantContainer.find('.mobile-phone-input').val(participant.mobile_phone);
            participantContainer.find('.shirt_size-input').val(participant.shirt_size_id);
            participantContainer.find('.dob-input').val(participant.date_of_birth);
            participantContainer.find('.gender-input').val(participant.gender_id);

            participantContainer.removeClass('new').removeClass('known').addClass('known');
        });
    });
}