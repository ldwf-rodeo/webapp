/*
 * Automatically include all the JS Leaderboard scripts by including this file.
 *
 * Nathan Cooper
 */

//= require jquery.masonry.min
//= require leaderboard/jquery.marquee
//= require leaderboard/animator
//= require leaderboard/rankingTableUpdate
//= require leaderboard/leaderboard
//= require leaderboard/helpers
//= require leaderboard/playback