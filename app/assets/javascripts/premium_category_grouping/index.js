/**
 * Created by danielward on 7/29/15.
 */

$(document).on('ready page:load', function(){
    var p = $('div.premium-category-groupings-index');

    p.find('table').DataTable({
        "columnDefs": [{ orderable: false, targets: [2] }]
    });
});