class PartitionTwilioNumbersJob < ActiveJob::Base

  queue_as :default

  def perform
    client = TwilioClient.build_client

    # release a twilio number 48 hours after an event has ended.
    Event.where("(end_date + '48 hours'::interval) > ? AND twilio_expired IS false AND use_sms is true AND twilio_number IS NOT NULL ", Time.now).each do |event|
      # get the sid of the number
      incoming_numbers = client.account.incoming_phone_numbers
      sid =  incoming_numbers.list({:phone_number => event.twilio_phone}).first.sid

      # set the urls to point to the server
      twilio_number = incoming_numbers.get(sid)
      twilio_number.delete

      event.twilio_expired = true
      event.save!
    end

    # buy a twilio number 48 hours before the event starts
    Event.where("(start_date - '2 weeks'::interval) > ? AND twilio_expired IS false AND use_sms IS true AND twilio_number IS NULL", Time.now).each do |event|
      number = TwilioClient.buy_new_number

      # save the number to the database
      event.twilio_phone = number
      event.twilio_expired = false
      event.save!

      # get the sid of the number
      incoming_numbers = client.account.incoming_phone_numbers
      sid =  incoming_numbers.list({:phone_number => number}).first.sid

      # set the urls to point to the server
      twilio_number = incoming_numbers.get(sid)
      twilio_number.update(
          :voice_url  => 'https://geauxfishlarodeo.com/twilio/voice',
          :sms_url    => 'https://geauxfishlarodeo.com/twilio/sms'
      )

    end
  end
end
