include SMSModule

class SMSJob < ActiveJob::Base

  queue_as :sms

  def perform(job_params)

    event = job_params[:event]
    type = job_params[:type].to_sym
    who = job_params[:who].to_sym
    message = job_params[:message].gsub(/\r\n/, "\n")

    if event.nil?
      raise SMSModule::SMSError.new 'CANNOT SEND MESSAGES WITHOUT EVENT GIVEN.'
    end

    # SEND THE MESSAGES.

    if who == :all_enrolled_participants
      event.sms_participants.each do |participant|
        send_broadcast(participant, type, event, message)
      end

    elsif who == :enrolled_team_captains_only
      event.teams.each do |team|
        send_broadcast(team, type, event, message)
      end

    else
      id = who.to_s.to_i

      event.sms_participants.joins(:participations).where('participations.event_id = ? and participations.event_participation_type_id = ?', event.id, id).each do |participant|
        send_broadcast(participant, type, event, message)
      end

    end

  end

  private

  def send_broadcast(model, type, event, msg)

    if model.is_a? Team
      item = model
    else
      item = model.participation(event)
    end

    if type == :standings

      msgs = [SMSSender.get_standings_messages(item.is_a? Team ? model : model.team)]

    elsif type == :custom
      msgs = SMSSender.split_string_to_sms_chunks(msg)

    else
      return

    end

    msgs.each do |message|
      SMSBroadcastJob.perform_later(item, message)
    end
  end
end
