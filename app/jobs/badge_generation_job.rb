class BadgeGenerationJob < ActiveJob::Base
  queue_as :badges

  after_enqueue do |job|
    participation = job.arguments.first
    PendingJob.create!(target: participation.team, source: participation)
  end

  def perform(participation)
    participation.generate_badge
  end

  after_perform do |job|
    participation = job.arguments.first

    job = participation.pending_jobs.first
    job.destroy unless job.nil?
  end
end