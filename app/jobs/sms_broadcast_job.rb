include SMSModule

class SMSBroadcastJob < ActiveJob::Base
  queue_as :sms

  def perform(model, msg)
    if model.is_a? Team
      SMSSender.send_generic_message_to_team_captain(model,msg)
    elsif model.is_a? Participation
      SMSSender.send_generic_message_to_participation(model,msg)
    end

  end
end
