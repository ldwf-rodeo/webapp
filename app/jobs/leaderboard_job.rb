include LeaderboardUpdateModule
include SMSModule

class LeaderboardJob  < ActiveJob::Base

  queue_as :leaderboard

  def perform(job_params = {})
    event = job_params[:event]
    capture_id = job_params[:capture_id]
    send_sms = job_params[:send_sms]
    Leaderboard.initiate_leaderboard_update(event, capture_id, send_sms)
  end

end