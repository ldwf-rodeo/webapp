json.team         Team.joins(:participations).where(participations: {participant_id: @participant.id, event_id: @event.id}).first
json.participant  @participant
json.number       Participation.where(participant: @participant, event: @event).first.display_number