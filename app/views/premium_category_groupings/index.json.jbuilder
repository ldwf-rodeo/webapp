json.array!(@premium_category_groupings) do |premium_category_grouping|
  json.extract! premium_category_grouping, 
  json.url premium_category_grouping_url(premium_category_grouping, format: :json)
end