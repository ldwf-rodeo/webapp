class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_filter :set_browser_time_zone
  before_filter :authenticate_user!

  def set_browser_time_zone
    @browser_timezone = browser_timezone.nil? ? default_timezone : browser_timezone
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = 'You are not authorized to perform this action.'
    redirect_to root_path
  end

  def flash_object_errors(object,default_message = 'There was an error processing your request. Please try again later.', action = 'processed', now=false)
    f = now ? flash.now : flash
    if object.errors.any?
      f[:header] = "#{object.errors.count.to_s} error prevented this #{object.class.model_name.human.downcase} from being #{action.downcase}:"
      f[:error] = object.errors.full_messages
    else
      f[:error] = default_message
    end
  end

  def flash_success(message = 'Success.',now = false)
    f = now ? flash.now : flash
    f[:success] = message
  end

  def append_info_to_payload(payload)
    super
    client = DeviceDetector.new(request.user_agent)

    payload[:remote_ip] = request.remote_ip
    payload[:user_id] = current_user.try(:id)
    payload[:device] = {
        device_type: client.device_type,
        device_name: client.device_name,
        os_name: client.os_name,
        name: client.name
    }

  end

  private

  def default_timezone
    event = Event.where('slug ILIKE ?', params[:event_slug]).first if params[:event_slug].present?
    event.nil? ? 'UTC' : event.timezone
  end

end
