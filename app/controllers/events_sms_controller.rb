class EventsSmsController < ApplicationController

  include EventSluggable

  before_action :authorize_access

  def index
    #ndex a form.

    @options = [
        ["Enrolled Team Captains Only (#{@event.teams.where(does_receive_notifications: true).to_a.count})",'enrolled_team_captains_only'],
        ["All Enrolled Participants (#{@event.sms_participants.to_a.count})" ,'all_enrolled_participants']
    ] + @event.event_participation_types.collect { |item|
      participation_type = item.participation_type
      count = @event.sms_participants.joins(:participations).where('participations.event_id = ? and participations.event_participation_type_id = ?', @event.id, item.id).to_a.count
      ["Enrolled #{participation_type.name.pluralize} (#{count})", item.id.to_s]
    }
  end

  def send_standings_sms
    job_params = {
        event: @event,
        type: 'standings',
        who: params[:send_option].downcase.parameterize.underscore,
        message: nil
    }

    SMSJob.perform_later job_params

    respond_to do |format|
      format.js { render js: '' }
    end
  end

  def send_custom_sms
    job_params = {
        event: @event,
        type: 'custom',
        who: params[:send_option].downcase.parameterize.underscore,
        message: params[:message]
    }

    SMSJob.perform_later job_params

    respond_to do |format|
      format.js { render js: '' }
    end
  end

  def authorize_access
    authorize! :send_sms_messages, @event
  end
end