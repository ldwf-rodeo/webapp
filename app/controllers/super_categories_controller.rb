class SuperCategoriesController < ApplicationController

  include EventSluggable

  before_action :authenticate_user!
  before_filter :get_super_category, :except => [:create, :new, :index, :categories_index]

  include ActionAuthorizable

  def index
    @super_categories = SuperCategory.where(:event_id => @event.id).order('page ASC, position ASC, name ASC, id ASC')
    respond_to do |format|
      format.html
      format.json { render json: @super_categories }
    end
  end

  def categories_index
    @categories = Category.where(:super_category_id => params[:super_category_id]).order('grouping_number ASC, position ASC, name ASC, id ASC')
    respond_to do |format|
      format.html
      format.json { render json: @categories }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @super_category }
    end
  end

  def new
    @super_category = SuperCategory.new
    respond_to do |format|
      format.html
      format.json { render json: @super_category }
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.json { render json: @super_category }
    end
  end

  def create
    @error = false
    @super_category = SuperCategory.new(super_category_params)
    begin
      SuperCategory.transaction do
        @super_category.event_id = @event.id
        @super_category.save!

        @event.super_categories << @super_category
        @super_category.event = @event
      end
    rescue Exception => e
      @error = true
    end

    respond_to do |format|
      if !@error
        flash_success('Super category successfully created.')
        format.html { redirect_to super_category_path(:event_slug => @event.slug, :id => @super_category.id) }
        format.json { render json: @super_category, status: :created, location: @super_category }
      else
        flash_object_errors(@super_category,nil,'created')
        format.html { render action: "new" }
        format.json { render json: @super_category.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy
    @super_category.destroy!
    flash_success('Super category successfully deleted.')
    respond_to do |format|
      format.html { redirect_to super_categories_path(:event_slug => @event.slug) }
      format.json { render json: @super_category }
    end
  end

  def update
    respond_to do |format|
      if @super_category.update_attributes(super_category_params)
        flash_success('Super category successfully updated.')
        format.html { render action: "show" }
        format.json { head :no_content }
      else
        flash_object_errors(@super_category,nil,'updated')
        format.html { render action: "edit" }
        format.json { render json: @super_category.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def get_super_category
    @super_category = @event.super_categories.where(:id => params[:id]).first
    if @super_category.nil?
      raise ActionController::RoutingError.new('Super Category not found.')
    end
  end

  def super_category_params
    params.require(:super_category).permit(:id, :name, :position, :page, :category_text_color, :category_background_color)
  end

end