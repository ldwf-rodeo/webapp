class TwilioController < ApplicationController

  include Webhookable
  include SMSModule

  before_filter :preprocess, :get_event, :get_people, :log_incoming
  after_filter :set_header

  skip_before_action :authenticate_user!
  skip_before_action :verify_authenticity_token

  def preprocess
    @from_number = params['From']
    @to_number = params['To']

    @body = params['Body'] || ''
    if @from_number.nil? or @to_number.nil?
      logger.skynet.warn 'Unable to process message from unknown sender/receiver.'
    end

    @from_number = @from_number.gsub(/^\+1/,'').downcase.strip
    @to_number = @to_number.gsub(/^\+1/,'').downcase.strip
    @body = @body.downcase.gsub(/["']/,'').strip
  end

  def get_event
    response = nil
      @event = Event.find_by_twilio_phone(@to_number)

    if @event.nil?
      message = 'Sorry, there is no event associated with this phone number.'
      log_skynet_outgoing(message)
      response = Twilio::TwiML::Response.new do |r|
        r.Message message
      end
    elsif @event.is_over
      message = "The #{@event.name} has ended. No more texts will be broadcast from this number or processed by it."
      log_skynet_outgoing(message)
      response = Twilio::TwiML::Response.new do |r|
        r.Message message
      end
    end

    @species_id_list = @event.categories.collect {|c| c.category_species_unit_items.collect {|s| s.species_id} }.flatten.uniq

    if !response.nil?
      render_twiml response
    end
  end

  def get_people
    @participant = @event.participants.where("regexp_replace(home_phone, '[^0-9]', '','g') = ? or regexp_replace(mobile_phone, '[^0-9]', '','g') = ?", @from_number, @from_number).first
    @participation = @participant.nil? ? nil : @participant.participation(@event)
    @team = @participation.nil? ? nil : @participation.team
    @team_captain = @team.nil? ? nil : @team.captain
  end

  def log_incoming
    log_skynet_incoming(@body)
  end

  def sms
    help_message = 'If you are facing a real emergency, please call 911. \nThese are the texts you can send me:\n-Stop\n-Start\n-Leaderboard\n-Close Time\n-Open Time\n-Largest (Required: name of fish | Example: Largest Redfish)\n'

    responses = [
        {
            #HELP
            :is_match => lambda {|body| !(/^.*?(h+)(e+)(l+)(p+).*?/.match(body).nil?)},
            :error => lambda {nil},
            :response => lambda {|body| help_message}
        },
        {
            # START/STOP
            :is_match => lambda {|body| !(/^.*?((s+)(t+)(o+)(p+)|(s+)(t+)(a+)(h+)(p+)|start).*?/.match(body).nil?) },
            :error => lambda { nil },
            :response => lambda {|body| start_stop(body) }
        },
        {
            #LEADERBOARD
            :is_match => lambda {|body| !(/^(?:(\w+)\s){0,1}(leaderboard|update|result|results)(\s[0-9]){0,1}/.match(body).nil?)},
            :error => lambda { nil },
            :response => lambda {|body| specific_categories(body) }
        },
        {
            #THANKS
            :is_match => lambda{|body| !(/.*?(thanks|thank\syou|awesome|cool|great|thx|10x).*?/.match(body).nil?)},
            :error => lambda {nil},
            :response => lambda {|body| "You're welcome!"}
        },
        {
            #Participant info look up
            :is_match => lambda{|body| !(/^(my number|who am i)/.match(body).nil?)},
            :error => lambda {nil},
            :response => lambda {|body| find_participant_number }
        },
        {
            #CLOSE TIME
            :is_match => lambda {|body| /^(((close|closing){0,1}(.?time){0,1})|((time.?){0,1}(close|closing){0,1}))/.match(body).to_a.reject {|t| t.blank?}.count > 0 },
            :error => lambda {nil},
            :response => lambda {|body| open_close }
        },
        {
            #OPEN TIME
            :is_match => lambda {|body| /^(((open|opening){0,1}(.?time){0,1})|((time.?){0,1}(open|opening){0,1}))/.match(body).to_a.reject {|t| t.blank?}.count > 0 },
            :error => lambda {nil},
            :response => lambda {|body| open_close }
        },
        {
            #OPEN TIME -> USING SCALES
            :is_match => lambda {|body| /^.*?(scales).*?/.match(body).to_a.reject {|t| t.blank?}.count > 0 },
            :error => lambda {nil},
            :response => lambda {|body| open_close }
        },
        {
           #PODIUM
            :is_match => lambda { |body|
              match = /^.*?(?:(heaviest|largest|biggest|top|first|best)\s){0,1}(\w+)$/.match(body)
              !match.nil? && (Species.where('id in (?) and name ILIKE ?', @species_id_list, "%#{match.captures.last}%").count > 0)
            },
            :error => lambda {nil},
            :response => lambda {|body| podium(body)}
        },
        {
            #IDENTIFY SELF
            :is_match => lambda{|body| !(/^.*?(who\s(are|r)\s(you|u)|who\sis\sthis|what\s(are|r)\s(you|u)|what\sis\s(this|dis)).*?/.match(body).nil?)},
            :error => lambda {nil},
            :response => lambda {|body| "I am the text messaging system for #{@event.name}."}
        },
        {
            #LEADERBOARD CATEGORY OR SUPERCATEGORY NAME
            :is_match => lambda {|body|
              cat_count = @event.categories.where('categories.name ilike ?', "%#{/^(\w+).*/.match(body).captures.first}%").count
              super_cat_count = @event.super_categories.where('super_categories.name ilike ?', "%#{/^(\w+).*/.match(body).captures.first}%").count
              (cat_count + super_cat_count) > 0
            },
            :error => lambda {nil},
            :response => lambda {|body| specific_categories(body) }
        },
        {
            #UNKNOWN COMMAND
            :is_match => lambda{|body| true},
            :error => lambda{nil},
            :response => lambda{ |body| [
                'I did not understand that.',
                'I have a limited number of functions.',
                "My primary directive is to text updates about #{@event.name}",
                '---',
                'You can try a messages like:',
                'results',
                'red fish'
            ].join("\n")
            }
        }
    ]

    responses.each do |response|
      if response[:is_match].call(@body)
        #It is a match.
        error = response[:error].call

        if !error.nil?
          log_skynet_outgoing(error)

          response = Twilio::TwiML::Response.new do |r|
            r.Message error
          end

        else
          message_text = response[:response].call(@body)
          log_skynet_outgoing(message_text)

          msgs = message_text.is_a?(Array) ? message_text : [message_text]

          response = Twilio::TwiML::Response.new do |r|
            msgs.each {|m|  r.Message m }
          end
        end

        render_twiml response

        return
      end
    end
  end

  # THE RESPONSE METHODS
  def podium(body)

    matches = /^.*?(?:(heaviest|largest|biggest|top|first|best)\s){0,1}(\w+)$/.match(body)
    fishname = matches.captures.last
    SMSSender.get_specific_categories(fishname, @event)

    # # get the id list of all the species that this event is being scored on
    # species = Species.where("name ilike ? AND id IN (?)", "%#{fishname}%", @species_id_list)
    #
    # if species.count > 0
    #   #Is this a recognized species?
    #   measurements = CaptureMeasurement.joins(:capture).where('captures.species_id in (?) AND captures.event_id = ?', species.ids, @event.id).select('distinct ON (captures.species_id, unit_id) capture_measurements.*').order('captures.species_id, unit_id, measurement DESC')
    #   results_text = measurements.map {|m| c = m.capture; "#{c.participant.full_name}: #{c.species.name} #{m.measurement} #{m.unit.abbreviation}"}.join "\n"
    #   if results_text.blank?
    #     result = "There are no captures entered for #{species.collect{|s|s.name}.join(' or ')} yet."
    #   else
    #     result = results_text
    #   end
    # else
    #   result = "Sorry, we couldn't find a category for \"#{fishname}.\""
    # end
    #
    # result
  end

  def specific_categories(body)
    matches = /^(?:(\w+)\s){0,1}(leaderboard|update|result|results)(\s[0-9]){0,1}/.match(body)

    if matches
      SMSSender.get_specific_categories(matches.captures.first, @event, matches.captures.last)
    else
      matches ||= /^(\w+).*/.match(body)
      SMSSender.get_specific_categories(matches.captures.first, @event, nil)
    end


  end

  def open_close
    today_times = @event.event_scale_openings.where('timezone(?, now())::date BETWEEN timezone(?,start_time)::date AND timezone(?,end_time)::date', 'America/Chicago', 'America/Chicago', 'America/Chicago').order('start_time ASC')
    future_times = @event.event_scale_openings.where('timezone(?, now())::date < timezone(?,end_time)::date', 'America/Chicago', 'America/Chicago').order('start_time ASC')

    results = []

    if today_times.empty?
      results << 'There are no scale times for today.'
    else
      results << 'Scales are open today:'
      results += today_times.map {|t| "-- from #{format_time(t.start_time)} to #{format_time(t.end_time)}"}
    end

    unless future_times.empty?
      results << ''
      results << 'Upcoming scale times:'
      results += future_times.map {|t| "-- #{format_time(t.start_time,'%a %b %e %I:%M %P %Z')} to #{format_time(t.end_time,'%a %b %e %I:%M %P %Z')}"}
    end

    results.join("\n")
  end

  def start_stop(body)
    is_start = /^((s+)(t+)(o+)(p+)|(s+)(t+)(a+)(h+)(p+))/.match(body).nil?
    if !@participant.nil?
      @participant.update_attribute(:does_receive_notifications, is_start)
    end
    if !@participant.nil? and @team_captain == @participant
      @participation.team.update_attribute(:does_receive_notifications, is_start)
    end
    return 'You have been ' + (is_start ? 'subscribed to' : 'unsubscribed from') + " #{@event.name} text updates"
  end

  def voice
    to_number   = params['To']
    event = Event.where(twilio_phone: to_number).first

    response = Twilio::TwiML::Response.new do |r|
      r.Say "Hello. This is the #{event.name} text messaging line. We will be sending daily leaderboard updates, as well as letting you and your fishing team know when they have moved down on the leaderboard. If you do not wish to receive text notifications, please text STOP to this number at anytime.", :voice => 'alice'
    end

    render_twiml response
  end

  def find_participant_number
    if @participation
      [
          "Hi #{@participant.first_name}",
          '',
          "Your number for #{@event.name} is:",
          "#{@participation.display_number}",
          "Your team is:",
          "#{@team.name}#{ ': Number #' + @team.number.to_s if @team.number }"
      ].join("\n")
    else
      "Your number has not been registered with #{@event.name}"
    end
  end

  private

  def get_participant(params)
    received_number = params['From'].gsub(/^\+1/,'').gsub(/[~0-9]/, '')

    pparticipant = Participant.where("regexp_replace(home_phone, '[^0-9]', '') = ? or regexp_replace(mobile_phone, '[^0-9]', '') = ?", received_number, received_number)

    if pparticipant.nil? or params['Body'].downcase.strip == 'stop'
      head :not_found
      return
    end

    return pparticipant
  end

  def format_time(time, time_format = nil )
    time.in_time_zone(timezone).strftime(time_format || '%I:%M %Z')
  end

  def timezone
    @event.timezone || @browser_timezone
  end
end
