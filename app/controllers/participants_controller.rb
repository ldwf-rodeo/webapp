class ParticipantsController < ApplicationController

  include EventSluggable

  before_action :get_participant, only: [:show, :edit, :update, :destroy, :badge, :get_info]

  include ActionAuthorizable

  # GET /participants
  # GET /participants.json
  # GET /participants.xlsx
  def index
    @participants = Participant
                        .joins({:participations => [:team, { :event_participation_type => :participation_type }]}, :gender)
                        .where( participations: {event_id: @event.id} )
                        .order(%w(participants.last_name participants.first_name))
                        .select([
                                    'participants.*',
                                    'teams.number AS team_number',
                                    'teams.name as team_name',
                                    'teams.checked_in as team_checked_in',
                                    'genders.name as gender_name',
                                    'participation_types.name as participation_type'
                                ])
  end

  # GET /participants/1
  def show
  end

  def get_info

  end

  # GET /participants/new
  def new
    @participant = Participant.new
  end

  # GET /participants/1/edit
  def edit
    @team = @participant.participation(@event).team
  end

  # POST /participants
  # POST /participants.json
  def create
    @participant = Participant.new(participant_params)

    respond_to do |format|
      if @participant.save
        @team = Team.where(:id => params[:team_id]).first
        @team.participants << @participant
        format.html { redirect_to @participant, notice: 'Participant was successfully created.' }
        format.json { render json: @participant, status: :created, location: @participant }
        format.js
      else
        format.html { render action: "new" }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /participants/1
  # PUT /participants/1.json
  def update

    result = false
    Participant.transaction do

      @participant.update_attributes(participant_params)

      participation = @participant.participation(@event)
      participation.event_participation_type = EventParticipationType.find(params[:event_participation_type_id])

      participation.save!

      result = true
    end

    respond_to do |format|
      if result
        format.html { redirect_to participants_path(event_slug: @event.slug), notice: 'Participant was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /participants/1
  # DELETE /participants/1.json
  def destroy

    @participant.destroy

    respond_to do |format|
      format.html { redirect_to participants_url }
      format.json { head :no_content }
      format.js
    end
  end

  #leaderboard page for a participant
  def summary
    @participant = Participant.find(params[:participant_id])
    @category_place = Hash.new()

    @event.super_categories.each do |s_cat|
      data = Hash.new

      s_cat.each do |category|
        data[category.id] = category.caps({:participant_id => params[:participant_id]})
      end

      @category_place[s_cat.name] = data

    end
    render :layout => 'plain'
  end

  ##### front facing routes

  def badge
    relative_part = @participant.participation(@event)

    if relative_part.pending_jobs.count > 0
      redirect_to participants_path(event_slug: @event.slug, id: @participant.id), notice: 'There are badges that have not been generated yet'
      return
    end

    respond_to do |format|
      format.pdf{
        badges = relative_part.badge
        send_file badges.path, :filename => "#{@participant.last_name} Badges.pdf"
      }
    end

  end

  private

  def get_participant
    @participant = @event.participants.where(id: params[:id]).first
    if @participant.nil?
      raise 'Participant not found.'
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def participant_params
    params.require(:event_participation_type_id)
    params.require(:participant).permit(:age, :city, :email, :first_name, :participant_number ,:home_phone, :last_name, :gender, :does_receive_notifications, :participation_type, :mobile_carrier, :mobile_phone, :shirt_size, :street, :zipcode, :shirt_size_id, :date_of_birth, :gender_id)
  end

end


