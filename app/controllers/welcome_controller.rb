class WelcomeController < ApplicationController

  skip_before_action :authenticate_user!

  def index
    now = Time.now.in_time_zone(@browser_time_zone)
    @past = Event.where("end_date < '#{now - 1.year}'").order("end_date DESC")
    @recent = Event.where("end_date < '#{now}' AND end_date >= '#{now - 1.year}'").order('end_date DESC')
    @current = Event.where("start_date < '#{now}' and end_date > '#{now}'").order("start_date DESC")
    @future = Event.where("start_date > '#{now}'").order("start_date ASC")
  end

end