class CategoryItemsController < ApplicationController

  include EventSluggable

  before_filter :get_category
  before_filter :get_category_item, :only => [:show, :edit, :update, :destroy]

  before_filter :verify_instance_access, :only => [:show, :edit, :update, :destroy]
  before_filter :verify_class_access, :only => [:index, :create, :new]

  def index
    @category_items = @category.items

    respond_to do |format|
      format.html
      format.json { render json: @category_items }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @category_item }
    end
  end

  def new
    @category_item = @category.category_item_class.new
    respond_to do |format|
      format.html
      format.json {}
    end
  end

  def create
    ActiveRecord::Base.transaction do
      @category_item = @category.category_item_class.new(item_params)
      item_list = @category.category_item_class.to_s.underscore + 's'
      @category.send(item_list) << @category_item
      @category_item.category_id = @category.id
      @category_item.save!
      @category_item.category.save!
    end

    respond_to do |format|
      if @category_item.errors.full_messages.to_a.empty?
        format.html { redirect_to category_category_item_path(:event_slug => @event.slug, :category_id => @category.id, :id => @category_item.id), notice: 'Item was successfully created.' }
        format.json { render json: @category_item, status: :created, location: @category_item }
      else
        format.html { render action: "new" }
        format.json { render json: @category_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.json {}
    end
  end

  def update
    ip = item_params
    ActiveRecord::Base.transaction do
      @category_item.update_attributes!(ip.except(:super_category_ids))
      if @category.category_type == :group
        #Update each of the super categories that this group applies to.
        ip[:super_category_ids] ||= []
        @category_item.category_grouping_item_super_categories.each {|sc| if !ip[:super_category_ids].include?(sc.super_category_id) then sc.destroy end }
        ip[:super_category_ids].each do |sc_id|
          CategoryGroupingItemSuperCategory.where(:category_grouping_item => @category_item, :super_category_id => sc_id).first_or_create!
        end
      end
      @category_item.category.save!
    end

    respond_to do |format|
      if @category_item.errors.full_messages.to_a.empty?
        format.html { redirect_to category_category_item_path(:event_slug => @event.slug, :category_id => @category.id, :id => @category_item.id), notice: 'Item was successfully updated.' }
        format.json { render json: @category_item, status: :created, location: @category_item }
      else
        format.html { render action: 'edit' }
        format.json { render json: @category_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @category_item.destroy!

    respond_to do |format|
      format.html { redirect_to category_category_items_path(:event_slug => @event.slug, :category_id => @category.id) }
      format.json { head :no_content }
    end
  end

  private

  def verify_class_access
    authorize! params[:action].to_sym, Category
  end

  def verify_instance_access
    authorize! params[:action].to_sym, @category
  end

  # If this item has no category, then it doesn't exist
  def get_category
    @category = Category.where(:id => params[:category_id]).first
    if @category.nil?
      raise ActionController::RoutingError.new('Category not found.')
    end
  end

  def get_category_item
    @category_item = @category.category_item_class.where(:id => params[:id], :category_id => @category.id).first
    if @category_item.nil?
      raise ActionController::RoutingError.new('Category item found.')
    end
  end

  def item_params
    type = @category.category_item_class
    item_symbol = (type.to_s.to_s.underscore).to_sym
    # Exclude id and category_id from the valid params, and convert the rest of the params to symbols
    valid_params = ((type.column_names - ['id', 'category_id']).map &:to_sym)
    f = valid_params + [:super_category_ids => []]
    params.require(item_symbol).permit(f)
  end

end