class CategoriesController < ApplicationController
  include EventSluggable

  before_filter :get_category, :only => [:edit, :update, :destroy, :show]

  include ActionAuthorizable

  def index
    @categories = @event.categories.joins(:super_category).order('super_categories.page ASC, super_categories.position ASC, super_categories.name ASC, super_categories.id ASC, categories.grouping_number ASC, categories.position ASC, categories.name ASC, categories.id ASC')
    respond_to do |format|
      format.html
      format.json { render json: @categories }
    end
  end

  def show
    @category = Category.where(:id => params[:id]).first
    respond_to do |format|
      format.html
      format.json { render json: @category }
    end
  end

  def new
    @category = Category.new
    respond_to do |format|
      format.html
      format.json { render json: @category }
    end
  end

  def edit
    @category = Category.where(:id => params[:id]).first
    respond_to do |format|
      format.html
      format.json { render json: @category }
    end
  end

  def create
    params = copy_sources_and_orders(category_params)
    super_category = SuperCategory.where(:id => params[:super_category_id]).first

    @category = Category.new(params.except(:items))
    @error = false
    begin
      Category.transaction do
        @category.save!
        super_category.categories << @category
        @category.super_category = super_category
        # If there are items, add them to the category's items
        if params[:items]
          params[:items].each do |item|
            @item = Categoryitem.create!(item)
            @category.categoryitems << @item
          end
        end
      end
    rescue Exception => e
      @error = true
    end

    respond_to do |format|
      if !@error
        flash_success('Category successfully created.',false)
        format.html { redirect_to category_path(:event_slug => @event.slug, :id => @category.id) }
        format.json { render json: @category, status: :created, location: @category }
      else
        flash_object_errors(@category,nil,'created')
        format.html { render action: "new" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @category = Category.where(:id => params[:id]).first
    @category.destroy!
    flash_success('Category successfully deleted.')
    respond_to do |format|
      format.html { redirect_to categories_path(:event_slug => @event.slug) }
      format.json { render json: @category }
    end
  end

  def update
    params = copy_sources_and_orders(category_params)
    respond_to do |format|
      if @category.update_attributes(params)
        flash_success('Category successfully updated.')
        format.html { render action: "show"}
        format.json { head :no_content }
      else
        flash_object_errors(@category,nil,'updated')
        format.html { render action: "edit" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def get_category
    @category = @event.categories.where(:id => params[:id]).first
    if @category.nil?
      raise ActionController::RoutingError.new('Category not found.')
    end
  end

  # Helper method to set blank selects to nil
  def copy_sources_and_orders(category_params)
    if category_params[:score_2_source].blank?
      category_params[:score_2_source] = nil
    end
    if category_params[:score_2_order].blank?
      category_params[:score_2_order] = nil
    end
    if category_params[:score_3_source].blank?
      category_params[:score_3_source] = nil
    end
    if category_params[:score_3_order].blank?
      category_params[:score_3_order] = nil
    end

    # If there is no source or order, the is_random boolean should be null instead of false
    if category_params[:score_2_source].nil? && category_params[:score_2_order].nil? && category_params[:score_2_is_random] == '0'
      category_params[:score_2_is_random] = nil
    end
    # If there is no source or order, the random boolean should be null instead of false
    if category_params[:score_3_source].nil? && category_params[:score_3_order].nil? && category_params[:score_3_is_random]== '0'
      category_params[:score_3_is_random] = nil
    end
    return category_params
  end

  def category_params
    params.require(:category).permit(:start_age, :end_age, :positions, :name, :subtitle, :super_category_id, :score_entity, :score_grouping, :score_grouping_limit, :category_type, :has_distinct_places, :valid_start_time, :valid_end_time, :gender_id, :premium_category_grouping_id, :score_1_source, :score_1_order, :score_2_source, :score_2_order, :score_2_is_random, :score_3_source, :score_3_order, :score_3_is_random, :display_mode, :position, :grouping_number, :category_background_color,:category_text_color)
  end

end