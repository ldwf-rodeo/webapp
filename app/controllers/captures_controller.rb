class CapturesController < ApplicationController

  include EventSluggable

  before_action :get_capture, only: [:show, :edit, :update, :destroy]
  before_action :get_grouped_options, only: [:new, :edit, :create, :update]

  include ActionAuthorizable

  after_action :update_boards_without_sms, only: [:update, :destroy]
  after_action :update_boards_with_sms, only: [:create]

  # GET /captures
  # GET /captures.json
  def index
    @captures = Capture
                    .joins({participant: { :participations => :team }}, :user, :species)
                    .where(participations: { event_id: @event.id }, :event_id => @event.id )
                    .order([
                               'captures.entered_at DESC',
                               'captures.created_at DESC',
                               'captures.id DESC',
                           ])
                    .select([
                              'captures.*',
                              "concat(to_char(captures.entered_at AT TIME ZONE '#{@browser_timezone}','Dy, Mon DD @ HH24:MI:ss TZ')) AS time_entered",
                              "extract(EPOCH from (captures.entered_at AT TIME ZONE '#{@browser_timezone}')) as epoch_time",
                              'teams.name as team_name',
                              'teams.number as team_number',
                              'users.user_name as user_name',
                              'species.name as species_name',
                              'concat(participants.first_name,\' \', participants.last_name) as participant_full_name',
                              'row_number() OVER (ORDER BY captures.entered_at ASC, captures.created_at ASC, captures.id ASC) as index',
                              'measurements_for_capture(captures.id) as measurements'
                            ])
  end

  def edit
    @capture_measurements = CaptureMeasurement.none
    #Add in new units.
    @event.units.each do |unit|
      m = CaptureMeasurement.where(:unit_id => unit.id, :capture_id => @capture.id).first
      if m.nil?
         @capture_measurements = @capture_measurements << CaptureMeasurement.new(:unit_id => unit.id, :measurement => nil)
      else
        @capture_measurements = @capture_measurements << m
      end
    end
    @captures = [@capture]
    @is_editing = true
  end

  def destroy

    ActiveRecord::Base.transaction do
      capture = @event.captures.find(params[:id].to_i)
      capture.capture_measurements.each do |cm|
        cm.destroy!
      end
      capture.destroy!
    end

    respond_to do |format|
      flash[:success] = "Successfully deleted capture."
      format.html { redirect_to captures_path(:event_slug => @event.slug )}
      format.json { render json: @capture, status: :created, location: @capture }
      format.js
    end
  end

  def update
    @captures = []
    @capture = nil
    first_capture = nil

    begin
      Capture.transaction do
        params = capture_params
        event_id = params[:event_id]
        user_id = params[:user_id]
        participant_id = params[:participant_id]

        params[:captures].each do |k1,v1|
          #Update the capture.
          capture = Capture.find(v1[:id].to_i)

          if first_capture.nil? or (capture.entered_at < first_capture.entered_at)
            first_capture = capture
            @capture = capture
          end

          # add the attributes that are not part of the form
          v1[:participant_id] = participant_id
          v1[:user_id] = user_id
          v1[:event_id] = event_id

          destroys = []; updates = []; news = []
          v1[:capture_measurements_attributes].each do |no,pairs|
            if pairs[:measurement].blank?
              destroys << v1[:capture_measurements_attributes][no]
            elsif pairs[:id].blank?
              news << v1[:capture_measurements_attributes][no]
            else
              updates << v1[:capture_measurements_attributes][no]
            end
          end

          capture.update_attributes!(v1.except(:capture_measurements_attributes))
          capture.capture_measurements.where('id IS NOT NULL AND id IN (?)',destroys.collect{|c| c[:id] }).destroy_all
          updates.each do |update|
            CaptureMeasurement.where(:id => update[:id]).first.update_attributes!(update)
          end
          news.each do |new_item|
            CaptureMeasurement.create!(:measurement => new_item[:measurement], :capture => capture, :unit_id => new_item[:unit_id])
          end

          @captures << capture
        end

      end
    rescue => error
      e = error.message
      puts 'Error Has Occurred'
      puts error
      @success = false
    else
      @success = true
    end

    respond_to do |format|
      if @success
        format.html { redirect_to capture_path(:event_slug => @event.slug, :id => @capture.id), notice: 'Capture was successfully created.' }
        format.json { render json: @capture, status: :created, location: @capture }
        format.js
      else
        flash[:error] = e
        format.html { render action: "edit" }
        format.json { render json: @capture.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # GET /captures/1
  # GET /captures/1.json
  def show
    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # show.html.erb
      format.json { render json: @capture }
    end
  end

  # GET /captures/new
  # GET /captures/new.json
  def new
    @path = captures_path

    @captures = [Capture.new]
    @capture_measurements = CaptureMeasurement.none

    @captures.each do |capture|
      capture.capture_measurements = []
      @event.units.each do |unit|
        @capture_measurements = @capture_measurements << CaptureMeasurement.new(:unit_id => unit.id, :measurement => nil)
      end
    end

    puts "FOUND: " + @capture_measurements.count.to_s
    @is_editing = false

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # new.html.erb
      format.json { render json: @capture }
    end
  end


  # POST /captures
  # POST /captures.json
  def create
    #This action handles errors differently because of the requirements of capture entry. The js responder needs to
    # pop an alert on failure to make sure nothing went wrong.
    first_capture = nil
    begin
      Capture.transaction do
        params = capture_params
        event_id = params[:event_id]
        user_id = params[:user_id]
        participant_id = params[:participant_id]

        params[:captures].each do |k1,v1|
          # If entered_at was not filled, set it to the current time.
          capture = Capture.create!(
              :species_id => v1[:species_id],
              :entered_at => v1[:entered_at],
              :is_redundant => v1[:is_redundant],
              :event_id => event_id,
              :user_id => user_id, :participant_id => participant_id)

          if first_capture.nil? or (capture.entered_at < first_capture.entered_at)
            first_capture = capture
          end

          v1[:capture_measurements_attributes].each do |k2,v2|
            if (v2[:measurement] != nil and not v2[:measurement].empty?)
              CaptureMeasurement.create!(:capture_id => capture.id, :unit_id => v2[:unit_id], :measurement => v2[:measurement])
            end
          end
        end
      end
    rescue => error
      puts 'Error Has Occurred'
      puts error
      @success = false
    else
      @success = true
    end

    @capture = first_capture

    respond_to do |format|
      if @success
        format.html { redirect_to capture_path(:event_slug => @event.slug, :id => @capture.id), notice: 'Capture was successfully created.' }
        format.json { render json: @capture, status: :created, location: @capture }
        format.js
      else
        format.html { render action: "new" }
        format.json { render json: @capture.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  #I have no idea what this is for... Dustin 5/1/14
  def test_new
    event_id = params[:event_id]
    data = Hash.new()
    event = Event.find(event_id)
    data[:species] = event.get_species
    data[:participants] = event.get_participants
    render :json => data.to_json
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def get_capture
    @capture = @event.captures.find(params[:id])
  end

  def get_grouped_options
    target_species = @event.get_species
    #other_species = Species.order('name') - target_species
    @grouped_options = {
        'Category Species' => target_species.map {|s| [s.name,s.id]}
        #'Other Species'    => other_species.map {|s| [s.name,s.id]}
    }
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def capture_params
    params.permit(:participant_id, :user_id, :event_id, :time_entered, :user_id, :captures => [:id, :species_id, :entered_at, :is_redundant, :capture_measurements_attributes => [:id, :measurement, :unit_id]], :suppress_sms => [:value])
  end

  def update_boards_with_sms
    if !@capture.nil? #Maybe the capture didn't go through.
      cp = capture_params
      send_sms = (@event.is_active? && (cp.has_key?(:suppress_sms) and cp[:suppress_sms].has_key?(:value) and cp[:suppress_sms][:value] === '0')) ? true : false
      job_params = {
          capture_id: @capture.id,
          event: @event,
          send_sms: send_sms
      }
      LeaderboardJob.perform_later job_params
    end
  end

  def update_boards_without_sms
    if !@capture.nil? #Maybe the capture didn't go through.
      # Push leaderboard updates into the queue
      job_params = {
          capture_id: @capture.id,
          event: @event,
          send_sms: false
      }
      LeaderboardJob.perform_later job_params
    end
  end

end
