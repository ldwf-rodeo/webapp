module ActionAuthorizable

  extend ActiveSupport::Concern

  included do
    append_before_filter :check_class_action_permissions, :only => [:index, :new, :create]
    append_before_filter :check_instance_action_permissions, :only => [:show, :edit, :update, :destroy]
  end

  # Default CRUD authorization based on action name.

  def check_class_action_permissions
    #e.g. authorize! :index, Capture
    authorize! action_symbol, controller_class
  end

  def check_instance_action_permissions
    #e.g. authorize! :edit, @capture
    authorize_instance(action_symbol)
  end

  # CLASS AUTHORIZATION


  def authorize_index_access
    authorize! :index, controller_class
  end

  def authorize_new_access
    authorize! :new, controller_class
  end

  def authorize_create_access
    authorize! :create, controller_class
  end

  # INSTANCE AUTHORIZATION

  def authorize_show_access
    authorize_instance(:show)
  end

  def authorize_edit_access
    authorize_instance(:edit)
  end

  def authorize_update_access
    authorize_instance(:update)
  end

  def authorize_destroy_access
    authorize_instance(:destroy)
  end


  private

  def authorize_instance(action_symbol)
    var = controller_variable
    if !var.nil?
      authorize! action_symbol, var
    else
      raise 'Could not find matching instance variable for authorization.'
    end
  end

  def action_symbol
    params[:action].to_sym
  end

  def controller_class
    controller_name.classify.constantize
  end

  def controller_variable
    var_name = params[:controller].singularize.downcase
    var = instance_variable_get("@#{var_name}")
  end

end