module TeamImportable
  extend ActiveSupport::Concern


  ## HEADER STRING CONSTANTS
  CAPTAIN = 'Captain'
  CONTACT = 'Contact'
  TEAM_NAME = 'Team Name'
  TEAM_COMPANY = 'Company'

  GENDER = 'Gender'
  SHIRT_SIZE = 'Shirt Size'
  EMAIL = 'Email'
  PHONE = 'Phone'
  ADDRESS = 'Address'
  CITY = 'City'
  STATE = 'State'
  ZIP_CODE = 'Zip Code'

  ANGLER_HEADER_PREFIX = 'Angler'
  GUEST_HEADER_PREFIX = 'Guest'

  NUMBER_OF_ANGLERS_PER_TEAM = 6
  NUMBER_OF_GUESTS_PER_TEAM = 4

  # If genders are not necessary to the rodeo, default to male
  DEFAULT_GENDER_ID = 1

  def create_team(row, event)

    team = Team.new
    team.name = row[TEAM_NAME]
    team.company = row[TEAM_COMPANY]
    team.event = event
    team.save!

    anglers = build_angler_participations(row, team, event)
    guests = build_guest_participations(row, team, event)

    anglers.each do |participation|
      # participation.participant.save!
      participation.save!
    end

    guests.each do |participation|
      # participation.participant.save!
      participation.save!
    end

    # build the captain and contact participant
    captain_participation = build_captain(row, team, event)
    contact_participation = build_contact(row, team, event)

    captain_participation.save!
    contact_participation.save!

    team.captain = captain_participation.participant
    team.contact = contact_participation.participant

    team.save!

    team
  end

  def build_captain(row, team, event)
    participation = build_participation(team, event, angler_participation_type(event))
    participation.participant = build_participant(row, CAPTAIN)

    possible = get_participant_with_name(team, participation.participant)
    participation = possible.participation(team) unless possible.nil?

    participation
  end

  def build_contact(row, team, event)
    participation = build_participation(team, event, guest_participation_type(event))
    participation.participant = build_participant(row, CONTACT)

    possible = get_participant_with_name(team, participation.participant)

    if possible
      participation.participant.attributes.each_pair do |contact_key, contact_value|
        possible.attributes[contact_key] = contact_value unless contact_value.nil?
      end

      participation = possible.participation(team)
    end

    participation
  end

  private

  def build_angler_participations(row, team, event)
    event_participation_type = angler_participation_type(event)
    create_participants_for_type(row, NUMBER_OF_ANGLERS_PER_TEAM, team, event, event_participation_type, ANGLER_HEADER_PREFIX)
  end

  def build_guest_participations(row, team, event)
    event_participation_type = guest_participation_type(event)
    create_participants_for_type(row, NUMBER_OF_GUESTS_PER_TEAM, team, event, event_participation_type, GUEST_HEADER_PREFIX)
  end

  def create_participants_for_type(row, number, team, event, participation_type, header_prefix )
    result = []

    # Read the anglers from the spreadsheet
    (1..number).each do |i|
      start_header = header_prefix + ' ' + i.to_s
      # if the angler name is blank, then we can skip
      unless row[start_header].blank?
        participation = build_participation(team, event, participation_type)
        participation.participant = build_participant(row, start_header)
        result << participation
      end

    end

    result
  end

  def build_participation(team, event, participation_type)
    participation = Participation.new
    participation.team = team
    participation.event = event
    participation.event_participation_type = participation_type

    participation
  end

  def build_participant(row, header_prefix)
    prefix = header_prefix + ' '
    participant = Participant.new

    name_parser = People::NameParser.new
    name_fields = name_parser.parse(row[header_prefix])

    # if we could not parse the name then split on the space character
    unless name_fields[:parsed]
      # If the participant's name is to be determined, use TBD for both first and last name
      if row[header_prefix] == 'TBD'
        name_fields[:first] = 'TBD'
        name_fields[:last] = 'TBD'
      # If only a single name was given, use TBD as the last name
      elsif row[header_prefix].split(' ').size < 2
        name_fields[:first] = row[header_prefix]
        name_fields[:last] = 'TBD'
      # If name could not be parsed and is more than one part, use the first two parts as the name
      else
        name_fields[:first] = row[header_prefix].split(' ')[0].strip
        name_fields[:last] = row[header_prefix].split(' ')[1].strip
      end
    end

    participant.first_name = name_fields[:first]
    participant.last_name = build_last_name(name_fields)
    # If no gender is given, use the default (male)
    participant.gender = Gender.where("abbreviation ILIKE COALESCE(?,'M')", row[prefix + GENDER]).first
    participant.shirt_size = ShirtSize.where("abbreviation ILIKE COALESCE(?,'')", row[prefix + SHIRT_SIZE]).first
    participant.email = row[prefix + EMAIL]
    participant.mobile_phone = row[prefix + PHONE]
    participant.street = row[prefix + ADDRESS]
    # If city and state are given, concatenate them to form the city
    unless row[prefix + CITY].nil? || row[prefix + STATE].nil?
      participant.city = row[prefix + CITY].to_s + ', ' + row[prefix + STATE].to_s
    # Else, use just the given city name as the city
    else
      participant.city = row[prefix + CITY]
    end
    participant.zipcode = row[prefix + ZIP_CODE]

    participant
  end

  def build_last_name(fields)
    last_name = fields[:suffix].blank? ? fields[:last] : fields[:last] + ' ' + fields[:suffix]
    last_name = 'TBD' if last_name.nil?
    last_name
  end

  def angler_participation_type(event)
    event.event_participation_types.where(participation_type: ParticipationType.where("name ILIKE 'angler'").first).first
  end

  def guest_participation_type(event)
    event.event_participation_types.where(participation_type: ParticipationType.where("name ILIKE 'guest'").first).first
  end

  def get_participant_with_name(team, participant)
    team.participants.where('first_name ILIKE ? and last_name ILIKE ?', participant.first_name, participant.last_name).first
  end
end
