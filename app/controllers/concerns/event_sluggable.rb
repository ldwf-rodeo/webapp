module EventSluggable

    extend ActiveSupport::Concern

    included do
      prepend_before_action :find_event
    end

    private

    def find_event
      @event = Event.where('slug ILIKE ?',params[:event_slug]).first
      if @event.nil?
        raise ActionController::RoutingError.new('Could not find event.')
      end
      @ability_scope = @event
    end
end