module EventClonable

  extend ActiveSupport::Concern

  # Clones an event, its super categories, and its categories.
  def clone_event(event)
    @new_event = nil

    ActiveRecord::Base.transaction do
      kept_values = {
          :name => event.name + " (New At " + Time.now.to_s + ")",
          :start_date => event.start_date,
          :end_date => event.end_date,
          :timezone => event.timezone,
          :banner_color => event.banner_color,
          :category_text_color => event.category_text_color,
          :category_background_color => event.category_background_color
      }
      @new_event = clone_object(Event,event,Event.attribute_names,kept_values)
      @new_event.save!

      #Copy each SuperCategory.
      event.super_categories.each do |old_super_category|

        new_super_category = clone_object(SuperCategory,old_super_category)
        new_super_category.event = @new_event
        new_super_category.save!

        #Copy each SuperCategory's categories.
        old_super_category.categories.each do |old_category|
          category = clone_category(old_category, new_super_category)
          category.save!

          old_category.category_grouping_items.each do |old_category_grouping_item|
            new_category_grouping_item = clone_category_sub_component(CategoryGroupingItem, old_category_grouping_item, category)
            old_category_grouping_item.category_grouping_item_super_categories.each do |old_category_grouping_item_super_category|
              i = clone_object(CategoryGroupingItemSuperCategory,old_category_grouping_item_super_category,['category'],{:category_grouping_item => new_category_grouping_item})
              i.save!
            end
          end
          old_category.category_species_points_items.each { |item| clone_category_sub_component(CategorySpeciesPointsItem, item, category) }
          old_category.category_species_unit_items.each { |item| clone_category_sub_component(CategorySpeciesUnitItem, item, category) }

        end
      end

      #Copy each event participation type.
      event.event_participation_types.each do |event_participation_type|
        new_event_participation_type = clone_object(EventParticipationType,event_participation_type)
        new_event_participation_type.event_id = @new_event.id
        puts new_event_participation_type.inspect
        new_event_participation_type.save!
      end

    end

    #Save each category to update the descriptions.
    @new_event.categories.each do |category|
      category.save!
    end

    return @new_event
  end

  private

  def clone_category(old_item, parent_item)
    new_item = clone_object(Category, old_item, [:summary_url])
    new_item.super_category = parent_item
    new_item.save!
    new_item
  end

  def clone_category_sub_component(model, old_item, category ,exceptions=[],mapping={})
    new_item = clone_object(model, old_item, exceptions, mapping)
    new_item.category = category
    new_item.save!
    new_item
  end

  # Copies an object from a model, except for the listed exceptions.
  def clone_object(model,old_object,exceptions=[],mapping={})
    attrs = old_object.attributes.except('id','created_at','updated_at').except(*exceptions).merge(mapping)
    new_object = model.new(attrs)
    new_object
  end
end