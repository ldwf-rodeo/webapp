class EventPublicController < ApplicationController

  skip_before_action :authenticate_user!

  include EventSluggable

  def landing
    #scoped to event
    #show the even home page, this is were the event entries will be done
    render :layout => 'event_home'
  end

  def leaderboard
    render :layout => 'leaderboard'
  end

  def facts_show
    @no_captures_yet = (@event.captures.count == 0)
    @results = @event.facts_information
  end

end