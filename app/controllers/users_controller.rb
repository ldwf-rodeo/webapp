class UsersController < ApplicationController

  before_action :get_user, only: [:show, :edit, :update, :destroy, :edit_password, :update_password]

  include ActionAuthorizable

  before_action :authorize_update_access, :only => [:edit_password, :update_password]

  # GET /users
  # GET /users.json
  def index
    @users = User.all.order('user_name ASC')

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    @participant = Participant.new

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # edit.html.erb
    end
  end

  def edit_password
    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # edit.html.erb
    end
  end

  def update_password
    respond_to do |format|
      if @user.update_attributes(params.require(:user).permit(:password,:password_confirmation,:id))
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: :edit_password }
        format.json { render json: @user.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # POST /users
  # POST /users.json
  # Not sure if this needs fixing, do we even create users and participants at the same time?
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
        format.js
      else
        flash[:error] = flash_object_errors(@user)
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    ps = user_params
    ps[:role_ids] ||= []
    @user.user_roles.each {|ur| if !ps[:role_ids].include?(ur.role_id) then ur.destroy end }
    ps[:role_ids].each do |role_id|
      role = Role.find(role_id)
      if !@user.roles.include?(role)
        @user.roles << role
      end
    end

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
      format.js
    end
  end

  private

  def get_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:participant, :user_name, :password, :password_confirmation, :role_ids => [])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def participant_params
    params.require(:participant).permit(:city, :email, :first_name, :home_phone, :last_name, :mobile_carrier, :mobile_phone, :shirt_size, :street, :participant_type, :zipcode)
  end
end
