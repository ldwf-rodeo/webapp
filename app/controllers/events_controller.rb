include EventHelper

class EventsController < ApplicationController

  include EventClonable
  include TeamImportable

  before_action :authenticate_user!
  before_action :get_event, only: [ :show, :edit, :update, :destroy]
  before_action :get_event_by_id, only: [:import]

  include ActionAuthorizable

  before_action :authorize_create_access, :only => [:clone, :import]

  # GET /:name/home
  def admin
    @participants = @event.participants
  end

  # GET /events
  # GET /events.json
  def index
    #not scoped
    # list all events
    @events = Event.all.reject {|e| !(can? :show, e) }
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  # GET /events/new.json
  def new
    #not scoped
    @event = Event.new
    @event.event_scale_openings.build
  end

  # GET /events/1/edit
  def edit
    @event.event_scale_openings.build if @event.event_scale_openings.count == 0
  end

  # POST /events
  # POST /events.json
  def create

    #not scoped
    #every transaction needs to be wrapped in a begin
    #for now we catch all errors indiscriminately but as we learn what errors are triggered where, we can decide what to
    # for each error in particular
    begin
      Event.transaction do

        params_clean = event_params
        params_clean[:start_date] = Chronic.parse(params_clean[:start_date])
        params_clean[:end_date] = Chronic.parse(params_clean[:end_date])

        @event = Event.new(params_clean.except(:participation_types))
        authorize! :manage, @event
        @event.save!

        # add the participation types
        selected_types = ParticipationType.where('id IN (?)',params_clean[:participation_types])
        types_to_add = selected_types - @event.event_participation_types.collect { |e| e.participation_type }.flatten
        types_to_add.each {|t| EventParticipationType.create(event: @event, participation_type: t)}

      end # end transaction
    rescue ActiveRecord::RecordNotUnique  => error
      puts 'An Error Has Occured'
      puts error
      status = :unprocessable_entity
      flash[:error] = 'This event name is already in use for this year.'
    rescue => error
      puts 'An Error Has Occurred'
      puts error
      puts error.backtrace
      status = :unprocessable_entity
      flash[:error] = 'There was an error updating this event. Contact a system administrator.'

    else
      status = :created
    ensure
      respond_to do |format|
        if status != :created
          format.html { render action: "new" }
          format.json { render json: error, status: :unprocessable_entity }
          format.js
        else
          path = event_path(@event.id).html_safe.gsub("'", "\\\\'")
          format.html { redirect_to @event, notice: 'Event was successfully created.' }
          format.json { render json: @event, status: status, location: @event }
          format.js { render js: "window.location = '#{ path }';"}
        end
      end
    end

  end



  def clone
    @event = Event.find(params[:event_id])
    @new_event = clone_event(@event)
    respond_to do |format|
      if @new_event.id.nil?
        format.html { render action: "edit" }
        format.json { render json: @new_event.errors, status: :unprocessable_entity }
        format.js
      else
        format.html { redirect_to edit_event_path(@new_event), notice: 'Event was successfully cloned.' }
        format.json { render json: @new_event, status: :created, location: @new_event }
        format.js
      end
    end
  end

  # PUT /events/1
  # PUT /events/1.json
  def update
    #not scoped

    begin
      Event.transaction do

        params_clean = event_params
        params_clean[:start_date] = Chronic.parse(params_clean[:start_date])
        params_clean[:end_date] = Chronic.parse(params_clean[:end_date])

        @event = Event.find(params[:id])

        @event.update(params_clean.except(:id, :participation_types))
        @event.save!

        # find all participation types that were de-selected
        if params_clean[:participation_types].count > 0
          @event.event_participation_types.where('participation_type_id NOT IN (?)', params_clean[:participation_types]).destroy_all
        end

        # add the participation types
        selected_types = ParticipationType.where('id IN (?)',params_clean[:participation_types])
        types_to_add = selected_types - @event.event_participation_types.collect { |e| e.participation_type }.flatten
        types_to_add.each {|t| EventParticipationType.create(event: @event, participation_type: t)}

      end # end transaction
    rescue ActiveRecord::RecordNotUnique  => error
      puts 'An Error Has Occured 1'
      puts error
      status = :unprocessable_entity
      flash[:error] = 'This event name is already in use for this year.'
    rescue => error
      puts 'An Error Has Occurred'
      puts error
      status = :unprocessable_entity
      flash[:error] = 'There was an error updating this event. Contact a system administrator.'
    else
      status = :ok
    ensure
      respond_to do |format|
        if status != :ok
          format.html { render action: "edit" }
          format.json { render json: error, status: status }
          format.js
        else
          path = event_path.html_safe.gsub("'", "\\\\'")
          format.html { redirect_to @event, notice: 'Event was successfully updated.' }
          format.json { render json: @event, status: status, location: @event }
          format.js { render js: "window.location = '#{ path }';"}
        end
      end
    end

  end

  # DELETE /events/1
  # DELETE /events/1.json
  #TODO this does not cascade so the categories and what not are left in the system
  def destroy
    #not scoped
    authorize! :destroy, @event

    if current_user.has_role? :admin
      @event.destroy
    end

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
      format.js
    end
  end

  def import
    begin
      Event.transaction do
        # If the file given is not nil, and it has a .xlsx extension, import the data from that file
        tmp = params[:file].tempfile
        file = File.join("public", params[:file].original_filename)
        FileUtils.cp tmp.path, file
        @teams = read_team_spreadsheet(file)
        FileUtils.rm file

        # # See if any of the participants/captains in the spreadsheet may already be in the database
        # @matches = Hash.new
        # @teams.each do |team|
        #   # For each captain, look for a participant with the same name
        #   unless team.captain.first_name == 'TBD'
        #     find_match(team.captain.last_name, team.captain.first_name)
        #   end
        #   # For each participant, search for an existing participant with the same name.
        #   team.participants.each do |participant|
        #     unless participant.first_name == 'TBD'
        #       find_match(participant.last_name, participant.first_name)
        #     end
        #   end
        # end
      end
    rescue => error
      puts 'An Error Has Occurred'
      puts error
      status = :unprocessable_entity
      flash[:error] = 'There was an error updating this event. Contact a system administrator.'
    else
      status = :ok
    ensure
      respond_to do |format|
        if status != :ok
          format.html { render action: "show" }
          format.json { render json: error, status: :unprocessable_entity }
          format.js
        else
          format.html {
            redirect_to teams_path(event_slug: @event.slug)
          }
        end
      end
    end
  end

  ########################
  ## PUBLIC FACING PAGES
  # GET /:event_slug


  private

  # Use callbacks to share common setup or constraints between actions.
  def get_event
    # use the sug instead of the id
    @event = Event.where(:id => params[:id]).first
    @ability_scope = @event
  end

  def get_event_by_id
    @event = Event.find(params[:event_id])
    @ability_scope = @event
  end

  def get_event_by_slug
    @event = Event.where('slug ILIKE ?', params[:event_slug]).first
    if @event.nil?
      raise 'Event not found.'
    end
    @ability_scope = @event
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:id, :name, :start_date, :end_date, :timezone, :icon, :background, :badge_template, :ballot_template, :banner_color, :leaderboard_background, :category_text_color, :category_background_color, :participation_types => [])
  end

  # Method to read team information from a spreadsheet
  def read_team_spreadsheet(file)
    teams = []
    spreadsheet = Roo::Excelx.new(file)
    header = spreadsheet.row(1)

    ActiveRecord::Base.transaction do
      2.upto(spreadsheet.last_row) do |line|
        row = Hash[[header, spreadsheet.row(line)].transpose]

        teams  << create_team(row, @event)
      end # end of row iteration
    end # end of transaction

    teams
  end

  # # Helper method to find participants who already exist in the database
  # # TODO: Instead of putting IDs into the dictionary, put the participants' participant_number. Using IDs as placeholders as test database has no participant numbers.
  # def find_match(last_name, first_name)
  #   matches = Participant.where(:last_name => last_name, :first_name => first_name)
  #   unless matches.empty?
  #     matches.each do |match|
  #       street = match.street.nil? ? 'No street data' : match.street
  #       city = match.city.nil? ? 'No city data' : match.city
  #       zipcode = match.zipcode.nil? ? 'No zip code' : match.zipcode
  #       # If the match's last name is not a key in the dictionary, make it a key and add the match's first name as its value
  #       if @matches[match.last_name].nil?
  #         @matches[match.last_name] = Hash.new
  #         @matches[match.last_name][match.first_name] = []
  #         @matches[match.last_name][match.first_name] << match.first_name + ' ' + match.last_name + ' - ' + street + ', ' + city + ', ' + zipcode + ', ID: ' + match.id.to_s
  #       elsif @matches[match.last_name][match.first_name].nil?
  #         @matches[match.last_name][match.first_name] = []
  #         @matches[match.last_name][match.first_name] << match.first_name + ' ' + match.last_name + ' - ' + street + ', ' + city + ', ' + zipcode + ', ID: ' + match.id.to_s
  #       # Otherwise, add the match's first name to the values at that key
  #       else
  #         @matches[match.last_name][match.first_name] << match.first_name + ' ' + match.last_name + ' - ' + street + ', ' + city + ', ' + zipcode + ', ID: ' + match.id.to_s
  #       end
  #     end
  #   end
  # end

end
