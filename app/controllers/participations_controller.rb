class ParticipationsController < ApplicationController
  include EventSluggable

  autocomplete :participation, :capture, :display_value => :autocomplete_capture_display, :selected_value => :participant_full_name, extra_data: [:participant_id], :cascade => true, :search_action => :autocomplete_participants
  autocomplete :participation, :form, :display_value => :autocomplete_form_display, :selected_value => :participant_first_name , :cascade => true, :search_action => :autocomplete_participants_on_names

  before_filter :set_participation, only: [:get_info]

  def get_info
    json = Jbuilder.new do |json|
      json.id @participation.id
      json.participant @participation.participant
    end

    render :json => json.target!
  end

  private

  def set_participation
    @participation = Participation.find(params[:id])
  end
end
