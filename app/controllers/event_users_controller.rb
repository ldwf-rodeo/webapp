class EventUsersController < ApplicationController

  before_action :get_event
  before_action :get_user, :only => [:show, :edit, :destroy, :update]

  def index
    authorize! :show, @event
    # Don't show administrators here so event admins won't think they have to add us.
    @users = User.all.order('user_name ASC').reject{|u| u.has_role?('admin') }
  end

  def edit
    authorize! :edit, @event
  end

  def update
    authorize! :edit, @event
    ps = event_users_params
    ps[:role_ids] ||= []
    @user.event_users.each {|eu| if !ps[:role_ids].include?(eu.role_id) && eu.event == @event then eu.destroy end }
    ps[:role_ids].each do |role_id|
      role = Role.find(role_id)
      if @user.event_users.where(:role => role, :event => @event).first.nil?
        @user.event_users << EventUser.create!(:user => @user, :event => @event, :role => role)
      end
    end

    respond_to do |format|
      format.html { redirect_to event_users_path(:event_id => @event.id), notice: 'User was successfully updated.' }
      format.json { head :no_content }
      format.js
    end
  end

  private

  def get_event
    @event = Event.where(:id => params[:event_id].to_i).first
    if @event.nil?
      raise ActionController::RoutingError.new('Could not find event.')
    end
    @ability_scope = @event
  end

  def get_user
    @user = User.where(:id => params[:id].to_i).first
    if @user.nil?
      raise ActionController::RoutingError.new('Could not find user.')
    end
  end

  def event_users_params
    params.require(:event_user).permit(:role_ids => [])
  end



end