class TeamsController < ApplicationController

  include ERB::Util
  include EventSluggable

  before_action :get_team, only: [:show, :edit, :update, :destroy, :badges, :check_in, :check_out]
  before_action :get_captain, only: [:show, :edit]
  before_action :get_team_by_slug, only: [:leaderboard, :captain_edit_confirmation]

  skip_before_action :authenticate_user!, only: [:captain_edit_confirmation, :leaderboard, :captain_edit_commit, :teams]

  include ActionAuthorizable

  # GET /teams
  # GET /teams.json
  def index
    @teams = @event.teams.order('number ASC NULLS FIRST, name ASC')
    @checked_in = @event.teams.where('checked_in = true').order('number ASC NULLS FIRST, name ASC')
    @not_checked_in = @event.teams.where('checked_in = false').order('number ASC NULLS FIRST, name ASC')

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # index.html.erb
      format.json { render json: @teams }
      format.xlsx {
        stuff = Axlsx::Package.new do |p|
          p.workbook.add_worksheet(name: 'Edit Team Info') do |sheet|
            sheet.add_row ['Team Name', 'Captain Name', 'Edit Team Link']
            @teams.each do |team|
              sheet.add_row [team.name, team.contact.full_name, team.token.nil? ? 'No Token ' : "#{root_url}#{team.event.slug}/teams/#{team.slug}/#{team.token.token}"]
            end
          end
        end
        begin
          temp = Tempfile.new('Edit_Team_Info.xlsx')
          stuff.serialize temp.path
          send_data stuff.to_stream.read, :filename => 'Edit_Team_Info.xlsx'
        ensure
          temp.close
          temp.unlink
        end

      }
    end
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    @path = team_path(:event_slug => @event.slug, :id => @team.id)


    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # show.html.erb
      format.json { render json: @team }
    end
  end

  # GET /teams/new
  # GET /teams/new.json
  def new
    @path = teams_path

    if params[:team_id]
      @team = Team.find(params[:team_id]).clone_for_event(@event)

    else
      @team = Team.new

      @team.name = params[:team_name]

      # need to build the first 2 participants, captain and 1 regular one
      @team.participations.build
      @team.participations.build

      @team.participations.each { |team_participant| team_participant.build_participant if team_participant.participant.nil? }

    end

    create_premium_options

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # new.html.erb
      format.json { render json: @team }
    end
  end

  # GET /teams/1/edit
  def edit
    @path = team_path(:event_slug => @event.slug, :id => @team.id)
    create_premium_options


    # raise @team.team_premium_categories.inspect

    @team.participations.build if @team.participations.count < 1
    @team.participations.each { |participation| participation.build_participant if participation.participant.nil? }

    # if the participants do not have any particpant numebrs then build one for each
    # @team.participants.each {|participant| participant.participant_numbers.build if participant.participant_numbers.empty?  }

    respond_to do |format|
      format.html {
        if request.headers['ajax-load']
          render :layout => false
        end
      } # edit.html.erb
    end
  end

  # POST /teams
  # POST /teams.json
  def create
    ret = create_helper

    respond_to do |format|
      if ret[:status] != :ok
        flash[:error] = ret[:notice]
        format.json { render json: @team.errors, status: ret[:status] }
        format.js
      else
        #TODO we may have to fix this gsub to be more inclusive at some point
        path = team_path(:event_slug => CGI.escape(@event.slug), :id => @team.id)
        puts path
        flash[:notice] = ret[:notice]
        format.html { redirect_to team_path(:event_slug => @event.slug, :id => @team.id), :notice => ret[:notice] }
        format.json { render json: @team, status: :created, location: @team }
        format.js {flash[:notice] = nil; flash[:error] = nil; render js: "window.location = '#{ path }';"}
      end
    end
  end

  # PUT /teams/1
  # PUT /teams/1.json
  def update
    result = update_helper

    respond_to do |format|
      if result[:status] == :ok
        @success = true
        path = team_path(:event_slug => CGI.escape(@event.slug), :id =>  @team.id)
        format.html { redirect_to path, notice: result[:notice] }
        format.json { head :no_content }
        format.js { render js: "window.location = '#{path}';"}
      else
        @success = false
        flash[:error] = result[:notice]
        format.html { redirect_to edit_team_path(:event_slug => @event.slug, :id =>  @team.id) }
        format.json { render json: @team.errors, status: result[:status] }
        format.js
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy

    respond_to do |format|
      format.html { redirect_to teams_url }
      format.json { head :no_content }
      format.js
    end
  end

  def remove_part(part_id)
    @team.participants.delete(Participant.find(part_id))

  end

  #print the badges for this team
  def badges

    # badges = nil

    # run the creation in a seperate thrad, to prevent the server from blocking
    # t = Thread.new do
    #   badges = BadgeModule::Badge.generate(@team.participants.order('last_name ASC, first_name ASC, id ASC'),@team.event)
    # end

    # wait for the thread to finish
    # t.join

    if @team.pending_jobs.count > 0
      redirect_to team_path(event_slug: @event.slug, id:@team.id), notice: 'There are badges that have not been generated yet'
      return
    end

    respond_to do |format|
      format.pdf{
        badges = BadgeModule::Badge.generate(@team.participants.order('last_name ASC, first_name ASC, id ASC'),@team.event)
        send_file badges.path, :filename => "#{@team.name} Badges.pdf"

        badges.close
      }
    end

  end

  # Mark this team as checked-in
  def check_in
    authorize! :check_in, @team
    @team.checked_in = true
    @team.save!
    respond_to do |format|
      format.html { redirect_to team_path(:event_slug => @event.slug, :id => @team.id) }
    end
  end

  # Mark this team as not checked-in
  def check_out
    authorize! :check_in, @team
    @team.checked_in = false
    @team.save!
    respond_to do |format|
      format.html { redirect_to team_path(:event_slug => @event.slug, :id => @team.id) }
    end
  end

  def team_lookup

    respond_to do |format|
      format.html
      format.js {

        # if not name is given, then do no work and return empty
        if params[:name].blank?
          @teams = []

        else
          @teams = Team.get_teams_as_json_with_name params[:name], { event_name: @event.name[0..2] }

        end
      }
    end
  end

  ###############################
  ## PUBLIC FACING ACTIONS ##
  ###############################

  # GET /:event_id/teams/:team_slug
  def leaderboard
    @super_categories = SuperCategory.where(:event_id => @event.id)
    render :layout => 'leaderboard'
  end

  # GET /:event_id/teams/registration
  def registration
    @non_user_edit = true
    @path = registration_path
    @team = Team.new
    @team.captain = Participant.new
    @team.vessels.build


    respond_to do |format|
      format.html { render :layout => 'event_home' } # registration.html.erb
    end
  end

  def registration_create

    #@team = Team.new  Don't think we need this for registration

    @capcha_success = false
    if verify_recaptcha
      @capcha_success = true
      create_helper
    end

    respond_to do |format|
      if @team.id.nil?
        format.html { redirect_to registration_path(:event_slug => @event.slug, :id => @team.id) }
        format.json { render json: @team.errors, status: :unprocessable_entity }
        format.js
      else
        format.html { redirect_to team_path(:event_slug => @event.slug, :id => @team.id), notice: 'Team was successfully created.' }
        format.json { render json: @team, status: :created, location: @team }
        format.js
      end
    end
  end

  def teams
    @teams = @event.teams.order('number ASC NULLS FIRST, name ASC')

    respond_to do |format|
      format.html {
        render :layout => 'event_home'
      } # index.html.erb
      format.json { render json: @teams }
    end
  end

  # GET /:event_slug/teams/:team_slug/:token
  def captain_edit
    @non_user_edit = true
    @team = Token.where(:token => params[:token]).first.tokenable

    @team.participations.build if @team.participations.count < 2
    @team.participations.each { |team_participant| team_participant.build_participant if team_participant.participant.nil? }

    @path = captain_edit_path


    respond_to do |format|
      format.html {

        if request.headers['ajax-load']
          render :layout => false
        end

        render :layout => 'captain_edit'

      } # edit.html.erb
    end

  end

  def captain_edit_commit
    @team = Token.where(:token => params[:token]).first.tokenable


    result = nil
    @captcha_success = verify_recaptcha ? true : false
    if @captcha_success
      result = update_helper
    end

     flash[:error] = nil; flash[:notice] = nil;
    respond_to do |format|
      @success = (@captcha_success and result != nil and result[:status] == :ok)
      if @success
        path = captain_edit_confirmation_dos_path(:event_slug => CGI.escape(@event.slug), :team_slug =>  CGI.escape(@team.slug))
        format.html { redirect_to path, notice: result[:notice] }
        format.json
        format.js {  render js: "window.location = '#{path}';"}
      else
        if !@captcha_success
          puts 'INVALID CAPTCHA FOUND'
          flash[:error] = "Invalid captcha input. Please try again."
        else
          flash[:error] = result == nil ? 'Invalid input. Please try again.' : result[:notice]
        end
        path = captain_edit_path(:event_slug => CGI.escape(@event.slug), :team_slug => CGI.escape(@team.slug), :token => @team.token.token)
        format.html { redirect_to path }
        format.json { render json: @team.errors, status: result[:status] }
        format.js
      end
    end

  end

  def captain_edit_confirmation
    respond_to do |format|
      format.html {
        render :layout => 'captain_edit'
      }
    end
  end

  def check_badge_status
    teams = Team.get_teams_with_badge_ready_state(params[:ids])

    data = {
        ready: teams.select {|t| t['ready'] }.collect {|t| t.id}.flatten,
        not_ready: teams.reject {|t| t['ready'] }.collect {|t| t.id}.flatten
    }

    render json: data
  end

  private

  def get_team
    @team = @event.teams.find(params[:id] || params[:team_id])
  end

  def get_team_by_slug
    @team = @event.teams.where(:slug => params[:team_slug], :event => @event).first
  end

  def get_captain
    @captain = @event.participants.find(@team.captain_id)
  end

  def create_premium_options
    # product_options
    @event.premium_category_groupings.each do |grouping|
      unless @team.premium_category_groupings.include?(grouping)
        @team.team_premium_category_groupings.build(premium_category_grouping: grouping)
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def team_params
    params.require(:team).permit(
        :id, :company, :name, :number, :notes, :does_receive_notifications, :event_id,
        :team_premium_category_groupings_attributes => [:_destroy, :id, :premium_category_grouping_id],
        :participations_attributes =>[ :id, :event_participation_type_id, :_destroy,
                                       :participant_attributes => [:id, :age, :city, :email, :first_name, :home_phone, :last_name, :mobile_carrier_id, :mobile_phone, :shirt_size_id, :street, :zipcode, :gender_id, :date_of_birth, :does_receive_notifications, :is_captain]
        ]
    )
  end

  def create_helper
    ret = {}

    begin
      # wrap all the DB changes in a transaction
      Team.transaction do

        clean_params = team_params

        # create the basic info for the team. We will make that participants in the next step.
        @team = Team.create!(clean_params.except(:participations_attributes))

        # create all the indicated participants
        clean_params[:participations_attributes].each do |key, team_participant_param|
            # save the is_captain field

            is_captain = team_participant_param[:participant_attributes][:is_captain]

            # delete the key since it is no longer needed
            team_participant_param[:participant_attributes].delete(:is_captain)

            team_participant_param[:team_id] = @team.id
            team_participant_param[:event_id] = @event.id

            # create the participation record
            if team_participant_param[:participant_attributes][:id].blank?
              #The participant hasn't been created before.
              p = Participation.create!(team_participant_param.except(:_destroy))

            elsif team_participant_param[:_destroy] != '1'
              #The participant ID is set. Load in an existing participant.
              p = Participation.new(team_participant_param.except(:_destroy, :participant_attributes))
              p.participant = Participant.find(team_participant_param[:participant_attributes][:id])
              p.participant.update_attributes(team_participant_param[:participant_attributes])
              p.save!
            end

            # if this participant is indicated as the captain, then add the reference
            if is_captain == '1'
              @team.captain = p.participant
            end
        end

        @team.save!
        @event.teams << @team
      end

      @team.reload

      # todo need to figure this out later
      Participation.where(team_id: @team.id).each do |part|
        BadgeGenerationJob.perform_later(part)
      end

    rescue ActiveRecord::RecordNotUnique => error
      ret[:status] = :failed_dependency
      ret[:notice] = 'Team name already in use for this event.'
      logger.fatal 'Duplicate Team Name'
    rescue Exception=> error
      ret[:status] = :unprocessable_entity
      ret[:notice] = 'There was an error saving the team'
      logger.fatal error
    else
      ret[:status] = :ok
      ret[:notice] = 'Team was successfully created.'
    end

    ret
  end

  def update_helper
    result_hash = Hash.new

    begin
      Team.transaction do
        clean_params = team_params

        # create the basic info for the team. We will make that participants in the next step.
        @team.update_attributes!(clean_params.except(:participations_attributes))

        # update/create all the participants
        clean_params[:participations_attributes].each do |key, team_participant_param|

          team_participant_param[:team_id] = @team.id
          team_participant_param[:event_id] = @event.id

          # delete the participation, if requested
          if team_participant_param[:_destroy] == '1'
            p = Participation.find(team_participant_param[:id])
            p.destroy unless p.nil?

          # create the participant if they are new.
          elsif team_participant_param[:participant_attributes][:id].blank?
            is_captain = team_participant_param[:participant_attributes][:is_captain]
            team_participant_param[:participant_attributes].delete(:is_captain)

            # create the participation record
            p = Participation.create!(team_participant_param.except(:_destroy))

            if is_captain == '1'
              @team.captain = p.participant
            end

          elsif team_participant_param[:id].blank? && !team_participant_param[:participant_attributes][:id].blank?
            #The participant ID is set. Load in an existing participant.
            p = Participation.new(team_participant_param.except(:_destroy, :participant_attributes))
            p.participant = Participant.find(team_participant_param[:participant_attributes][:id])
            p.participant.update_attributes(team_participant_param[:participant_attributes].except(:is_captain))
            p.save!

          else # if not new, then update
            is_captain = team_participant_param[:participant_attributes][:is_captain]
            team_participant_param[:participant_attributes].delete(:is_captain)

            participation = Participation.find(team_participant_param[:id])
            participation.update_attributes(team_participant_param.except(:_destroy))

            if is_captain == '1'
              @team.captain = participation.participant
            end
          end


        end

        @team.save!

        @team.reload
        @team.participations.where(badge_file_name: nil).each {|p| BadgeGenerationJob.perform_later p }
      end

    rescue ActiveRecord::RecordNotUnique => error
      result_hash[:status] = :failed_dependency
      result_hash[:notice] = 'Team name or number already in use for this event.'
      logger.fatal 'Duplicate Team Name'

    rescue => error
      puts 'An Error Has Occurred'
      logger.fatal error
      result_hash[:updated] = false
      result_hash[:status] = :unprocessable_entity
      result_hash[:notice] = 'An error occurred, please try again.'

    else
      result_hash[:updated] = true
      result_hash[:status] = :ok
      result_hash[:notice] = 'Successfully updated team.'

    end

    result_hash
  end

end