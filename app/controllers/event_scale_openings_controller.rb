class EventScaleOpeningsController < ApplicationController

  include EventSluggable

  before_filter :get_event_scale_opening, :only => [:update, :edit, :destroy]

  include ActionAuthorizable

  def index
    @scale_openings = @event.event_scale_openings
  end

  def new
    @event_scale_opening = EventScaleOpening.new
  end

  def create
    @event_scale_opening = EventScaleOpening.new(scale_opening_params)
    @event_scale_opening.event = @event

    begin
      #There is a weird bug where .save throws an exception on certain PG index violations. Check for this.
      saved = @event_scale_opening.save
    rescue Exception => e
      @event_scale_opening.errors.add(:base,'There was an error saving the scale opening.')
    end

    if saved
      redirect_to event_scale_openings_path(:event_slug => @event.slug)
    else
      render action: 'new'
    end
  end

  def update
    begin
      #There is a weird bug where .save throws an exception on certain PG index violations. Check for this.
      saved = @event_scale_opening.update_attributes(scale_opening_params)
    rescue Exception => e
      @event_scale_opening.errors.add(:base,'There was an error saving the scale opening.')
    end

    if saved
      redirect_to event_scale_openings_path(:event_slug => @event.slug)
    else
      render action: 'edit'
    end
  end

  def edit
  end

  def destroy
    @event_scale_opening.destroy

    redirect_to event_scale_openings_path(:event_slug => @event.slug)
  end

  private

  def get_event_scale_opening
    @event_scale_opening = @event.event_scale_openings.where(:id => params[:id]).first
    if @event_scale_opening.nil?
      raise ActionController::RoutingError.new('Opening not found.')
    end
  end

  def scale_opening_params
    permitted_params = params.require(:event_scale_opening).permit(:start_time, :end_time)
    permitted_params[:start_time] = Chronic.parse(permitted_params[:start_time])
    permitted_params[:end_time] = Chronic.parse(permitted_params[:end_time])
    permitted_params
  end

end