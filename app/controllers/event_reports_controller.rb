class EventReportsController < ApplicationController
  include EventSluggable

  before_action :authenticate_user!

  include ActionAuthorizable

  def show_categories
    authorize! :show, @event
  end

  def team_categories
    authorize! :show, @event

    @teams = @event.teams.order('number ASC NULLS FIRST, name ASC')
    @premium_categories = @event.premium_category_groupings.order('name')
  end

  def results
    authorize! :show, @event

    (query = '') <<  <<-SQL
      SELECT * FROM get_event_leaderboard_display_structured_json_for_event(#{@event.id},NULL)
    SQL

    @results = JSON.parse(ActiveRecord::Base.connection.execute(query)[0]['get_event_leaderboard_display_structured_json_for_event'])
    respond_to do |format|
      format.html {render layout: 'plain'}
      format.xlsx {
        stuff = Axlsx::Package.new do |p|

          @results['category_groups'].each do |category_group|
            p.workbook.add_worksheet(name: category_group['name']) do |sheet|
              category_group['categories'].each do |category|
                sheet.add_row [category['name']]
                sheet.add_row ['','#','Name','Score','Team','Notes']
                category['rankings'].each do |ranking|
                  sheet.add_row ['',ranking['position'],ranking['entity_label'] || '(Empty)', (ranking['score_label'] ? ranking['score_label'] + ' ' + ranking['unit_label'] : ''), ranking['entity_label'] != ranking['team_name'] ? ranking['team_name'] : '', ranking['team_notes'] || '']
                end
                sheet.add_row ['']
              end

            end
          end
        end # end of new workbook

        begin
          temp = Tempfile.new("results.xlsx")
          stuff.serialize temp.path
          send_data stuff.to_stream.read, :filename => 'results.xlsx' #, :type=> "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        ensure
          temp.close
          temp.unlink
        end
      }
    end
  end
end
