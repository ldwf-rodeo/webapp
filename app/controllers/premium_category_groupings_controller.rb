class PremiumCategoryGroupingsController < ApplicationController
  include EventSluggable

  before_action :authenticate_user!
  before_action :set_premium_category_grouping, only: [:show, :edit, :update, :destroy]

  include ActionAuthorizable

  # GET /premium_category_groupings
  # GET /premium_category_groupings.json
  def index
    @premium_category_groupings = PremiumCategoryGrouping.where(event: @event)
  end

  # GET /premium_category_groupings/1
  # GET /premium_category_groupings/1.json
  def show
  end

  # GET /premium_category_groupings/new
  def new
    @premium_category_grouping = PremiumCategoryGrouping.new
  end

  # GET /premium_category_groupings/1/edit
  def edit
  end

  # POST /premium_category_groupings
  # POST /premium_category_groupings.json
  def create
    @premium_category_grouping = PremiumCategoryGrouping.new(premium_category_grouping_params)

    respond_to do |format|
      if @premium_category_grouping.save
        format.html { redirect_to premium_category_groupings_path(@event.slug), notice: 'Premium category grouping was successfully created.' }
        format.json { render action: 'show', status: :created, location: @premium_category_grouping }
      else
        format.html { render action: 'new' }
        format.json { render json: @premium_category_grouping.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /premium_category_groupings/1
  # PATCH/PUT /premium_category_groupings/1.json
  def update
    respond_to do |format|
      if @premium_category_grouping.update(premium_category_grouping_params)
        format.html { redirect_to premium_category_groupings_path(@event.slug), notice: 'Premium category grouping was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @premium_category_grouping.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /premium_category_groupings/1
  # DELETE /premium_category_groupings/1.json
  def destroy
    @premium_category_grouping.destroy
    respond_to do |format|
      format.html { redirect_to premium_category_groupings_url }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_premium_category_grouping
    @premium_category_grouping = PremiumCategoryGrouping.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def premium_category_grouping_params
    params.require(:premium_category_grouping).permit(:name, :price, :event_id)
  end

end
