class LeaderboardController < ApplicationController

  include EventSluggable
  include LeaderboardUpdateModule
  include LeaderboardHelper

  skip_before_action :authenticate_user!

  before_filter :get_category, :only => [:category]
  before_filter :get_team, :only => [:team]

  # Render each type of leaderboard based on the ID we give it.

  def index
    render 'events/leaderboard'
  end

  def category
    render 'categories/leaderboard'
  end

  def team
    render 'teams/leaderboard'
  end

  # JSON REQUESTS

  def event_leaderboard
    respond_to do |format|
      format.json {
        render :json => LeaderboardController.get_event_leaderboard_json(@event)
      }
      format.html {
        raise 'Unsupported format.'
      }
    end
  end

  def unlimited_leaderboard
    respond_to do |format|
      format.json {
        render :json => LeaderboardController.get_unlimited_leaderboard_json(@event)
      }
      format.html {
        raise 'Unsupported format.'
      }
    end
  end

  def leaderboard_at_capture
    capture = Capture.find(params[:capture_id])
    result = {
        capture_date: capture.entered_at.iso8601,
        data: JSON.parse(Leaderboard.get_event_leaderboard_at_capture_json(@event, capture))
    }
    render json: result
  end

  # WRAPPERS FOR SQL JSON FUNCTIONS

  def self.get_event_leaderboard_json(event)
    (query = '') <<  <<-SQL
      SELECT * FROM get_event_leaderboard_display_json_for_event(#{event.id},NULL)
    SQL
    return ActiveRecord::Base.connection.execute(query)[0]['get_event_leaderboard_display_json_for_event']
  end

  def self.get_unlimited_leaderboard_json(event)
    (query = '') <<  <<-SQL
      SELECT * FROM get_unlimited_leaderboard_display_json_for_event(#{event.id},NULL)
    SQL
    return ActiveRecord::Base.connection.execute(query)[0]['get_unlimited_leaderboard_display_json_for_event']
  end

  def self.get_event_leaderboard_at_capture_json(event, capture)
    (query = '') <<  <<-SQL
      SELECT * FROM get_event_leaderboard_display_json_for_event(#{event.id},'#{(capture.entered_at + (0.0001).seconds).iso8601}')
    SQL
    return ActiveRecord::Base.connection.execute(query)[0]['get_event_leaderboard_display_json_for_event']
  end

  def self.get_category_captures_json(event, category)
    (query = '') <<  <<-SQL
      SELECT * FROM get_unlimited_leaderboard_display_json_for_event_category(#{event.id}, #{category.id})
    SQL
    return ActiveRecord::Base.connection.execute(query)[0]['get_unlimited_leaderboard_display_json_for_event_category']
  end

  # WEBSOCKET

  def self.trigger_websockets(event)
    WebsocketRails['event_' + event.id.to_s + '_event_leaderboard'].trigger 'update',  LeaderboardController::get_event_leaderboard_json(event)
    WebsocketRails['event_' + event.id.to_s + '_unlimited_leaderboard'].trigger 'update',  LeaderboardController::get_unlimited_leaderboard_json(event)
  end

  private

  # BEFORE FILTERS

  def get_event
    # use the sug instead of the id
    @event = Event.where(:slug => params[:event_slug]).first
    if @event.nil?
      raise ActionController::RoutingError.new('Event not found.')
    end
  end

  def get_category
    @category = Category.where(:id => params[:id]).first
    if @category.nil? or @category.super_category.event != @event
      raise ActionController::RoutingError.new('Category not found.')
    end
  end

  def get_team
    @team = Team.where(:id => params[:id]).first
    if @team.nil? or @team.event != @event
      raise ActionController::RoutingError.new('Team not found.')
    end
  end
end
