class AdminController < ApplicationController

  def landing
    if current_user.nil?
      redirect_to new_user_session_path
    elsif cannot? :index, Event
      raise ActionController::RoutingError.new('Page not found')
    end
  end
end
