# == Schema Information
#
# Table name: super_categories
#
#  id                        :integer          not null, primary key
#  name                      :string(255)      not null
#  event_id                  :integer          not null
#  created_at                :datetime
#  updated_at                :datetime
#  page                      :integer          not null
#  position                  :integer          not null
#  category_text_color       :text
#  category_background_color :text
#

class SuperCategory < ActiveRecord::Base
  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :event

  has_many :categories, :dependent => :destroy

  validates_presence_of :page, :numericality => {:only_integer => true}
  validates_presence_of :position, :numericality => {:only_integer => true}
  
  validates_presence_of :event

  accepts_nested_attributes_for :categories, :allow_destroy => true

  def get_text_color
    return self.category_text_color || self.event.category_text_color || 'black'
  end

  def get_background_color
    return self.category_background_color || self.event.category_background_color || self.event.banner_color
  end

end
