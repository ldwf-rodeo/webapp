# == Schema Information
#
# Table name: entity_tags
#
#  id          :integer          not null, primary key
#  entity_id   :integer
#  entity_type :string
#  tag_id      :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class EntityTag < ActiveRecord::Base
  
  belongs_to :tag
  belongs_to :entity, polymorphic: true

  validates_presence_of :tag
  validates_presence_of :entity
  validates :tag, uniqueness: { scope: :entity, message: 'should be added once per entity'}

  after_destroy :delete_orphaned_tags

  private

  def delete_orphaned_tags
    tag = self.tag
    if tag.entity_tags.count == 0 && !tag.permanent
      tag.destroy
    end
  end
end
