# == Schema Information
#
# Table name: users_roles
#
#  user_id :integer
#  role_id :integer
#  id      :integer          not null, primary key
#

class UserRole < ActiveRecord::Base

  self.table_name = 'users_roles'

  belongs_to :user
  belongs_to :role

end
