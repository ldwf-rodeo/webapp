# == Schema Information
#
# Table name: categories
#
#  id                           :integer          not null, primary key
#  start_age                    :integer
#  end_age                      :integer
#  positions                    :integer          not null
#  created_at                   :datetime
#  updated_at                   :datetime
#  name                         :string(255)      not null
#  super_category_id            :integer          not null
#  score_entity                 :enum             not null
#  score_grouping               :enum             not null
#  score_grouping_limit         :integer
#  category_type                :enum             not null
#  has_distinct_places          :boolean          default(FALSE), not null
#  valid_start_time             :datetime
#  valid_end_time               :datetime
#  gender_id                    :integer
#  is_premium                   :boolean          default(FALSE), not null
#  score_1_source               :enum             not null
#  score_1_order                :enum             not null
#  score_2_source               :enum
#  score_2_order                :enum
#  score_3_source               :enum
#  score_3_order                :enum
#  display_mode                 :enum             not null
#  position                     :integer          not null
#  subtitle                     :text
#  grouping_number              :integer          default(1), not null
#  category_text_color          :text
#  category_background_color    :text
#  description                  :text
#  additional_description       :text
#  generated_description        :text
#  score_2_is_random            :boolean          default(FALSE)
#  score_3_is_random            :boolean          default(FALSE)
#  summary_url                  :string
#  premium_category_grouping_id :integer
#

class Category < ActiveRecord::Base
  include Pathable, Bitlyable, Taggable
  extend EnumType

  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :premium_category_grouping
  belongs_to :super_category
  belongs_to :gender
  has_many :teams, :through => :team_premium_categories
  has_one :event, through: :super_category

  has_many :team_premium_categories, :dependent => :destroy
  has_many :category_grouping_items, :dependent => :destroy
  has_many :category_species_points_items, :dependent => :destroy
  has_many :category_species_capture_items, :dependent => :destroy
  has_many :category_species_unit_items, :dependent => :destroy

  accepts_nested_attributes_for :category_grouping_items, :allow_destroy => true
  accepts_nested_attributes_for :category_species_unit_items, :allow_destroy => true
  accepts_nested_attributes_for :category_species_points_items, :allow_destroy => true

  validates_presence_of :name
  validates_presence_of :score_entity, :score_grouping, :category_type, :display_mode

  validates_inclusion_of :has_distinct_places, :in => [true,false]

  validates_presence_of :positions, :numericality => {:only_integer => true}
  validates_presence_of :grouping_number, :numericality => {:only_integer => true}
  validates_presence_of :position, :numericality => {:only_integer => true}

  validates_presence_of :score_1_source, :score_1_order

  validate :validate_items
  validate :validate_valid_times
  validate :validate_ages
  validate :validate_score_options

  before_save :generate_short_summary_url

  after_save :update_generated_description

  #
  # CATEGORY_TYPE describes the behavior of this category.
  #
  # species_unit - Captures of a specific species using a specific measurement. Point value defaults to measurement.
  # species_capture - Capture of a specific species. Doesn't require any measurement. Point value defaults to count.
  # species_points - Specific species are with a specific number of points. Point value determined by item points.
  # group - Placement from other categories are worth a specific number of points.
  #
  enum_type :category_type, :values => %w( species_unit species_points species_capture group )

  #
  # SCORE ENTITY describes the entity that is display for the positions
  # and also affects how scores are combines (when used with score_grouping).
  #
  # participant - The participant who made the capture.
  # team - The team whose participant made the capture.
  #
  enum_type :score_entity, :values => %w( participant team )

  #
  # SCORE GROUPING describes how individual captures/points are aggregated.
  #
  # all - All captures are combined into 1 total.
  # none - All captures are kept separate.
  # top_n - Captures are grouped in order in sets of (category_grouping_limit) items.
  # top_each - One from each category_item value is used.
  #
  enum_type :score_grouping, :values => %w( all none top_n top_each )

  #
  # SCORE_ORDER determines how point values from a SCORE SOURCE are ordered.
  #
  # asc - Placement is determined in ascending order.
  # desc - Placement is determined in descending order.
  #
  enum_type :score_1_order, :values => %w( asc desc )
  enum_type :score_2_order, :allow_nil => true, :values => %w( asc desc )
  enum_type :score_3_order, :allow_nil => true, :values => %w( asc desc )

  #
  # SCORE SOURCE determines the criteria of the capture/capture group that determines placement.
  #
  # score - The raw score (i.e. sum of measurement, species points, or group points)
  # capture_count - The number of captures aggregated into the capture grouping.
  # last_capture_at - The maximum entered_at capture in the capture grouping.
  # first_capture_at - The minimum entered_at capture in the capture grouping.
  # smallest_subscore - The smallest score within the capture grouping.
  # largest_subscore - The largest score within the capture grouping.
  #
  enum_type :score_1_source, :values => %w( score capture_count last_capture_at first_capture_at smallest_subscore largest_subscore )
  enum_type :score_2_source, :allow_nil => true, :values => %w( score capture_count last_capture_at first_capture_at smallest_subscore largest_subscore )
  enum_type :score_3_source, :allow_nil => true, :values => %w( score capture_count last_capture_at first_capture_at smallest_subscore largest_subscore )

  #
  # DISPLAY_MODE determines how the leaderboard should display the result.
  #
  # score_and_unit - Shows the score and unit (e.g. '5 lbs')
  # smallest_and_largest - Shows the largest and smallest results in two columns.
  # score_and_unit_and_count - Shows the score and unit as well as the number of captures.
  # count - Shows the capture count.
  # none - Doesn't show anything.
  #
  enum_type :display_mode, :values => %w( score_and_unit smallest_and_largest score_and_unit_and_count count none )

  SCORE_ENTITY = {
      :participant  => 'Individual',
      :team         => 'Team'
  }

  SCORE_GROUPING = {
      :all      => 'All',
      :none     => 'None',
      :top_n    => 'Top',
      :top_each => 'Top Each'
  }

  CATEGORY_TYPE = {
      :species_unit     => 'Species Units',
      :species_capture  => 'Species Capture',
      :species_points   => 'Species Points',
      :group            => 'Group'
  }

  DISPLAY_MODE = {
      :score_and_unit           => 'Score and Unit',
      :smallest_and_largest     => 'Smallest and Largest',
      :score_and_unit_and_count => 'Score and Unit and Count',
      :count                    => 'Count',
      :none                     => 'None'
  }

  SCORE_ORDER = {
      :desc   => 'Largest first',
      :asc    => 'Smallest first'
  }

  SCORE_SOURCE = {
      :score               => 'Score',
      :capture_count       => 'Capture Count',
      :last_capture_at     => 'Last Capture At',
      :first_capture_at    => 'First Capture At',
      :smallest_subscore   => 'Smallest Subscore',
      :largest_subscore    => 'Largest Subscore'
  }

  enum :score_entity,   SCORE_ENTITY
  enum :score_grouping, SCORE_GROUPING
  enum :category_type,  CATEGORY_TYPE
  enum :score_1_source, SCORE_SOURCE
  enum :score_1_order, SCORE_ORDER
  enum :score_2_source, SCORE_SOURCE
  enum :score_2_order, SCORE_ORDER
  enum :score_3_source, SCORE_SOURCE
  enum :score_3_order, SCORE_ORDER
  enum :display_mode, DISPLAY_MODE

  def items
    #Intelligently select the appropriate item type.
    case self.category_type
      when :species_unit
        return self.category_species_unit_items
      when :species_capture
        return self.category_species_capture_items
      when :species_points
        return self.category_species_points_items
      when :group
        return self.category_grouping_items
      else
        raise 'Unknown category type.'
    end
  end

  def category_item_class
    if self.category_type == :species_unit
      return CategorySpeciesUnitItem
    elsif self.category_type == :species_capture
      return CategorySpeciesCaptureItem
    elsif self.category_type == :species_points
       return CategorySpeciesPointsItem
    elsif self.category_type == :group
        return CategoryGroupingItem
    else
      raise 'Unknown category type.'
    end
  end

  def get_text_color
    return self.category_text_color || self.super_category.category_text_color || self.super_category.event.category_text_color || 'black'
  end

  def get_background_color
    return self.category_background_color || self.super_category.category_background_color || self.super_category.event.category_background_color || (self.is_premium? ? '#b5ff90' : self.super_category.event.banner_color )
  end

  #----------------
  # VALIDATION
  #----------------

  def validate_items
    #Can't have mixed-unit species unit items.
    first_item = self.category_species_unit_items.first
    self.category_species_unit_items.each do |item|
      if item.unit != first_item.unit
        self.errors[:base] << 'All category species unit items must have the same unit type.'
      end
    end
  end

  def validate_valid_times
    if !self.valid_start_time.nil? and !self.valid_start_time.nil? and self.valid_start_time > self.valid_end_time
      self.errors[:base] << 'Start time must be before end time.'
    end
  end

  def validate_ages
    if !self.start_age.nil? and self.start_age < 0
      self.errors.add(:start_age,'cannot be negative')
    end
    if !self.end_age.nil? and self.end_age < 0
      self.errors.add(:end_age,'cannot be negative')
    end
    if !self.start_age.nil? and !self.end_age.nil? and self.start_age > self.end_age
      self.errors[:base] << 'Start age must be before end age.'
    end
  end

  def validate_score_options
    if self.score_2_source.nil? != self.score_2_order.nil? or self.score_2_is_random.nil? != self.score_2_source.nil?
      self.errors[:base] << 'All score 2 options must either be set or not set.'
    end
    if self.score_3_source.nil? != self.score_3_order.nil? or self.score_3_is_random.nil? != self.score_3_source.nil?
      self.errors[:base] << 'All score 3 options must either be set or not set.'
    end
    if !self.score_3_source.nil? and self.score_2_source.nil?
      self.errors[:base] << 'Score 3 options should not be set before score 2 options.'
    end
  end

  def validate_grouping_number
    if self.grouping_number < 0
      self.errors.add(:grouping_number,'cannot be negative')
    end
  end

  #-------------------------
  # GENERATED DESCRIPTIONS
  #-------------------------

  def get_description
    if !self.description.nil?
      #We have a non-generated description. Use it instead.
      description = self.description
      if !self.additional_description.nil?
        description << self.additional_description
      end
      return description.join(' ')
    elsif !self.generated_description.nil?
      #We have a cached generated description.
      return self.generated_description
    else
      #We don't have a cached generated description. Generate it now.
      self.update_generated_description
      return self.generated_description
    end

  end

  def is_premium?
    return !self.premium_category_grouping.nil?
  end

  def has_items?
    case self.category_type
      when :species_unit
        has_items = self.category_species_unit_items.count > 0
      when :species_capture
        has_items = self.category_species_capture_items.count > 0
      when :species_points
        has_items = self.category_species_points_items.count > 0
      when :group
        has_items = self.category_grouping_items.count > 0
      else
        has_items = false
    end
    return has_items
  end

  def update_generated_description
    description = []
    entity_with_article = self.score_entity == :participant ? 'An individual' : 'A team'
    entity = self.score_entity == :participant ? 'angler' : 'team'
    entity_is = self.score_entity == :participant ? 'he/she is' : 'they are'

    #Are any rules configured yet?

    #What species are under consideration?
    if self.has_items?
      case self.category_type
        when :species_unit
          origin = 'qualifying capture'
          species = self.category_species_unit_items.collect{|i| i.species.name.downcase }
          unit = self.category_species_unit_items.first.unit.dimension_name.downcase
        when :species_capture
          origin = 'qualifying capture'
          species = self.category_species_capture_items.collect{|i| i.species.name.downcase }
          unit = 'captures'
        when :species_points
          origin = 'qualifying capture'
          species = self.category_species_points_items.collect{|i| "#{i.species.name.downcase} (#{i.points} pts.)"  }
          unit = 'points'
        when :group
          origin = 'placed category'
          super_categories = []
          self.category_grouping_items.collect{|g| g.category_grouping_item_super_categories.collect {|i| super_categories << ('"' + i.super_category.name + '"') }}
          super_categories = super_categories.uniq
        else
      end
    end

    if self.category_type == :group
      if self.has_items? && super_categories.count > 0
        description << "#{entity_with_article} category based on standings from the #{super_categories.to_sentence} divisions."
      else
        # Grouping wasn't setup yet.
        description << 'A category based on standings from other divisions.'
      end
    else
      if self.has_items?
        description << "#{entity_with_article} category for #{unit} of #{species.to_sentence}."
      else
        # Items aren't setup yet.
        description << 'A category based on captured species.'
      end
    end

    if self.has_items?
      case self.score_grouping
        when :none
          description << "Determined by the #{entity}'s single best #{origin}."
        when :all
          description << "Determined from all of the #{entity}'s #{origin.pluralize}."
        when :top_n
          description << "Determined from the #{entity}'s best #{self.score_grouping_limit} #{origin.pluralize}."
        when :top_each
          description << "Determined by the #{entity}'s best #{self.category_type == :group ? 'standings from distinct categories' : 'set of distinct species'}."
      end
    end

    if self.has_items?
      description << self.score_description(score_1_source,score_1_order).capitalize + ' wins.'

      if !self.score_2_source.nil? && self.score_2_source != self.score_1_source
        if self.score_2_is_random
          description << 'Ties are broken randomly.'
        else
          description << "Ties are broken by #{self.score_description(score_2_source,score_2_order)}."
        end
      end

      if !self.score_3_source.nil? && self.score_3_source != self.score_1_source && self.score_3_source != self.score_2_source && !self.score_2_is_random
        if self.score_3_is_random
          description << 'After that any ties are broken randomly.'
        else
          description << "After that by #{self.score_description(score_3_source,score_3_order)}."
        end
      end
    end

    #Premium
    if self.is_premium?
      description << "This is a premium category and only captures #{self.score_entity == :team ? 'from registered teams' : 'individuals on registered teams'} will qualify."
    end

    #Gender qualifications?
    if !self.gender.nil?
      description << "Only captures by #{self.gender.name.downcase} anglers are eligible."
    end

    #Time qualifications?
    start_time = self.valid_start_time.nil? ? nil : self.valid_start_time.in_time_zone(self.super_category.event.timezone)
    end_time = self.valid_end_time.nil? ? nil : self.valid_end_time.in_time_zone(self.super_category.event.timezone)
    if !start_time.nil? || !end_time.nil?
      if start_time.nil?
        description << "Captures must be entered at or before #{end_time.strftime('%H:%M at %A, %b %d %Z')} to qualify."
      elsif end_time.nil?
        description << "Captures must be entered at or after #{start_time.strftime('%H:%M on %A, %b %d %Z')} to qualify."
      else
        start_day = start_time.strftime('%A, %b %d')
        end_day = end_time.strftime('%A, %b %d')
        if start_day == end_day
          range = "#{start_time.strftime('%H:%M')} and #{end_time.strftime('%H:%M')} #{end_time.strftime('%Z')} on #{start_day}"
        else
          range = "#{start_time.strftime('%H:%M on %A, %b %d')} and #{end_time.strftime('%H:%M on %A, %b %d %Z')}, #{end_time.strftime('%Z')}"
        end
        description << "Captures must be entered between #{range} to qualify."
      end
    end

    #Age qualifications?
    if !self.start_age.nil? || !self.end_age.nil?
      if self.end_age.nil?
        description << "Anglers must be at least #{self.start_age} years old to qualify."
      elsif self.end_age.nil?
        description << "Anglers must be under #{self.end_age} years old to qualify."
      else
        description << "Anglers must be between #{self.start_age} and #{self.end_age} years old to qualify."
      end
    end

    #Distinct places?
    if self.positions > 1 && self.score_grouping != :all
      description << "Each #{entity} can place #{self.has_distinct_places ? ' only once' : " as many times as #{entity_is} eligible" }."
    end

    #Anything else?
    if !self.additional_description.nil?
      description << self.additional_description
    end

    self.update_column(:generated_description,description.join(' '))
  end

  def score_description(source,order)
    case source
      when :score
        if self.category_type == :species_unit
          aggregate = (self.score_grouping == :none) ? 'the capture' : 'the set of captures'
          unit = self.category_species_unit_items.first.unit
          qualifier = (order == :desc) ? unit.descending_qualifier : unit.ascending_qualifier
          if self.score_grouping != :none
            qualifier = qualifier + ' total'
          end
          return "#{aggregate} with the #{qualifier} #{unit.dimension_name}"
        elsif self.category_type == :species_capture
          return (order == :desc) ? 'most number of captures' : 'least number of captures'
        else
          return (order == :desc) ? 'most points' : 'least points'
        end
      when :last_capture_at
        modifier = (order == :desc) ? 'last' : 'earliest'
        aggregate = (self.score_grouping == :none) ? 'capture' : 'final capture in set'
        return "#{modifier} time of #{aggregate} entered"
      when :first_capture_at
        modifier = (order == :desc) ? 'last' : 'earliest'
        aggregate = (self.score_grouping == :none) ? 'capture' : 'first capture in set'
        return "#{modifier} time of #{aggregate} entered"
      when :capture_count
        modifier = (order == :desc) ? 'greatest number' : 'fewest number'
        if self.score_grouping == :top_each
          origin = (self.category_type == :group) ? 'categories' : 'species'
          action = (self.category_type == :group) ? 'placing in' : 'capturing'
          return "#{action} the #{modifier} of distinct #{origin}"
        else
          return "#{modifier} of captures"
        end
      when :smallest_subscore
        return (order == :desc) ? 'highest of the smallest' : 'lowest of the smallest'
      when :largest_subscore
        return (order == :desc) ? 'highest of the largest' : 'lowest of the largest'
    end
    return ''
  end

  private

  def generate_short_summary_url
    if !self.id.nil? && self.summary_url.nil?
      url = SERVER_HOST['path'] + paths.leaderboard_category_path(event_slug: event.slug, id: self.id)
      self.summary_url = generate_bitly_url(url)
    end
  end

end
