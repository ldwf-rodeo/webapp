# == Schema Information
#
# Table name: category_species_points_items
#
#  id          :integer          not null, primary key
#  category_id :integer          not null
#  species_id  :integer          not null
#  points      :integer          not null
#

class CategorySpeciesPointsItem < ActiveRecord::Base
  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :species
  belongs_to :category

end
