# == Schema Information
#
# Table name: units
#
#  id                   :integer          not null, primary key
#  description          :text             not null
#  abbreviation         :text             not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  dimension_name       :text             not null
#  ascending_qualifier  :text             not null
#  descending_qualifier :text             not null
#  name                 :text             not null
#

class Unit < ActiveRecord::Base
    resourcify
    has_paper_trail

    has_many :captures
    has_many :capture_measurements
end
