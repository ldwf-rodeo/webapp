# == Schema Information
#
# Table name: participants
#
#  id                         :integer          not null, primary key
#  first_name                 :string(256)      not null
#  last_name                  :string(256)      not null
#  street                     :string(256)
#  city                       :string(256)
#  zipcode                    :string(25)
#  email                      :string(256)
#  home_phone                 :string(25)
#  mobile_phone               :string(25)
#  updated_at                 :datetime         not null
#  created_at                 :datetime         not null
#  temporary                  :boolean          default(FALSE)
#  does_receive_notifications :boolean          default(FALSE), not null
#  gender_id                  :integer          not null
#  shirt_size_id              :integer
#  date_of_birth              :date
#  name_vector_index_col      :tsvector
#

class Participant < ActiveRecord::Base
  nilify_blanks
  resourcify
  has_paper_trail

  require 'geocoder'

  has_one :user
  belongs_to :gender
  belongs_to :shirt_size
  belongs_to :mobile_carrier

  has_many :captures, dependent: :destroy
  has_many :participations

  has_many :teams, :through => :participations
  has_many :events, :through => :participations
  has_many :event_participation_types, :through => :participations
  has_many :participation_types, :through => :event_participation_types

  validates_presence_of :first_name, :last_name, :does_receive_notifications, :gender
  validates_datetime :date_of_birth,
                     after: lambda { Chronic.parse('1900-01-01') },
                     after_message: 'must be after Jan 1, 1900',
                     before: :today,
                     before_message: 'must be not be after today'

  before_save :check_change
  after_save :update_vector_column

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def address
    s = Geocoder.search(self.zipcode)[0]

    "#{self.street}, #{self.city}, #{s.nil? ? "" : s.state} #{self.zipcode}" unless self.street.nil? && self.city.nil? && self.zipcode.nil?
  end

  def participation(object)
    if object.is_a? Event
      self.participations.where(event: object).first

    else if object.is_a? Team
           self.participations.where(team: object).first
         end
    end
  end

  def qr_code(size, event)
    # TODO make this an attachment

    data = Hash.new
    data[:event_url] = "/#{event.slug}/participants/get_info"
    data[:participant_id] = self.id

    "https://chart.googleapis.com/chart?cht=qr&chld=M|0&chs=#{size}x#{size}&chl=#{CGI.escape(data.to_json)}"
  end

  def self.spreadsheet_header
    [
        'Team',
        'First Name',
        'Last Name',
        'Street',
        'City',
        'Zip',
        'Email',
        'Home Phone',
        'Mobile Phone',
        'Gender',
        'Shirt Size'
    ]
  end

  def spreadsheet_row(event)
    attrs = attributes.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}.except(:created_at, :updated_at, :id, :temporary, :does_receive_notifications, :gender_id, :shirt_size_id, :mobile_carrier_id, :date_of_birth, :name_vector_index_col).values
    [self.participation(event).try(:team).try(:name)] + attrs + [self.gender.try(:name), self.shirt_size.try(:abbreviation)]
  end

  private

  def check_change
    #we only want to do the rest of the code if we have changed
    return unless self.changed.include?('first_name') || self.changed.include?('last_name')
    self.participations.reject {|t| t.event.is_over}.each{|part| BadgeGenerationJob.perform_later(part)}
  end

  def update_vector_column
    Participant.connection.execute("UPDATE participants SET name_vector_index_col = to_tsvector('english', coalesce(first_name,'') || ' ' || coalesce(last_name,'')) WHERE id = #{self.id}")
  end

end
