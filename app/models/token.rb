# == Schema Information
#
# Table name: tokens
#
#  id             :integer          not null, primary key
#  token          :string(255)
#  expiration     :datetime
#  tokenable_id   :integer
#  tokenable_type :string(255)
#

class Token < ActiveRecord::Base
  resourcify
  nilify_blanks

  belongs_to :tokenable, :polymorphic => true
end
