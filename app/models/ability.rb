class Ability

  include CanCan::Ability

  CAPTURE_ENTERERS = [Role.organizer,Role.capture_helper]
  TEAM_ENTERERS = [Role.organizer,Role.registration_helper]

  def initialize(user,scope=nil)

    alias_action :index, :new, :create, :to => :class_crud
    alias_action :show, :edit, :update, :destroy, :to => :instance_crud

    #Are we able to scope by event?
    event = (!scope.nil? && scope.kind_of?(Event)) ? scope : nil
    has_event = !event.nil?

    cannot :manage, :all #Start by blacklisting everything.

    if user.nil? or user.id.nil?

      can :write, Team #Guests can write to Teams using the captain edit page.

    elsif user.has_role?(:admin)

      can :manage, :all #Anyone with the :admin role can do anything.

    else

      # General event actions.
      can :see_navbar, [Event] #Any user can see the admin bar now, since they should be able to log out.
      can :send_sms_messages, [Event] { |event| user.has_event_id_for_role?(event.id,Role.event_admin) }

      # Event admin for main event management things.
      can :instance_crud, [Event] { |event| user.has_event_id_for_role?(event.id, Role.event_admin) }
      can_class [:index], [Event] { user.has_role?('event_admin') || EventUser.where(:role => Role.event_admin, :user => user).count > 0}
      cannot :destroy, [Event]

      # Event admin for categories and scale openings. Can manage everything.
      can :instance_crud, [EventScaleOpening, SuperCategory, PremiumCategoryGrouping] { |t| user.has_event_id_for_role?(t.event_id,Role.event_admin) }
      can :instance_crud, [Category] { |category| user.has_event_id_for_role?(category.super_category.event_id,Role.event_admin) }
      can :instance_crud, [CategoryGroupingItem, CategorySpeciesUnitItem, CategorySpeciesPointsItem] { |item| user.has_event_id_for_role?(item.category.super_category.event_id,Role.event_admin) }
      can :instance_crud, [CategoryGroupingItemSuperCategory] { |item| user.has_event_id_for_role?(item.category_grouping_item.category.super_category.event_id,Role.event_admin) }
      can :instance_crud, [Capture, Team, Participation] { |t| user.has_event_id_for_role?(t.event_id,Role.event_admin) }
      can :check_in, [Team] { |t| user.has_event_id_for_role?(t.event_id,Role.event_admin) }
      can_class :class_crud, [SuperCategory, Category, PremiumCategoryGrouping, CategoryGroupingItem, CategoryGroupingItemSuperCategory, CategorySpeciesPointsItem, CategorySpeciesUnitItem] { has_event && user.has_event_id_for_role?(event.id,Role.event_admin) }
      can_class :class_crud, [EventScaleOpening, Team, Participation, Capture, Participant] { has_event && user.has_event_id_for_role?(event.id,Role.event_admin) }

      # Helpers for participations and teams. Can edit/add/destroy teams before the event is over and can always show/index them.
      can [:edit, :update, :destroy], [Team, Participation] { |t| !t.event.is_over && user.has_event_id_for_role_list?(t.event_id,TEAM_ENTERERS) }
      can [:check_in], [Team] { |t| !t.event.is_over && user.has_event_id_for_role_list?(t.event_id,TEAM_ENTERERS) }
      can [:show], [Team, Participation] { |t| user.has_event_id_for_role_list?(t.event_id,TEAM_ENTERERS) }
      can_class [:create, :new], [Team, Participation, Participant] { has_event && !event.is_over && user.has_event_id_for_role_list?(event.id,TEAM_ENTERERS) }
      can_class [:index], [Team, Participation, Participant] { has_event && user.has_event_id_for_role_list?(event.id,TEAM_ENTERERS) }

      # Helpers for captures. Can enter captures but not edit/delete them.
      can [:show], [Capture] { |c| user.has_event_id_for_role_list?(c.event_id,CAPTURE_ENTERERS) }
      can [:show], [CaptureMeasurement] { |c| user.has_event_id_for_role_list?(c.capture.event_id,CAPTURE_ENTERERS) }
      can_class [:create, :new], [Capture,CaptureMeasurement] { has_event && event.is_active? && user.has_event_id_for_role_list?(event.id,CAPTURE_ENTERERS) }
      can_class [:index], [Capture,CaptureMeasurement] { has_event && user.has_event_id_for_role_list?(event.id,CAPTURE_ENTERERS) } #Can index even if event isn't active.

      # Team viewer. Can view but not edit the team, but can check them in.
      can [:show], [Team, Participation] { |t| user.has_event_id_for_role?(t.event_id,Role.team_viewer) }
      can [:show], [Participant] { |p| part = p.participation(event); has_event && !part.nil? && user.has_event_id_for_role?(part.event_id,Role.team_viewer) }
      can :check_in, [Team] {|t| !t.event.is_over && user.has_event_id_for_role?(t.event_id,Role.team_viewer) }
      can_class [:index], [Team, Participation, Participant] { has_event && user.has_event_id_for_role?(event.id,Role.team_viewer) }

    end

  end

end