# == Schema Information
#
# Table name: team_premium_category_groupings
#
#  id                           :integer          not null, primary key
#  team_id                      :integer          not null
#  premium_category_grouping_id :integer          not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class TeamPremiumCategoryGrouping < ActiveRecord::Base
  belongs_to :team
  belongs_to :premium_category_grouping

  validates_uniqueness_of :team, scope: [:premium_category_grouping]
  validates_presence_of :team
  validates_presence_of :premium_category_grouping
end
