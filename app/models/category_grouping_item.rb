# == Schema Information
#
# Table name: category_grouping_items
#
#  id          :integer          not null, primary key
#  category_id :integer          not null
#  place       :integer          not null
#  points      :integer          not null
#

class CategoryGroupingItem < ActiveRecord::Base
  resourcify
  has_paper_trail

  belongs_to :category

  has_many :category_grouping_item_super_categories, dependent: :destroy
  has_many :super_categories, :through => :category_grouping_item_super_categories

  accepts_nested_attributes_for :category_grouping_item_super_categories, :allow_destroy => true

end
