# == Schema Information
#
# Table name: species
#
#  id              :integer          not null, primary key
#  name            :string(256)      not null
#  quantity        :integer          default(1), not null
#  base_species_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Species < ActiveRecord::Base

  nilify_blanks
  resourcify
  has_paper_trail

  has_many :captures
  belongs_to :base_species, :foreign_key => 'base_species_id', :class_name => 'Species'

  validate :check_base_species

  def is_stringer?
    return self.quantity > 1
  end

  def check_base_species
    if self.quantity > 1 && self.base_species_id.nil?
      self.errors.add(:base_species_id,'cannot be null for quantity greater than 1')
    end

    if !self.base_species_id.nil?
      base = Species.find(self.base_species_id)
      if base.quantity != 1 or !base.base_species_id.nil?
        # The base species cannot have its own base species and its quantity must be 1.
        self.errors.add(:base_species_id,'must point to a single species')
      end
    end
  end

end
