# == Schema Information
#
# Table name: participations
#
#  id                          :integer          not null, primary key
#  team_id                     :integer          not null
#  event_id                    :integer          not null
#  participant_id              :integer          not null
#  number                      :integer          not null
#  event_participation_type_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  badge_file_name             :string
#  badge_content_type          :string
#  badge_file_size             :integer
#  badge_updated_at            :datetime
#

class Participation < ActiveRecord::Base
  resourcify
  has_paper_trail

  #ASSOCIATIONS
  belongs_to :team
  belongs_to :event
  belongs_to :participant
  belongs_to :event_participation_type

  has_many :pending_jobs, as: :source

  # VALIDATIONS
  validates_presence_of :team,
                        :event,
                        :participant,
                        :number,
                        :event_participation_type

  validates :number, numericality: { greater_than: 0 }
  validates :event, uniqueness: { scope: [:event_participation_type, :number] }
  validates :event, uniqueness: { scope: [:participation] }
  validates :team, uniqueness:  { scope: [:participation] }

  has_attached_file :badge
  validates_attachment_content_type :badge, :content_type => ["application/pdf"]

  before_save :update_number

  before_destroy :recycle_number

  accepts_nested_attributes_for :participant

  def display_number
    "#{self.event_participation_type.number_prefix}#{ self.number.to_s.rjust(self.event_participation_type.number_padding, '0') }#{self.event_participation_type.number_suffix}"
  end

  def is_captain
    (self.participant.id == self.team.captain_id)
  end

  def age_at_event_start
    puts self.participant.inspect
    if self.participant.date_of_birth.nil?
      return 'Unknown'
    end
    end_date = self.event.start_date.to_date
    start_date = self.participant.date_of_birth
    return (end_date.year - start_date.year) - ((end_date.month > start_date.month || (end_date.month == start_date.month && end_date.day >= start_date.day)) ? 0 : 1)
  end


  def generate_badge

    #give the badge a maximum of 5 seconds to update the needed qr files
    6.times do |i|
      if i == 5
        #if we didn't finish we need to raise some sort of error
        raise 'Butts'
      end
      if File.exists?(self.team.qr_code.path) && File.exists?(self.event.qr_code.path)
        break
      else
        sleep(1.seconds)
        self.team.reload
        self.event.reload
      end
    end

    template_path = self.event.badge_template.path.nil? ? "#{Rails.root}/stuff/badges/badge_template.otg" : self.event.badge_template.path

    #we have to create a badge for each participant and that is what the temp report is for
    temp_report = Tempfile.new(['badge','.otg'])
    temp_pdf = Tempfile.new(['badge','.pdf'])


    # if the event has its own template, use that otherwise use the default one
    report = ODFReport::Report.new(template_path) do |r|
      r.add_field :participant, "#{self.participant.first_name} #{self.participant.last_name}"
      r.add_field :participant_number, self.display_number
      r.add_field :team, self.team.name
      r.add_field :company, self.team.company
      r.add_field :event_name, self.event.name
      #TODO This needs to be like a string computation of the start date end date
      #r.add_field :event_date, self.participant.event_date
      r.add_field :team_link, self.team.summary_short_url
      r.add_field :event_link, self.event.short_leaderboard_url

      # update the images
      r.add_image :team_qrcode , self.team.qr_code.path
      r.add_image :event_qrcode , self.event.qr_code.path
      r.add_image :logo , self.event.icon.path unless self.event.icon.path.nil?
    end

    report.generate(temp_report.path)
    `#{LIBREOFFICE_BINARY["path"]} -o #{temp_pdf.path} #{temp_report.path}`

    begin
      self.badge = File.open(temp_pdf.path)
      self.save
    ensure
      temp_report.close
      temp_report.unlink # cleanup the temp file...
      temp_pdf.close
      temp_pdf.unlink
    end
  end

  def autocomplete_capture_display
    participant = self.participant
    team = self.team
    is_captain = team.captain_id == participant.id

    return (number == nil ? '' : "##{self.display_number}: ") + participant.full_name + (is_captain ? ' [Captain]' : '') + ' / ' + "#{team.name}#{team.number == nil ? '' : (' (#' + team.number.to_s + ')')}"
  end

  def autocomplete_form_display
    participant = self.participant
    phone_number = participant.mobile_phone.nil? ? '' : " [#{participant.mobile_phone}]"
    "#{participant.full_name} #{phone_number}- #{team.name} (#{event.name})"
  end

  def participant_first_name
    self.participant.first_name
  end

  def participant_full_name
    self.participant.full_name
  end

  def self.autocomplete_participants(parameters)
    event_id  = parameters[:extra][:event_id].blank? ? nil : Event.where(:id => parameters[:extra][:event_id]).first.try(:id)
    term      = parameters[:term].strip
    options   = parameters[:options]
    limit     = parameters[:limit] || 15

    binds = []
    where_clause = []
    template_where = '(participants.first_name ~* ? OR participants.last_name ~* ? OR concat(event_participation_types.number_prefix,smart_pad(cast(participations.number as text),event_participation_types.number_padding,\'0\'),event_participation_types.number_suffix) ~* ? OR teams.name ~* ? OR cast(teams.number as text) ~* ?)'

    if options[:cascade]

      term.split(/\s+/).each do |inner_term|
        where_clause << template_where
        binds = binds + ( [inner_term] * 5)
      end

    else
      where_clause << template_where
      binds = binds + ([term] * 5)
    end

    where_clause = where_clause.join(' AND ') + ' AND participations.event_id = COALESCE(?, participations.event_id)'
    where_combined = [where_clause, binds.flatten, event_id].flatten

    Participation.joins(:participant, :team, :event_participation_type).where(where_combined).limit(limit)
  end

  def self.autocomplete_participants_on_names(parameters)
    term      = parameters[:term].strip
    options   = parameters[:options]
    limit     = parameters[:limit] || 15

    terms = [term]
    terms = term.split(/\s+/).reject{|t| t.blank?} if options[:cascade]
    terms = ActiveRecord::Base.sanitize(terms.map{|t| "#{t}:*" }.join('|'))

    Participation
        .joins("INNER JOIN participants ON participants.id = participations.participant_id, to_tsquery(#{terms}) query")
        .where('query @@ name_vector_index_col')
        .select('participations.*, ts_rank_cd(name_vector_index_col, query, 32) AS rank')
        .order('rank DESC')
        .limit(limit)
  end

  private

  def update_number

    raise self.inspect if self.event_participation_type.nil?

    # if the this is new Participation, ask for a new number
    if self.number.nil?
      self.number = self.event_participation_type.generate_number
    # if this is an existing participation and the type was changed,
    # recycle the old number and ask for a new one.
    elsif self.event_participation_type_id_changed?
      #Find the old event participation type and recycle the number being used.
      EventParticipationType.find(self.event_participation_type_id_was).recycle_number(self.number)
      #Now request a new number using the current event participation type.
      self.number = self.event_participation_type.generate_number
    end
  end

  def recycle_number
    self.event_participation_type.recycle_number(self.number)
  end

end
