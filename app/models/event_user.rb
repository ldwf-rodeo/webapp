# == Schema Information
#
# Table name: events_users
#
#  event_id   :integer          not null
#  user_id    :integer          not null
#  role_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  id         :integer          not null, primary key
#

class EventUser < ActiveRecord::Base

  resourcify
  has_paper_trail

  self.table_name = 'events_users'

  belongs_to :event
  belongs_to :user
  belongs_to :role

  validate :check_not_admin

  def check_not_admin
    if self.role == Role.admin
      self.errors.add(:role,'cannot be an admin')
    end
  end

end
