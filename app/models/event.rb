# == Schema Information
#
# Table name: events
#
#  id                                  :integer          not null, primary key
#  name                                :string(256)      not null
#  start_date                          :datetime         not null
#  end_date                            :datetime         not null
#  updated_at                          :datetime         not null
#  created_at                          :datetime         not null
#  slug                                :string(255)
#  icon_file_name                      :string(255)
#  icon_content_type                   :string(255)
#  icon_file_size                      :integer
#  icon_updated_at                     :datetime
#  background_file_name                :string(255)
#  background_content_type             :string(255)
#  background_file_size                :integer
#  background_updated_at               :datetime
#  short_leaderboard_url               :string(255)
#  badge_template_file_name            :string(255)
#  badge_template_content_type         :string(255)
#  badge_template_file_size            :integer
#  badge_template_updated_at           :datetime
#  ballot_template_file_name           :string(255)
#  ballot_template_content_type        :string(255)
#  ballot_template_file_size           :integer
#  ballot_template_updated_at          :datetime
#  leaderboard_background_file_name    :string(255)
#  leaderboard_background_content_type :string(255)
#  leaderboard_background_file_size    :integer
#  leaderboard_background_updated_at   :datetime
#  banner_color                        :string(255)      default("#7A5229")
#  twilio_phone                        :string(255)
#  twilio_expired                      :boolean          default(FALSE)
#  category_text_color                 :text
#  category_background_color           :text
#  timezone                            :string(255)      default("Central Time (US & Canada)"), not null
#  qr_code_file_name                   :string
#  qr_code_content_type                :string
#  qr_code_file_size                   :integer
#  qr_code_updated_at                  :datetime
#

class Event < ActiveRecord::Base
  include Bitlyable, Pathable

  nilify_blanks
  resourcify
  has_paper_trail

  ##################################################
  ## Related models
  ##################################################

  # each event has a set of captures
  has_many :captures, :dependent => :destroy
  has_many :teams, :dependent => :destroy

  has_many :participants, :through => :participations

  has_many :sms_participants,
            -> { where('does_receive_notifications = TRUE').select('distinct on (mobile_phone) participants.*') },
           :through => :participations,
           :class_name => 'Participant',
           :foreign_key => 'participant_id',
           :source => :participant

  has_many :participations, :dependent => :destroy
  has_many :team_captains, -> { uniq },
           :through => :teams,
           :class_name => 'Participant',
           :foreign_key => 'captain_id',
           :source => :captain


  has_many :event_scale_openings, dependent: :destroy
  has_many :event_participation_types, :dependent => :destroy

  has_many :super_categories, :dependent => :destroy
  has_many :categories, through: :super_categories

  has_many :premium_category_groupings
  has_many :premium_categories, through: :premium_category_groupings, source: :categories

  accepts_nested_attributes_for :event_participation_types

  has_many :event_users
  has_many :users, -> { uniq }, :through => :event_users
  has_many :roles, -> { uniq }, :through => :event_users

  ##################################################
  ## Attachments
  ##################################################

  has_attached_file :icon
  validates_attachment_content_type :icon, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"], :default_url => "/images/:style/missing.png"

  has_attached_file :background
  validates_attachment_content_type :background, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  has_attached_file :leaderboard_background
  validates_attachment_content_type :leaderboard_background, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  has_attached_file :badge_template
  validates_attachment_content_type :badge_template, :content_type => ['application/vnd.oasis.opendocument.graphics-template']

  has_attached_file :ballot_template
  validates_attachment_content_type :ballot_template, :content_type => ['application/vnd.oasis.opendocument.graphics-template']

  has_attached_file :qr_code
  validates_attachment_content_type :qr_code, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


  ##################################################
  ## Validations
  ##################################################

  validates_uniqueness_of :slug, :case_sensitive => false

  ##################################################
  ## Callbacks
  ##################################################

  before_save :create_slug,
                if: Proc.new { |event| event.slug.nil? }

  before_save :create_leaderboard_url

  before_save :check_slug_change
  before_save :check_change

  def has_premium_categories?
    self.premium_categories.count > 0
  end

  # Method to determine if an event has ended. Used for SMS messaging.
  def is_over
    Time.now > self.end_date
  end

  def is_active?
    Time.now >= self.start_date && Time.now <= self.end_date
  end

  def get_species
    (query = '') << <<-SQL
      SELECT
        species.*
      FROM
        event_all_species_view
        INNER JOIN species ON species.id = event_all_species_view.species_id
      WHERE
        event_all_species_view.event_id = #{self.id}
    SQL
    Species.find_by_sql(query)
  end

  def units
    (query = '') << <<-SQL
      SELECT
        units.*
      FROM
        event_all_units_view
        INNER JOIN units ON units.id = event_all_units_view.unit_id
      WHERE
        event_all_units_view.event_id = #{self.id}
    SQL
    Unit.find_by_sql(query)
  end

  def facts_information
    query = <<-SQL
      SELECT
        *
      FROM
        event_facts_view
      WHERE
        event_facts_view.event_id = #{self.id}
    SQL
    ActiveRecord::Base.connection.execute(query).map {|m| r = OpenStruct.new m; r.measurements = JSON.parse(r.measurements, symbolize_names: true) unless r.measurements.nil?; r}
  end

  #generate the qr code associated with the event
  def gen_qr
    #the event qr code is not going to change so we can fetch it and store it in a temp file now
    event_qr = Tempfile.new(['event_qr','.png'])

    open(event_qr.path,'wb') do |temp_file|
      qr = RQRCode::QRCode.new( self.short_leaderboard_url, :size => 5, :level => :m )
      qr.to_img.resize(200, 200).save(temp_file.path)
    end


    begin
      self.qr_code = File.open(event_qr.path)
    ensure
      event_qr.close
      event_qr.unlink # cleanup the temp file...
    end

  end

  def self.find_by_twilio_phone(number)
    return Event.where("regexp_replace(twilio_phone,'^\\+1|\\D','') = ?", number.gsub(/^\+1|\D/,'')).first
  end

  private

  def create_slug
    if self.slug.nil?
      self.slug = self.name + ' ' + self.start_date.strftime('%Y')
    end
    #Remove redundant spaces, replace spaces with underscores, remove non-alphanumerics, and downcase.
    self.slug = self.slug.strip.gsub(/[^a-zA-Z0-9_ ]/,'').gsub(/[ ]+/,' ').gsub(/[ ]+/,'_').downcase()
  end

  def create_leaderboard_url
    if self.short_leaderboard_url.nil?
      # is there is not a short url, then use bit.ly to make one
      url = SERVER_HOST['path'] + paths.leaderboard_path(event_slug: self.slug)
      self.short_leaderboard_url = generate_bitly_url(url)
    end
  end

  def check_slug_change
    return unless self.changed.include?('slug')
    gen_qr
    self.teams.each do |team|
      team.gen_qr
      team.save
    end
  end

  def check_change
    return unless self.changed.any? { |x| %w(start_date end_date badge_template_updated_at name icon_update_at).include?(x) }

    Thread.new do
      sleep(5.seconds)

      self.participations.each do |part|
        BadgeGenerationJob.perform_later(part)
      end
    end
  end

end
