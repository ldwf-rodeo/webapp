# == Schema Information
#
# Table name: event_participation_types
#
#  id                    :integer          not null, primary key
#  event_id              :integer          not null
#  participation_type_id :integer          not null
#  number_padding        :integer          default(3), not null
#  number_prefix         :text             default(""), not null
#  number_suffix         :text             default(""), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class EventParticipationType < ActiveRecord::Base
  resourcify
  has_paper_trail

  belongs_to :event
  belongs_to :participation_type

  has_many :participations

  before_destroy :destroy_sequence

  def generate_number
    EventParticipationType.connection.execute("SELECT gapless_sequence_nextval('#{self.sequence_name}');")[0]['gapless_sequence_nextval'].to_i
  end

  def recycle_number(number)
    EventParticipationType.connection.execute("SELECT gapless_sequence_recycle('#{self.sequence_name}', #{number});")
  end

  def sequence_name
   "gapless_event_participation_types_numbers_#{self.id}"
  end

  private

  def destroy_sequence
    EventParticipationType.connection.execute("SELECT gapless_sequence_destroy('#{self.sequence_name}');");
  end

end
