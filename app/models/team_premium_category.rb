# == Schema Information
#
# Table name: team_premium_categories
#
#  id          :integer          not null, primary key
#  team_id     :integer
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class TeamPremiumCategory < ActiveRecord::Base

  resourcify
  has_paper_trail

  belongs_to :team
  belongs_to :category
  has_one :super_category, :through => :category

end
