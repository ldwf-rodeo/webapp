# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  permanent  :boolean          default(FALSE), not null
#

class Tag < ActiveRecord::Base
  has_many :entity_tags, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name, :case_sensitive => false

  before_save :strip_name
  before_destroy :check_if_permanent

  private

  def strip_name
    self.name = self.name.strip
  end

  def check_if_permanent
    # if this is not a permanent record, then return true, else false
    !self.permanent
  end

end
