# == Schema Information
#
# Table name: captures
#
#  id             :integer          not null, primary key
#  updated_at     :datetime         not null
#  created_at     :datetime         not null
#  user_id        :integer          not null
#  participant_id :integer          not null
#  species_id     :integer          not null
#  event_id       :integer          not null
#  entered_at     :datetime         not null
#  is_redundant   :boolean          default(FALSE), not null
#

class Capture < ActiveRecord::Base
  include Taggable

  resourcify
  nilify_blanks
  has_paper_trail

  belongs_to :user
  belongs_to :species
  belongs_to :participant
  belongs_to :event

  has_many :capture_measurements, :dependent => :destroy

  validate :check_not_future

  accepts_nested_attributes_for :capture_measurements, reject_if: proc { |attributes| attributes['measurement'].blank? }

  before_create :update_entered_at

  def measurements_for_spreadsheet(unit_ids = nil)
    # We need the measurements in the proper order and include null ones
    query = <<-SQL
      SELECT
        t.*
      FROM
        units
        LEFT OUTER JOIN (
                  SELECT
                    capture_measurements.*
                  FROM capture_measurements
                  WHERE
                    capture_measurements.capture_id = ?
                ) t ON units.id = t.unit_id
      WHERE
        units.id IN (?)
      ORDER BY
        units.id ASC
    SQL

    unit_ids ||= self.event.units.collect{|c| c.id}
    CaptureMeasurement.find_by_sql([query, self.id, unit_ids])
  end
  private

  def update_entered_at
    if self.entered_at.nil?
      self.entered_at = Time.now
    end
  end

  def check_not_future
    if !self.entered_at.nil? and self.entered_at.in_time_zone(@browser_timezone) > Time.now.in_time_zone(@browser_timezone)
      self.errors.add(:entered_at,'cannot be in the future')
    end
  end

end
