# == Schema Information
#
# Table name: premium_category_groupings
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  price      :float            default(0.0)
#  event_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PremiumCategoryGrouping < ActiveRecord::Base
  has_many :categories, dependent: :nullify
  belongs_to :event

  has_many :team_premium_category_groupings, dependent: :destroy
  has_many :teams, through: :team_premium_category_groupings

  validates_uniqueness_of :name, scope: [:event_id]
  validates_presence_of :name
  validates_presence_of :event_id
  validates :price, numericality: { greater_than_or_equal_to: 0 }
end
