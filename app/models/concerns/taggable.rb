module Taggable
  extend ActiveSupport::Concern

  included do
    has_many :entity_tags, as: :entity, dependent: :destroy
    has_many :tags, through: :entity_tags
  end

  #adds a tag to an entity
  def add_tag(tag)
    found_tag = get_tag(tag)
    found_tag = Tag.where(name: tag).first_or_create! unless found_tag && !tag.is_a?(String)

    EntityTag.where(entity: self, tag: found_tag).first_or_create! unless found_tag.nil?
  end

  #takes a list of tags and adds each in turn
  def add_tags(tag_list, delimiter = ',')
    search_tag_list = []

    if tag_list.is_a? String
      search_tag_list = tag_list.split(delimiter).compact.map {|t| t.strip}
    elsif tag_list.is_a? Array
      search_tag_list = tag_list
    else
      # does nothing
    end

    search_tag_list.each {|t| self.add_tag(t)}
  end

  def has_tag?(tag)
    !get_entity_tag(tag).nil?
  end

  #removes the tag if it exists
  def remove_tag(tag)
    get_entity_tag(tag).try(:destroy)
  end

  private

  #returns the entity tag if it exists or nil if it does not
  def get_entity_tag(tag)
    EntityTag.where(entity: self, tag: get_tag(tag)).first
  end

  def get_tag(tag)
    if tag.is_a? String
      found_tag = Tag.where('name ILIKE ?', tag).first
    elsif tag.is_a? Tag
      found_tag = tag
    elsif tag.is_a? Integer
      found_tag = Tag.find(tag)
    else
      return nil
    end

    found_tag
  end

end