module Bitlyable
  def generate_bitly_url(url)
    result = url

    # do not use bit.ly if the host is localhost
    unless url.include? 'localhost'
      client = Bitly.client
      url = client.shorten(url)
      result = url.short_url
    end

    result
  end
end