# == Schema Information
#
# Table name: mobile_carriers
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  slug       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MobileCarrier < ActiveRecord::Base
  resourcify
  has_paper_trail

  has_many :participants

end
