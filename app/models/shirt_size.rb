# == Schema Information
#
# Table name: shirt_sizes
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  abbreviation :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ShirtSize < ActiveRecord::Base
  resourcify
  has_paper_trail

  has_many :participants
end
