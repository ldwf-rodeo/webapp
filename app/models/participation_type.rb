# == Schema Information
#
# Table name: participation_types
#
#  id         :integer          not null, primary key
#  name       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ParticipationType < ActiveRecord::Base
  resourcify
  has_paper_trail

  has_many :event_participation_types
end
