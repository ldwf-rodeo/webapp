# == Schema Information
#
# Table name: pending_jobs
#
#  id          :integer          not null, primary key
#  target_id   :integer
#  target_type :string
#  source_id   :integer
#  source_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class PendingJob < ActiveRecord::Base
  belongs_to :target, polymorphic: true
  belongs_to :source, polymorphic: true
end
