# == Schema Information
#
# Table name: category_species_unit_items
#
#  id          :integer          not null, primary key
#  category_id :integer          not null
#  species_id  :integer          not null
#  unit_id     :integer          not null
#

class CategorySpeciesUnitItem < ActiveRecord::Base
  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :species
  belongs_to :category
  belongs_to :unit

end
