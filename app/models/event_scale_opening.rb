# == Schema Information
#
# Table name: event_scale_openings
#
#  id         :integer          not null, primary key
#  start_time :datetime         not null
#  end_time   :datetime         not null
#  created_at :datetime
#  updated_at :datetime
#  event_id   :integer          not null
#  opening    :tstzrange        not null
#

class EventScaleOpening < ActiveRecord::Base
  resourcify
  has_paper_trail

  belongs_to :event

  validates_presence_of :start_time
  validates_presence_of :end_time
  validates_presence_of :event

  validate :range_valid
  validate :range_in_event

  before_save :update_opening_range


  def update_opening_range
    self.opening = "[#{self.start_time},#{self.end_time}]"
  end

  def range_valid
    if self.start_time >= self.end_time
      self.errors[:base] << 'Start time must occur before end time.'
    end
  end

  def range_in_event

    if self.start_time < self.event.start_date
      self.errors.add(:start_time,'must be after the event start.')
    end

    if self.end_time > self.event.end_date
      self.errors.add(:end_time,'must be before the event end.')
    end

  end
end
