# == Schema Information
#
# Table name: roles
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  resource_id      :integer
#  resource_type    :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  decoration_text  :text             not null
#  decoration_color :text             not null
#  display_name     :text             not null
#  description      :text             not null
#

class Role < ActiveRecord::Base

  resourcify
  has_paper_trail

  has_many :user_roles
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  
  scopify

  def self.admin
    Role.where(name: 'admin').first
  end

  def self.event_admin
    Role.where(name: 'event_admin').first
  end

  def self.organizer
    Role.where(name: 'organizer').first
  end

  def self.capture_helper
    Role.where(name: 'capture_helper').first
  end

  def self.registration_helper
    Role.where(name: 'registration_helper').first
  end

  def self.team_viewer
    Role.where(name: 'team_viewer').first
  end

end
