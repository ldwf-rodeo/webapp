# == Schema Information
#
# Table name: category_species_capture_items
#
#  id          :integer          not null, primary key
#  category_id :integer          not null
#  species_id  :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CategorySpeciesCaptureItem < ActiveRecord::Base
  
  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :species
  belongs_to :category

end
