# == Schema Information
#
# Table name: teams
#
#  id                         :integer          not null, primary key
#  name                       :string(256)      not null
#  company                    :string(256)
#  description                :string(256)
#  updated_at                 :datetime         not null
#  created_at                 :datetime         not null
#  captain_id                 :integer
#  slug                       :string(255)
#  summary_short_url          :string(255)
#  event_id                   :integer          not null
#  contact_id                 :integer
#  does_receive_notifications :boolean          default(TRUE), not null
#  checked_in                 :boolean          default(FALSE)
#  notes                      :text
#  number                     :integer
#  qr_code_file_name          :string
#  qr_code_content_type       :string
#  qr_code_file_size          :integer
#  qr_code_updated_at         :datetime
#

class Team < ActiveRecord::Base
  include Taggable

  nilify_blanks
  resourcify
  has_paper_trail

  belongs_to :captain, :class_name => 'Participant'
  belongs_to :contact, :class_name => 'Participant'

  belongs_to :event

  # =====================
  # TODO delete this when the new premium category stuff is done.
  has_many :team_premium_categories, :dependent => :destroy

  has_many :premium_categories,
           :through => :team_premium_categories,
           :source => :category
  # =====================

  has_many :team_premium_category_groupings, dependent: :destroy
  has_many :premium_category_groupings, through: :team_premium_category_groupings

  has_many :participations, :inverse_of => :team, dependent: :destroy
  has_many :participants, :through => :participations

  has_many :captures, through: :participants

  has_many :pending_jobs, as: :target

  has_one :token, :as => :tokenable, :dependent => :destroy

  accepts_nested_attributes_for :captain
  accepts_nested_attributes_for :contact

  accepts_nested_attributes_for :participations, :allow_destroy => true
  accepts_nested_attributes_for :team_premium_category_groupings, :allow_destroy => true

  has_attached_file :qr_code
  validates_attachment_content_type :qr_code, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates_presence_of :name, :event, :does_receive_notifications
  validates_uniqueness_of :name, scope: [:event]
  validates_uniqueness_of :slug, scope: [:event]
  validates_uniqueness_of :number, scope: [:event]

  before_save :create_slug
  before_save :create_summary_url
  before_save :update_contact
  before_save :update_captain
  before_save :check_slug_change

  before_destroy :remove_captain_and_captain, prepend: true

  after_save :check_change

  after_create :gen_token

  class << self

    # Will find teams that with a similar name.
    # An options hash can also be provide to narrow the search.
    # These are the keys that the [Hash] will accept:
    # :event_name -# the name of the event, can be fuzzied
    # :event -# a specific [Event] to search in
    #
    # @param [String] name The team name, can be done as a fuzzy search
    # @param [Hash] options A hash with key to narrow the search by events
    # @return [Hash] A hash representing JSON data
    def get_teams_as_json_with_name(name, options)

      # defuallt options and values
      options = {
          event: nil,
          event_name: '%'
      }.deep_merge options

      query = <<-SQL
        SELECT
          COALESCE(json_agg(t), '[]') as "data"
        FROM (
          SELECT
            teams.id   as "id",
            teams.name as "name",
            events.name as "event_name"
          FROM
            teams LEFT OUTER JOIN events on teams.event_id = events.id
          WHERE
            teams.name ILIKE ?
            AND
            (
              events.id = COALESCE(?,events.id)
              AND
              events.name ILIKE COALESCE(?,'%')
            )
          ORDER BY
          teams.name ASC
          LIMIT
           10
        ) t
      SQL

      # enable fuzzy searching by replacing spaces with the wildcard character and prepend/append with the wildcard
      team_term = "%#{name.split(' ').join('%')}%"
      event_term = "%#{options[:event_name].split(' ').join('%')}%"

      # execute the SQL statement and parse the JSON result into a Hash
      sql = ActiveRecord::Base.send(:sanitize_sql_array, [query, team_term, options[:event], event_term])
      JSON.parse(ActiveRecord::Base.connection.execute(sql)[0]['data'], symbolize_names: true)
    end

    def get_teams_with_badge_ready_state(ids = nil)
      result = Team.find_by_sql [
                           'SELECT teams.*, count(pending_jobs.id) = 0 as "ready" FROM teams full outer join pending_jobs on pending_jobs.target_id = teams.id AND pending_jobs.target_type = ? WHERE teams.id in (?) GROUP BY teams.id',
                           Team.name,
                           ids
                       ]

      # for the teams that are not ready, we should make sure that the resque and the pending job table is in sync
      result.reject {|t| t['ready'] }.flatten.each do |t|
        if Resque.size('badges') == 0 && (t.participations.select {|p| p.badge_file_name == nil }.count == 0)
          PendingJob.where(target: t).destroy_all

          # if the queue is empty and at least one badge wasn't made, then we need to just reenqueu the badges again
        elsif Resque.size('badges') == 0 && (t.participations.select {|p| p.badge_file_name == nil }.count > 0)
          PendingJob.where(target: t).destroy_all

          t.participations.each {|p| BadgeGenerationJob.perform_later p }
        end
      end

      result
    end

  end # end self

  def summary_url
    self.summary_short_url
  end

  def send_captain_email
    CaptainEditMailer.send_captain_edit_link(self).deliver
  end

  # Will create an in memory clone of a team with the proper Participations and Participants
  #
  # @param [Event] event The Event that the clone will be a part of.
  def clone_for_event(event)
    team = self.dup
    team.summary_short_url = nil
    team.slug = nil
    team.number = nil

    self.participations.each do |p|
      temp_participation = team.participations.build
      temp_participation.participant = p.participant
      temp_participation.event = event
    end

    team
  end

  #generate the qr code associated with the team
  def gen_qr

    team_qr = Tempfile.new(['team_qr','.png'])

    open(team_qr.path,'wb') do |temp_file|
      team_url = self.summary_short_url

      qr = RQRCode::QRCode.new( team_url, :size => 5, :level => :m )
      qr.to_img.resize(200, 200).save(temp_file.path)
    end


    begin
      self.qr_code = File.open(team_qr.path)
    ensure
      team_qr.close
      team_qr.unlink # cleanup the temp file...
    end
  end

  private

  def create_slug
    self.slug = "#{self.name.underscore.gsub(/\s+/,'_').gsub(/_+/, '_').gsub(/[^a-zA-Z0-9_]/, '')}"
  end

  def create_summary_url

    if self.summary_short_url.nil?
      path = "#{self.event.slug}/teams/#{self.slug}"

      url = "#{SERVER_HOST["path"]}/#{path}"

      if url.include? 'localhost'
        self.summary_short_url = url
      else
        client = Bitly.client
        url = client.shorten(url)
        self.summary_short_url = url.short_url
      end
    end

  end

  def update_contact
    # if the contact is not longer in the participants list
    # or if the contact is not nil
    if self.participants.where(:id => self.contact_id).empty? or self.contact.nil?
      self.contact = self.captain
    end
  end


  def update_captain
    # if the captain is no longer in the participants list,
    # set the captain to the first participant
    if self.participants.where(:id => self.captain_id).empty?
        self.captain = self.participants.first
    end
  end

  def gen_token
    #create a unique hash for this team
    info = self.event.name + self.name + self.id.to_s
    h = Digest::SHA2.new << info

    #get time the token should expire
    exp = self.event.start_date - 1.days

    #create the token and set up the relationship
    @token = Token.new(token: h.hexdigest, expiration: exp, tokenable: self)
    @token.save!

  end

  def remove_captain_and_captain
    Team.connection.execute("UPDATE teams SET captain_id = NULL, contact_id = NULL WHERE teams.id = #{self.id}")
  end

  def check_slug_change
    return unless self.changed.include?('slug')
    self.gen_qr
  end


  def check_change
    return unless self.changed.any? { |x| %w(name company slug summary_short_url number qr_code_updated_at).include?(x) }

    self.participations.each do |part|
      BadgeGenerationJob.perform_later(part)
    end
  end

end
