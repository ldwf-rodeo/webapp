# == Schema Information
#
# Table name: genders
#
#  id           :integer          not null, primary key
#  name         :text             not null
#  abbreviation :text             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Gender < ActiveRecord::Base
  resourcify
  has_paper_trail

  has_many :participants

end
