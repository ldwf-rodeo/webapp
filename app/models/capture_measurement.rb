# == Schema Information
#
# Table name: capture_measurements
#
#  id          :integer          not null, primary key
#  unit_id     :integer          not null
#  capture_id  :integer          not null
#  measurement :decimal(, )      not null
#

class CaptureMeasurement < ActiveRecord::Base
  resourcify
  has_paper_trail

  belongs_to :capture
  belongs_to :unit

  validates :measurement, numericality: {greater_than: 0}

  def to_s
    "#{self.measurement} #{self.unit.abbreviation} [#{self.unit.description}]"
  end

  def to_str
    to_s
  end
end
