# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  user_name              :string(25)       not null
#  updated_at             :datetime         not null
#  created_at             :datetime         not null
#  participant_id         :integer
#  encrypted_password     :string(128)      default(""), not null
#  password_salt          :string(255)
#  authentication_token   :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  reset_password_token   :string(255)
#  remember_token         :string(255)
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  failed_attempts        :integer          default(0)
#  unlock_token           :string(255)
#  locked_at              :datetime
#  reset_password_sent_at :datetime
#

class User < ActiveRecord::Base

  nilify_blanks
  resourcify
  has_paper_trail only: [:user_name, :participant_id]

  belongs_to :participant
  has_many :captures
  has_and_belongs_to_many :events
  has_and_belongs_to_many :roles , :join_table => 'users_roles'
  has_many :user_roles, :class_name => 'UserRole'

  has_many :event_users
  has_many :events, lambda { uniq }, :through => :event_users

  accepts_nested_attributes_for :roles
  accepts_nested_attributes_for :participant

  validate :password_complexity
  validates_presence_of :user_name

  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :authentication_keys => [:user_name]

  delegate :can?, :cannot?, :to => :ability

  #attr_accessible :participant_id, :user_name, :password, :password_confirmation, :roles

  def password_complexity
    if password.present? and not password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,128}/)
      errors.add :password, "must include at least one lowercase letter, one uppercase letter, and one digit"
    end
  end

  # these are needed to make devise ignroe the email field for a user. We are storing the email in the
  # associated participant
  def email_required?
    false
  end

  def  email_changed?
    false
  end

  # rolify does not work on rails 4. These methods give us the same functionality
  # that we will need.
  def has_role? (role_name)
    role_ids.include?(Role.where(:name => role_name).first.id)
  end

  def add_role (role_name)
    role_to_add = Role.where(:name => role_name).first
    roles << role_to_add unless role_to_add.nil?
  end

  def remove_role (role_name)
    role_to_delete = Role.where(:name => role_name).first
    @a.roles.delete(role_to_delete) unless role_to_delete.nil?
  end

  def has_event_id_for_role?(event_id,role)
    return false if self.id.nil?
    (query = '') << <<-SQL
      SELECT (
        -- We have an events_users entry for this event and role.
        EXISTS(SELECT 1 FROM events_users WHERE user_id = #{self.id} AND role_id = #{role.id} AND event_id = #{event_id})
        OR
        -- We have this role as a user role, which will take precedence, OR they are an admin.
        EXISTS(SELECT 1 FROM users_roles WHERE user_id = #{self.id} AND role_id IN (#{role.id},#{Role.admin.id}))
      ) as is_in
     SQL
     return (ActiveRecord::Base.connection.execute(query))[0]['is_in'] == 't'
  end

  def has_event_id_for_role_list?(event_id,roles)
    return false if self.id.nil?
    (query = '') << <<-SQL
      SELECT (
        -- We have an events_users entry for this event and role.
        EXISTS(SELECT 1 FROM events_users WHERE user_id = #{self.id} AND role_id IN (#{roles.collect{|r|r.id}.join(',')}) AND event_id = #{event_id})
        OR
        -- We have this role as a user role, which will take precedence, OR they are an admin.
        EXISTS(SELECT 1 FROM users_roles WHERE user_id = #{self.id} AND role_id IN (#{roles.collect{|r|r.id}.join(',')},#{Role.admin.id}))
      ) as is_in
    SQL
    return (ActiveRecord::Base.connection.execute(query))[0]['is_in'] == 't'
  end

  #Get all of this users effective roles (events_users and users_roles) with the corresponding event counts.
  def effective_roles(event=nil)
    (query = '') << <<-SQL
      SELECT * FROM (
        SELECT
          roles.*,
          NULL as event_count
        FROM users_roles INNER JOIN roles ON roles.id = users_roles.role_id
        WHERE user_id = #{self.id}
       UNION ALL
        SELECT DISTINCT ON (roles.id)
          roles.*,
          (SELECT COUNT(*) FROM events_users WHERE role_id = roles.id AND user_id = #{self.id}) as event_count
        FROM events_users INNER JOIN roles ON roles.id = events_users.role_id
        WHERE
          user_id = #{self.id}
          #{event.nil? ? '' : 'AND event_id=' + event.id.to_s} -- We are optionally only interested in this event's roles.
      ) t
      ORDER BY (event_count IS NOT NULL), t.name ASC
    SQL
    return Role.find_by_sql(query)
  end

  #Get a nice decoration based on current roles.

  def decorated_name(event=nil)
    effective_roles = self.effective_roles(event)
    if effective_roles.count == 0
      "<span class=\"user-decoration\" title=\"#{self.participant.nil? ? '(No Associated Participant)' : self.participant.full_name}\">#{self.user_name}</span>".html_safe
    else
      "<span class=\"user-decoration\" title=\"#{self.participant.nil? ? '(No Associated Participant)' : self.participant.full_name}\">#{self.user_name} (#{effective_roles.collect{|r| self.decorate_effective_role(event,r)}.join(' ')})</span>".html_safe
    end
  end

  def decorate_effective_role(event,role)
    "<span class=\"role-decoration\" title=\"#{self.make_role_decoration_title(event,role)}\" style=\"color:#{role.decoration_color}\">#{role.decoration_text}#{self.make_role_event_count_span(event,role)}</span>"
  end

  def make_role_decoration_title(event,role)
    if role.event_count.nil? or role.event_count == 0
      "#{role.display_name} for all events."
    elsif !event.nil?
      "#{role.display_name} for this event."
    elsif role.event_count == 1
      "#{role.display_name} for #{EventUser.where(:role => role, :user => self).first.event.name}."
    else
      "#{role.display_name} for:\n\n#{EventUser.where(:role => role, :user => self).collect{|er| er.event.name }.join("\n")}"
    end
  end

  def make_role_event_count_span(event,role)
    if role.event_count.nil?
      #Indicate that this this is a user role.
      '<span class="role-event-count-decoration">&star;</span>'
    elsif !event.nil? and !role.event_count.nil?
      #It's an EventUser role for this event in particular.
      '<span class="role-event-count-decoration">&bull;</span>'
    elsif event.nil? and !role.event_count.nil?
      # Only display the count if we are not considering an event.
      '<span class="role-event-count-decoration">' + role.event_count.to_s + '</span>'
    else
      ''
    end
  end

  # rails_admin do
  #   configure :event_users do
  #     visible(false)
  #   end
  #
  #   configure :events do
  #     orderable(true) # only for multiselect widget currently. Will add the possibility to order blocks
  #     # configuration here
  #   end
  # end

  private

  def ability
    @ability ||= Ability.new(self)
  end
end
