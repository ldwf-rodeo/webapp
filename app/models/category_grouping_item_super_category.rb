# == Schema Information
#
# Table name: category_grouping_items_super_categories
#
#  id                        :integer          not null, primary key
#  category_grouping_item_id :integer          not null
#  super_category_id         :integer          not null
#

class CategoryGroupingItemSuperCategory < ActiveRecord::Base
  resourcify
  has_paper_trail

  self.table_name = 'category_grouping_items_super_categories'

  belongs_to :super_category
  belongs_to :category_grouping_item

end
