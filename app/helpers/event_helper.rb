module EventHelper

  def format_weight(value)
    if value < 2204.62
      "#{value} lbs."
    else
      "#{(value/2204.62).round(2)} metric tons"
    end
  end
end