module PremiumCategoryGroupingsHelper
  def format_premium_grouping_name(grouping)
    grouping.price > 0 ? "#{grouping.name} (#{number_to_currency(grouping.price)})" : grouping.name
  end
end
