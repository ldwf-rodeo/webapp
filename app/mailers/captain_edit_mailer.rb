class CaptainEditMailer < ActionMailer::Base
  default :from => '"Geaux Fish Rodeos" <no-reply@geauxfishlarodeo.com>'

  # Send the captain of a team the edit team link
  def send_captain_edit_link(team)
    @team = team
    @captain = team.captain
    @url = "#{root_url}/#{@team.event.slug}/teams/#{@team.slug}/#{@team.token.token}"
    if @captain.email
      mail(:to => @captain.email, :subject => 'Edit Link For Your Team')
    end


  end


end