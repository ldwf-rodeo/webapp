#Geauxfishlarodeo.com Server


### Getting started

To run and develop this Rails app, you will need a few utilites installed

- Virtualbox
- Vagrant

It is also recommended to have acouple of vagrant plugins installed. You can runt he following in the host machines terminal.

```
$ vagrant plugin install vagrant-gatling-rsync
$ vagrant plugin install vagrant-rsync-back
```

### Login into the Vagrant Cloud

If you have not already, you will need to login into the vagrant cloud to access the
private box.

```
$ vagrant login
```

### Starting the VM

You will need to start the VM enviroment

```
$ vagrant up
```

After the machine is up you can then log into the machine with:

```
$ vagrant ssh
```

##### You should run a bundle install and do a sync back after first up (provision)

```
$ vagrant ssh -c "cd ~/workspace; bundle install; rake db:migrate"
$ vagrant rsync-back
```

### Staring the Rails server

From the terminal

```
rails s -b 0.0.0.0
```

If you want to test it in staging mode, similar to the production env.

```
cap staging deploy
```

the server can be accessed from http://localhost:8080, while in staging

To stop the staging server:

```
cap staging all:stop
```

## Production server

To update the server, you run the following in the VM

```
cap production deploy
```

### Syncing files from the host to the guest

The Vagrant enviroment is setup to user ``rsync`` to copy files from the host to the guest.
To enable auto syncing you will need to run the following.

```
$ vagrant gatling-rsync-auto
```
> **Note**: The previous command will tie up the terminal while it is running

### Syncing files from the guest to the host

If you need to sync files that were ceated on the guest, then you can run the following.

```
$ vagrant rsync-back
```

### Configure RubyMine to work with the Vagrant VM


You will need to add the remote ruby interpretor to RubyMine.
You can look at Chapter 2 of this [Guide](https://www.gitbook.com/book/danielbitwizardward/rails_guide/details) to see how to add the interpretor.

- The interpretor path is ``/home/vagrant/.rvm/gems/ruby-2.2.2@global``