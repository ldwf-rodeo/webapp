--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: captures; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE captures (
    id bigint NOT NULL,
    species character varying(256) NOT NULL,
    weight numeric NOT NULL,
    length numeric,
    time_entered timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    user_id bigint NOT NULL,
    participant_id bigint NOT NULL
);


ALTER TABLE public.captures OWNER TO postgres;

--
-- Name: captures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.captures_id_seq OWNER TO postgres;

--
-- Name: captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE captures_id_seq OWNED BY captures.id;


SET default_with_oids = true;

--
-- Name: captures_events; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE captures_events (
    event_id bigint NOT NULL,
    capture_id bigint NOT NULL
);


ALTER TABLE public.captures_events OWNER TO postgres;

--
-- Name: events_teams; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE events_teams (
    team_id bigint NOT NULL,
    event_id bigint NOT NULL
);


ALTER TABLE public.events_teams OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: events; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE events (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    start_date timestamp with time zone NOT NULL,
    end_date timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.events OWNER TO postgres;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO postgres;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: events_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE events_users (
    event_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.events_users OWNER TO postgres;

--
-- Name: participants; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE participants (
    id bigint NOT NULL,
    first_name character varying(256) NOT NULL,
    last_name character varying(256) NOT NULL,
    gender character varying(256),
    street character varying(256),
    city character varying(256),
    zipcode character varying(25),
    email character varying(256),
    home_phone character varying(25),
    mobile_phone character varying(25),
    mobile_carrier character varying(256),
    shirt_size character varying(256),
    participant_type character varying(256),
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.participants OWNER TO postgres;

--
-- Name: participants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participants_id_seq OWNER TO postgres;

--
-- Name: participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE participants_id_seq OWNED BY participants.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255),
    resource_id integer,
    resource_type character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: team_participants; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE team_participants (
    team_id bigint NOT NULL,
    participant_id bigint NOT NULL
);


ALTER TABLE public.team_participants OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: teams; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE teams (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    company character varying(256),
    description character varying(256),
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    vessel_id bigint NOT NULL,
    captain_id bigint
);


ALTER TABLE public.teams OWNER TO postgres;

--
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teams_id_seq OWNER TO postgres;

--
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE teams_id_seq OWNED BY teams.id;


SET default_with_oids = true;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    user_name character varying(25),
    updated_at timestamp with time zone,
    created_at timestamp with time zone,
    participant_id bigint,
    encrypted_password character varying(128) DEFAULT ''::character varying NOT NULL,
    password_salt character varying(255),
    authentication_token character varying(255),
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    reset_password_token character varying(255),
    remember_token character varying(255),
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    failed_attempts integer DEFAULT 0,
    unlock_token character varying(255),
    locked_at timestamp without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET default_with_oids = false;

--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_roles (
    user_id integer,
    role_id integer
);


ALTER TABLE public.users_roles OWNER TO postgres;

--
-- Name: vessels; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vessels (
    id bigint NOT NULL,
    name text,
    description text,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    participant_id bigint NOT NULL
);


ALTER TABLE public.vessels OWNER TO postgres;

--
-- Name: vessels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vessels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vessels_id_seq OWNER TO postgres;

--
-- Name: vessels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vessels_id_seq OWNED BY vessels.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY captures ALTER COLUMN id SET DEFAULT nextval('captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participants ALTER COLUMN id SET DEFAULT nextval('participants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teams ALTER COLUMN id SET DEFAULT nextval('teams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vessels ALTER COLUMN id SET DEFAULT nextval('vessels_id_seq'::regclass);


--
-- Data for Name: captures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY captures (id, species, weight, length, time_entered, updated_at, created_at, user_id, participant_id) FROM stdin;
\.


--
-- Name: captures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('captures_id_seq', 1, false);


--
-- Data for Name: captures_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY captures_events (event_id, capture_id) FROM stdin;
\.


--
-- Data for Name: events_teams; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY events_teams (team_id, event_id) FROM stdin;
\.


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY events (id, name, start_date, end_date, updated_at, created_at) FROM stdin;
\.


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('events_id_seq', 1, false);


--
-- Data for Name: events_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY events_users (event_id, user_id) FROM stdin;
\.


--
-- Data for Name: participants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY participants (id, first_name, last_name, street, city, zipcode, email, home_phone, mobile_phone, mobile_carrier, shirt_size, participant_type, updated_at, created_at) FROM stdin;
\.


--
-- Name: participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('participants_id_seq', 1, false);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY roles (id, name, resource_id, resource_type, created_at, updated_at) FROM stdin;
1	admin	\N	\N	2013-04-22 15:47:38.018537	2013-04-22 15:47:38.018537
2	organizer	\N	\N	2013-04-22 15:47:38.04622	2013-04-22 15:47:38.04622
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_id_seq', 2, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20130418195132
20130418200638
20130418200811
20130418200858
20130418202057
20130418203211
20130418212914
20130426141920
\.


--
-- Data for Name: team_participants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY team_participants (team_id, participant_id) FROM stdin;
\.


--
-- Data for Name: teams; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY teams (id, name, company, updated_at, created_at, vessel_id, captain_id) FROM stdin;
\.


--
-- Name: teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('teams_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, user_name, updated_at, created_at, participant_id, encrypted_password, password_salt, authentication_token, confirmation_token, confirmed_at, confirmation_sent_at, reset_password_token, remember_token, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, failed_attempts, unlock_token, locked_at) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Data for Name: users_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users_roles (user_id, role_id) FROM stdin;
\.


--
-- Data for Name: vessels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY vessels (id, name, description, updated_at, created_at, participant_id) FROM stdin;
\.


--
-- Name: vessels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vessels_id_seq', 1, false);


--
-- Name: captures_events_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY captures_events
    ADD CONSTRAINT captures_events_pk PRIMARY KEY (event_id, capture_id);


--
-- Name: events_teams_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY events_teams
    ADD CONSTRAINT events_teams_pk PRIMARY KEY (team_id, event_id);


--
-- Name: events_primary_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_primary_key PRIMARY KEY (id);


--
-- Name: events_users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT events_users_pk PRIMARY KEY (event_id, user_id);


--
-- Name: participant_60393_uq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT participant_60393_uq UNIQUE (participant_id);


--
-- Name: primary_key_capture_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT primary_key_capture_id PRIMARY KEY (id);


--
-- Name: primary_key_participants_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT primary_key_participants_id PRIMARY KEY (id);


--
-- Name: primary_key_team_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT primary_key_team_id PRIMARY KEY (id);


--
-- Name: primary_key_users_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT primary_key_users_id PRIMARY KEY (id);


--
-- Name: primary_key_vessel_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vessels
    ADD CONSTRAINT primary_key_vessel_id PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: team_participants_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY team_participants
    ADD CONSTRAINT team_participants_pk PRIMARY KEY (team_id, participant_id);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_roles_on_name ON roles USING btree (name);


--
-- Name: index_roles_on_name_and_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_roles_on_name_and_resource_type_and_resource_id ON roles USING btree (name, resource_type, resource_id);


--
-- Name: index_users_roles_on_user_id_and_role_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_users_roles_on_user_id_and_role_id ON users_roles USING btree (user_id, role_id);


--
-- Name: participants_type_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX participants_type_index ON participants USING btree (participant_type);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: captures_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY captures_events
    ADD CONSTRAINT captures_fk FOREIGN KEY (capture_id) REFERENCES captures(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: events_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events_teams
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: events_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY captures_events
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: events_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vessels
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY team_participants
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: teams_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events_teams
    ADD CONSTRAINT teams_fk FOREIGN KEY (team_id) REFERENCES teams(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: teams_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY team_participants
    ADD CONSTRAINT teams_fk FOREIGN KEY (team_id) REFERENCES teams(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: users_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: users_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vessels_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT vessels_fk FOREIGN KEY (vessel_id) REFERENCES vessels(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

