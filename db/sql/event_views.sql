DROP VIEW IF EXISTS event_facts_view;
DROP VIEW IF EXISTS event_all_units_view;
DROP VIEW IF EXISTS event_default_units_view;
DROP VIEW IF EXISTS event_all_species_view;
DROP VIEW IF EXISTS event_default_species_view;

-----------------------------------------
-- SPECIES
-----------------------------------------

-- Returns a list of species id values associated with any of the events categories.
CREATE VIEW event_default_species_view AS (
  SELECT DISTINCT ON (event_id,species_id)
    event_id,
    species_id
  FROM (
    SELECT
      super_categories.event_id AS event_id,
      category_species_unit_items.species_id as species_id
    FROM
      category_species_unit_items
      INNER JOIN categories ON categories.id = category_species_unit_items.category_id AND categories.category_type = 'species_unit'
      INNER JOIN super_categories on super_categories.id = categories.super_category_id

    UNION ALL

    SELECT
      super_categories.event_id AS event_id,
      category_species_points_items.species_id as species_id
    FROM
      category_species_points_items
      INNER JOIN categories ON categories.id = category_species_points_items.category_id AND categories.category_type = 'species_points'
      INNER JOIN super_categories on super_categories.id = categories.super_category_id

    UNION ALL

    SELECT
      super_categories.event_id AS event_id,
      category_species_capture_items.species_id as species_id
    FROM
      category_species_capture_items
      INNER JOIN categories ON categories.id = category_species_capture_items.category_id AND categories.category_type = 'species_capture'
      INNER JOIN super_categories on super_categories.id = categories.super_category_id
  ) t
);

-- Returns a list of species id values associated with (1) any category and (2) any capture.
CREATE VIEW event_all_species_view AS  (
  SELECT DISTINCT ON (event_id,species_id)
    event_id,
    species_id
  FROM (
    SELECT
      event_id,
      species_id
    FROM
      event_default_species_view

      UNION ALL

      SELECT distinct ON (event_id,species_id)
        captures.event_id AS event_id,
        captures.species_id AS species_id
      FROM
        captures
    ) t
);

-----------------------------------------
-- UNITS
-----------------------------------------

-- Returns a list of all unit id values associated with any of the events categories.
CREATE VIEW event_default_units_view AS (
  SELECT DISTINCT ON (event_id,unit_id)
    *
  FROM (
    SELECT
      super_categories.event_id AS event_id,
      unit_id AS unit_id
    FROM
      units
      INNER JOIN category_species_unit_items ON units.id = category_species_unit_items.unit_id
      INNER JOIN categories ON categories.id = category_species_unit_items.category_id
      INNER JOIN super_categories ON super_categories.id = categories.super_category_id
    ) t
);

-- Returns a list of unit id values associated with (1) any category and (2) any capture.
CREATE VIEW event_all_units_view AS (
  SELECT DISTINCT ON (event_id,unit_id)
    event_id,
    unit_id
  FROM (
   SELECT
     event_id,
     unit_id
   FROM
     event_default_units_view

   UNION ALL

   SELECT distinct ON (event_id,unit_id)
     captures.event_id AS event_id,
     capture_measurements.unit_id AS unit_id
   FROM
     capture_measurements
     INNER JOIN captures ON captures.id = capture_measurements.capture_id
 ) t
);

-----------------------------------------
-- EVENT FACTS
-----------------------------------------

CREATE VIEW event_facts_view AS (

  -- Get all of the non-redundant captures, but normalize them to their base_species_id first.
  WITH joined_captures AS (
      SELECT
        captures.id,
        captures.event_id AS event_id,
        coalesce(species.quantity,base_species.quantity) as quantity,
        coalesce(base_species.id,species.id) as species_id
      FROM
        captures
        INNER JOIN species ON species.id = captures.species_id
        LEFT JOIN species AS base_species ON species.base_species_id = base_species.id
      WHERE
        captures.is_redundant = FALSE
  ),

  -- Now get all of the related capture measurements.
  species_measurements AS (
    SELECT
      joined_captures.species_id AS species_id,
      joined_captures.event_id AS event_id,
      units.id AS unit_id,
      format_number(sum(measurement)) AS total,
      concat(format_number(sum(measurement)), ' ', units.abbreviation) AS label,
      CASE WHEN sum(quantity) = 0 THEN NULL ELSE sum(measurement)/sum(quantity) END AS average
    FROM
      capture_measurements
      INNER JOIN joined_captures ON joined_captures.id = capture_measurements.capture_id
      RIGHT JOIN event_all_units_view ON event_all_units_view.event_id = joined_captures.event_id AND event_all_units_view.unit_id = capture_measurements.unit_id
      LEFT JOIN units ON units.id = event_all_units_view.unit_id
    GROUP BY
      joined_captures.event_id,
      joined_captures.species_id,
      units.id
  )

  -- Now perform the full select.
  SELECT
    event_all_species_view.event_id AS event_id,
    event_all_species_view.species_id as species_id,
    species.name as species_name,
    coalesce(sum(joined_captures.quantity),0) AS quantity,
    (
      SELECT
    json_agg(t)
      FROM (
        SELECT * FROM species_measurements
        WHERE species_measurements.species_id = event_all_species_view.species_id AND species_measurements.event_id = event_all_species_view.event_id
        ORDER BY unit_id ASC
      ) t
    ) AS measurements
  FROM
    events
    LEFT JOIN event_all_species_view ON event_all_species_view.event_id = events.id
    INNER JOIN species ON species.id = event_all_species_view.species_id
    -- Left join since we want to include all species, even if nothing was captures for it.
    LEFT JOIN joined_captures ON joined_captures.species_id = species.id AND joined_captures.event_id = event_all_species_view.event_id
  WHERE
    species.base_species_id IS NULL
  GROUP BY
    event_all_species_view.event_id,
    event_all_species_view.species_id,
    species.name
);