DROP TYPE IF EXISTS leaderboard_display_place_type CASCADE;

CREATE TYPE leaderboard_display_place_type AS (
  category_id int,

  position bigint,
  team_position bigint,

  is_empty boolean,

  entity_hash text,

  participant_id bigint,
  team_id bigint,
  grouping_index bigint,

  contributing_captures bigint[],
  contributing_ranks bigint[][],

  position_label text,
  entity_label text,
  score_label text,
  unit_label text,

  capture_count bigint,
  last_capture_at timestamp with time zone,
  first_capture_at timestamp with time zone,
  smallest_subscore_label text,
  largest_subscore_label text
);


--================================================================
--================================================================
-- EVENT LEADERBOARD
-- Contains an entry for every category and every place.
--================================================================
--================================================================

-- Computes the scores for all places for all categories, regardless of whether there are actually entries yet.
DROP FUNCTION IF EXISTS get_event_leaderboard_for_event(bigint, timestamp with time zone);
CREATE FUNCTION get_event_leaderboard_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_place_type AS $$
  WITH category_places as (
    WITH RECURSIVE places(category_id, position, positions) AS (
      SELECT
        categories.id,
        CAST(1 AS bigint) as place,
        categories.positions
      FROM
        categories
        INNER JOIN super_categories ON super_categories.id = categories.super_category_id
        INNER JOIN events ON events.id = super_categories.event_id
      WHERE
        events.id = $1
    UNION ALL -- Recurse to get the places for each category we are considering...
      SELECT
        category_id,
        position+1,
        positions as positions
      FROM
        places
      WHERE
        position < positions
    )
    SELECT
      category_id,
      position
    FROM
      places
  )
  SELECT
    category_places.category_id,
    category_places.position,
    team_id IS NULL AS is_empty,
    leaderboard.team_position,
    leaderboard.participant_id,
    leaderboard.team_id,
    leaderboard.grouping_index,
    leaderboard.capture_id,
    leaderboard.contributing_captures,
    leaderboard.contributing_ranks,
    leaderboard.score,
    leaderboard.unit_id,
    leaderboard.capture_count,
    leaderboard.last_capture_at,
    leaderboard.first_capture_at,
    leaderboard.smallest_subscore,
    leaderboard.largest_subscore
  FROM
    category_places
    LEFT JOIN get_leaderboard_for_event(event_id,snapshot) AS leaderboard ON category_places.category_id = leaderboard.category_id AND category_places.position = leaderboard.position

$$ LANGUAGE SQL;

-- A join on the leaderboard rankings that provides additional formatting information.
DROP FUNCTION IF EXISTS get_event_leaderboard_display_for_event(bigint, timestamp with time zone);
DROP FUNCTION IF EXISTS get_event_leaderboard_display_for_event(bigint);
CREATE FUNCTION get_event_leaderboard_display_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_display_place_type AS $$
  SELECT
    -- The basic info from the function call.
    ranks.category_id,
    ranks.position,
    ranks.team_position,
    team_id IS NULL as is_empty,
    CAST(CASE
      WHEN team_id IS NULL THEN CONCAT('category_',category_id,'_empty_',ranks.position)
      ELSE
        CONCAT(
          'category_',ranks.category_id,'_',
          CASE
            WHEN categories.score_entity = 'team' THEN CONCAT('team_',team_id,'_')
            WHEN categories.score_entity = 'participant' THEN CONCAT('participant_',participant_id,'_')
          END,
          CASE
            WHEN categories.score_grouping = 'none' THEN CONCAT('capture_',ranks.capture_id)
            ELSE CONCAT('group_',grouping_index)
          END
        )
    END AS text) as entity_hash,
    ranks.participant_id as participant_id,
    ranks.team_id as team_id,
    ranks.grouping_index as grouping_index,
    ranks.contributing_captures AS contributing_captures,
    ranks.contributing_ranks AS contributing_ranks,
    -- Nice labels for leaderboard
    TRIM(to_char(ranks.position, '9999999th')) AS position_label,
    CASE
      WHEN categories.score_entity = 'team' THEN truncate_text(teams.name,50)
      WHEN categories.score_entity = 'participant' THEN truncate_text(abbreviate_name(participants.first_name,participants.last_name,50),50)
    END AS entity_label,
    format_number(ranks.score) as score_label,
    COALESCE(units.abbreviation,CASE WHEN categories.category_type = 'species_capture' THEN 'ct.' ELSE 'pts.' END) as unit_label,
    ranks.capture_count as capture_count,
    ranks.last_capture_at,
    ranks.first_capture_at,
    format_number(ranks.smallest_subscore) as smallest_subscore_label,
    format_number(ranks.largest_subscore) as largest_subscore_label
  FROM
    get_event_leaderboard_for_event($1,snapshot) as ranks
    INNER JOIN categories ON categories.id = ranks.category_id
    INNER JOIN super_categories ON super_categories.id = categories.super_category_id
    LEFT JOIN participants ON participants.id = ranks.participant_id
    LEFT JOIN teams ON teams.id = ranks.team_id
    LEFT JOIN units ON units.id = ranks.unit_id
  ORDER BY
    ranks.category_id ASC,
    ranks.position ASC
$$ LANGUAGE SQL;

-- JSON for static data.
DROP FUNCTION IF EXISTS get_event_leaderboard_display_json_for_event(bigint, timestamp with time zone);
CREATE FUNCTION get_event_leaderboard_display_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS JSON AS $$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_event_leaderboard_display_for_event($1,snapshot)
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$$ LANGUAGE SQL;


-- structured JSON for static data.
DROP FUNCTION IF EXISTS get_event_leaderboard_display_structured_json_for_event(bigint, timestamp with time zone);
CREATE FUNCTION get_event_leaderboard_display_structured_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS JSON AS $$
  WITH rankings as (
      SELECT
        t.*,
        categories.super_category_id as "super_category_id"
      FROM
        get_event_leaderboard_display_for_event($1,snapshot) as t
        LEFT JOIN categories ON t.category_id = categories.id
      ORDER BY
        categories.super_category_id ASC,
        t.category_id ASC,
        t.position ASC
  )
  SELECT
    to_json(d) AS "stuff"
  FROM
    (
      SELECT
        events.name as "name",
        events.start_date as "start_date",
        events.end_date as "end_date",
        (
          -- aggregate all the super categories for this event tuple
          SELECT json_agg(t)
          FROM (
                 -- select all the super categories for the event
                 SELECT
                   super_categories.name,
                   (
                     -- aggregate all  the categories for this super category tuple
                     SELECT
                       json_agg(t)
                      FROM (
                        -- select all the categories for the super category
                        SELECT
                          categories.name,
                          categories.display_mode,
                          (
                            -- aggregate all the rankings that relate to this category
                            SELECT json_agg(t)
                            FROM (

                              -- select the rankings
                              SELECT
                                rankings.*,
                                teams.notes as team_notes,
                                teams.name as team_name
                              FROM
                                rankings
                                LEFT JOIN teams on rankings.team_id = teams.id
                              WHERE
                                rankings.category_id = categories.id
                              ORDER BY
                                rankings.position ASC
                            ) t
                            ---------------------------------------------

                          ) as "rankings"
                        FROM
                          categories
                        WHERE
                          categories.super_category_id = super_categories.id
                        ORDER BY
                          categories.grouping_number ASC,
                          categories.position ASC,
                          categories.name ASC,
                          categories.id ASC
                      ) t
                      ---------------------------------------------

                   ) as "categories"
                 FROM
                   super_categories
                 WHERE
                   super_categories.event_id = events.id
                 ORDER BY
                   super_categories.page ASC,
                   super_categories.position ASC,
                   super_categories.name ASC,
                   super_categories.id ASC
               ) t
               ---------------------------------------------

        ) as category_groups
      FROM
        events
      WHERE
        events.id = event_id
    ) d

$$ LANGUAGE SQL;


--================================================================
--================================================================
-- UNLIMITED LEADERBOARD
--================================================================
--================================================================

-- A join on the leaderboard rankings that provides additional formatting information.
DROP FUNCTION IF EXISTS get_unlimited_leaderboard_display_for_event(bigint);
DROP FUNCTION IF EXISTS get_unlimited_leaderboard_display_for_event(bigint, timestamp with time zone);
CREATE FUNCTION get_unlimited_leaderboard_display_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_display_place_type AS $$
  SELECT
    -- The basic info from the function call.
    ranks.category_id,
    ranks.position,
    ranks.team_position,
    ranks.team_id IS NULL as is_empty,
    CAST(CASE
      WHEN team_id IS NULL THEN CONCAT('category_',category_id,'_empty_',ranks.position)
      ELSE
        CONCAT(
          'category_',ranks.category_id,'_',
          CASE
            WHEN categories.score_entity = 'team' THEN CONCAT('team_',team_id,'_')
            WHEN categories.score_entity = 'participant' THEN CONCAT('participant_',participant_id,'_')
          END,
          CASE
            WHEN categories.score_grouping = 'none' THEN CONCAT('capture_',ranks.capture_id)
            ELSE CONCAT('group_',grouping_index)
          END
        )
    END AS text) as entity_hash,
    ranks.participant_id as participant_id,
    ranks.team_id as team_id,
    ranks.grouping_index AS grouping_index,
    ranks.contributing_captures,
    ranks.contributing_ranks,
    -- Nice labels for leaderboard
    TRIM(to_char(ranks.position, '9999999th')) AS position_label,
    CASE
      WHEN categories.score_entity = 'team' THEN truncate_text(teams.name,50)
      WHEN categories.score_entity = 'participant' THEN truncate_text(abbreviate_name(participants.first_name,participants.last_name,50),50)
    END AS entity_label,
    format_number(ranks.score) as score_label,
    COALESCE(units.abbreviation,'pts.') as unit_label,
    ranks.capture_count as capture_count,
    ranks.last_capture_at,
    ranks.first_capture_at,
    format_number(ranks.smallest_subscore) as smallest_subscore_label,
    format_number(ranks.largest_subscore) as largest_subscore_label
  FROM
    get_leaderboard_for_event(event_id,snapshot) as ranks
    INNER JOIN categories ON categories.id = ranks.category_id
    INNER JOIN super_categories ON super_categories.id = categories.super_category_id
    LEFT JOIN participants ON participants.id = ranks.participant_id
    LEFT JOIN teams ON teams.id = ranks.team_id
    LEFT JOIN units ON units.id = ranks.unit_id
  ORDER BY
    ranks.category_id ASC,
    ranks.position ASC
$$ LANGUAGE SQL;

-- JSON for static data.
DROP FUNCTION IF EXISTS get_unlimited_leaderboard_display_json_for_event(bigint, timestamp with time zone);
DROP FUNCTION IF EXISTS get_unlimited_leaderboard_display_json_for_event(bigint);
CREATE FUNCTION get_unlimited_leaderboard_display_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS JSON AS $$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_unlimited_leaderboard_display_for_event($1,snapshot)
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$$ LANGUAGE SQL;

DROP FUNCTION IF EXISTS get_unlimited_leaderboard_display_json_for_event_category(bigint,bigint);
CREATE FUNCTION get_unlimited_leaderboard_display_json_for_event_category(event_id bigint, category_id bigint) RETURNS JSON AS $$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_unlimited_leaderboard_display_for_event($1,NULL) as ranks
            WHERE
              ranks.category_id = $2
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$$ LANGUAGE SQL;

--================================================================
--================================================================
-- BUMPING
--================================================================
--================================================================

DROP FUNCTION IF EXISTS get_leaderboard_bumps_for_event(bigint,timestamp with time zone);
DROP FUNCTION IF EXISTS get_leaderboard_bumps_for_event(bigint,timestamp);
CREATE FUNCTION get_leaderboard_bumps_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_bump_type AS $$
  WITH
  full_old AS (SELECT * FROM get_event_leaderboard_for_event(event_id,snapshot)),
  full_new AS (SELECT * FROM get_event_leaderboard_for_event(event_id,NULL)),
  old AS (SELECT * FROM (SELECT DISTINCT ON (category_id, participant_id, team_id) * FROM full_old WHERE team_id IS NOT NULL ORDER by category_id, participant_id, team_id, position) t ORDER BY category_id ASC, position ASC),
  new AS (SELECT * FROM (SELECT DISTINCT ON (category_id, participant_id, team_id) * FROM full_new WHERE team_id IS NOT NULL ORDER BY category_id, participant_id, team_id, position) t ORDER BY category_id ASC, position ASC),
  -- Find the people who have gone down in the leaderboard.
  losers AS (SELECT
    old.category_id as category_id,
    old.unit_id as unit_id,
    old.participant_id as participant_id,
    old.team_id as team_id,
    old.position as old_position,
    new.position as new_position,
    old.score as score
  FROM
    old
    LEFT JOIN new ON old.category_id = new.category_id AND ((old.participant_id IS NULL AND new.participant_id IS NULL) OR old.participant_id = new.participant_id) AND old.team_id = new.team_id
  WHERE
    (
      new.category_id IS NULL -- THEY WERE KICKED OFF THE LEADERBOARD
      OR
      new.position > old.position -- THEY WERE MOVED DOWN A SLOT
    )
  )
  -- Now find who replaced them.
  SELECT
    losers.*,
    winners.participant_id as usurping_participant_id,
    winners.team_id as usurping_team_id,
    winners.score as usurping_score
  FROM
    losers
    INNER JOIN full_new AS winners ON losers.category_id = winners.category_id AND losers.old_position = winners.position
  ORDER BY
    category_id ASC,
    old_position ASC

$$ LANGUAGE SQL;