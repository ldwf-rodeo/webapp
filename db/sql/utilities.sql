
-- Abbreviates a person's first name if the entire name is longer than a given number of characters.
DROP FUNCTION IF EXISTS abbreviate_name(text, text, int);
CREATE FUNCTION abbreviate_name(first_name text, last_name text, length int) RETURNS TEXT AS $$
  SELECT
    CASE
      WHEN char_length(full_name) >= length THEN concat(substring(first_name,1,1),'. ',last_name)
      ELSE t.full_name
    END
  FROM (
    SELECT concat(first_name,' ',last_name) as full_name
  ) t
$$ LANGUAGE SQL;

-- Abbreviates a person's first name if the entire name is longer than a given number of characters.
DROP FUNCTION IF EXISTS truncate_text(text, int);
CREATE FUNCTION truncate_text(word text, length int) RETURNS TEXT AS $$
  SELECT
    CASE
      WHEN char_length(word) > length THEN concat(substring(word,1,length-1),'...')
      ELSE word
    END
$$ LANGUAGE SQL;

DROP FUNCTION IF EXISTS format_number(numeric);
CREATE OR REPLACE FUNCTION format_number(n numeric) RETURNS text AS $$
  SELECT
    CASE
      WHEN CAST(n AS bigint) = n THEN TRIM(to_char(n,'999,999,999'))
      ELSE TRIM(to_char(n,'999,999,999.FM99999999999'))
    END
$$ LANGUAGE SQL;

DROP FUNCTION IF EXISTS imperial_weight(numeric);
CREATE FUNCTION imperial_weight(numeric) RETURNS TEXT AS $$
  SELECT
    CASE
      WHEN $1 < 2204.62 THEN concat( format_number($1),' lbs.')
      WHEN $1 >= 2204.62 THEN concat( format_number(ROUND($1/2204.62,2)),' metric tons')
	  ELSE trim(to_char($1,'99999999.999'))
    END
$$ LANGUAGE SQL;

DROP FUNCTION IF EXISTS get_participant_age(bigint,date,date);
CREATE OR REPLACE FUNCTION get_participant_age(age bigint, date_of_birth date, at_date date) RETURNS integer AS $$
  SELECT
    CASE
      WHEN age IS NOT NULL THEN CAST(age as integer)
      WHEN date_of_birth IS NOT NULL THEN CAST(EXTRACT(year from AGE(at_date, date_of_birth)) as integer)
      ELSE NULL
    END
$$ LANGUAGE  SQL;

DROP FUNCTION IF EXISTS smart_pad(text,bigint,text);
CREATE OR REPLACE FUNCTION smart_pad(val text, places bigint, padding text) RETURNS text AS $$
  SELECT
    CASE
      WHEN char_length(cast(val as text)) >= places THEN cast(val as text)
      ELSE lpad(cast(val as text),cast(places as integer),padding)
    END
$$ LANGUAGE  SQL;

DROP FUNCTION IF EXISTS measurements_for_capture(bigint);
CREATE OR REPLACE FUNCTION measurements_for_capture(bigint) RETURNS text[] AS $$
  SELECT
    array_agg(concat(format_number(capture_measurements.measurement), ' ', units.abbreviation))
  FROM
    capture_measurements LEFT JOIN units ON capture_measurements.unit_id = units.id
  WHERE
    capture_measurements.capture_id = $1
$$ LANGUAGE  SQL;
