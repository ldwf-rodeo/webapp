CREATE TABLE IF NOT EXISTS gapless_sequence_recycled_numbers (
  id bigserial not null primary key,
  sequence_name text NOT NULL,
  number bigint NOT NULL
);

--ALTER TABLE gapless_sequence_recycled_numbers ADD CONSTRAINT uq_gapless_sequence_recycled_numbers_sequence_name_and_number UNIQUE (sequence_name,number);

--FUNCTION TO CREATE SEQUENCE IF IT DOESN'T ALREADY EXIST...
DROP FUNCTION IF EXISTS gapless_sequence_create (text);
CREATE FUNCTION gapless_sequence_create (seq_name text) RETURNS VOID AS $$
  BEGIN
    IF EXISTS (SELECT 0 FROM pg_class WHERE relname = $1) THEN
      RETURN;
    ELSE
      -- Initialize to 0 and then immediately call nextval to populate currval.
      EXECUTE 'CREATE SEQUENCE ' || $1 || ' minvalue 0;';
      PERFORM nextval($1);
    END IF;
  END
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hacked_currval (text);
CREATE FUNCTION hacked_currval (seq_name text) RETURNS bigint AS $$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    EXECUTE 'SELECT last_value FROM ' || $1 || ';' INTO var;
    return var;
  END;
$$ LANGUAGE plpgsql;

--FUNCTION TO RECYCLE A NUMBER INTO A SEQUENCE...
DROP FUNCTION IF EXISTS gapless_sequence_recycle(text,bigint);
CREATE FUNCTION gapless_sequence_recycle(seq_name text, rec_number bigint) RETURNS VOID AS $$
  BEGIN
    PERFORM gapless_sequence_create($1);
    IF EXISTS (SELECT 0 FROM gapless_sequence_recycled_numbers WHERE sequence_name = $1 AND number = $2) THEN
      RAISE WARNING 'Number % has already been recycled from sequence %.', $2, $1;
    ELSIF (hacked_currval($1) < $2) THEN
      RAISE WARNING 'Cannot recycle % from sequence % as it is greater than the sequence value.', $2, $1;
    ELSE
      -- Recycle the number.
      INSERT INTO gapless_sequence_recycled_numbers ("sequence_name","number") VALUES ($1,$2);
    END IF;
  END
$$ LANGUAGE plpgsql;

--FUNCTION TO FETCH EITHER A RECYCLED NUMBER OR THE NEXT VALUE OF A SEQUENCE...
DROP FUNCTION IF EXISTS gapless_sequence_nextval(text);
CREATE FUNCTION gapless_sequence_nextval(seq_name text) RETURNS bigint AS $$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    SELECT number INTO var FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 ORDER BY number ASC LIMIT 1;
    IF (var IS NULL) THEN
      --Nothing recycled. Call the sequence.
      var := nextval($1);
    ELSE
      --We have a recycled number. Remove the entry and return it.
      DELETE FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND number = var;
    END IF;
    RETURN var;
  END;
$$ LANGUAGE plpgsql;

--FUNCTION TO REMOVE A SEQUENCE AND PURGE RECYCLED VALUES...
DROP FUNCTION IF EXISTS gapless_sequence_destroy(text);
CREATE FUNCTION gapless_sequence_destroy(seq_name text) RETURNS VOID AS $$
  BEGIN
    IF EXISTS (SELECT 0 FROM pg_class WHERE relname = $1) THEN
      EXECUTE 'DROP SEQUENCE IF EXISTS ' || $1 || ';';
    END IF;
    DELETE FROM gapless_sequence_recycled_numbers WHERE sequence_name = $1;
  END
$$ LANGUAGE plpgsql;

--FUNCTION TO SYNC A SEQUENCE WITH VALUES FROM A TABLE.
DROP FUNCTION IF EXISTS gapless_sequence_sync_with_values(text, text, text);
CREATE FUNCTION gapless_sequence_sync_with_values(seq_name text, table_name text, column_name text) RETURNS VOID AS $$
  BEGIN
    PERFORM gapless_sequence_destroy($1);
    EXECUTE 'SELECT gapless_sequence_getval(''' || $1 || ''',' || $3 || ') FROM ' || $2 || ' ORDER BY ID ASC;';
  END
$$ LANGUAGE plpgsql;

--FUNCTION TO GET AN EXPLICIT VALUE FROM A GAPLESS SEQUENCE...
DROP FUNCTION IF EXISTS gapless_sequence_getval(text,bigint);
CREATE FUNCTION gapless_sequence_getval(seq_name text, requested_number bigint) RETURNS bigint AS $$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    IF ($2 IS NULL) THEN
      -- If the requested number is NULL, then just return the next value.
      RETURN gapless_sequence_nextval($1);
    END IF;
    SELECT "number" INTO var FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND "number" = $2 LIMIT 1;
      IF (var IS NULL) THEN
        --It is not recycled.
        IF (hacked_currval($1) >= $2) THEN
          --The number has already been passed up. We can't get it.
          RAISE EXCEPTION 'The gapless value % is not available for sequence %.', $2, $1;
        ELSE
          --Recycle all un-recycled numbers less than the requested number.
          WITH trashed AS (
            SELECT
              $1 as sequence_name,
              series."number" as "number"
            FROM
              generate_series(hacked_currval($1)+1,$2-1) as series("number")
              LEFT JOIN
                gapless_sequence_recycled_numbers
              ON
                series."number" = gapless_sequence_recycled_numbers."number"
                AND gapless_sequence_recycled_numbers.sequence_name = $1
            WHERE
              gapless_sequence_recycled_numbers."number" IS NULL
            ORDER BY
              series."number"
          )
          INSERT INTO gapless_sequence_recycled_numbers (sequence_name,"number") SELECT * FROM trashed;
          --Bump up the sequence.
          EXECUTE 'ALTER SEQUENCE ' || $1 || ' RESTART WITH ' || $2 || ';';
          --Get the value.
          var := nextval($1);
          RETURN var;
        END IF;
      ELSE
        --The requested number is already recycled. Just get it.
        DELETE FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND "number" = var;
        RETURN var;
      END IF;
  END;
$$ LANGUAGE plpgsql;