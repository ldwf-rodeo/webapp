--================================================================
--================================================================
-- CREATE TYPES
--================================================================
--================================================================

DROP AGGREGATE IF EXISTS array_cat_agg(anyarray);

CREATE AGGREGATE array_cat_agg(anyarray) (
  SFUNC = array_cat,
  STYPE = anyarray
);

DROP AGGREGATE IF EXISTS my_array_sum(anyarray);

CREATE AGGREGATE my_array_sum (anyarray)(
  SFUNC = array_cat,
  STYPE = anyarray,
  INITCOND = '{}'
);

DROP TYPE IF EXISTS leaderboard_point_type CASCADE;

CREATE TYPE leaderboard_point_type AS (
  point_index bigint,
  category_id int,
  participant_id bigint,
  team_id bigint,
  points numeric,
  unit_id bigint,
  capture_entered_at timestamp with time zone
);

DROP TYPE IF EXISTS leaderboard_place_type CASCADE;

CREATE TYPE leaderboard_place_type AS (
  category_id int,
  position bigint,
  is_empty boolean,
  team_position bigint,
  participant_id bigint,
  team_id bigint,
  grouping_index bigint,
  capture_id bigint,
  contributing_captures bigint[],
  contributing_ranks bigint[][], -- {category_id, position, points}
  score numeric,
  unit_id bigint,
  capture_count bigint,
  last_capture_at timestamp with time zone,
  first_capture_at timestamp with time zone,
  smallest_subscore numeric,
  largest_subscore numeric
);

DROP TYPE IF EXISTS leaderboard_bump_type CASCADE;

CREATE TYPE leaderboard_bump_type AS (
 category_id int,
 unit_id bigint,

 participant_id bigint,
 team_id bigint,
 old_position bigint,
 new_position bigint,
 score numeric,

 usurping_participant_id bigint,
 usurping_team_id bigint,
 usurping_score numeric

);

--================================================================
--================================================================
-- MAKE THE LEADERBOARD
--================================================================
--================================================================

-- The workhorse that computes the rankings for a given event.
DROP FUNCTION IF EXISTS get_leaderboard_for_event(bigint);
DROP FUNCTION IF EXISTS get_leaderboard_for_event(bigint);
DROP FUNCTION IF EXISTS get_leaderboard_for_event(bigint, timestamp with time zone);
CREATE FUNCTION get_leaderboard_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_place_type AS $$

  WITH

  category_information AS (
    SELECT
      categories.id AS category_id,
      categories.start_age AS category_start_age,
      categories.end_age AS category_end_age,
      categories.gender_id AS category_gender_id,
      categories.category_type AS category_type,
      categories.positions AS category_positions,
      categories.name AS category_name,
      categories.score_entity AS category_score_entity,
      categories.score_grouping AS category_score_grouping,
      categories.score_grouping_limit AS category_score_grouping_limit,
      categories.has_distinct_places as category_has_distinct_places,
      categories.valid_start_time as category_valid_start_time,
      categories.valid_end_time as category_valid_end_time,
      categories.premium_category_grouping_id as category_premium_category_grouping_id,
      categories.score_1_source, categories.score_2_source, categories.score_3_source,
      categories.score_1_order, categories.score_2_order, categories.score_3_order
    FROM
      categories
      INNER JOIN super_categories ON categories.super_category_id = super_categories.id
      INNER JOIN events ON super_categories.event_id = events.id AND categories.positions > 0
    WHERE
      events.id = $1
      AND categories.category_type IN ('species_unit','species_points','species_capture')
  ),

  potential_captures AS (
     SELECT
      captures.entered_at AS capture_entered_at,
      captures.id AS capture_id,
      captures.species_id AS species_id,
      participants.id AS participant_id,
      participants.gender_id AS participant_gender_id,
      teams.id AS team_id,
      category_information.*
    FROM
      captures
      INNER JOIN species ON species.id = captures.species_id
      INNER JOIN participants ON participants.id = captures.participant_id
      INNER JOIN participations ON participations.participant_id = participants.id
      INNER JOIN teams on teams.id = participations.team_id AND teams.event_id = $1
      INNER JOIN events on events.id = teams.event_id
      INNER JOIN category_information ON ( TRUE
        -- Join each capture on each of the categories that it qualifies for.
        AND (category_information.category_gender_id IS NULL OR participants.gender_id = category_information.category_gender_id)
        AND (category_information.category_start_age IS NULL OR (participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,participants.date_of_birth,CAST(events.start_date AS date)) >= category_information.category_start_age))
        AND (category_information.category_end_age   IS NULL OR (participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,participants.date_of_birth,CAST(events.start_date AS date)) <= category_information.category_end_age))
        AND (category_information.category_valid_start_time IS NULL OR captures.entered_at >= category_information.category_valid_start_time)
        AND (category_information.category_valid_end_time   IS NULL OR captures.entered_at <= category_information.category_valid_end_time)
        AND (
          --Check that the participant's team qualifies if the category belongs to a premium grouping.
          category_information.category_premium_category_grouping_id IS NULL
          OR EXISTS (
            SELECT
              *
            FROM
              team_premium_category_groupings
              INNER JOIN premium_category_groupings ON premium_category_groupings.id = team_premium_category_groupings.premium_category_grouping_id
              INNER JOIN categories ON categories.premium_category_grouping_id = premium_category_groupings.id
            WHERE
              categories.id = category_information.category_id
              AND team_premium_category_groupings.team_id = teams.id
          )
        )
      )
    WHERE
      (snapshot IS NULL OR captures.entered_at < snapshot)
      AND captures.event_id = $1
  ),

  potential_measurements AS (
    SELECT
      potential_captures.*,
      capture_measurements.measurement AS capture_measurement,
      units.id AS unit_id
    FROM
      potential_captures
      INNER JOIN capture_measurements ON capture_measurements.capture_id = potential_captures.capture_id
      INNER JOIN units ON units.id = capture_measurements.unit_id
  ),

  -- Fit each raw value into the appropriate category item (if it fits) and find the measurement or point value.
  species_points AS (
   SELECT
     row_number() OVER () AS species_points_index,
     *
   FROM (
     SELECT
       potential_measurements.capture_measurement AS points,
       potential_measurements.*
     FROM
       potential_measurements
       INNER JOIN category_species_unit_items ON category_species_unit_items.category_id = potential_measurements.category_id AND category_species_unit_items.species_id = potential_measurements.species_id AND category_species_unit_items.unit_id = potential_measurements.unit_id
     WHERE
       potential_measurements.category_type = 'species_unit'

     UNION ALL

      SELECT
        category_species_points_items.points as points,
        potential_captures.*,
        NULL AS capture_measurement,                -- species_points are not based on a measurement, so null these out.
        NULL AS unit_id
      FROM
        potential_captures
        INNER JOIN category_species_points_items ON category_species_points_items.category_id = potential_captures.category_id AND category_species_points_items.species_id = potential_captures.species_id
      WHERE
        potential_captures.category_type = 'species_points'

     UNION ALL

     SELECT
       1 AS points,
       potential_captures.*,
       NULL AS capture_measurement,                -- species_capture are not based on a measurement, so null these out.
       NULL AS unit_id
     FROM
       potential_captures
       INNER JOIN category_species_capture_items ON category_species_capture_items.category_id = potential_captures.category_id AND category_species_capture_items.species_id = potential_captures.species_id
     WHERE
       potential_captures.category_type = 'species_capture'
    ) t
  ),

  ------------------------------------------------------------------
  -- SPECIES SCORES
  ------------------------------------------------------------------

  -- Group and sort appropriately and then possibly filter out only the top N values.
  species_partitioned_points AS (
    SELECT
      *
    FROM
      (
        SELECT
          row_number() OVER (
            PARTITION BY
              species_points.category_id,
              CASE WHEN species_points.category_score_entity = 'team' THEN species_points.team_id END,
              CASE WHEN species_points.category_score_entity = 'participant' THEN species_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN points END DESC
          ) as top_point_rank,
          row_number() OVER (
            PARTITION BY
              species_points.category_id,
              species_points.species_id,
              CASE WHEN species_points.category_score_entity = 'team' THEN species_points.team_id END,
              CASE WHEN species_points.category_score_entity = 'participant' THEN species_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN points END DESC
          ) as each_species_rank,
          species_points.*
        FROM
          species_points
      ) t
  ),

  --Find out everybody's score for each category.
  species_scores AS (
   SELECT
     category_score_entity,
     category_has_distinct_places,
     category_score_grouping,
     category_id,
     score_1_source, score_2_source, score_3_source,
     score_1_order, score_2_order, score_3_order,
     COUNT(capture_id) AS capture_count,
     array_agg(capture_id) AS contributing_captures,
     CAST (CASE
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN MAX(species_partitioned_points.species_points_index)
        WHEN species_partitioned_points.category_score_grouping = 'top_each' THEN MAX(species_partitioned_points.each_species_rank)
        WHEN species_partitioned_points.category_score_grouping = 'top_n' THEN floor((MAX(species_partitioned_points.top_point_rank)-1)/MAX(category_score_grouping_limit))
        WHEN species_partitioned_points.category_score_grouping = 'all' THEN 1
     END AS bigint) AS grouping_index,
     CASE
       WHEN species_partitioned_points.category_score_entity = 'team' THEN NULL
       WHEN species_partitioned_points.category_score_entity = 'participant' THEN MAX(participant_id) -- Hack
     END AS participant_id,
     MAX(team_id) AS team_id, -- Hack
     CASE
        -- If this refers to a specific single capture, keep the reference.
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN MAX(capture_id)
        ELSE NULL
     END as capture_id,
     SUM(points) AS score, -- Not a hack
     MAX(capture_entered_at) AS last_capture_at,
     MIN(capture_entered_at) AS first_capture_at,
     MAX(points) as largest_subscore,
     MIN(points) as smallest_subscore,
     unit_id AS unit_id
   FROM
     species_partitioned_points
   GROUP BY
     category_id,
     category_score_entity,
     category_has_distinct_places,
     category_score_grouping,
     score_1_source, score_2_source, score_3_source,
     score_1_order, score_2_order, score_3_order,
     unit_id,
     CASE
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN species_partitioned_points.species_points_index
        WHEN species_partitioned_points.category_score_grouping = 'top_each' THEN species_partitioned_points.each_species_rank
        WHEN species_partitioned_points.category_score_grouping = 'top_n' THEN floor((species_partitioned_points.top_point_rank-1)/category_score_grouping_limit)
        -- The 'all' case will group on everything.
     END,
     CASE
        -- If we are grouping scores, then also group by the appropriate entity.
        WHEN species_partitioned_points.category_score_grouping <> 'none' AND species_partitioned_points.category_score_entity = 'team' THEN species_partitioned_points.team_id
        WHEN species_partitioned_points.category_score_grouping <> 'none' AND species_partitioned_points.category_score_entity = 'participant' THEN species_partitioned_points.participant_id
        ELSE NULL
     END
  ),

  -- If the positions must be unique, then we purge out redundant entries.
  species_filtered_scores AS (
    SELECT
      *
    FROM (
      SELECT
        *,
        row_number() OVER (
          PARTITION BY
            category_id,
            CASE
              WHEN category_score_entity = 'team' THEN team_id
              WHEN category_score_entity = 'participant' THEN participant_id
            END
          ORDER BY
            -- FIRST
            CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
            CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
            -- SECOND
            CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
            CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
            -- THIRD
            CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
            CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as member_position
      FROM
        species_scores
    ) t
    WHERE
      category_has_distinct_places = FALSE OR member_position = 1
  ),

  ------------------------------------------------------------------
  -- SPECIES RANKS
  ------------------------------------------------------------------

  -- Now order appropriately and find everyone's actual rank.
  species_ranks AS (
    SELECT
      category_id,
      -- We don't need distinct positions, so just partition over each category.
      row_number() OVER (
        PARTITION BY category_id
        ORDER BY
          -- FIRST
          CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
          CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
          -- SECOND
          CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
          CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
          -- THIRD
          CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
          CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
      ) as position,
      participant_id,
      team_id,
      grouping_index as grouping_index,
      capture_id AS capture_id,
      contributing_captures,
      ARRAY[]::bigint[][] AS contributing_ranks,
      score,
      unit_id,
      capture_count,
      last_capture_at,
      first_capture_at,
      smallest_subscore,
      largest_subscore
    FROM
      species_filtered_scores
  ),

  ------------------------------------------------------------------
  -- GROUPING POINTS
  ------------------------------------------------------------------

  grouping_points AS (
    SELECT
      row_number() OVER () as grouping_points_index,
      group_categories.id as category_id,
      ranked_categories.id as ranking_category_id,
      group_categories.score_entity as category_score_entity,
      group_categories.score_grouping as category_score_grouping,
      group_categories.score_grouping_limit as category_score_grouping_limit,
      group_categories.has_distinct_places as category_has_distinct_places,
      group_categories.score_1_source, group_categories.score_2_source, group_categories.score_3_source,
      group_categories.score_1_order, group_categories.score_2_order, group_categories.score_3_order,
      species_ranks.participant_id as participant_id,
      species_ranks.team_id as team_id,
      cast(category_grouping_items.points as numeric) AS points,
      capture_count as capture_count,
      species_ranks.contributing_captures AS contributing_captures,
      ARRAY[species_ranks.category_id, species_ranks.position, category_grouping_items.points] as contributing_rank,
      last_capture_at as last_capture_at,
      first_capture_at as first_capture_at,
      largest_subscore as largest_subscore,
      smallest_subscore as smallest_subscore
    FROM
      species_ranks
      INNER JOIN categories AS ranked_categories ON ranked_categories.id = species_ranks.category_id
      INNER JOIN super_categories AS ranked_super_categories ON ranked_super_categories.id = ranked_categories.super_category_id
      LEFT JOIN participants AS ranked_participants ON species_ranks.participant_id = ranked_participants.id
      LEFT JOIN teams AS ranked_teams ON species_ranks.team_id = ranked_teams.id
      INNER JOIN category_grouping_items_super_categories ON category_grouping_items_super_categories.super_category_id = ranked_super_categories.id
      INNER JOIN category_grouping_items ON category_grouping_items.id = category_grouping_items_super_categories.category_grouping_item_id AND category_grouping_items.place = species_ranks.position AND category_grouping_items.place <= ranked_categories.positions
      INNER JOIN categories AS group_categories ON group_categories.id = category_grouping_items.category_id
      INNER JOIN super_categories AS group_super_categories ON group_categories.super_category_id = group_super_categories.id
      INNER JOIN events ON events.id = group_super_categories.event_id
    WHERE
      group_categories.category_type = 'group'
      AND ranked_categories.category_type IN ('species_unit','species_points','species_capture') --Don't allow groups of groups.
      -- Setting an age/gender limit on a grouping category forces us to ignore team categories.
      AND (group_categories.gender_id IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.gender_id = group_categories.gender_id))
      AND (group_categories.start_age IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,ranked_participants.date_of_birth,CAST(events.start_date as date)) >= group_categories.start_age))
      AND (group_categories.end_age   IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,ranked_participants.date_of_birth,CAST(events.start_date as date)) <= group_categories.end_age))
      -- A valid_start/end time for a grouping category means that each category's valid start/end times must be within this range.
      AND (group_categories.valid_start_time IS NULL OR (ranked_categories.valid_start_time IS NOT NULL AND ranked_categories.valid_start_time >= group_categories.valid_start_time))
      AND (group_categories.valid_end_time   IS NULL OR (ranked_categories.valid_end_time   IS NOT NULL AND ranked_categories.valid_end_time <= group_categories.valid_end_time))
      AND (
        --Check that the participant's team qualifies if the category belongs to a premium grouping.
        group_categories.premium_category_grouping_id IS NULL
        OR EXISTS (
            SELECT
              *
            FROM
              team_premium_category_groupings
              INNER JOIN premium_category_groupings ON premium_category_groupings.id = team_premium_category_groupings.premium_category_grouping_id
              INNER JOIN categories ON categories.premium_category_grouping_id = premium_category_groupings.id
            WHERE
              categories.id = group_categories.id
              AND team_premium_category_groupings.team_id = ranked_teams.id
        )
    )
  ),

  ------------------------------------------------------------------
  -- GROUPING SCORES
  ------------------------------------------------------------------

  -- Group and sort appropriately and then possibly filter out only the top N values.
  grouping_partitioned_points AS (
    SELECT
      *
    FROM
      (
        SELECT
          row_number() OVER (
            PARTITION BY
              grouping_points.category_id,
              CASE WHEN grouping_points.category_score_entity = 'team' THEN grouping_points.team_id END,
              CASE WHEN grouping_points.category_score_entity = 'participant' THEN grouping_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as top_point_rank,
          row_number() OVER (
            PARTITION BY
              grouping_points.category_id,
              grouping_points.ranking_category_id,
              CASE WHEN grouping_points.category_score_entity = 'team' THEN grouping_points.team_id END,
              CASE WHEN grouping_points.category_score_entity = 'participant' THEN grouping_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as each_category_rank,
        grouping_points.*
        FROM
          grouping_points
      ) t
  ),

  --Find out everybody's score for each category.
  grouping_scores AS (
     SELECT
       category_score_entity,
       category_has_distinct_places,
       category_id,
       score_1_source, score_2_source, score_3_source,
       score_1_order, score_2_order, score_3_order,
       CAST (CASE
          WHEN grouping_partitioned_points.category_score_grouping = 'none' THEN MAX(grouping_partitioned_points.grouping_points_index)
          WHEN grouping_partitioned_points.category_score_grouping = 'top_each' THEN MAX(grouping_partitioned_points.each_category_rank)
          WHEN grouping_partitioned_points.category_score_grouping = 'top_n' THEN floor((MAX(grouping_partitioned_points.top_point_rank)-1)/MAX(category_score_grouping_limit))
          WHEN grouping_partitioned_points.category_score_grouping = 'all' THEN 1
       END AS bigint) AS grouping_index,
       CASE
         WHEN grouping_partitioned_points.category_score_entity = 'team' THEN NULL
         WHEN grouping_partitioned_points.category_score_entity = 'participant' THEN MAX(participant_id) -- Hack
       END AS participant_id,
       array_cat_agg(contributing_captures) AS redundant_contributing_captures,
       my_array_sum(ARRAY[contributing_rank]) as contributing_ranks,
       MAX(team_id) AS team_id, -- Hack
       SUM(points) AS score, -- Not a hack
       CAST(NULL AS bigint) AS unit_id,
       MAX(last_capture_at) AS last_capture_at,
       MIN(first_capture_at) as first_capture_at,
       MAX(points) as largest_subscore,
       MIN(points) as smallest_subscore
     FROM
       grouping_partitioned_points
     GROUP BY
       category_id,
       category_score_entity,
       category_score_grouping,
       category_has_distinct_places,
       score_1_source, score_2_source, score_3_source,
       score_1_order, score_2_order, score_3_order,
       CASE
          WHEN grouping_partitioned_points.category_score_grouping = 'none' THEN grouping_partitioned_points.grouping_points_index
          WHEN grouping_partitioned_points.category_score_grouping = 'top_each' THEN grouping_partitioned_points.each_category_rank
          WHEN grouping_partitioned_points.category_score_grouping = 'top_n' THEN floor((grouping_partitioned_points.top_point_rank-1)/category_score_grouping_limit)
          -- The 'all' case will group on everything.
       END,
       CASE
          -- If we are grouping scores, then also group by the appropriate entity.
          WHEN grouping_partitioned_points.category_score_grouping <> 'none' AND grouping_partitioned_points.category_score_entity = 'team' THEN grouping_partitioned_points.team_id
          WHEN grouping_partitioned_points.category_score_grouping <> 'none' AND grouping_partitioned_points.category_score_entity = 'participant' THEN grouping_partitioned_points.participant_id
          ELSE NULL
       END
  ),

  -- Capture_count will not be accurate since we may be taking from multiple categories in the grouping aggregate.
  corrected_grouping_scores AS (
      SELECT
        *,
        ARRAY(SELECT DISTINCT UNNEST(redundant_contributing_captures)) as contributing_captures,
        (SELECT COUNT(*) FROM (SELECT DISTINCT UNNEST(redundant_contributing_captures)) t) as capture_count
      FROM
        grouping_scores
  ),

  -- If the positions must be unique, then we purge out redundant entries.
  grouping_filtered_scores AS (
    SELECT
      *
    FROM (
      SELECT
        *,
        row_number() OVER (
          PARTITION BY
            category_id,
            CASE
              WHEN category_score_entity = 'team' THEN team_id
              WHEN category_score_entity = 'participant' THEN participant_id
            END
          ORDER BY
            -- FIRST
            CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
            CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
            -- SECOND
            CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
            CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
            -- THIRD
            CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
            CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as member_position
      FROM
        corrected_grouping_scores
    ) t
    WHERE
      category_has_distinct_places = FALSE OR member_position = 1
  ),

  ------------------------------------------------------------------
  -- GROUPING RANKS
  ------------------------------------------------------------------

  -- Now order appropriately and find everyone's actual rank.
  grouping_ranks AS (
    SELECT
      category_id,
      -- We don't need distinct positions, so just partition over each category.
      row_number() OVER (
        PARTITION BY category_id
        ORDER BY
          -- FIRST
          CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
          CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
          -- SECOND
          CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
          CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
          -- THIRD
          CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
          CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
      ) as position,
      participant_id,
      team_id,
      grouping_index as grouping_index,
      CAST(NULL AS bigint) AS capture_id,
      contributing_captures,
      contributing_ranks,
      score,
      unit_id,
      capture_count,
      last_capture_at,
      first_capture_at,
      smallest_subscore,
      largest_subscore
    FROM
      grouping_filtered_scores
  ),

  ------------------------------------------------------------------
  -- COMBINE SPECIES AND GROUPING
  ------------------------------------------------------------------

   both_ranks AS (
    SELECT * FROM species_ranks
    UNION ALL
    SELECT * FROM grouping_ranks
   )

  ------------------------------------------------------------------
  -- NOW JUST SELECT
  ------------------------------------------------------------------

  SELECT
    both_ranks.category_id,
    both_ranks.position,
    both_ranks.team_id IS NULL as is_empty,
    row_number() OVER (PARTITION BY both_ranks.category_id, both_ranks.team_id ORDER BY both_ranks.category_id, both_ranks.position) as team_position,
    both_ranks.participant_id,
    both_ranks.team_id,
    both_ranks.grouping_index,
    both_ranks.capture_id,
    both_ranks.contributing_captures,
    both_ranks.contributing_ranks,
    both_ranks.score,
    both_ranks.unit_id,
    both_ranks.capture_count,
    both_ranks.last_capture_at,
    both_ranks.first_capture_at,
    both_ranks.smallest_subscore,
    both_ranks.largest_subscore
  FROM
    both_ranks

$$ LANGUAGE SQL;