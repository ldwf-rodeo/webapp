# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

######################################################
####            GLOBAL INFORMATION
######################################################

%w(admin event_admin organizer capture_helper).each do |role|
  Role.where(:name => role).first_or_create!
end

# add a user to the system as an adminPp
if User.where(:user_name => 'testadmin').first.nil?
  User.create(:user_name => 'testadmin', :password => 'thelocalFishmonger1', :password_confirmation => 'thelocalFishmonger1')
  User.where(:user_name => 'testadmin').first.add_role :admin
end

# add a non admin user to the database
if User.where(:user_name => 'test').first.nil?
  User.create(:user_name => 'test', :password => 'thelocalFishmonger1', :password_confirmation => 'thelocalFishmonger1')
end

Species.where(name: 'Barracuda').first_or_create!

PRESERVED_TAGS.each do |k,v|
  Tag.where(name: v, permanent: true).first_or_create
end

# groupings for the faux pas events
groups = [
    { name: 'Inshore', price: 90.00},
    { name: 'Inshore Grand Slam', price: 100.00},
    { name: 'Offshore', price: 150.00},
    { name: 'Offshore Grand Slam', price: 150.00},
    { name: 'Rig', price: 90.00},
    { name: 'Rig Grand Slam', price: 100.00},
    { name: 'Live Redfish Stringer (3)', price: 100.00},
    { name: 'Speckled Trout Stringer (5)', price: 100.00}
]

# adds the groupings to the Faux Pas events
Event.where("name ilike '%Faux%'").each do |event|
  groups.each do |group|
    PremiumCategoryGrouping.where(name: group[:name], price: group[:price], event: event).first_or_create!
  end
end

# adjust the teams to be in the proper groupings
Event.where("name ilike '%Faux%'").each do |event|
  teams = event.teams.reject {|m| m.premium_categories.count == 0}

  teams.each do |team|
    team.premium_categories.each do |premium_category|

      sc = premium_category.super_category
      event.premium_category_groupings.each do |group|
        TeamPremiumCategoryGrouping.where(premium_category_grouping: group, team: team ).first_or_create! if sc.name.include?(group.name)
        TeamPremiumCategoryGrouping.where(premium_category_grouping: group, team: team ).first_or_create! if group.name.include?('Redfish Stringer') && premium_category.name.include?('Redfish Stringer')
        TeamPremiumCategoryGrouping.where(premium_category_grouping: group, team: team ).first_or_create! if group.name.include?('Trout Stringer') && premium_category.name.include?('Trout Stringer')
        TeamPremiumCategoryGrouping.where(premium_category_grouping: group, team: team ).first_or_create! if group.name.include?('Grand') && sc.name.include?(group.name.split(' ').first) && premium_category.name.include?('Grand')
      end

    end
  end

  event.categories.where(is_premium: true).each do |premium_category|
    sc = premium_category.super_category
    event.premium_category_groupings.each do |group|
      if sc.name.include?(group.name) || (group.name.include?('Redfish Stringer') && premium_category.name.include?('Redfish Stringer')) || (group.name.include?('Trout Stringer') && premium_category.name.include?('Trout Stringer')) || (group.name.include?('Grand') && sc.name.include?(group.name.split(' ').first) && premium_category.name.include?('Grand'))
        premium_category.premium_category_grouping = group
        premium_category.save!
      end
    end
  end
end



