-- object: public.events_users | type: TABLE -- 
CREATE TABLE public.events_users(
	id_events bigint,
	id_users bigint,
	CONSTRAINT events_users_pk PRIMARY KEY (id_events,id_users)
)
WITH (OIDS=FALSE);
-- ddl-end --

-- ddl-end --

-- object: events_fk | type: CONSTRAINT -- 
ALTER TABLE public.events_users ADD CONSTRAINT events_fk FOREIGN KEY (id_events)
REFERENCES public.events (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: users_fk | type: CONSTRAINT -- 
ALTER TABLE public.events_users ADD CONSTRAINT users_fk FOREIGN KEY (id_users)
REFERENCES public.users (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --
