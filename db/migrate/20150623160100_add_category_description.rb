class AddCategoryDescription < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD COLUMN description text;
      ALTER TABLE categories ADD COLUMN additional_description text;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE categories DROP COLUMN description;
      ALTER TABLE categories DROP COLUMN additional_description;
    SQL
  end


end