class AddCategorySubtitle < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD COLUMN subtitle text;
    SQL
  end


end