class AddDisplayMode < ActiveRecord::Migration

  def up
    execute <<-SQL
      CREATE TYPE display_mode AS ENUM ('score_and_unit','smallest_and_largest','score_and_unit_and_count');
      ALTER TABLE categories ADD COLUMN display_mode display_mode;
      UPDATE categories SET display_mode = 'score_and_unit';
      ALTER TABLE categories ALTER COLUMN display_mode SET NOT NULL;
    SQL
  end


end