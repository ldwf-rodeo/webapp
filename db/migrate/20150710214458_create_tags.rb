class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name, uniq: true, null: false

      t.timestamps null: false
    end
  end
end
