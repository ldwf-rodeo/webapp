class CapturesEventIdRequired < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE captures ALTER COLUMN event_id SET NOT NULL;
    SQL
  end

end