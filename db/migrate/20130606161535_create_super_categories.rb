class CreateSuperCategories < ActiveRecord::Migration
  def self.up
    create_table :super_categories do |t|
      t.string :name
      t.belongs_to :event
      t.timestamps
    end

    change_table :categories do |t|
      t.belongs_to :super_category
      t.remove :event_id

    end
  end

  def self.down
    drop_table :super_categories

    change_table :categories do |t|
      t.remove :super_category_id
      t.belongs_to :event
    end
  end
end
