class UpdateSuperCategories < ActiveRecord::Migration

  def up
    execute <<-SQL
      UPDATE super_categories SET page = 1 WHERE page IS NULL;
      UPDATE super_categories SET position = 1 WHERE position IS NULL;

      ALTER TABLE super_categories ALTER COLUMN page SET NOT NULL;
      ALTER TABLE super_categories ALTER COLUMN position SET NOT NULL;
    SQL
  end

end