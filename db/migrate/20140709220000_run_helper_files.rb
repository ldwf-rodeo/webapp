class RunHelperFiles < ActiveRecord::Migration

  def self.up
    sql = ''
    source = File.new("./db/sql/utilities.sql", "r")
    while (line = source.gets)
      sql << line
    end
    source.close
    execute sql

    sql = ''
    source = File.new("./db/sql/gapless_sequence.sql", "r")
    while (line = source.gets)
      sql << line
    end
    source.close
    execute sql
  end
end


