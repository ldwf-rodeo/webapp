class NoMultipleTeamsConstraint < ActiveRecord::Migration
  def up

    execute <<-SQL
      -- SIMPLE FUNCTION THAT FINDS THE MAXIMUM NUMBER OF REGISTRATIONS PER EVENT.
      CREATE FUNCTION max_per_event_team_participant_enrollments () RETURNS bigint AS $$
        SELECT
          MAX(entries_per_event)
        FROM (
          SELECT
            COUNT(*) as entries_per_event
          FROM (
            SELECT
              events.id as event_id, team_participants.participant_id as participant_id, teams.id as team_id
            FROM
              team_participants
              INNER JOIN teams ON teams.id = team_participants.team_id
              INNER JOIN events ON events.id = teams.event_id
          ) t
          GROUP BY event_id, participant_id
        ) t2
        $$ LANGUAGE  SQL;

      --Create the procedure function to raise the exception.
      CREATE OR REPLACE FUNCTION trigger_max_participants_per_team_exception() RETURNS trigger AS $$
        BEGIN
          IF (max_per_event_team_participant_enrollments() > 1) THEN
            RAISE EXCEPTION 'A participant is in multiple teams for one event.';
          END IF;
          return new;
        END;
      $$ LANGUAGE plpgsql;

      -- Establish the trigger.
      CREATE
        TRIGGER tg_max_participants_per_team
      AFTER
        INSERT OR UPDATE
      ON
        team_participants
      EXECUTE PROCEDURE
        trigger_max_participants_per_team_exception();
    SQL

  end

  def down
    execute <<-SQL
      DROP TRIGGER IF EXISTS tg_max_participants_per_team ON team_participants;
      DROP FUNCTION IF EXISTS trigger_max_participants_per_team_exception();
      DROP FUNCTION IF EXISTS max_per_event_team_participant_enrollments ();
    SQL
  end
end