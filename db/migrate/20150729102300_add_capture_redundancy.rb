class AddCaptureRedundancy < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE captures ADD COLUMN is_redundant boolean NOT NULL default false;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE captures DROP COLUMN is_redundant;
    SQL
  end


end
