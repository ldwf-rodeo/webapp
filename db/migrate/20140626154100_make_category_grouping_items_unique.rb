class MakeCategoryGroupingItemsUnique < ActiveRecord::Migration
  def up

    execute <<-SQL

        -- ADD THE CONSTRAINT
        ALTER TABLE category_grouping_items ADD CONSTRAINT uq_category_grouping_items_all UNIQUE (category_id,place) ;
    SQL

  end

  def down
    execute <<-SQL
      ALTER TABLE category_grouping_items DROP CONSTRAINT uq_category_grouping_items_all;
    SQL
  end
end