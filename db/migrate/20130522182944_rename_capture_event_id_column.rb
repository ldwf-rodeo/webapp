class RenameCaptureEventIdColumn < ActiveRecord::Migration
  def change
    rename_column :captures, :events_id, :event_id
  end
end
