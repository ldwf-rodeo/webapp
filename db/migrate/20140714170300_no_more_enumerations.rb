class NoMoreEnumerations < ActiveRecord::Migration

  def up
    execute <<-SQL

      CREATE TABLE genders (
        id bigserial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        abbreviation text NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
      );

      ALTER TABLE genders ADD CONSTRAINT uq_genders_name UNIQUE (name);
      ALTER TABLE genders ADD CONSTRAINT ck_genders_name_valid CHECK (TRIM(name)<>'' AND TRIM(name)=name);
      ALTER TABLE genders ADD CONSTRAINT ck_genders_abbreviation_valid CHECK (TRIM(abbreviation)<>'' AND TRIM(abbreviation)=abbreviation);
      INSERT INTO genders (name,abbreviation,created_at,updated_at) VALUES ('Male','M',now(),now());
      INSERT INTO genders (name,abbreviation,created_at,updated_at) VALUES ('Female','F',now(),now());

      -- MIGRATE
      ALTER TABLE participants ADD COLUMN gender_id bigint REFERENCES genders(id);
      WITH gender_mapping AS (
        SELECT
          t.e, genders.name, genders.id as gender_id
        FROM (
          SELECT 'male'::public.gender as e
          UNION ALL
          SELECT 'female'::public.gender as e
        ) t INNER JOIN genders ON lower(cast(t.e as text)) = lower(genders.name)
      )
      UPDATE participants p
      SET gender_id = gender_mapping.gender_id
      FROM gender_mapping
      WHERE gender_mapping.e = p.gender;

      ALTER TABLE participants ALTER COLUMN gender_id SET NOT NULL;
      ALTER TABLE participants DROP COLUMN gender;

      ALTER TABLE categories ADD COLUMN gender_id bigint REFERENCES genders(id);
      WITH gender_mapping AS (
        SELECT
          t.e, genders.name, genders.id as gender_id
        FROM (
          SELECT 'male'::public.gender as e
          UNION ALL
          SELECT 'female'::public.gender as e
        ) t INNER JOIN genders ON lower(cast(t.e as text)) = lower(genders.name)
      )
      UPDATE categories c
      SET gender_id = gender_mapping.gender_id
      FROM gender_mapping
      WHERE gender_mapping.e = c.gender;

      ALTER TABLE categories DROP COLUMN gender;

      ----------------------------------------------------------------------------------

      CREATE TABLE shirt_sizes (
        id bigserial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        abbreviation text NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
      );

      ALTER TABLE shirt_sizes ADD CONSTRAINT uq_shirt_sizes_name UNIQUE (name);
      ALTER TABLE shirt_sizes ADD CONSTRAINT ck_shirt_sizes_name_valid CHECK (TRIM(name)<>'' AND TRIM(name)=name);
      ALTER TABLE shirt_sizes ADD CONSTRAINT ck_shirt_sizes_abbreviation_valid CHECK (TRIM(abbreviation)<>'' AND TRIM(abbreviation)=abbreviation);
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('Small','S',now(),now());
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('Medium','M',now(),now());
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('Large','L',now(),now());
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('Extra Large','XL',now(),now());
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('2-Extra Large','XXL',now(),now());
      INSERT INTO shirt_sizes (name,abbreviation,created_at,updated_at) VALUES ('3-Extra Large','XXXL',now(),now());

      -- MIGRATE
      ALTER TABLE participants ADD COLUMN shirt_size_id bigint REFERENCES shirt_sizes(id);
      WITH shirt_size_mapping AS (
        SELECT
          t.e, shirt_sizes.name, shirt_sizes.abbreviation, shirt_sizes.id as shirt_size_id
        FROM (
          SELECT 's'::public.shirt_size as e
          UNION ALL
          SELECT 'm'::public.shirt_size as e
          UNION ALL
          SELECT 'l'::public.shirt_size as e
          UNION ALL
          SELECT 'xl'::public.shirt_size as e
          UNION ALL
          SELECT 'xxl'::public.shirt_size as e
          UNION ALL
          SELECT 'xxxl'::public.shirt_size as e
        ) t INNER JOIN shirt_sizes ON lower(cast(t.e as text)) = lower(shirt_sizes.abbreviation)
      )
      UPDATE participants p
      SET shirt_size_id = shirt_size_mapping.shirt_size_id
      FROM shirt_size_mapping
      WHERE shirt_size_mapping.e = p.shirt_size;

      ALTER TABLE participants DROP COLUMN shirt_size;

   --------------------------------------------------------------------------------------------

    CREATE TABLE mobile_carriers (
        id bigserial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        slug text NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL
      );

      ALTER TABLE mobile_carriers ADD CONSTRAINT uq_mobile_carriers_name UNIQUE (name);
      ALTER TABLE mobile_carriers ADD CONSTRAINT ck_mobile_carriers_name_valid CHECK (TRIM(name)<>'' AND TRIM(name)=name);
      ALTER TABLE mobile_carriers ADD CONSTRAINT uq_mobile_carriers_slug UNIQUE (slug);
      ALTER TABLE mobile_carriers ADD CONSTRAINT ck_mobile_carriers_slug_valid CHECK (TRIM(slug)<>'' AND TRIM(slug)=slug);

      INSERT INTO mobile_carriers (name,slug,created_at,updated_at) VALUES ('AT&T','att',now(),now());
      INSERT INTO mobile_carriers (name,slug,created_at,updated_at) VALUES ('Verizon','verizon',now(),now());
      INSERT INTO mobile_carriers (name,slug,created_at,updated_at) VALUES ('T-Mobile','tmobile',now(),now());
      INSERT INTO mobile_carriers (name,slug,created_at,updated_at) VALUES ('Sprint','sprint',now(),now());

      -- MIGRATE
      ALTER TABLE participants ADD COLUMN mobile_carrier_id bigint REFERENCES mobile_carriers(id);

      UPDATE participants p
      SET mobile_carrier_id = mobile_carriers.id
      FROM mobile_carriers
      WHERE lower(mobile_carriers.slug) = lower(p.mobile_carrier);

      ALTER TABLE participants DROP CONSTRAINT ck_participants_phone_check;
      ALTER TABLE participants ADD CONSTRAINT ck_participants_phone_check CHECK ((mobile_phone IS NULL) = (mobile_carrier_id IS NULL));
      ALTER TABLE participants DROP COLUMN mobile_carrier;

    SQL
  end

  def down
    execute <<-SQL
      DROP TABLE IF EXISTS genders;
      DROP TABLE IF EXISTS shirt_sizes;
      DROP TABLE IF EXISTS mobile_carriers;
    SQL
  end

end