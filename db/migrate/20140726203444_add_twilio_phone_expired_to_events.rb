class AddTwilioPhoneExpiredToEvents < ActiveRecord::Migration
  def change
    add_column :events, :twilio_expired, :boolean, default: false

    Event.where('slug != ?', 'faux_pas_14').each do |event|
      event.update_attribute :twilio_expired, true
    end

  end
end
