class AddBaseSpecies < ActiveRecord::Migration

  def up
    execute <<-SQL

      -- Add the base species id column.
      ALTER TABLE species ADD COLUMN base_species_id bigint REFERENCES species (id);
      UPDATE species SET base_species_id = 45 WHERE id = 46; -- Speckled Trout
      UPDATE species SET base_species_id = 32 WHERE id = 34; -- Red Drum

      -- You must have a base species if your quantity is greater than 1. And you can't point to yourself.
      ALTER TABLE species ADD CONSTRAINT ck_species_require_base_species_id CHECK (base_species_id <> id AND NOT (quantity > 1 AND base_species_id IS NULL) );

      -- Also the quantity shouldn't be allowed to be less than 1.
      ALTER TABLE species ADD CONSTRAINT ck_species_quantity_is_positive CHECK (quantity >= 1);

      -- Set timestamps.
      ALTER TABLE species ADD COLUMN created_at timestamp with time zone;
      ALTER TABLE species ADD COLUMN updated_at timestamp with time zone;
      UPDATE species SET created_at = now();
      UPDATE species SET updated_at = now();
      ALTER TABLE species ALTER COLUMN created_at SET NOT NULL;
      ALTER TABLE species ALTER COLUMN updated_at SET NOT NULL;

    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE species DROP CONSTRAINT ck_species_require_base_species_id;
      ALTER TABLE species DROP CONSTRAINT ck_species_quantity_is_positive;
      ALTER TABLE species DROP COLUMN base_species_id;
      ALTER TABLE species DROP COLUMN created_at;
      ALTER TABLE species DROP COLUMN updated_at;
    SQL
  end


end
