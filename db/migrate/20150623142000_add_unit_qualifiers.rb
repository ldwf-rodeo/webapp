class AddUnitQualifiers < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE units ADD COLUMN ascending_qualifier text;
      UPDATE units SET ascending_qualifier = 'lightest';
      ALTER TABLE units ALTER COLUMN ascending_qualifier SET NOT NULL;

      ALTER TABLE units ADD COLUMN descending_qualifier text;
      UPDATE units SET descending_qualifier = 'heaviest';
      ALTER TABLE units ALTER COLUMN descending_qualifier SET NOT NULL;

    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE units DROP COLUMN ascending_qualifier;
      ALTER TABLE units DROP COLUMN descending_qualifier;
    SQL
  end


end