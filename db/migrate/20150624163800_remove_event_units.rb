class RemoveEventUnits < ActiveRecord::Migration

  #Units are now automatically inferred based on categories.
  def up
    execute <<-SQL
      DROP TABLE IF EXISTS event_units;
    SQL
  end

end