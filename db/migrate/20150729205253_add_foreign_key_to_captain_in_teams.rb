class AddForeignKeyToCaptainInTeams < ActiveRecord::Migration
  def change
    add_foreign_key :teams, :participants, column: :captain_id
  end
end
