class AddEventConstraints < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE events ADD CONSTRAINT ck_events_valid_dates CHECK (end_date > start_date);
      ALTER TABLE events DROP COLUMN leaderboard_history;
    SQL
  end

end