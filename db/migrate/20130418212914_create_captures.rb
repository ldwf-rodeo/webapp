class CreateCaptures < ActiveRecord::Migration
  def change
      create_table :captures do |t|
        t.string :species
        t.decimal :weight
        t.decimal :length
        t.datetime :time_entered
        t.integer :user_id

        t.timestamps
      end
  end
end
