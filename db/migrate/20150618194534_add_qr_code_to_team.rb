class AddQrCodeToTeam < ActiveRecord::Migration
  def self.up
    change_table :teams do |t|
      t.attachment :qr_code
    end
  end

  def self.down
    drop_attached_file :teams, :qr_code
  end
end
