class CreateEventScaleOpenings < ActiveRecord::Migration
  def up
    create_table :event_scale_openings do |t|
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false

      t.timestamps
    end

    execute <<-SQL
      -- add the event_id column
      ALTER TABLE event_scale_openings ADD COLUMN event_id bigint NOT NULL;
      ALTER TABLE event_scale_openings ADD COLUMN opening tstzrange NOT NULL;

      -- add the foreign key to the events table
      ALTER TABLE event_scale_openings ADD CONSTRAINT fk_event_scale_openings_event FOREIGN KEY (event_id) REFERENCES events(id);
      -- ensure that the start time is less than the end time
      ALTER TABLE event_scale_openings ADD CONSTRAINT chk_event_scale_openings_start_end CHECK("start_time" < "end_time");
      -- ensure that the times do not overlap for a given angler
      ALTER TABLE event_scale_openings ADD CONSTRAINT "event_scale_openings_overlapping_time_ranges" EXCLUDE USING gist (opening WITH &&, event_id WITH =);
    SQL
  end

  def down
    execute <<-SQL
      -- add the foreign key to the events table
      ALTER TABLE event_scale_openings DROP CONSTRAINT fk_event_scale_openings_event;
      -- ensure that the start time is less than the end time
      ALTER TABLE event_scale_openings DROP CONSTRAINT chk_event_scale_openings_start_end;
      -- ensure that the times do not overlap for a given angler
      ALTER TABLE event_scale_openings DROP CONSTRAINT "event_scale_openings_overlapping_time_ranges";
    SQL

    drop_table :event_scale_openings
  end
end
