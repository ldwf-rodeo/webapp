class ReplaceAgeWithDob < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE participants DROP COLUMN age;
      ALTER TABLE participants ADD COLUMN date_of_birth date;
      ALTER TABLE participants ADD CONSTRAINT ck_participants_date_of_birth_valid CHECK (date_of_birth BETWEEN '1900-01-01' AND cast(now() as date));
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE participants ADD COLUMN age bigint;
      ALTER TABLE participants DROP COLUMN date_of_birth;
    SQL
  end

end