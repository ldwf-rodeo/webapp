class AddEnteredAtToCaptures < ActiveRecord::Migration
  def change
    add_column :captures, :entered_at, :datetime
    execute <<-SQL
      UPDATE captures SET entered_at = created_at;
      ALTER TABLE CAPTURES ALTER COLUMN entered_at SET NOT NULL;
    SQL
  end
end
