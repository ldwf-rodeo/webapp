class CreateTeamPremiumCategoryGroupings < ActiveRecord::Migration
  def change
    create_table :team_premium_category_groupings do |t|
      t.bigint :team_id, null: false
      t.bigint :premium_category_grouping_id, null: false

      t.timestamps null: false
    end

    add_index :team_premium_category_groupings, [:team_id, :premium_category_grouping_id], unique: true, name: 'idx_team_prem_cat_group_team_cat_group'
    add_foreign_key :team_premium_category_groupings, :teams
    add_foreign_key :team_premium_category_groupings, :premium_category_groupings
  end
end
