class FixEventTeams < ActiveRecord::Migration
  #change events and teams relationship from many to many to many to 1
  def change
    add_column :teams, :event_id, :integer
    execute('update teams set event_id = (select id from events limit 1)')
    change_column :teams, :event_id, :integer, :null => false
    drop_join_table :events, :teams
  end
end
