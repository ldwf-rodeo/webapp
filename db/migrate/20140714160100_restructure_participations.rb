class RestructureParticipations < ActiveRecord::Migration

  def up
      sql = ""
      source = File.new("./db/restructure_participants.sql", "r")
      while (line = source.gets)
        sql << line
      end
      source.close
      execute sql
  end
end