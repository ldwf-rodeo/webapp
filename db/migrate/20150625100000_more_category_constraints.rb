class MoreCategoryConstraints < ActiveRecord::Migration

  def up
    execute <<-SQL

      UPDATE categories SET score_2_is_random = NULL WHERE score_2_source IS NULL;
      UPDATE categories SET score_2_is_random = false WHERE score_2_source IS NOT NULL;

      -- Faux Pas uses randomized tie-breaking.
      UPDATE categories SET score_2_is_random = true WHERE super_category_id IN (41, 42, 43, 44);

      ALTER TABLE categories ADD CONSTRAINT ck_categories_score_2_is_random_check CHECK ((score_2_is_random IS NULL) = (score_2_source IS NULL));

      UPDATE categories SET score_3_is_random = NULL WHERE score_3_source IS NULL;
      UPDATE categories SET score_3_is_random = false WHERE score_3_source IS NOT NULL;

      ALTER TABLE categories ADD CONSTRAINT ck_categories_score_3_is_random_check CHECK ((score_3_is_random IS NULL) = (score_3_source IS NULL));

      ALTER TABLE categories ADD CONSTRAINT ck_categories_require_score_2_if_score_3 CHECK (NOT (score_3_source IS NOT NULL AND score_2_source IS NULL));

      -- This isn't named very good.
      ALTER TABLE categories DROP CONSTRAINT IF EXISTS times_valid;
      ALTER TABLE categories ADD CONSTRAINT ck_categories_valid_start_and_end_times CHECK (valid_start_time < valid_end_time);

      -- There's no reason why we can't have a start time but not and end time or the other way around.
      ALTER TABLE categories DROP CONSTRAINT ck_categories_valid_times_both_present_or_not;
    SQL
  end

end