class CreatePremiumCategoryGroupings < ActiveRecord::Migration
  def change
    create_table :premium_category_groupings do |t|
      t.text :name, null: false
      t.float :price, default: 0
      t.bigint :event_id, null: false

      t.timestamps null: false
    end

    add_index :premium_category_groupings, [:name, :event_id], unique: true
    add_foreign_key :premium_category_groupings, :events
  end
end
