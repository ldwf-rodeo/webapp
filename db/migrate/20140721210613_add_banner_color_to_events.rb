class AddBannerColorToEvents < ActiveRecord::Migration
  def change
    add_column :events, :banner_color, :string, :default => '#00CC99'

    events = Event.all

    colors = ['#00005C','#2E5CB8','#1bade9']

    events.each_with_index do |event, index|
      event.banner_color = colors[index]
      event.save!
    end
  end
end
