class AddSuperCategoryColumnToCategoryitems < ActiveRecord::Migration
  def change
    add_column :categoryitems, :super_category_id, :integer
  end
end
