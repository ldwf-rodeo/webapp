class CreateSpeciesTable < ActiveRecord::Migration
  def self.up

    # create the species table
    (create_species_table = '') << <<-eod
CREATE TABLE public.species(
	name varchar(256),
	CONSTRAINT species_pk PRIMARY KEY (name)
)
    eod

    #create the join table
    (create_cat_species_join_table = '')  << <<-eod
CREATE TABLE public.categories_species(
  species_name varchar(256),
  category_id bigint,
  CONSTRAINT categories_species_pk PRIMARY KEY (species_name,category_id)
)
    eod

    # create the correct foreign keys for the captures table and the categories table
    (create_constraints = '') << <<-eod
ALTER TABLE public.captures ADD CONSTRAINT species_fk FOREIGN KEY (species)
  REFERENCES public.species (name) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE public.categories_species ADD CONSTRAINT species_fk FOREIGN KEY (species_name)
  REFERENCES public.species (name) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE public.categories_species ADD CONSTRAINT categories_fk FOREIGN KEY (category_id)
  REFERENCES public.categories (id) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

    eod

    ActiveRecord::Base.connection.execute(create_species_table)
    ActiveRecord::Base.connection.execute(create_cat_species_join_table)
    ActiveRecord::Base.connection.execute(create_constraints)

  end

  def self.down
    (query = '') << <<-eod
ALTER TABLE public.categories DROP CONSTRAINT species_fk;
ALTER TABLE public.categories_species DROP CONSTRAINT categories_fk;
ALTER TABLE public.categories_species DROP CONSTRAINT species_fk;

DROP TABLE public.categories_species;
DROP TABLE public.species;
    eod

    ActiveRecord::Base.connection.execute(query)

  end
end
