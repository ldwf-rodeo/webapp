class AddCaptainAsContactToOldData < ActiveRecord::Migration
  def change
    execute <<-SQL
      UPDATE teams SET contact_id = captain_id WHERE contact_id IS NULL;
    SQL
  end
end
