class AddShortSummaryUrlToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :summary_url, :string
  end
end
