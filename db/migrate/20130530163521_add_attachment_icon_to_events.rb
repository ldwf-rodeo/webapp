class AddAttachmentIconToEvents < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :icon
    end
  end

  def self.down
    drop_attached_file :events, :icon
  end
end
