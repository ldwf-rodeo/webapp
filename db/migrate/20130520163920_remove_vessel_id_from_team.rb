class RemoveVesselIdFromTeam < ActiveRecord::Migration
  def change
    remove_column :teams, :vessel_id
  end
end
