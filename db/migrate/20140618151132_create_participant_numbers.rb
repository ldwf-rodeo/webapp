class CreateParticipantNumbers < ActiveRecord::Migration
  def change

    create_table :participant_numbers do |t|
      t.text :value

      t.references :event
      t.references :participant

      t.timestamps
    end

    execute <<-SQL
      alter table participant_numbers add constraint uq_participant_numbers_event_participant UNIQUE(event_id, participant_id);
      alter table participant_numbers add constraint uq_participant_numbers_event_value UNIQUE(event_id, value);

      alter table participant_numbers add constraint fk_participant_numbers_event_id FOREIGN KEY (event_id) REFERENCES events(id);
      alter table participant_numbers add constraint fk_participant_numbers_participant_id FOREIGN KEY (participant_id) REFERENCES participants(id);

      CREATE INDEX idx_participant_numbers_event_id ON participant_numbers (event_id);
      CREATE INDEX idx_participant_numbers_event_id_participant_id ON participant_numbers (event_id, participant_id);
    SQL

    execute <<-SQL
      update participants
      set participant_number = concat(participant_number, '*')
      where id in (
        select id
        from (
          select *
          from (
            select id, participant_number, row_number() over (partition by  participant_number order by participant_number) as row
            from participants order by participant_number
          ) b
          where b.row = 2
        ) a
      )
    SQL

    Participant.all.each do |participant|
      num = ParticipantNumber.new(
                                    {
                                      value: participant.participant_number,
                                      event: Event.first,
                                      participant: participant
                                    }
                                  )
      num.save!
    end

    execute <<-SQL
      alter table participants drop column participant_number;
    SQL
  end
end
