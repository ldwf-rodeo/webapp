class AddLengthToCategoryItems < ActiveRecord::Migration
  def change
    add_column :categoryitems, :min_length, :integer
    add_column :categoryitems, :max_length, :integer
  end
end
