class AddCategoryTimeConstraint < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD CONSTRAINT ck_categories_valid_times_both_present_or_not CHECK((valid_start_time IS NULL) = (valid_end_time IS NULL));
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE categories DROP CONSTRAINT ck_categories_valid_times_both_present_or_not;
    SQL
  end

end
