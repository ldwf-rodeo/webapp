class AddCategoryPosition < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD COLUMN position bigint;
      UPDATE categories SET position = 0;
      ALTER TABLE categories ALTER COLUMN position SET NOT NULL;
    SQL
  end


end