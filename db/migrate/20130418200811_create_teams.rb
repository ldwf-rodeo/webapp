class CreateTeams < ActiveRecord::Migration
  def change
      create_table :teams do |t|
        t.string :name
        t.string :company
        t.integer :vessel_id

        t.timestamps
      end
  end
end
