class AddCategoryGroup < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD COLUMN grouping_number integer DEFAULT (1);
      ALTER TABLE categories ADD CONSTRAINT ck_categories_grouping_number_valid CHECK (grouping_number >= 0);
      UPDATE categories SET grouping_number = 1;
      ALTER TABLE categories ALTER COLUMN grouping_number SET NOT NULL;
    SQL
  end


end