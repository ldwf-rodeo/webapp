class CleanUpLeaderboard < ActiveRecord::Migration

  def up
    execute <<-SQL

      -- Finally add an official name column to units.
      ALTER TABLE units ADD COLUMN name text;
      UPDATE units SET name = 'pounds';
      ALTER TABLE units ALTER COLUMN name SET NOT NULL;

      -- Perform cleanup on units.
      UPDATE units SET
        name = 'pounds',
        ascending_qualifier = 'lightest',
        descending_qualifier = 'heaviest',
        dimension_name = 'weight'
      WHERE abbreviation IN ('lbs.','lbs*');

      UPDATE units SET
        name = 'inches',
        ascending_qualifier = 'shortest',
        descending_qualifier = 'longest',
        dimension_name = 'length'
      WHERE abbreviation = 'in.';

      UPDATE units SET
        name = 'number',
        ascending_qualifier = 'fewest',
        descending_qualifier = 'greatest',
        dimension_name = 'number'
      WHERE abbreviation = 'ct.';

      UPDATE units SET
        name = 'spots',
        ascending_qualifier = 'fewest number of',
        descending_qualifier = 'greatest number of',
        dimension_name = 'spots'
      WHERE abbreviation = 'spots';

      --Cache the description for major speedups.
      ALTER TABLE categories ADD COLUMN generated_description text;

    SQL
  end

  def down
    execute <<-SQL

    SQL
  end


end