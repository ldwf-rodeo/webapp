class RemoveTentType < ActiveRecord::Migration
  def change
    execute <<-SQL
      update participants set participant_type = 'guest' where participant_type = 'tent'
    SQL
  end
end
