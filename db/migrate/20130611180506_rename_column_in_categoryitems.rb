class RenameColumnInCategoryitems < ActiveRecord::Migration
  def change
    rename_column :categoryitems, :categories_id, :category_id
  end
end
