class AddNewRolesForUsers < ActiveRecord::Migration
  def change
    %w(peon event_admin sms_sender).each do |role|
      Role.where(name: role).first_or_create!
    end
  end
end
