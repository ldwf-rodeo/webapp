class AddSequencerToEventsForParticipantNumbers < ActiveRecord::Migration
  def up

    # create the needed table to hold the sequence values
    execute <<-SQL
      CREATE TABLE event_participant_sequences (
        id bigserial PRIMARY KEY,
        sequence_name TEXT NOT NULL,
        min_number bigint NOT NULL,
        max_number bigint NOT NULL,
        event_id bigint NOT NULL,
        participant_type participant_type NOT NULL
      );

      ALTER TABLE event_participant_sequences ADD CONSTRAINT uq_event_id_participant_type UNIQUE(event_id, participant_type);
      ALTER TABLE event_participant_sequences ADD CONSTRAINT fk_event_id FOREIGN KEY (event_id) REFERENCES events(id);
      ALTER TABLE event_participant_sequences ADD CONSTRAINT ck_min_less_than_max CHECK (min_number < max_number);
    SQL

  end

  def down
    # create the needed table to hold the sequence values
    execute <<-SQL

      ALTER TABLE event_participant_sequences DROP CONSTRAINT uq_event_id_participant_type;
      ALTER TABLE event_participant_sequences DROP CONSTRAINT fk_event_id;
      ALTER TABLE event_participant_sequences DROP CONSTRAINT ck_min_less_than_max;

      DROP TABLE event_participant_sequences;

    SQL
  end
end