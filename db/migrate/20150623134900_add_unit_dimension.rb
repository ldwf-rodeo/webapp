class AddUnitDimension < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE units ADD COLUMN dimension_name text;
      UPDATE units SET dimension_name = 'weight';
      ALTER TABLE units ALTER COLUMN dimension_name SET NOT NULL;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE units DROP COLUMN dimension_name;
    SQL
  end


end