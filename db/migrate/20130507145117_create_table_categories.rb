class CreateTableCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :start_age
      t.integer :end_age
      t.integer :positions
      t.string  :species
      t.string  :gender

      t.references :event

      t.timestamps
    end
  end
end
