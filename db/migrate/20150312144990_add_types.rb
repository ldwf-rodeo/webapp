class AddTypes < ActiveRecord::Migration

  self.disable_ddl_transaction!

  def up
    execute <<-SQL
      ALTER TYPE score_source ADD VALUE 'largest_subscore';
    SQL
    execute <<-SQL
      ALTER TYPE score_source ADD VALUE 'smallest_subscore';
    SQL
  end


end