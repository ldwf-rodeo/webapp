class AddJoinForTeamsAndVessels < ActiveRecord::Migration
  def change
    create_table :teams_vessels, :id => false do |t|
      t.references :team, :vessel
    end
    add_index :teams_vessels, [:team_id, :vessel_id]
  end
end
