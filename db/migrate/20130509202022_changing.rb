class Changing < ActiveRecord::Migration

  def change
    execute "ALTER TABLE species DROP CONSTRAINT species_pk ;"
    execute "ALTER TABLE species ADD PRIMARY KEY (id);"
  end

end
