class AddTimeZoneToEnteredAt < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE captures ALTER COLUMN entered_at SET DATA TYPE timestamp with time zone;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE captures ALTER COLUMN entered_at SET DATA TYPE timestamp without time zone;
    SQL
  end
end