class AddNotificationOptions < ActiveRecord::Migration
  def up

    execute <<-SQL
      ALTER TABLE participants ADD COLUMN does_receive_notifications boolean default false not null;
      ALTER TABLE teams ADD COLUMN does_receive_notifications boolean default true not null;
    SQL

  end

  def down
    execute <<-SQL
      ALTER TABLE participants DROP COLUMN does_receive_notifications;
      ALTER TABLE teams DROP COLUMN does_receive_notifications;
    SQL
  end
end