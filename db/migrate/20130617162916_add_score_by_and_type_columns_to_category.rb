class AddScoreByAndTypeColumnsToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :score_on, :string
    add_column :categories, :category_type, :string
  end
end
