class AddAttachmentBadgeTemplateToEvents < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :badge_template
    end
  end

  def self.down
    drop_attached_file :events, :badge_template
  end
end
