class CreateParticipants < ActiveRecord::Migration
  def change
      create_table :participants do |t|
        t.string :first_name
        t.string :last_name
        t.string :street
        t.string :city
        t.string :zipcode
        t.string :email
        t.string :home_phone
        t.string :mobile_phone
        t.string :mobile_carrier
        t.string :shirt_size
        t.string :type

        t.timestamps
      end
  end
end
