class AddRecycledSequenceTable < ActiveRecord::Migration

  def up
    execute <<-SQL
      --TABLE TO HOLD RECYCLED NUMBERS...
      CREATE TABLE IF NOT EXISTS gapless_sequence_recycled_numbers (
        id bigserial not null primary key,
        sequence_name text NOT NULL,
        number bigint NOT NULL
      );
      ALTER TABLE gapless_sequence_recycled_numbers ADD CONSTRAINT uq_gapless_sequence_recycled_numbers_sequence_name_and_number UNIQUE (sequence_name,number);
    SQL
  end

  def down
    execute <<-SQL
      DROP TABLE gapless_sequence_recycled_numbers;
    SQL
  end

end