class CreateEntityTags < ActiveRecord::Migration
  def change
    create_table :entity_tags do |t|
      t.references :entity, polymorphic: true, index: true
      t.bigint :tag_id, null: false

      t.timestamps null: false
    end

    add_foreign_key :entity_tags, :tags
  end
end
