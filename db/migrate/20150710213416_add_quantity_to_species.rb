class AddQuantityToSpecies < ActiveRecord::Migration
  def change
    add_column :species, :quantity, :integer, default: 1, null: false
  end
end
