class OverhaulCapturesAndConstraints < ActiveRecord::Migration
  def change
    execute <<-SQL

        -- create enums

       CREATE TYPE public.participant_type AS
        ENUM ('angler','volunteer','organizer','guest','sponsor','vendor','vip');

       CREATE TYPE public.shirt_size AS
        ENUM ('s','m','l','xl','xxl','xxxl', 'xxxxl');

       CREATE TYPE public.gender AS
        ENUM ('male','female');

       CREATE TYPE public.score_entity AS
        ENUM ('team','participant');

       CREATE TYPE public.score_grouping AS
        ENUM ('all','none','top_n', 'selected_n');

       CREATE TYPE public.category_type AS
        ENUM ('species_unit','species_points','group');

       CREATE TYPE public.score_order AS
        ENUM ('asc','desc');

      create table units (
        id bigserial primary key,
        description text not null,
        abbreviation text not null,
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL
      );

      -- add the new columns
      alter table categories add column score_entity public.score_entity;
      alter table categories add column score_grouping public.score_grouping;
      alter table categories add column score_grouping_limit bigint;
      alter table categories add column score_order public.score_order default 'desc';

      insert into units (id, abbreviation, description, created_at, updated_at) values (1, 'lbs.', 'Weight (lbs.)', now(), now());
      insert into units (id, abbreviation, description, created_at, updated_at) values (2, 'in.', 'Length (in.)', now(), now());

      update categories set score_on = 'participant' where score_on is null;

      update categories set score_entity = score_on::score_entity;
      update categories set score_grouping = case
          when score_by = 'points' then 'all'::score_grouping
          else 'none'::score_grouping
        end;

      alter table categories alter column score_entity set not null;
      alter table categories alter column score_grouping set not null;
      alter table categories alter column score_order set not null;

      update categories set category_type = case
        when category_type = 'aggregate' then 'group'
        when score_by = 'points' then 'species_points'
        else 'species_unit'
      end;
      alter table categories add column category_type2 public.category_type;
      update categories set category_type2 = category_type::public.category_type;
      alter table categories drop column category_type;
      alter table categories rename category_type2 TO category_type;
      alter table categories alter column category_type set not null;

      -- drop old columns
      alter table categories drop column score_by;
      alter table categories drop column score_on;


      -- add the new contraints to the categories table
      alter table categories add constraint ck_categories_valid_start_and_end_ages CHECK (start_age >= 0 AND end_age >= 0 AND start_age <= end_age);
      alter table categories add constraint ck_categories_score_grouping_limit_valid CHECK (score_grouping_limit > 0);

      -------------------------------------------------------
      -- SPLIT CATEGORY_ITEMS INTO THREE CATEGORIES
      --------------------------------------------------------

      --TABLE FOR NORMAL SPECIES ITEMS
      create table category_species_unit_items (
        id bigserial primary key,
        category_id bigint not null,
        species_id bigint not null,
        unit_id bigint not null
      );

      CREATE FUNCTION category_species_unit_items_for_category_and_not_unit(p_category_id bigint, p_unit_id bigint) RETURNS bigint AS $$
          SELECT COUNT(*) FROM category_species_unit_items WHERE category_id = p_category_id AND unit_id <> p_unit_id;
      $$ LANGUAGE 'sql';

    ALTER TABLE public.category_species_unit_items ADD CONSTRAINT uq_category_species_unit_items UNIQUE (category_id, species_id);

    -- ENSURE THAT WE ARE ONLY USING ONE UNIT FOR ALL SPECIES_UNIT ITEMS IN THIS CATEGORY.
      ALTER TABLE public.category_species_unit_items ADD CONSTRAINT uq_category_species_unit_items_same_unit CHECK (
        category_species_unit_items_for_category_and_not_unit(category_id,unit_id) = 0
      );

      ALTER TABLE public.category_species_unit_items ADD CONSTRAINT fk_category_species_unit_items_category_id FOREIGN KEY (category_id)
       REFERENCES public.categories (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.category_species_unit_items ADD CONSTRAINT fk_category_species_unit_items_species_id FOREIGN KEY (species_id)
       REFERENCES public.species (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.category_species_unit_items ADD CONSTRAINT fk_category_species_unit_items_unit_id FOREIGN KEY (unit_id)
       REFERENCES public.units (id) MATCH FULL;

      --TABLE FOR POINTS ITEMS

      create table category_species_points_items (
        id bigserial primary key,
        category_id bigint not null,
        species_id bigint not null,
        points bigint not null
      );

      alter table category_species_points_items add constraint ck_category_species_point_items_points_valid CHECK (points >= 0);

      ALTER TABLE public.category_species_points_items ADD CONSTRAINT fk_category_species_points_items_category_id FOREIGN KEY (category_id)
       REFERENCES public.categories (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.category_species_points_items ADD CONSTRAINT fk_category_species_points_items_species_id FOREIGN KEY (species_id)
       REFERENCES public.species (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      --TABLE FOR GROUP ITEMS

      create table category_grouping_items (
        id bigserial primary key,
        category_id bigint not null,
        place bigint not null,
        points bigint not null
      );

      ALTER TABLE public.category_grouping_items ADD CONSTRAINT fk_category_grouping_items_category_id FOREIGN KEY (category_id)
       REFERENCES public.categories (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      alter table category_grouping_items add constraint ck_category_grouping_items_points_valid CHECK (points >= 0);

      -- JOIN TABLE BETWEEN GROUP ITEMS AND SUPER CATEGORIES

      create table category_grouping_items_super_categories (
          id bigserial primary key,
          category_grouping_item_id bigint not null,
          super_category_id bigint not null
      );

       ALTER TABLE public.category_grouping_items_super_categories ADD CONSTRAINT uq_category_grouping_items_super_categories_category_grouping_item_id_and_super_category_id UNIQUE (category_grouping_item_id, super_category_id);

      ALTER TABLE public.category_grouping_items_super_categories ADD CONSTRAINT fk_category_grouping_items_super_categories_category_grouping_item_id FOREIGN KEY (category_grouping_item_id)
       REFERENCES public.category_grouping_items (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.category_grouping_items_super_categories ADD CONSTRAINT fk_category_grouping_items_super_categories_super_category_id FOREIGN KEY (super_category_id)
       REFERENCES public.super_categories (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      -------------------------------------
      ---------------------------------------

      INSERT INTO category_species_unit_items (category_id, species_id, unit_id)  (
          SELECT
            category_id,
            species_id,
            1 -- weight
          FROM categoryitems
          WHERE species_id IS NOT NULL and category_id IS NOT NULL AND points IS NULL
      );

      INSERT INTO category_species_points_items (category_id, species_id, points) (
          SELECT
            category_id,
            species_id,
            points
          FROM categoryitems
          WHERE species_id IS NOT NULL and category_id IS NOT NULL AND points IS NOT NULL
      );

--FIX

      INSERT INTO category_grouping_items (category_id, place, points) (
          SELECT
            category_id,
            place,
            points
          FROM categoryitems
          WHERE points IS NOT NULL and super_category_id IS NULL AND place IS NOT NULL
      );

      -- Create a join on the super category for each old categoryitems entry.
      INSERT INTO category_grouping_items_super_categories (category_grouping_item_id, super_category_id)
        with stuff as (
          SELECT
            category_id,
            super_category_id
          FROM categoryitems
          WHERE super_category_id IS NOT NULL
        )
        select category_grouping_items.id, stuff.super_category_id
        from stuff, category_grouping_items
        where stuff.category_id = category_grouping_items.category_id
      ;

      -- drop the categoryitems table
      -- drop table categoryitems;

      alter table categories add column gender2 public.gender;
      update categories set gender2 = gender::public.gender;
      alter table categories drop column gender;
      alter table categories rename gender2 TO gender;

      --Set whether places has to be distinct.
      alter table categories add column has_distinct_places boolean default false;
      update categories set has_distinct_places = false;
      alter table categories alter column has_distinct_places set not null;

      -- update participants
      alter table participants add column gender2 public.gender;
      update participants set gender2 = gender::public.gender;
      alter table participants drop column gender;
      alter table participants rename gender2 TO gender;

      update participants set shirt_size = null WHERE shirt_size = 'null';
      update participants set gender = 'male'::public.gender WHERE gender IS NULL;

      alter table participants add column shirt_size2 public.shirt_size;
      update participants set shirt_size2 = shirt_size::public.shirt_size WHERE shirt_size IS NOT NULL;
      alter table participants drop column shirt_size;
      alter table participants rename shirt_size2 TO shirt_size;


      update participants set participant_type = 'angler' where participant_type is null;
      alter table participants add column participant_type2 public.participant_type;
      update participants set participant_type2 = participant_type::public.participant_type;
      alter table participants drop column participant_type;
      alter table participants rename participant_type2 TO participant_type;

      alter table participants alter column gender set not null;
      alter table participants alter column participant_type set not null;



      alter table users alter column user_name set not null;
      alter table users alter column created_at set not null;
      alter table users alter column updated_at set not null;



      -- create capture_attributes table

      create table capture_measurements (
        id bigserial primary key,
        unit_id bigint not null,
        capture_id bigint not null,
        measurement numeric not null
      );

      --Remove old, invalid weights.
      update captures set weight = 0 where weight = 1000;

      --Migrate existing measurements into new table.
      insert into capture_measurements (unit_id, capture_id, measurement)
        select 1, id, weight
        from captures
        where weight > 0;

      alter table captures drop column weight;
      alter table captures drop column length;
      alter table captures drop column time_entered;

       -- add the new foreign keys
       ALTER TABLE public.categories ADD CONSTRAINT fk_categories_super_category_id FOREIGN KEY (super_category_id)
       REFERENCES public.super_categories (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.super_categories ADD CONSTRAINT fk_super_categories_event_id FOREIGN KEY (event_id)
       REFERENCES public.events (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.teams ADD CONSTRAINT fk_teams_captain_id FOREIGN KEY (captain_id)
       REFERENCES public.participants (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.teams ADD CONSTRAINT fk_teams_event_id FOREIGN KEY (event_id)
       REFERENCES public.events (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.teams_vessels ADD CONSTRAINT fk_team_vessels_team_id FOREIGN KEY (team_id)
       REFERENCES public.teams (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.teams_vessels ADD CONSTRAINT fk_team_vessels_vessel_id FOREIGN KEY (vessel_id)
       REFERENCES public.vessels (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.users_roles ADD CONSTRAINT fk_users_roles_user_id FOREIGN KEY (user_id)
       REFERENCES public.users (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

       ALTER TABLE public.users_roles ADD CONSTRAINT fk_users_roles_role_id FOREIGN KEY (role_id)
       REFERENCES public.roles (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

        ALTER TABLE public.capture_measurements ADD CONSTRAINT fk_capture_measurements_unit_id FOREIGN KEY (unit_id)
       REFERENCES public.units (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

        ALTER TABLE public.capture_measurements ADD CONSTRAINT fk_capture_measurements_capture_id FOREIGN KEY (capture_id)
       REFERENCES public.captures (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.capture_measurements ADD CONSTRAINT uq_capture_measurements_capture_id UNIQUE (capture_id, unit_id);


       ALTER TABLE public.users_roles ADD CONSTRAINT uq_users_roles_user_id_and_role_id UNIQUE (user_id, role_id);

      drop table teams_vessels;
      drop table vessels;
      drop table categories_categoryitems;
      drop table categoryitems;

      create table public.event_units (
        id bigserial primary key,
        event_id bigint not null,
        unit_id bigint not null
      );

       ALTER TABLE public.event_units ADD CONSTRAINT fk_event_units_event_id FOREIGN KEY (event_id)
       REFERENCES public.events (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

      ALTER TABLE public.event_units ADD CONSTRAINT fk_event_units_unit_id FOREIGN KEY (unit_id)
       REFERENCES public.units (id) MATCH FULL
       ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;

    SQL

    ## Add inches and weights to Fourchon.
    Event.first.units << Unit.all[0]
    Event.first.units << Unit.all[1]
  end
end
