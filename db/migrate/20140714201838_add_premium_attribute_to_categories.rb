class AddPremiumAttributeToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :is_premium, :boolean, :default => false
  end
end
