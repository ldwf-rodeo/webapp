class AddSpeciesCaptureType < ActiveRecord::Migration

  def up
    execute <<-SQL
      CREATE TABLE category_species_capture_items (
        id bigserial NOT NULL PRIMARY KEY,
        category_id bigint NOT NULL REFERENCES categories (id),
        species_id bigint NOT NULL REFERENCES species (id),
        created_at timestamp with time zone NOT NULL,
        updated_at timestamp with time zone NOT NULL
      );
      ALTER TABLE category_species_capture_items ADD CONSTRAINT uq_category_species_capture_items_species UNIQUE (category_id,species_id);
    SQL
  end

  def down
    execute <<-SQL
      DROP TABLE category_species_capture_items;
    SQL
  end

end
