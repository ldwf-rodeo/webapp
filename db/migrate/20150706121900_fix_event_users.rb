class FixEventUsers < ActiveRecord::Migration

  def up
    execute <<-SQL

      -- Add the role. Everyone already in gets to be an event admin.
      ALTER TABLE events_users ADD COLUMN role_id bigint REFERENCES roles (id);
      UPDATE events_users SET role_id = #{Role.event_admin.id};
      ALTER TABLE events_users ALTER COLUMN role_id SET NOT NULL;

      -- It should be unique across all three of these values.
      ALTER TABLE events_users ADD CONSTRAINT uq_events_users_scope UNIQUE (event_id,user_id,role_id);

      -- Add timestamps.
      ALTER TABLE events_users ADD COLUMN created_at TIMESTAMP with time zone;
      ALTER TABLE events_users ADD COLUMN updated_at TIMESTAMP with time zone;
      UPDATE events_users SET created_at = now();
      UPDATE events_users SET updated_at = now();
      ALTER TABLE events_users ALTER COLUMN created_at SET NOT NULL;
      ALTER TABLE events_users ALTER COLUMN updated_at SET NOT NULL;

      -- You cannot have the admin role for an event.
      ALTER TABLE events_users ADD CONSTRAINT ck_events_users_role_not_admin CHECK (role_id <> #{Role.admin.id});
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE events_users DROP COLUMN role_id;
    SQL
  end

end