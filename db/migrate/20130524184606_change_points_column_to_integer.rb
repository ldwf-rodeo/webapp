class ChangePointsColumnToInteger < ActiveRecord::Migration
  def change
    remove_column :categoryitems, :points
    add_column :categoryitems, :points, :integer
  end
end
