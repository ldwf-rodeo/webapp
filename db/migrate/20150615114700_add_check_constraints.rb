class AddCheckConstraints < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ALTER COLUMN super_category_id SET NOT NULL;
      ALTER TABLE categories ALTER COLUMN positions SET NOT NULL;
      ALTER TABLE categories ALTER COLUMN name SET NOT NULL;

      ALTER TABLE super_categories ALTER COLUMN event_id SET NOT NULL;
      ALTER TABLE super_categories ALTER COLUMN name SET NOT NULL;
    SQL
  end


end