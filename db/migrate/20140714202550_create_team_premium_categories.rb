class CreateTeamPremiumCategories < ActiveRecord::Migration
  def change
    create_table :team_premium_categories do |t|
      t.references :team
      t.references :category

      t.timestamps
    end
  end
end
