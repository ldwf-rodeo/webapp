class FixTeamPremiumCategories < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE team_premium_categories ADD CONSTRAINT uq_team_premium_categories_scope UNIQUE (team_id,category_id);
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE team_premium_categories DROP CONSTRAINT uq_team_premium_categories_scope;
    SQL
  end

end
