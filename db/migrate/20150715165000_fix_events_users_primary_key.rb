class FixEventsUsersPrimaryKey < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE events_users DROP CONSTRAINT events_users_pk;
      ALTER TABLE events_users ADD COLUMN id bigserial NOT NULL PRIMARY KEY;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE events_users DROP CONSTRAINT events_users_pkey;
      ALTER TABLE events_users ADD CONSTRAINT events_users_pk PRIMARY KEY (event_id, user_id);
      ALTER TABLE events_users DROP COLUMN id;
    SQL
  end

end
