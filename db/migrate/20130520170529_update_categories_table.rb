class UpdateCategoriesTable < ActiveRecord::Migration
  def self.up

    create_table :categoryitems do |t|
      t.string     :points
      t.references :species
    end

    create_table :categories_categoryitems, :id => false do |t|
      t.references :categoryitem, :category
    end

    add_index :categories_categoryitems, [:category_id,:categoryitem_id], {:name => 'category_item_index', :unique => true}

    #drop_table :categories_species
  end

  def self.down

    remove_index! :categories_categoryitems, 'category_item_index'

  drop_table :categories_categoryitems

    drop_table :categor_yitems
  end
end
