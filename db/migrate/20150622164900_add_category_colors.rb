class AddCategoryColors < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE events ADD COLUMN category_text_color text;
      ALTER TABLE events ADD COLUMN category_background_color text;

      ALTER TABLE super_categories ADD COLUMN category_text_color text;
      ALTER TABLE super_categories ADD COLUMN category_background_color text;

      ALTER TABLE categories ADD COLUMN category_text_color text;
      ALTER TABLE categories ADD COLUMN category_background_color text;

    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE events DROP COLUMN category_text_color;
      ALTER TABLE events DROP COLUMN category_background_color;

      ALTER TABLE super_categories DROP COLUMN category_text_color;
      ALTER TABLE super_categories DROP COLUMN category_background_color;

      ALTER TABLE categories DROP COLUMN category_text_color;
      ALTER TABLE categories DROP COLUMN category_background_color;
    SQL
  end


end