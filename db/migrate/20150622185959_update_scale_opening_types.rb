class UpdateScaleOpeningTypes < ActiveRecord::Migration
  def change
    execute <<-SQL
      ALTER TABLE event_scale_openings ALTER COLUMN start_time SET DATA TYPE timestamptz;
      ALTER TABLE event_scale_openings ALTER COLUMN end_time SET DATA TYPE timestamptz;
    SQL
  end
end