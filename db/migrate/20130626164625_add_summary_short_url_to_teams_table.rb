class AddSummaryShortUrlToTeamsTable < ActiveRecord::Migration
  def change
    add_column :teams, :summary_short_url, :string
  end
end
