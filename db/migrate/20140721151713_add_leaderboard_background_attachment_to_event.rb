class AddLeaderboardBackgroundAttachmentToEvent < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :leaderboard_background
    end
  end

  def self.down
    drop_attached_file :events, :leaderboard_background
  end
end
