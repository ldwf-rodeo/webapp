class AddRoleDescriptions < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE roles ADD COLUMN description text;
      UPDATE roles SET description = 'A standard role.';
      ALTER TABLE ROLES ALTER COLUMN description SET NOT NULL;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE roles DROP COLUMN description;
    SQL
  end

end
