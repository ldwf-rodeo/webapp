class CreateTokens < ActiveRecord::Migration
  def change
    #we need tokens to semi secure team editing before an event
    create_table :tokens do |t|
      t.string :token
      t.timestamp :expiration
      t.references :tokenable, polymorphic: true
    end
  end
end