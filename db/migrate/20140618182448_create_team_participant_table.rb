class CreateTeamParticipantTable < ActiveRecord::Migration
  def change
    create_table :team_participants do |t|
      t.references :participant
      t.references :team

      t.timestamps
    end

    execute <<-SQL
      insert into team_participants (team_id, participant_id) SELECT team_id, participant_id FROM participants_teams;
      alter table participants_teams rename to participants_teams_old;
    SQL
  end
end
