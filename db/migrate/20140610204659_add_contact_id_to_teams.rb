class AddContactIdToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :contact_id, :integer
  end
end
