class CreateVessels < ActiveRecord::Migration
  def change
      create_table :vessels do |t|
        t.string :name
        t.text :description
        t.integer :participant_id

        t.timestamps
      end
  end
end
