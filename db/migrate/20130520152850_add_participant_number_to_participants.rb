class AddParticipantNumberToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :participant_number, :string
  end
end
