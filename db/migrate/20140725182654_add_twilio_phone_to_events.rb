class AddTwilioPhoneToEvents < ActiveRecord::Migration
  def up
    add_column :events, :twilio_phone, :string
    Event.last.update_attribute(:twilio_phone, '+15042649710')
  end

  def down
    remove_column :events, :twilio_phone, :string
  end
end
