class AddPremiumCategoryGroupingToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :premium_category_grouping_id, :bigint
    add_foreign_key :categories, :premium_category_groupings
  end
end
