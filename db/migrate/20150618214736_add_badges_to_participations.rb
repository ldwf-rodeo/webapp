class AddBadgesToParticipations < ActiveRecord::Migration
  def self.up
    change_table :participations do |t|
      t.attachment :badge
    end
  end

  def self.down
    drop_attached_file :participations, :badge
  end
end
