class RunLeaderboardFile < ActiveRecord::Migration

  def self.up
    sql = ""
    source = File.new("./db/sql/leaderboard_compute.sql", "r")
    while (line = source.gets)
      sql << line
    end
    source.close
    execute sql

    sql = ""
    source = File.new("./db/sql/leaderboard_display.sql", "r")
    while (line = source.gets)
      sql << line
    end
    source.close
    execute sql
  end

end