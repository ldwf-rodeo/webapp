class UpdateTheFoa14ParticipantNumbers < ActiveRecord::Migration
  def up

    # grab the FOA 2014 event
    event = Event.where(slug: 'FOA_2014').first

    # if the event exists then create the number sequences
    if event

      # create the angler number sequence
      EventParticipantSequence.create!({
                                           event: event,
                                           min_number: 1000,
                                           max_number: 1999,
                                           participant_type: :angler,
                                           sequence_name: 'fao_2014_angler_participant_number'

                                       })

      # create the guest number sequence
      EventParticipantSequence.create!({
                                           event: event,
                                           min_number: 2000,
                                           max_number: 2999,
                                           participant_type: :guest,
                                           sequence_name: 'fao_2014_guest_participant_number'

                                       })

      # create the sponsor number sequence
      EventParticipantSequence.create!({
                                           event: event,
                                           min_number: 3000,
                                           max_number: 3999,
                                           participant_type: :sponsor,
                                           sequence_name: 'fao_2014_sponsor_participant_number'

                                       })

      # create the vip number sequence
      EventParticipantSequence.create!({
                                           event: event,
                                           min_number: 5000,
                                           max_number: 5999,
                                           participant_type: :vip,
                                           sequence_name: 'fao_2014_vip_participant_number'

                                       })

      # create the vip number sequence
      EventParticipantSequence.create!({
                                           event: event,
                                           min_number: 7000,
                                           max_number: 7999,
                                           participant_type: :organizer,
                                           sequence_name: 'fao_2014_organizer_participant_number'

                                       })

      ###############################################################
      ## End number sequence creation
      ###############################################################


      # update participants that do not have a participant number
      event.get_participants.each do |participant|
        participant_number = participant.participant_numbers.where(event: event).first

        if participant_number.nil?
          participant_number = ParticipantNumber.new({participant: participant, event: event})
          participant_number.save!
        end
      end

    end

    def down

    end
    # end if for event
  end
end
