class DropColumnMobileCarrierIdFromParticipants < ActiveRecord::Migration
  def change
    execute <<-SQL
      ALTER TABLE participants DROP CONSTRAINT ck_participants_phone_check;
    SQL

    remove_column :participants, :mobile_carrier_id, :integer
  end
end
