class FixParticipantPhoneCheckConstraint < ActiveRecord::Migration
  def change
    execute <<-SQL
      ALTER TABLE participants DROP CONSTRAINT "ck_participants_phone_check";
      ALTER TABLE participants ADD CONSTRAINT "ck_participants_phone_check" CHECK (NOT ( (mobile_phone IS NULL) AND (mobile_carrier_id IS NOT NULL))) ;
    SQL
  end
end
