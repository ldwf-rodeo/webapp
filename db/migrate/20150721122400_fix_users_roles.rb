class FixUsersRoles < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE users_roles ADD COLUMN id bigserial NOT NULL PRIMARY KEY;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE users_roles DROP COLUMN id;
    SQL
  end

end
