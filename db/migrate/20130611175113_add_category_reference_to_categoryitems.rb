class AddCategoryReferenceToCategoryitems < ActiveRecord::Migration
  def change
    add_reference :categoryitems, :categories
  end
end
