class AddAttachmentBallotTemplateToEvents < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :ballot_template
    end
  end

  def self.down
    drop_attached_file :events, :ballot_template
  end
end
