class AddPageAndPositionToSuperCategory < ActiveRecord::Migration
  def change
    add_column :super_categories, :page, :integer
    add_column :super_categories, :position, :integer
  end
end
