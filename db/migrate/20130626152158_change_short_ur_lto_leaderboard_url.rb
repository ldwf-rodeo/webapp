class ChangeShortUrLtoLeaderboardUrl < ActiveRecord::Migration
  def change
    rename_column :events, :short_url, :short_leaderboard_url
  end
end
