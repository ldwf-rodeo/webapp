class MakeTeamParticipantsUnique < ActiveRecord::Migration
  def up

    execute <<-SQL
        -- REMOVE ANY DUPLICATE ENTRIES THAT MIGHT ALREADY EXIST
        DELETE FROM team_participants WHERE id IN (
          SELECT team_participants.id FROM
            team_participants
            LEFT JOIN (
              SELECT
                MIN(id) as id, team_id, participant_id
              FROM
                team_participants
              GROUP BY team_id, participant_id
            ) r ON team_participants.id = r.id
          WHERE r.team_id IS NULL
        );

        -- ADD THE CONSTRAINT
        ALTER TABLE team_participants ADD CONSTRAINT uq_team_participants_team_id_and_participant_id UNIQUE (team_id,participant_id) ;
    SQL

  end

  def down
    execute <<-SQL
      ALTER TABLE team_participants DROP CONSTRAINT uq_team_participants_team_id_and_participant_id;
    SQL
  end
end