class ChangeTeamNameIndex < ActiveRecord::Migration
  def change
    remove_index :teams, :column => :slug
    add_index :teams, [:event_id, :slug], :unique => true
  end
end
