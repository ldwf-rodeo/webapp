class ChangeTimestampsToHaveTimezone < ActiveRecord::Migration
  def change
    execute <<-SQL
        DO
        $$
        DECLARE
            row record;
        BEGIN
            FOR row IN select table_name, column_name, data_type from information_schema.columns where data_type = 'timestamp without time zone'
            LOOP
                EXECUTE 'ALTER TABLE ' || quote_ident(row.table_name) || ' ALTER COLUMN ' || quote_ident(row.column_name) ||' SET DATA TYPE timestamp with time zone;';
            END LOOP;
        END;
        $$;
    SQL
  end
end
