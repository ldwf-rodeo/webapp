class CreatePendingJobs < ActiveRecord::Migration
  def change
    create_table :pending_jobs do |t|
      t.references :target, polymorphic: true, index: true
      t.references :source, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
