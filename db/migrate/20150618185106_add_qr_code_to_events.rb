class AddQrCodeToEvents < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.attachment :qr_code
    end
  end

  def self.down
    drop_attached_file :events, :qr_code
  end
end
