class AddRoleDecorations < ActiveRecord::Migration

  def up
    execute <<-SQL

      ALTER TABLE roles ADD COLUMN decoration_text text;
      UPDATE roles SET decoration_text = 'A' WHERE name = 'admin';
      UPDATE roles SET decoration_text = 'E' WHERE name = 'event_admin';
      UPDATE roles SET decoration_text = 'O' WHERE name = 'organizer';
      UPDATE roles SET decoration_text = 'P' WHERE name = 'peon';
      UPDATE roles SET decoration_text = 'S' WHERE name = 'sms_sender';
      UPDATE roles SET decoration_text = 'C' WHERE name = 'capture_helper';
      ALTER TABLE ROLES ALTER COLUMN decoration_text SET NOT NULL;

      ALTER TABLE roles ADD COLUMN decoration_color text;
      UPDATE ROLES set decoration_color = '#FF0000' WHERE name = 'admin';
      UPDATE ROLES set decoration_color = '#009000' WHERE name = 'event_admin';
      UPDATE ROLES set decoration_color = '#000000' WHERE name = 'organizer';
      UPDATE ROLES set decoration_color = '#000000' WHERE name = 'peon';
      UPDATE ROLES set decoration_color = '#000000' WHERE name = 'sms_sender';
      UPDATE ROLES set decoration_color = '#000000' WHERE name = 'capture_helper';
      ALTER TABLE ROLES ALTER COLUMN decoration_color SET NOT NULL;

      ALTER TABLE roles ADD COLUMN display_name text;
      UPDATE roles SET display_name = 'Administrator' WHERE name = 'admin';
      UPDATE roles SET display_name = 'Event Administrator' WHERE name = 'event_admin';
      UPDATE roles SET display_name = 'Organizer' WHERE name = 'organizer';
      UPDATE roles SET display_name = 'Peon' WHERE name = 'peon';
      UPDATE roles SET display_name = 'SMS Sender' WHERE name = 'sms_sender';
      UPDATE roles SET display_name = 'Capture Helper' WHERE name = 'capture_helper';
      ALTER TABLE roles ALTER COLUMN display_name SET NOT NULL;

      CREATE UNIQUE INDEX idx_roles_lower_name ON roles (lower(name));

    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE roles DROP COLUMN decoration_text;
      ALTER TABLE roles DROP COLUMN decoration_color;
    SQL
  end

end
