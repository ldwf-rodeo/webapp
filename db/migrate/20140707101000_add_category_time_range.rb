class AddCategoryTimeRange < ActiveRecord::Migration
  def up

    execute <<-SQL
     --ALTER TABLE captures ADD COLUMN entered_at TIMESTAMP WITH TIME ZONE;
     --UPDATE captures SET entered_at = created_at;
     --ALTER TABLE captures ALTER COLUMN entered_at SET NOT NULL;
     ALTER TABLE categories ADD COLUMN valid_start_time TIMESTAMP WITH TIME ZONE;
     ALTER TABLE categories ADD COLUMN valid_end_time TIMESTAMP WITH TIME ZONE;
     ALTER TABLE categories ADD CONSTRAINT times_valid CHECK (valid_start_time < valid_end_time);
    SQL

  end

  def down
    execute <<-SQL
       ALTER TABLE categories DROP CONSTRAINT time_valid;
       ALTER TABLE categories DROP COLUMN valid_start_time;
       ALTER TABLE categories DROP COLUMN valid_end_time;
       --ALTER TABLE captures DROP COLUMN entered_at;
    SQL
  end
end