class AddTeamNumber < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE teams ADD COLUMN number bigint;
      ALTER TABLE teams ADD CONSTRAINT uq_teams_number UNIQUE (event_id,number);
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE teams DROP COLUMN number;
    SQL
  end
end