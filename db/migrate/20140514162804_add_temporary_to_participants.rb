class AddTemporaryToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :temporary, :boolean, :default => false
  end
end
