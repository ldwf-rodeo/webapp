class EnhanceLeaderboardScoring < ActiveRecord::Migration

  def up

    execute 'END'
    execute "ALTER TYPE score_grouping ADD VALUE 'top_each';"
    execute <<-SQL
      BEGIN;
      SELECT nextval('units_id_seq');
      SELECT nextval('units_id_seq');
      SELECT nextval('units_id_seq');
      CREATE TYPE score_source AS ENUM ('score','capture_count','first_capture_at','last_capture_at');
      ALTER TABLE categories ALTER COLUMN is_premium SET NOT NULL;
      ALTER TABLE categories ADD COLUMN score_1_source score_source;
      ALTER TABLE categories ADD COLUMN score_1_order score_order;
      ALTER TABLE categories ADD COLUMN score_2_source score_source;
      ALTER TABLE categories ADD COLUMN score_2_order score_order;
      ALTER TABLE categories ADD COLUMN score_3_source score_source;
      ALTER TABLE categories ADD COLUMN score_3_order score_order;
      UPDATE categories SET score_1_source = 'score';
      UPDATE categories SET score_1_order = 'desc';
      UPDATE categories SET score_2_source = 'last_capture_at';
      UPDATE categories SET score_2_order = 'asc';
      ALTER TABLE categories ALTER COLUMN score_1_source SET NOT NULL;
      ALTER TABLE categories ALTER COLUMN score_1_order SET NOT NULL;
      ALTER TABLE categories ADD CONSTRAINT ck_categories_score_2_set_or_not CHECK ((score_2_source IS NULL) = (score_2_order IS NULL));
      ALTER TABLE categories ADD CONSTRAINT ck_categories_score_3_set_or_not CHECK ((score_3_source IS NULL) = (score_3_order IS NULL));

      ALTER TABLE categories DROP COLUMN score_order;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE categories DROP COLUMN score_1_source;
      ALTER TABLE categories DROP COLUMN score_1_order;
      ALTER TABLE categories DROP COLUMN score_2_source;
      ALTER TABLE categories DROP COLUMN score_2_order;
      ALTER TABLE categories DROP COLUMN score_3_source;
      ALTER TABLE categories DROP COLUMN score_3_order;
      DROP TYPE score_source;
      ALTER TABLE categories ADD COLUMN score_order score_order;
      UPDATE categories SET score_order = 'desc';
      ALTER TABLE categories ALTER COLUMN score_order SET NOT NULL;
    SQL
  end

end