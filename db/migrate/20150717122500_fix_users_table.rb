class FixUsersTable < ActiveRecord::Migration

  def up
    execute <<-SQL
      CREATE UNIQUE INDEX idx_users_user_name ON users (lower(user_name));
    SQL
  end

  def down
    execute <<-SQL
      DROP INDEX idx_users_user_name;
    SQL
  end

end
