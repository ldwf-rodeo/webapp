class AddLeaderboardHistoryColumnToEvents < ActiveRecord::Migration
  def change
    add_column :events, :leaderboard_history, :json
  end
end
