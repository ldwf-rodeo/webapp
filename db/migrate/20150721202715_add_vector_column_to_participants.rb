class AddVectorColumnToParticipants < ActiveRecord::Migration
  def change
    execute <<-SQL
      ALTER TABLE participants ADD COLUMN name_vector_index_col tsvector;
        UPDATE participants SET name_vector_index_col = to_tsvector('english', coalesce(first_name,'') || ' ' || coalesce(last_name,''));
      CREATE INDEX idx_participants_name_vector ON participants USING gin(name_vector_index_col);
    SQL
  end
end
