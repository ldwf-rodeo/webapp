class CategoryRandomOrder < ActiveRecord::Migration

  def up
    execute <<-SQL
      ALTER TABLE categories ADD COLUMN score_2_is_random boolean DEFAULT (false);
      ALTER TABLE categories ADD COLUMN score_3_is_random boolean DEFAULT (false);
    SQL
  end

end