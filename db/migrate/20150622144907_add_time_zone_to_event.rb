class AddTimeZoneToEvent < ActiveRecord::Migration
  def change
    add_column :events, :timezone, :string, null: false, default: 'Central Time (US & Canada)'
  end
end
