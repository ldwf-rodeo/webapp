class AddCheckedInToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :checked_in, :boolean, default: false
  end
end
