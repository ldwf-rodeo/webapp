class FIxAssociations < ActiveRecord::Migration
  def self.up



    (query = '') << <<-eod
ALTER TABLE public.categories DROP CONSTRAINT IF EXISTS species_fk CASCADE;
ALTER TABLE public.categories_species DROP CONSTRAINT IF EXISTS  categories_fk CASCADE;
ALTER TABLE public.categories_species DROP CONSTRAINT IF EXISTS  species_fk CASCADE;

DROP TABLE IF EXISTS public.categories_species CASCADE;
DROP TABLE IF EXISTS public.species CASCADE;
    eod

    ActiveRecord::Base.connection.execute(query)


    #remove_column :captures, :species
    #add_column :captures, :species_id, :integer

    # create the species table
    (create_species_table = '') << <<-eod
CREATE TABLE public.species(
  id bigserial,
	name varchar(256),
	CONSTRAINT species_pk PRIMARY KEY (name)

);

ALTER TABLE public.species ADD CONSTRAINT species_uniq_id UNIQUE (id);
ALTER TABLE public.captures DROP COLUMN IF EXISTS species CASCADE;
ALTER TABLE public.captures ADD COLUMN species_id bigint;


    eod

    #create the join table
    (create_cat_species_join_table = '')  << <<-eod
CREATE TABLE public.categories_species(
  species_id bigint,
  category_id bigint,
  CONSTRAINT categories_species_pk PRIMARY KEY (species_id,category_id)
)
    eod

    # create the correct foreign keys for the captures table and the categories table
    (create_constraints = '') << <<-eod
ALTER TABLE public.captures ADD CONSTRAINT species_fk FOREIGN KEY (species_id)
  REFERENCES public.species (id) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE public.captures ALTER COLUMN species_id SET NOT NULL;

ALTER TABLE public.categories_species ADD CONSTRAINT species_fk FOREIGN KEY (species_id)
  REFERENCES public.species (id) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE public.categories_species ADD CONSTRAINT categories_fk FOREIGN KEY (category_id)
  REFERENCES public.categories (id) MATCH FULL
  ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;

    eod

    ActiveRecord::Base.connection.execute(create_species_table)
    ActiveRecord::Base.connection.execute(create_cat_species_join_table)
    ActiveRecord::Base.connection.execute(create_constraints)

  end
end