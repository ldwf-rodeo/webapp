class RemoveSpeciesColumnFromCategory < ActiveRecord::Migration
  def change
    remove_column :categories, :species
  end
end
