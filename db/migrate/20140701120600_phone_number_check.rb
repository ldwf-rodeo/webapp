class PhoneNumberCheck < ActiveRecord::Migration
  def up

    execute <<-SQL
      UPDATE participants SET mobile_phone = NULL WHERE mobile_carrier IS NULL;
      UPDATE participants SET mobile_carrier = NULL WHERE mobile_phone IS NULL;
      ALTER TABLE participants ADD CONSTRAINT ck_participants_phone_check CHECK ((mobile_phone IS NULL) = (mobile_carrier IS NULL));
    SQL

  end

  def down
    execute <<-SQL
      ALTER TABLE participants DROP CONSTRAINT ck_participants_phone_check;
    SQL
  end
end