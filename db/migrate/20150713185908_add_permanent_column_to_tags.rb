class AddPermanentColumnToTags < ActiveRecord::Migration
  def change
    add_column :tags, :permanent, :boolean, null: false, default: false
  end
end
