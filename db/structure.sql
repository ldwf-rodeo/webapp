--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


SET search_path = public, pg_catalog;

--
-- Name: category_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE category_type AS ENUM (
    'species_unit',
    'species_points',
    'group',
    'species_capture'
);


--
-- Name: display_mode; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE display_mode AS ENUM (
    'score_and_unit',
    'smallest_and_largest',
    'score_and_unit_and_count',
    'count',
    'none'
);


--
-- Name: gender; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE gender AS ENUM (
    'male',
    'female'
);


--
-- Name: leaderboard_bump_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE leaderboard_bump_type AS (
	category_id integer,
	unit_id bigint,
	participant_id bigint,
	team_id bigint,
	old_position bigint,
	new_position bigint,
	score numeric,
	usurping_participant_id bigint,
	usurping_team_id bigint,
	usurping_score numeric
);


--
-- Name: leaderboard_display_place_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE leaderboard_display_place_type AS (
	category_id integer,
	"position" bigint,
	team_position bigint,
	is_empty boolean,
	entity_hash text,
	participant_id bigint,
	team_id bigint,
	grouping_index bigint,
	contributing_captures bigint[],
	contributing_ranks bigint[],
	position_label text,
	entity_label text,
	score_label text,
	unit_label text,
	capture_count bigint,
	last_capture_at timestamp with time zone,
	first_capture_at timestamp with time zone,
	smallest_subscore_label text,
	largest_subscore_label text
);


--
-- Name: leaderboard_place_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE leaderboard_place_type AS (
	category_id integer,
	"position" bigint,
	is_empty boolean,
	team_position bigint,
	participant_id bigint,
	team_id bigint,
	grouping_index bigint,
	capture_id bigint,
	contributing_captures bigint[],
	contributing_ranks bigint[],
	score numeric,
	unit_id bigint,
	capture_count bigint,
	last_capture_at timestamp with time zone,
	first_capture_at timestamp with time zone,
	smallest_subscore numeric,
	largest_subscore numeric
);


--
-- Name: leaderboard_point_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE leaderboard_point_type AS (
	point_index bigint,
	category_id integer,
	participant_id bigint,
	team_id bigint,
	points numeric,
	unit_id bigint,
	capture_entered_at timestamp with time zone
);


--
-- Name: participant_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE participant_type AS ENUM (
    'angler',
    'volunteer',
    'organizer',
    'guest',
    'sponsor',
    'vendor',
    'vip'
);


--
-- Name: score_entity; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE score_entity AS ENUM (
    'team',
    'participant'
);


--
-- Name: score_grouping; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE score_grouping AS ENUM (
    'all',
    'none',
    'top_n',
    'selected_n',
    'top_each'
);


--
-- Name: score_order; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE score_order AS ENUM (
    'asc',
    'desc'
);


--
-- Name: score_source; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE score_source AS ENUM (
    'score',
    'capture_count',
    'first_capture_at',
    'last_capture_at',
    'largest_subscore',
    'smallest_subscore'
);


--
-- Name: shirt_size; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE shirt_size AS ENUM (
    's',
    'm',
    'l',
    'xl',
    'xxl',
    'xxxl',
    'xxxxl'
);


--
-- Name: abbreviate_name(text, text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION abbreviate_name(first_name text, last_name text, length integer) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
      WHEN char_length(full_name) >= length THEN concat(substring(first_name,1,1),'. ',last_name)
      ELSE t.full_name
    END
  FROM (
    SELECT concat(first_name,' ',last_name) as full_name
  ) t
$$;


--
-- Name: category_species_unit_items_for_category_and_not_unit(bigint, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION category_species_unit_items_for_category_and_not_unit(p_category_id bigint, p_unit_id bigint) RETURNS bigint
    LANGUAGE sql
    AS $$
          SELECT COUNT(*) FROM category_species_unit_items WHERE category_id = p_category_id AND unit_id <> p_unit_id;
      $$;


--
-- Name: format_number(numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION format_number(n numeric) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
      WHEN CAST(n AS bigint) = n THEN TRIM(to_char(n,'999,999,999'))
      ELSE TRIM(to_char(n,'999,999,999.FM99999999999'))
    END
$$;


--
-- Name: gapless_sequence_create(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_create(seq_name text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
  BEGIN
    IF EXISTS (SELECT 0 FROM pg_class WHERE relname = $1) THEN
      RETURN;
    ELSE
      -- Initialize to 0 and then immediately call nextval to populate currval.
      EXECUTE 'CREATE SEQUENCE ' || $1 || ' minvalue 0;';
      PERFORM nextval($1);
    END IF;
  END
$_$;


--
-- Name: gapless_sequence_destroy(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_destroy(seq_name text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
  BEGIN
    IF EXISTS (SELECT 0 FROM pg_class WHERE relname = $1) THEN
      EXECUTE 'DROP SEQUENCE IF EXISTS ' || $1 || ';';
    END IF;
    DELETE FROM gapless_sequence_recycled_numbers WHERE sequence_name = $1;
  END
$_$;


--
-- Name: gapless_sequence_getval(text, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_getval(seq_name text, requested_number bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    IF ($2 IS NULL) THEN
      -- If the requested number is NULL, then just return the next value.
      RETURN gapless_sequence_nextval($1);
    END IF;
    SELECT "number" INTO var FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND "number" = $2 LIMIT 1;
      IF (var IS NULL) THEN
        --It is not recycled.
        IF (hacked_currval($1) >= $2) THEN
          --The number has already been passed up. We can't get it.
          RAISE EXCEPTION 'The gapless value % is not available for sequence %.', $2, $1;
        ELSE
          --Recycle all un-recycled numbers less than the requested number.
          WITH trashed AS (
            SELECT
              $1 as sequence_name,
              series."number" as "number"
            FROM
              generate_series(hacked_currval($1)+1,$2-1) as series("number")
              LEFT JOIN
                gapless_sequence_recycled_numbers
              ON
                series."number" = gapless_sequence_recycled_numbers."number"
                AND gapless_sequence_recycled_numbers.sequence_name = $1
            WHERE
              gapless_sequence_recycled_numbers."number" IS NULL
            ORDER BY
              series."number"
          )
          INSERT INTO gapless_sequence_recycled_numbers (sequence_name,"number") SELECT * FROM trashed;
          --Bump up the sequence.
          EXECUTE 'ALTER SEQUENCE ' || $1 || ' RESTART WITH ' || $2 || ';';
          --Get the value.
          var := nextval($1);
          RETURN var;
        END IF;
      ELSE
        --The requested number is already recycled. Just get it.
        DELETE FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND "number" = var;
        RETURN var;
      END IF;
  END;
$_$;


--
-- Name: gapless_sequence_nextval(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_nextval(seq_name text) RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    SELECT number INTO var FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 ORDER BY number ASC LIMIT 1;
    IF (var IS NULL) THEN
      --Nothing recycled. Call the sequence.
      var := nextval($1);
    ELSE
      --We have a recycled number. Remove the entry and return it.
      DELETE FROM gapless_sequence_recycled_numbers WHERE "sequence_name" = $1 AND number = var;
    END IF;
    RETURN var;
  END;
$_$;


--
-- Name: gapless_sequence_recycle(text, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_recycle(seq_name text, rec_number bigint) RETURNS void
    LANGUAGE plpgsql
    AS $_$
  BEGIN
    PERFORM gapless_sequence_create($1);
    IF EXISTS (SELECT 0 FROM gapless_sequence_recycled_numbers WHERE sequence_name = $1 AND number = $2) THEN
      RAISE WARNING 'Number % has already been recycled from sequence %.', $2, $1;
    ELSIF (hacked_currval($1) < $2) THEN
      RAISE WARNING 'Cannot recycle % from sequence % as it is greater than the sequence value.', $2, $1;
    ELSE
      -- Recycle the number.
      INSERT INTO gapless_sequence_recycled_numbers ("sequence_name","number") VALUES ($1,$2);
    END IF;
  END
$_$;


--
-- Name: gapless_sequence_sync_with_values(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION gapless_sequence_sync_with_values(seq_name text, table_name text, column_name text) RETURNS void
    LANGUAGE plpgsql
    AS $_$
  BEGIN
    PERFORM gapless_sequence_destroy($1);
    EXECUTE 'SELECT gapless_sequence_getval(''' || $1 || ''',' || $3 || ') FROM ' || $2 || ' ORDER BY ID ASC;';
  END
$_$;


--
-- Name: get_event_leaderboard_display_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_event_leaderboard_display_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_display_place_type
    LANGUAGE sql
    AS $_$
  SELECT
    -- The basic info from the function call.
    ranks.category_id,
    ranks.position,
    ranks.team_position,
    team_id IS NULL as is_empty,
    CAST(CASE
      WHEN team_id IS NULL THEN CONCAT('category_',category_id,'_empty_',ranks.position)
      ELSE
        CONCAT(
          'category_',ranks.category_id,'_',
          CASE
            WHEN categories.score_entity = 'team' THEN CONCAT('team_',team_id,'_')
            WHEN categories.score_entity = 'participant' THEN CONCAT('participant_',participant_id,'_')
          END,
          CASE
            WHEN categories.score_grouping = 'none' THEN CONCAT('capture_',ranks.capture_id)
            ELSE CONCAT('group_',grouping_index)
          END
        )
    END AS text) as entity_hash,
    ranks.participant_id as participant_id,
    ranks.team_id as team_id,
    ranks.grouping_index as grouping_index,
    ranks.contributing_captures AS contributing_captures,
    ranks.contributing_ranks AS contributing_ranks,
    -- Nice labels for leaderboard
    TRIM(to_char(ranks.position, '9999999th')) AS position_label,
    CASE
      WHEN categories.score_entity = 'team' THEN truncate_text(teams.name,50)
      WHEN categories.score_entity = 'participant' THEN truncate_text(abbreviate_name(participants.first_name,participants.last_name,50),50)
    END AS entity_label,
    format_number(ranks.score) as score_label,
    COALESCE(units.abbreviation,CASE WHEN categories.category_type = 'species_capture' THEN 'ct.' ELSE 'pts.' END) as unit_label,
    ranks.capture_count as capture_count,
    ranks.last_capture_at,
    ranks.first_capture_at,
    format_number(ranks.smallest_subscore) as smallest_subscore_label,
    format_number(ranks.largest_subscore) as largest_subscore_label
  FROM
    get_event_leaderboard_for_event($1,snapshot) as ranks
    INNER JOIN categories ON categories.id = ranks.category_id
    INNER JOIN super_categories ON super_categories.id = categories.super_category_id
    LEFT JOIN participants ON participants.id = ranks.participant_id
    LEFT JOIN teams ON teams.id = ranks.team_id
    LEFT JOIN units ON units.id = ranks.unit_id
  ORDER BY
    ranks.category_id ASC,
    ranks.position ASC
$_$;


--
-- Name: get_event_leaderboard_display_json_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_event_leaderboard_display_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS json
    LANGUAGE sql
    AS $_$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_event_leaderboard_display_for_event($1,snapshot)
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$_$;


--
-- Name: get_event_leaderboard_display_structured_json_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_event_leaderboard_display_structured_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS json
    LANGUAGE sql
    AS $_$
  WITH rankings as (
      SELECT
        t.*,
        categories.super_category_id as "super_category_id"
      FROM
        get_event_leaderboard_display_for_event($1,snapshot) as t
        LEFT JOIN categories ON t.category_id = categories.id
      ORDER BY
        categories.super_category_id ASC,
        t.category_id ASC,
        t.position ASC
  )
  SELECT
    to_json(d) AS "stuff"
  FROM
    (
      SELECT
        events.name as "name",
        events.start_date as "start_date",
        events.end_date as "end_date",
        (
          -- aggregate all the super categories for this event tuple
          SELECT json_agg(t)
          FROM (
                 -- select all the super categories for the event
                 SELECT
                   super_categories.name,
                   (
                     -- aggregate all  the categories for this super category tuple
                     SELECT
                       json_agg(t)
                      FROM (
                        -- select all the categories for the super category
                        SELECT
                          categories.name,
                          categories.display_mode,
                          (
                            -- aggregate all the rankings that relate to this category
                            SELECT json_agg(t)
                            FROM (

                              -- select the rankings
                              SELECT
                                rankings.*,
                                teams.notes as team_notes,
                                teams.name as team_name
                              FROM
                                rankings
                                LEFT JOIN teams on rankings.team_id = teams.id
                              WHERE
                                rankings.category_id = categories.id
                              ORDER BY
                                rankings.position ASC
                            ) t
                            ---------------------------------------------

                          ) as "rankings"
                        FROM
                          categories
                        WHERE
                          categories.super_category_id = super_categories.id
                        ORDER BY
                          categories.grouping_number ASC,
                          categories.position ASC,
                          categories.name ASC,
                          categories.id ASC
                      ) t
                      ---------------------------------------------

                   ) as "categories"
                 FROM
                   super_categories
                 WHERE
                   super_categories.event_id = events.id
                 ORDER BY
                   super_categories.page ASC,
                   super_categories.position ASC,
                   super_categories.name ASC,
                   super_categories.id ASC
               ) t
               ---------------------------------------------

        ) as category_groups
      FROM
        events
      WHERE
        events.id = event_id
    ) d

$_$;


--
-- Name: get_event_leaderboard_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_event_leaderboard_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_place_type
    LANGUAGE sql
    AS $_$
  WITH category_places as (
    WITH RECURSIVE places(category_id, position, positions) AS (
      SELECT
        categories.id,
        CAST(1 AS bigint) as place,
        categories.positions
      FROM
        categories
        INNER JOIN super_categories ON super_categories.id = categories.super_category_id
        INNER JOIN events ON events.id = super_categories.event_id
      WHERE
        events.id = $1
    UNION ALL -- Recurse to get the places for each category we are considering...
      SELECT
        category_id,
        position+1,
        positions as positions
      FROM
        places
      WHERE
        position < positions
    )
    SELECT
      category_id,
      position
    FROM
      places
  )
  SELECT
    category_places.category_id,
    category_places.position,
    team_id IS NULL AS is_empty,
    leaderboard.team_position,
    leaderboard.participant_id,
    leaderboard.team_id,
    leaderboard.grouping_index,
    leaderboard.capture_id,
    leaderboard.contributing_captures,
    leaderboard.contributing_ranks,
    leaderboard.score,
    leaderboard.unit_id,
    leaderboard.capture_count,
    leaderboard.last_capture_at,
    leaderboard.first_capture_at,
    leaderboard.smallest_subscore,
    leaderboard.largest_subscore
  FROM
    category_places
    LEFT JOIN get_leaderboard_for_event(event_id,snapshot) AS leaderboard ON category_places.category_id = leaderboard.category_id AND category_places.position = leaderboard.position

$_$;


--
-- Name: get_leaderboard_bumps_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_leaderboard_bumps_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_bump_type
    LANGUAGE sql
    AS $$
  WITH
  full_old AS (SELECT * FROM get_event_leaderboard_for_event(event_id,snapshot)),
  full_new AS (SELECT * FROM get_event_leaderboard_for_event(event_id,NULL)),
  old AS (SELECT * FROM (SELECT DISTINCT ON (category_id, participant_id, team_id) * FROM full_old WHERE team_id IS NOT NULL ORDER by category_id, participant_id, team_id, position) t ORDER BY category_id ASC, position ASC),
  new AS (SELECT * FROM (SELECT DISTINCT ON (category_id, participant_id, team_id) * FROM full_new WHERE team_id IS NOT NULL ORDER BY category_id, participant_id, team_id, position) t ORDER BY category_id ASC, position ASC),
  -- Find the people who have gone down in the leaderboard.
  losers AS (SELECT
    old.category_id as category_id,
    old.unit_id as unit_id,
    old.participant_id as participant_id,
    old.team_id as team_id,
    old.position as old_position,
    new.position as new_position,
    old.score as score
  FROM
    old
    LEFT JOIN new ON old.category_id = new.category_id AND ((old.participant_id IS NULL AND new.participant_id IS NULL) OR old.participant_id = new.participant_id) AND old.team_id = new.team_id
  WHERE
    (
      new.category_id IS NULL -- THEY WERE KICKED OFF THE LEADERBOARD
      OR
      new.position > old.position -- THEY WERE MOVED DOWN A SLOT
    )
  )
  -- Now find who replaced them.
  SELECT
    losers.*,
    winners.participant_id as usurping_participant_id,
    winners.team_id as usurping_team_id,
    winners.score as usurping_score
  FROM
    losers
    INNER JOIN full_new AS winners ON losers.category_id = winners.category_id AND losers.old_position = winners.position
  ORDER BY
    category_id ASC,
    old_position ASC

$$;


--
-- Name: get_leaderboard_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_leaderboard_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_place_type
    LANGUAGE sql
    AS $_$

  WITH

  category_information AS (
    SELECT
      categories.id AS category_id,
      categories.start_age AS category_start_age,
      categories.end_age AS category_end_age,
      categories.gender_id AS category_gender_id,
      categories.category_type AS category_type,
      categories.positions AS category_positions,
      categories.name AS category_name,
      categories.score_entity AS category_score_entity,
      categories.score_grouping AS category_score_grouping,
      categories.score_grouping_limit AS category_score_grouping_limit,
      categories.has_distinct_places as category_has_distinct_places,
      categories.valid_start_time as category_valid_start_time,
      categories.valid_end_time as category_valid_end_time,
      categories.premium_category_grouping_id as category_premium_category_grouping_id,
      categories.score_1_source, categories.score_2_source, categories.score_3_source,
      categories.score_1_order, categories.score_2_order, categories.score_3_order
    FROM
      categories
      INNER JOIN super_categories ON categories.super_category_id = super_categories.id
      INNER JOIN events ON super_categories.event_id = events.id AND categories.positions > 0
    WHERE
      events.id = $1
      AND categories.category_type IN ('species_unit','species_points','species_capture')
  ),

  potential_captures AS (
     SELECT
      captures.entered_at AS capture_entered_at,
      captures.id AS capture_id,
      captures.species_id AS species_id,
      participants.id AS participant_id,
      participants.gender_id AS participant_gender_id,
      teams.id AS team_id,
      category_information.*
    FROM
      captures
      INNER JOIN species ON species.id = captures.species_id
      INNER JOIN participants ON participants.id = captures.participant_id
      INNER JOIN participations ON participations.participant_id = participants.id
      INNER JOIN teams on teams.id = participations.team_id AND teams.event_id = $1
      INNER JOIN events on events.id = teams.event_id
      INNER JOIN category_information ON ( TRUE
        -- Join each capture on each of the categories that it qualifies for.
        AND (category_information.category_gender_id IS NULL OR participants.gender_id = category_information.category_gender_id)
        AND (category_information.category_start_age IS NULL OR (participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,participants.date_of_birth,CAST(events.start_date AS date)) >= category_information.category_start_age))
        AND (category_information.category_end_age   IS NULL OR (participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,participants.date_of_birth,CAST(events.start_date AS date)) <= category_information.category_end_age))
        AND (category_information.category_valid_start_time IS NULL OR captures.entered_at >= category_information.category_valid_start_time)
        AND (category_information.category_valid_end_time   IS NULL OR captures.entered_at <= category_information.category_valid_end_time)
        AND (
          --Check that the participant's team qualifies if the category belongs to a premium grouping.
          category_information.category_premium_category_grouping_id IS NULL
          OR EXISTS (
            SELECT
              *
            FROM
              team_premium_category_groupings
              INNER JOIN premium_category_groupings ON premium_category_groupings.id = team_premium_category_groupings.premium_category_grouping_id
              INNER JOIN categories ON categories.premium_category_grouping_id = premium_category_groupings.id
            WHERE
              categories.id = category_information.category_id
              AND team_premium_category_groupings.team_id = teams.id
          )
        )
      )
    WHERE
      (snapshot IS NULL OR captures.entered_at < snapshot)
      AND captures.event_id = $1
  ),

  potential_measurements AS (
    SELECT
      potential_captures.*,
      capture_measurements.measurement AS capture_measurement,
      units.id AS unit_id
    FROM
      potential_captures
      INNER JOIN capture_measurements ON capture_measurements.capture_id = potential_captures.capture_id
      INNER JOIN units ON units.id = capture_measurements.unit_id
  ),

  -- Fit each raw value into the appropriate category item (if it fits) and find the measurement or point value.
  species_points AS (
   SELECT
     row_number() OVER () AS species_points_index,
     *
   FROM (
     SELECT
       potential_measurements.capture_measurement AS points,
       potential_measurements.*
     FROM
       potential_measurements
       INNER JOIN category_species_unit_items ON category_species_unit_items.category_id = potential_measurements.category_id AND category_species_unit_items.species_id = potential_measurements.species_id AND category_species_unit_items.unit_id = potential_measurements.unit_id
     WHERE
       potential_measurements.category_type = 'species_unit'

     UNION ALL

      SELECT
        category_species_points_items.points as points,
        potential_captures.*,
        NULL AS capture_measurement,                -- species_points are not based on a measurement, so null these out.
        NULL AS unit_id
      FROM
        potential_captures
        INNER JOIN category_species_points_items ON category_species_points_items.category_id = potential_captures.category_id AND category_species_points_items.species_id = potential_captures.species_id
      WHERE
        potential_captures.category_type = 'species_points'

     UNION ALL

     SELECT
       1 AS points,
       potential_captures.*,
       NULL AS capture_measurement,                -- species_capture are not based on a measurement, so null these out.
       NULL AS unit_id
     FROM
       potential_captures
       INNER JOIN category_species_capture_items ON category_species_capture_items.category_id = potential_captures.category_id AND category_species_capture_items.species_id = potential_captures.species_id
     WHERE
       potential_captures.category_type = 'species_capture'
    ) t
  ),

  ------------------------------------------------------------------
  -- SPECIES SCORES
  ------------------------------------------------------------------

  -- Group and sort appropriately and then possibly filter out only the top N values.
  species_partitioned_points AS (
    SELECT
      *
    FROM
      (
        SELECT
          row_number() OVER (
            PARTITION BY
              species_points.category_id,
              CASE WHEN species_points.category_score_entity = 'team' THEN species_points.team_id END,
              CASE WHEN species_points.category_score_entity = 'participant' THEN species_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN points END DESC
          ) as top_point_rank,
          row_number() OVER (
            PARTITION BY
              species_points.category_id,
              species_points.species_id,
              CASE WHEN species_points.category_score_entity = 'team' THEN species_points.team_id END,
              CASE WHEN species_points.category_score_entity = 'participant' THEN species_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN points END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN points END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN capture_entered_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN capture_entered_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN points END DESC
          ) as each_species_rank,
          species_points.*
        FROM
          species_points
      ) t
  ),

  --Find out everybody's score for each category.
  species_scores AS (
   SELECT
     category_score_entity,
     category_has_distinct_places,
     category_score_grouping,
     category_id,
     score_1_source, score_2_source, score_3_source,
     score_1_order, score_2_order, score_3_order,
     COUNT(capture_id) AS capture_count,
     array_agg(capture_id) AS contributing_captures,
     CAST (CASE
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN MAX(species_partitioned_points.species_points_index)
        WHEN species_partitioned_points.category_score_grouping = 'top_each' THEN MAX(species_partitioned_points.each_species_rank)
        WHEN species_partitioned_points.category_score_grouping = 'top_n' THEN floor((MAX(species_partitioned_points.top_point_rank)-1)/MAX(category_score_grouping_limit))
        WHEN species_partitioned_points.category_score_grouping = 'all' THEN 1
     END AS bigint) AS grouping_index,
     CASE
       WHEN species_partitioned_points.category_score_entity = 'team' THEN NULL
       WHEN species_partitioned_points.category_score_entity = 'participant' THEN MAX(participant_id) -- Hack
     END AS participant_id,
     MAX(team_id) AS team_id, -- Hack
     CASE
        -- If this refers to a specific single capture, keep the reference.
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN MAX(capture_id)
        ELSE NULL
     END as capture_id,
     SUM(points) AS score, -- Not a hack
     MAX(capture_entered_at) AS last_capture_at,
     MIN(capture_entered_at) AS first_capture_at,
     MAX(points) as largest_subscore,
     MIN(points) as smallest_subscore,
     unit_id AS unit_id
   FROM
     species_partitioned_points
   GROUP BY
     category_id,
     category_score_entity,
     category_has_distinct_places,
     category_score_grouping,
     score_1_source, score_2_source, score_3_source,
     score_1_order, score_2_order, score_3_order,
     unit_id,
     CASE
        WHEN species_partitioned_points.category_score_grouping = 'none' THEN species_partitioned_points.species_points_index
        WHEN species_partitioned_points.category_score_grouping = 'top_each' THEN species_partitioned_points.each_species_rank
        WHEN species_partitioned_points.category_score_grouping = 'top_n' THEN floor((species_partitioned_points.top_point_rank-1)/category_score_grouping_limit)
        -- The 'all' case will group on everything.
     END,
     CASE
        -- If we are grouping scores, then also group by the appropriate entity.
        WHEN species_partitioned_points.category_score_grouping <> 'none' AND species_partitioned_points.category_score_entity = 'team' THEN species_partitioned_points.team_id
        WHEN species_partitioned_points.category_score_grouping <> 'none' AND species_partitioned_points.category_score_entity = 'participant' THEN species_partitioned_points.participant_id
        ELSE NULL
     END
  ),

  -- If the positions must be unique, then we purge out redundant entries.
  species_filtered_scores AS (
    SELECT
      *
    FROM (
      SELECT
        *,
        row_number() OVER (
          PARTITION BY
            category_id,
            CASE
              WHEN category_score_entity = 'team' THEN team_id
              WHEN category_score_entity = 'participant' THEN participant_id
            END
          ORDER BY
            -- FIRST
            CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
            CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
            -- SECOND
            CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
            CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
            -- THIRD
            CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
            CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as member_position
      FROM
        species_scores
    ) t
    WHERE
      category_has_distinct_places = FALSE OR member_position = 1
  ),

  ------------------------------------------------------------------
  -- SPECIES RANKS
  ------------------------------------------------------------------

  -- Now order appropriately and find everyone's actual rank.
  species_ranks AS (
    SELECT
      category_id,
      -- We don't need distinct positions, so just partition over each category.
      row_number() OVER (
        PARTITION BY category_id
        ORDER BY
          -- FIRST
          CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
          CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
          -- SECOND
          CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
          CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
          -- THIRD
          CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
          CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
      ) as position,
      participant_id,
      team_id,
      grouping_index as grouping_index,
      capture_id AS capture_id,
      contributing_captures,
      ARRAY[]::bigint[][] AS contributing_ranks,
      score,
      unit_id,
      capture_count,
      last_capture_at,
      first_capture_at,
      smallest_subscore,
      largest_subscore
    FROM
      species_filtered_scores
  ),

  ------------------------------------------------------------------
  -- GROUPING POINTS
  ------------------------------------------------------------------

  grouping_points AS (
    SELECT
      row_number() OVER () as grouping_points_index,
      group_categories.id as category_id,
      ranked_categories.id as ranking_category_id,
      group_categories.score_entity as category_score_entity,
      group_categories.score_grouping as category_score_grouping,
      group_categories.score_grouping_limit as category_score_grouping_limit,
      group_categories.has_distinct_places as category_has_distinct_places,
      group_categories.score_1_source, group_categories.score_2_source, group_categories.score_3_source,
      group_categories.score_1_order, group_categories.score_2_order, group_categories.score_3_order,
      species_ranks.participant_id as participant_id,
      species_ranks.team_id as team_id,
      cast(category_grouping_items.points as numeric) AS points,
      capture_count as capture_count,
      species_ranks.contributing_captures AS contributing_captures,
      ARRAY[species_ranks.category_id, species_ranks.position, category_grouping_items.points] as contributing_rank,
      last_capture_at as last_capture_at,
      first_capture_at as first_capture_at,
      largest_subscore as largest_subscore,
      smallest_subscore as smallest_subscore
    FROM
      species_ranks
      INNER JOIN categories AS ranked_categories ON ranked_categories.id = species_ranks.category_id
      INNER JOIN super_categories AS ranked_super_categories ON ranked_super_categories.id = ranked_categories.super_category_id
      LEFT JOIN participants AS ranked_participants ON species_ranks.participant_id = ranked_participants.id
      LEFT JOIN teams AS ranked_teams ON species_ranks.team_id = ranked_teams.id
      INNER JOIN category_grouping_items_super_categories ON category_grouping_items_super_categories.super_category_id = ranked_super_categories.id
      INNER JOIN category_grouping_items ON category_grouping_items.id = category_grouping_items_super_categories.category_grouping_item_id AND category_grouping_items.place = species_ranks.position AND category_grouping_items.place <= ranked_categories.positions
      INNER JOIN categories AS group_categories ON group_categories.id = category_grouping_items.category_id
      INNER JOIN super_categories AS group_super_categories ON group_categories.super_category_id = group_super_categories.id
      INNER JOIN events ON events.id = group_super_categories.event_id
    WHERE
      group_categories.category_type = 'group'
      AND ranked_categories.category_type IN ('species_unit','species_points','species_capture') --Don't allow groups of groups.
      -- Setting an age/gender limit on a grouping category forces us to ignore team categories.
      AND (group_categories.gender_id IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.gender_id = group_categories.gender_id))
      AND (group_categories.start_age IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,ranked_participants.date_of_birth,CAST(events.start_date as date)) >= group_categories.start_age))
      AND (group_categories.end_age   IS NULL OR (ranked_participants.id IS NOT NULL AND ranked_participants.date_of_birth IS NOT NULL AND get_participant_age(NULL,ranked_participants.date_of_birth,CAST(events.start_date as date)) <= group_categories.end_age))
      -- A valid_start/end time for a grouping category means that each category's valid start/end times must be within this range.
      AND (group_categories.valid_start_time IS NULL OR (ranked_categories.valid_start_time IS NOT NULL AND ranked_categories.valid_start_time >= group_categories.valid_start_time))
      AND (group_categories.valid_end_time   IS NULL OR (ranked_categories.valid_end_time   IS NOT NULL AND ranked_categories.valid_end_time <= group_categories.valid_end_time))
      AND (
        --Check that the participant's team qualifies if the category belongs to a premium grouping.
        group_categories.premium_category_grouping_id IS NULL
        OR EXISTS (
            SELECT
              *
            FROM
              team_premium_category_groupings
              INNER JOIN premium_category_groupings ON premium_category_groupings.id = team_premium_category_groupings.premium_category_grouping_id
              INNER JOIN categories ON categories.premium_category_grouping_id = premium_category_groupings.id
            WHERE
              categories.id = group_categories.id
              AND team_premium_category_groupings.team_id = ranked_teams.id
        )
    )
  ),

  ------------------------------------------------------------------
  -- GROUPING SCORES
  ------------------------------------------------------------------

  -- Group and sort appropriately and then possibly filter out only the top N values.
  grouping_partitioned_points AS (
    SELECT
      *
    FROM
      (
        SELECT
          row_number() OVER (
            PARTITION BY
              grouping_points.category_id,
              CASE WHEN grouping_points.category_score_entity = 'team' THEN grouping_points.team_id END,
              CASE WHEN grouping_points.category_score_entity = 'participant' THEN grouping_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as top_point_rank,
          row_number() OVER (
            PARTITION BY
              grouping_points.category_id,
              grouping_points.ranking_category_id,
              CASE WHEN grouping_points.category_score_entity = 'team' THEN grouping_points.team_id END,
              CASE WHEN grouping_points.category_score_entity = 'participant' THEN grouping_points.participant_id END
            ORDER BY
              -- FIRST
              CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN points END ASC,
              CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN points END DESC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
              -- SECOND
              CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN points END ASC,
              CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN points END DESC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
              -- THIRD
              CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN points END ASC,
              CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN points END DESC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
              CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
              CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
              CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
              CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
              CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as each_category_rank,
        grouping_points.*
        FROM
          grouping_points
      ) t
  ),

  --Find out everybody's score for each category.
  grouping_scores AS (
     SELECT
       category_score_entity,
       category_has_distinct_places,
       category_id,
       score_1_source, score_2_source, score_3_source,
       score_1_order, score_2_order, score_3_order,
       CAST (CASE
          WHEN grouping_partitioned_points.category_score_grouping = 'none' THEN MAX(grouping_partitioned_points.grouping_points_index)
          WHEN grouping_partitioned_points.category_score_grouping = 'top_each' THEN MAX(grouping_partitioned_points.each_category_rank)
          WHEN grouping_partitioned_points.category_score_grouping = 'top_n' THEN floor((MAX(grouping_partitioned_points.top_point_rank)-1)/MAX(category_score_grouping_limit))
          WHEN grouping_partitioned_points.category_score_grouping = 'all' THEN 1
       END AS bigint) AS grouping_index,
       CASE
         WHEN grouping_partitioned_points.category_score_entity = 'team' THEN NULL
         WHEN grouping_partitioned_points.category_score_entity = 'participant' THEN MAX(participant_id) -- Hack
       END AS participant_id,
       array_cat_agg(contributing_captures) AS redundant_contributing_captures,
       my_array_sum(ARRAY[contributing_rank]) as contributing_ranks,
       MAX(team_id) AS team_id, -- Hack
       SUM(points) AS score, -- Not a hack
       CAST(NULL AS bigint) AS unit_id,
       MAX(last_capture_at) AS last_capture_at,
       MIN(first_capture_at) as first_capture_at,
       MAX(points) as largest_subscore,
       MIN(points) as smallest_subscore
     FROM
       grouping_partitioned_points
     GROUP BY
       category_id,
       category_score_entity,
       category_score_grouping,
       category_has_distinct_places,
       score_1_source, score_2_source, score_3_source,
       score_1_order, score_2_order, score_3_order,
       CASE
          WHEN grouping_partitioned_points.category_score_grouping = 'none' THEN grouping_partitioned_points.grouping_points_index
          WHEN grouping_partitioned_points.category_score_grouping = 'top_each' THEN grouping_partitioned_points.each_category_rank
          WHEN grouping_partitioned_points.category_score_grouping = 'top_n' THEN floor((grouping_partitioned_points.top_point_rank-1)/category_score_grouping_limit)
          -- The 'all' case will group on everything.
       END,
       CASE
          -- If we are grouping scores, then also group by the appropriate entity.
          WHEN grouping_partitioned_points.category_score_grouping <> 'none' AND grouping_partitioned_points.category_score_entity = 'team' THEN grouping_partitioned_points.team_id
          WHEN grouping_partitioned_points.category_score_grouping <> 'none' AND grouping_partitioned_points.category_score_entity = 'participant' THEN grouping_partitioned_points.participant_id
          ELSE NULL
       END
  ),

  -- Capture_count will not be accurate since we may be taking from multiple categories in the grouping aggregate.
  corrected_grouping_scores AS (
      SELECT
        *,
        ARRAY(SELECT DISTINCT UNNEST(redundant_contributing_captures)) as contributing_captures,
        (SELECT COUNT(*) FROM (SELECT DISTINCT UNNEST(redundant_contributing_captures)) t) as capture_count
      FROM
        grouping_scores
  ),

  -- If the positions must be unique, then we purge out redundant entries.
  grouping_filtered_scores AS (
    SELECT
      *
    FROM (
      SELECT
        *,
        row_number() OVER (
          PARTITION BY
            category_id,
            CASE
              WHEN category_score_entity = 'team' THEN team_id
              WHEN category_score_entity = 'participant' THEN participant_id
            END
          ORDER BY
            -- FIRST
            CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
            CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
            -- SECOND
            CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
            CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
            -- THIRD
            CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
            CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
            CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
            CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
            CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
            CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
            CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
          ) as member_position
      FROM
        corrected_grouping_scores
    ) t
    WHERE
      category_has_distinct_places = FALSE OR member_position = 1
  ),

  ------------------------------------------------------------------
  -- GROUPING RANKS
  ------------------------------------------------------------------

  -- Now order appropriately and find everyone's actual rank.
  grouping_ranks AS (
    SELECT
      category_id,
      -- We don't need distinct positions, so just partition over each category.
      row_number() OVER (
        PARTITION BY category_id
        ORDER BY
          -- FIRST
          CASE WHEN score_1_source = 'score' AND score_1_order = 'asc' THEN score END ASC,
          CASE WHEN score_1_source = 'score' AND score_1_order = 'desc' THEN score END DESC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_1_source = 'capture_count' AND score_1_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_1_source = 'last_capture_at' AND score_1_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_1_source = 'first_capture_at' AND score_1_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_1_source = 'smallest_subscore' AND score_1_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_1_source = 'largest_subscore' AND score_1_order = 'desc' THEN largest_subscore END DESC,
          -- SECOND
          CASE WHEN score_2_source = 'score' AND score_2_order = 'asc' THEN score END ASC,
          CASE WHEN score_2_source = 'score' AND score_2_order = 'desc' THEN score END DESC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_2_source = 'capture_count' AND score_2_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_2_source = 'last_capture_at' AND score_2_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_2_source = 'first_capture_at' AND score_2_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_2_source = 'smallest_subscore' AND score_2_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_2_source = 'largest_subscore' AND score_2_order = 'desc' THEN largest_subscore END DESC,
          -- THIRD
          CASE WHEN score_3_source = 'score' AND score_3_order = 'asc' THEN score END ASC,
          CASE WHEN score_3_source = 'score' AND score_3_order = 'desc' THEN score END DESC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'asc' THEN capture_count END ASC,
          CASE WHEN score_3_source = 'capture_count' AND score_3_order = 'desc' THEN capture_count END DESC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'asc' THEN last_capture_at END ASC,
          CASE WHEN score_3_source = 'last_capture_at' AND score_3_order = 'desc' THEN last_capture_at END DESC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'asc' THEN first_capture_at END ASC,
          CASE WHEN score_3_source = 'first_capture_at' AND score_3_order = 'desc' THEN first_capture_at END DESC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'asc' THEN smallest_subscore END ASC,
          CASE WHEN score_3_source = 'smallest_subscore' AND score_3_order = 'desc' THEN smallest_subscore END DESC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'asc' THEN largest_subscore END ASC,
          CASE WHEN score_3_source = 'largest_subscore' AND score_3_order = 'desc' THEN largest_subscore END DESC
      ) as position,
      participant_id,
      team_id,
      grouping_index as grouping_index,
      CAST(NULL AS bigint) AS capture_id,
      contributing_captures,
      contributing_ranks,
      score,
      unit_id,
      capture_count,
      last_capture_at,
      first_capture_at,
      smallest_subscore,
      largest_subscore
    FROM
      grouping_filtered_scores
  ),

  ------------------------------------------------------------------
  -- COMBINE SPECIES AND GROUPING
  ------------------------------------------------------------------

   both_ranks AS (
    SELECT * FROM species_ranks
    UNION ALL
    SELECT * FROM grouping_ranks
   )

  ------------------------------------------------------------------
  -- NOW JUST SELECT
  ------------------------------------------------------------------

  SELECT
    both_ranks.category_id,
    both_ranks.position,
    both_ranks.team_id IS NULL as is_empty,
    row_number() OVER (PARTITION BY both_ranks.category_id, both_ranks.team_id ORDER BY both_ranks.category_id, both_ranks.position) as team_position,
    both_ranks.participant_id,
    both_ranks.team_id,
    both_ranks.grouping_index,
    both_ranks.capture_id,
    both_ranks.contributing_captures,
    both_ranks.contributing_ranks,
    both_ranks.score,
    both_ranks.unit_id,
    both_ranks.capture_count,
    both_ranks.last_capture_at,
    both_ranks.first_capture_at,
    both_ranks.smallest_subscore,
    both_ranks.largest_subscore
  FROM
    both_ranks

$_$;


--
-- Name: get_participant_age(bigint, date, date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_participant_age(age bigint, date_of_birth date, at_date date) RETURNS integer
    LANGUAGE sql
    AS $$
  SELECT
    CASE
      WHEN age IS NOT NULL THEN CAST(age as integer)
      WHEN date_of_birth IS NOT NULL THEN CAST(EXTRACT(year from AGE(at_date, date_of_birth)) as integer)
      ELSE NULL
    END
$$;


--
-- Name: get_unlimited_leaderboard_display_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_unlimited_leaderboard_display_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS SETOF leaderboard_display_place_type
    LANGUAGE sql
    AS $$
  SELECT
    -- The basic info from the function call.
    ranks.category_id,
    ranks.position,
    ranks.team_position,
    ranks.team_id IS NULL as is_empty,
    CAST(CASE
      WHEN team_id IS NULL THEN CONCAT('category_',category_id,'_empty_',ranks.position)
      ELSE
        CONCAT(
          'category_',ranks.category_id,'_',
          CASE
            WHEN categories.score_entity = 'team' THEN CONCAT('team_',team_id,'_')
            WHEN categories.score_entity = 'participant' THEN CONCAT('participant_',participant_id,'_')
          END,
          CASE
            WHEN categories.score_grouping = 'none' THEN CONCAT('capture_',ranks.capture_id)
            ELSE CONCAT('group_',grouping_index)
          END
        )
    END AS text) as entity_hash,
    ranks.participant_id as participant_id,
    ranks.team_id as team_id,
    ranks.grouping_index AS grouping_index,
    ranks.contributing_captures,
    ranks.contributing_ranks,
    -- Nice labels for leaderboard
    TRIM(to_char(ranks.position, '9999999th')) AS position_label,
    CASE
      WHEN categories.score_entity = 'team' THEN truncate_text(teams.name,50)
      WHEN categories.score_entity = 'participant' THEN truncate_text(abbreviate_name(participants.first_name,participants.last_name,50),50)
    END AS entity_label,
    format_number(ranks.score) as score_label,
    COALESCE(units.abbreviation,'pts.') as unit_label,
    ranks.capture_count as capture_count,
    ranks.last_capture_at,
    ranks.first_capture_at,
    format_number(ranks.smallest_subscore) as smallest_subscore_label,
    format_number(ranks.largest_subscore) as largest_subscore_label
  FROM
    get_leaderboard_for_event(event_id,snapshot) as ranks
    INNER JOIN categories ON categories.id = ranks.category_id
    INNER JOIN super_categories ON super_categories.id = categories.super_category_id
    LEFT JOIN participants ON participants.id = ranks.participant_id
    LEFT JOIN teams ON teams.id = ranks.team_id
    LEFT JOIN units ON units.id = ranks.unit_id
  ORDER BY
    ranks.category_id ASC,
    ranks.position ASC
$$;


--
-- Name: get_unlimited_leaderboard_display_json_for_event(bigint, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_unlimited_leaderboard_display_json_for_event(event_id bigint, snapshot timestamp with time zone) RETURNS json
    LANGUAGE sql
    AS $_$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_unlimited_leaderboard_display_for_event($1,snapshot)
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$_$;


--
-- Name: get_unlimited_leaderboard_display_json_for_event_category(bigint, bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION get_unlimited_leaderboard_display_json_for_event_category(event_id bigint, category_id bigint) RETURNS json
    LANGUAGE sql
    AS $_$
 SELECT
    to_json(d) AS "stuff"
  FROM (
    SELECT
      (SELECT COALESCE(json_agg(t),'[]') AS "rankings" FROM
          (
            SELECT
              *
            FROM
              get_unlimited_leaderboard_display_for_event($1,NULL) as ranks
            WHERE
              ranks.category_id = $2
            ORDER BY
              category_id ASC,
              position ASC
          ) t
      ) as rankings
  ) d
$_$;


--
-- Name: hacked_currval(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION hacked_currval(seq_name text) RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    var bigint;
  BEGIN
    PERFORM gapless_sequence_create($1);
    EXECUTE 'SELECT last_value FROM ' || $1 || ';' INTO var;
    return var;
  END;
$_$;


--
-- Name: imperial_weight(numeric); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION imperial_weight(numeric) RETURNS text
    LANGUAGE sql
    AS $_$
  SELECT
    CASE
      WHEN $1 < 2204.62 THEN concat( format_number($1),' lbs.')
      WHEN $1 >= 2204.62 THEN concat( format_number(ROUND($1/2204.62,2)),' metric tons')
	  ELSE trim(to_char($1,'99999999.999'))
    END
$_$;


--
-- Name: max_per_event_team_participant_enrollments(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION max_per_event_team_participant_enrollments() RETURNS bigint
    LANGUAGE sql
    AS $$
        SELECT
          MAX(entries_per_event)
        FROM (
          SELECT
            COUNT(*) as entries_per_event
          FROM (
            SELECT
              events.id as event_id, team_participants.participant_id as participant_id, teams.id as team_id
            FROM
              team_participants
              INNER JOIN teams ON teams.id = team_participants.team_id
              INNER JOIN events ON events.id = teams.event_id
          ) t
          GROUP BY event_id, participant_id
        ) t2
        $$;


--
-- Name: measurements_for_capture(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION measurements_for_capture(bigint) RETURNS text[]
    LANGUAGE sql
    AS $_$
  SELECT
    array_agg(concat(format_number(capture_measurements.measurement), ' ', units.abbreviation))
  FROM
    capture_measurements LEFT JOIN units ON capture_measurements.unit_id = units.id
  WHERE
    capture_measurements.capture_id = $1
$_$;


--
-- Name: smart_pad(text, bigint, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION smart_pad(val text, places bigint, padding text) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
      WHEN char_length(cast(val as text)) >= places THEN cast(val as text)
      ELSE lpad(cast(val as text),cast(places as integer),padding)
    END
$$;


--
-- Name: trigger_max_participants_per_team_exception(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION trigger_max_participants_per_team_exception() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
          IF (max_per_event_team_participant_enrollments() > 1) THEN
            RAISE EXCEPTION 'A participant is in multiple teams for one event.';
          END IF;
          return new;
        END;
      $$;


--
-- Name: truncate_text(text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION truncate_text(word text, length integer) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
      WHEN char_length(word) > length THEN concat(substring(word,1,length-1),'...')
      ELSE word
    END
$$;


--
-- Name: array_cat_agg(anyarray); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE array_cat_agg(anyarray) (
    SFUNC = array_cat,
    STYPE = anyarray
);


--
-- Name: my_array_sum(anyarray); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE my_array_sum(anyarray) (
    SFUNC = array_cat,
    STYPE = anyarray,
    INITCOND = '{}'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: capture_measurements; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE capture_measurements (
    id bigint NOT NULL,
    unit_id bigint NOT NULL,
    capture_id bigint NOT NULL,
    measurement numeric NOT NULL
);


--
-- Name: capture_measurements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE capture_measurements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: capture_measurements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE capture_measurements_id_seq OWNED BY capture_measurements.id;


--
-- Name: captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE captures (
    id bigint NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    user_id bigint NOT NULL,
    participant_id bigint NOT NULL,
    species_id bigint NOT NULL,
    event_id integer NOT NULL,
    entered_at timestamp with time zone NOT NULL,
    is_redundant boolean DEFAULT false NOT NULL
);


--
-- Name: captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE captures_id_seq OWNED BY captures.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE categories (
    id integer NOT NULL,
    start_age integer,
    end_age integer,
    positions integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    name character varying(255) NOT NULL,
    super_category_id integer NOT NULL,
    score_entity score_entity NOT NULL,
    score_grouping score_grouping NOT NULL,
    score_grouping_limit bigint,
    category_type category_type NOT NULL,
    has_distinct_places boolean DEFAULT false NOT NULL,
    valid_start_time timestamp with time zone,
    valid_end_time timestamp with time zone,
    gender_id bigint,
    is_premium boolean DEFAULT false NOT NULL,
    score_1_source score_source NOT NULL,
    score_1_order score_order NOT NULL,
    score_2_source score_source,
    score_2_order score_order,
    score_3_source score_source,
    score_3_order score_order,
    display_mode display_mode NOT NULL,
    "position" bigint NOT NULL,
    subtitle text,
    grouping_number integer DEFAULT 1 NOT NULL,
    category_text_color text,
    category_background_color text,
    description text,
    additional_description text,
    generated_description text,
    score_2_is_random boolean DEFAULT false,
    score_3_is_random boolean DEFAULT false,
    summary_url character varying,
    premium_category_grouping_id bigint,
    CONSTRAINT ck_categories_grouping_number_valid CHECK ((grouping_number >= 0)),
    CONSTRAINT ck_categories_require_score_2_if_score_3 CHECK ((NOT ((score_3_source IS NOT NULL) AND (score_2_source IS NULL)))),
    CONSTRAINT ck_categories_score_2_is_random_check CHECK (((score_2_is_random IS NULL) = (score_2_source IS NULL))),
    CONSTRAINT ck_categories_score_2_set_or_not CHECK (((score_2_source IS NULL) = (score_2_order IS NULL))),
    CONSTRAINT ck_categories_score_3_is_random_check CHECK (((score_3_is_random IS NULL) = (score_3_source IS NULL))),
    CONSTRAINT ck_categories_score_3_set_or_not CHECK (((score_3_source IS NULL) = (score_3_order IS NULL))),
    CONSTRAINT ck_categories_score_grouping_limit_valid CHECK ((score_grouping_limit > 0)),
    CONSTRAINT ck_categories_valid_start_and_end_ages CHECK ((((start_age >= 0) AND (end_age >= 0)) AND (start_age <= end_age))),
    CONSTRAINT ck_categories_valid_start_and_end_times CHECK ((valid_start_time < valid_end_time))
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: categories_species; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE categories_species (
    species_id bigint NOT NULL,
    category_id bigint NOT NULL
);


--
-- Name: category_grouping_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category_grouping_items (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    place bigint NOT NULL,
    points bigint NOT NULL,
    CONSTRAINT ck_category_grouping_items_points_valid CHECK ((points >= 0))
);


--
-- Name: category_grouping_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_grouping_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: category_grouping_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_grouping_items_id_seq OWNED BY category_grouping_items.id;


--
-- Name: category_grouping_items_super_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category_grouping_items_super_categories (
    id bigint NOT NULL,
    category_grouping_item_id bigint NOT NULL,
    super_category_id bigint NOT NULL
);


--
-- Name: category_grouping_items_super_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_grouping_items_super_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: category_grouping_items_super_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_grouping_items_super_categories_id_seq OWNED BY category_grouping_items_super_categories.id;


--
-- Name: category_species_capture_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category_species_capture_items (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    species_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: category_species_capture_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_species_capture_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: category_species_capture_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_species_capture_items_id_seq OWNED BY category_species_capture_items.id;


--
-- Name: category_species_points_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category_species_points_items (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    species_id bigint NOT NULL,
    points bigint NOT NULL,
    CONSTRAINT ck_category_species_point_items_points_valid CHECK ((points >= 0))
);


--
-- Name: category_species_points_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_species_points_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: category_species_points_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_species_points_items_id_seq OWNED BY category_species_points_items.id;


--
-- Name: category_species_unit_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE category_species_unit_items (
    id bigint NOT NULL,
    category_id bigint NOT NULL,
    species_id bigint NOT NULL,
    unit_id bigint NOT NULL,
    CONSTRAINT uq_category_species_unit_items_same_unit CHECK ((category_species_unit_items_for_category_and_not_unit(category_id, unit_id) = 0))
);


--
-- Name: category_species_unit_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE category_species_unit_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: category_species_unit_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE category_species_unit_items_id_seq OWNED BY category_species_unit_items.id;


--
-- Name: entity_tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE entity_tags (
    id integer NOT NULL,
    entity_id integer,
    entity_type character varying,
    tag_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: entity_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE entity_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entity_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE entity_tags_id_seq OWNED BY entity_tags.id;


--
-- Name: super_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE super_categories (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    event_id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    page integer NOT NULL,
    "position" integer NOT NULL,
    category_text_color text,
    category_background_color text
);


--
-- Name: event_default_species_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW event_default_species_view AS
 SELECT DISTINCT ON (t.event_id, t.species_id) t.event_id,
    t.species_id
   FROM ( SELECT super_categories.event_id,
            category_species_unit_items.species_id
           FROM ((category_species_unit_items
             JOIN categories ON (((categories.id = category_species_unit_items.category_id) AND (categories.category_type = 'species_unit'::category_type))))
             JOIN super_categories ON ((super_categories.id = categories.super_category_id)))
        UNION ALL
         SELECT super_categories.event_id,
            category_species_points_items.species_id
           FROM ((category_species_points_items
             JOIN categories ON (((categories.id = category_species_points_items.category_id) AND (categories.category_type = 'species_points'::category_type))))
             JOIN super_categories ON ((super_categories.id = categories.super_category_id)))
        UNION ALL
         SELECT super_categories.event_id,
            category_species_capture_items.species_id
           FROM ((category_species_capture_items
             JOIN categories ON (((categories.id = category_species_capture_items.category_id) AND (categories.category_type = 'species_capture'::category_type))))
             JOIN super_categories ON ((super_categories.id = categories.super_category_id)))) t;


--
-- Name: event_all_species_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW event_all_species_view AS
 SELECT DISTINCT ON (t.event_id, t.species_id) t.event_id,
    t.species_id
   FROM ( SELECT event_default_species_view.event_id,
            event_default_species_view.species_id
           FROM event_default_species_view
        UNION ALL
         SELECT DISTINCT ON (captures.event_id, captures.species_id) captures.event_id,
            captures.species_id
           FROM captures) t;


--
-- Name: units; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE units (
    id bigint NOT NULL,
    description text NOT NULL,
    abbreviation text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    dimension_name text NOT NULL,
    ascending_qualifier text NOT NULL,
    descending_qualifier text NOT NULL,
    name text NOT NULL
);


--
-- Name: event_default_units_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW event_default_units_view AS
 SELECT DISTINCT ON (t.event_id, t.unit_id) t.event_id,
    t.unit_id
   FROM ( SELECT super_categories.event_id,
            category_species_unit_items.unit_id
           FROM (((units
             JOIN category_species_unit_items ON ((units.id = category_species_unit_items.unit_id)))
             JOIN categories ON ((categories.id = category_species_unit_items.category_id)))
             JOIN super_categories ON ((super_categories.id = categories.super_category_id)))) t;


--
-- Name: event_all_units_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW event_all_units_view AS
 SELECT DISTINCT ON (t.event_id, t.unit_id) t.event_id,
    t.unit_id
   FROM ( SELECT event_default_units_view.event_id,
            event_default_units_view.unit_id
           FROM event_default_units_view
        UNION ALL
         SELECT DISTINCT ON (captures.event_id, capture_measurements.unit_id) captures.event_id,
            capture_measurements.unit_id
           FROM (capture_measurements
             JOIN captures ON ((captures.id = capture_measurements.capture_id)))) t;


--
-- Name: event_facts_view; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE event_facts_view (
    event_id integer,
    species_id bigint,
    species_name character varying(256),
    quantity bigint,
    measurements json
);

ALTER TABLE ONLY event_facts_view REPLICA IDENTITY NOTHING;


--
-- Name: event_participation_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE event_participation_types (
    id bigint NOT NULL,
    event_id bigint NOT NULL,
    participation_type_id bigint NOT NULL,
    number_padding bigint DEFAULT 3 NOT NULL,
    number_prefix text DEFAULT ''::text NOT NULL,
    number_suffix text DEFAULT ''::text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_event_participation_types_number_padding_valid CHECK ((number_padding >= 1))
);


--
-- Name: event_participation_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE event_participation_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_participation_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE event_participation_types_id_seq OWNED BY event_participation_types.id;


--
-- Name: event_scale_openings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE event_scale_openings (
    id integer NOT NULL,
    start_time timestamp with time zone NOT NULL,
    end_time timestamp with time zone NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    event_id bigint NOT NULL,
    opening tstzrange NOT NULL,
    CONSTRAINT chk_event_scale_openings_start_end CHECK ((start_time < end_time))
);


--
-- Name: event_scale_openings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE event_scale_openings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_scale_openings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE event_scale_openings_id_seq OWNED BY event_scale_openings.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE events (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    start_date timestamp with time zone NOT NULL,
    end_date timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    slug character varying(255),
    icon_file_name character varying(255),
    icon_content_type character varying(255),
    icon_file_size integer,
    icon_updated_at timestamp with time zone,
    background_file_name character varying(255),
    background_content_type character varying(255),
    background_file_size integer,
    background_updated_at timestamp with time zone,
    short_leaderboard_url character varying(255),
    badge_template_file_name character varying(255),
    badge_template_content_type character varying(255),
    badge_template_file_size integer,
    badge_template_updated_at timestamp with time zone,
    ballot_template_file_name character varying(255),
    ballot_template_content_type character varying(255),
    ballot_template_file_size integer,
    ballot_template_updated_at timestamp with time zone,
    leaderboard_background_file_name character varying(255),
    leaderboard_background_content_type character varying(255),
    leaderboard_background_file_size integer,
    leaderboard_background_updated_at timestamp with time zone,
    banner_color character varying(255) DEFAULT '#7A5229'::character varying,
    twilio_phone character varying(255),
    twilio_expired boolean DEFAULT false,
    timezone character varying(255) DEFAULT 'Central Time (US & Canada)'::character varying NOT NULL,
    category_text_color text,
    category_background_color text,
    qr_code_file_name character varying,
    qr_code_content_type character varying,
    qr_code_file_size integer,
    qr_code_updated_at timestamp with time zone,
    CONSTRAINT ck_events_valid_dates CHECK ((end_date > start_date))
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: events_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE events_users (
    event_id bigint NOT NULL,
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    id bigint NOT NULL,
    CONSTRAINT ck_events_users_role_not_admin CHECK ((role_id <> 1))
);


--
-- Name: events_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_users_id_seq OWNED BY events_users.id;


--
-- Name: fao_2014_angler_participant_number; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fao_2014_angler_participant_number
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    MAXVALUE 10000
    CACHE 1;


--
-- Name: fao_2014_guest_participant_number; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fao_2014_guest_participant_number
    START WITH 2000
    INCREMENT BY 1
    MINVALUE 2000
    MAXVALUE 2999
    CACHE 1;


--
-- Name: fao_2014_organizer_participant_number; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fao_2014_organizer_participant_number
    START WITH 7000
    INCREMENT BY 1
    MINVALUE 7000
    MAXVALUE 7999
    CACHE 1;


--
-- Name: fao_2014_sponsor_participant_number; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fao_2014_sponsor_participant_number
    START WITH 3000
    INCREMENT BY 1
    MINVALUE 3000
    MAXVALUE 3999
    CACHE 1;


--
-- Name: fao_2014_vip_participant_number; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fao_2014_vip_participant_number
    START WITH 5000
    INCREMENT BY 1
    MINVALUE 5000
    MAXVALUE 5999
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_1; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_1
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_11; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_11
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_13; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_13
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_14; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_14
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_19; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_19
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_2; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_2
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_20; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_20
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_21; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_21
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_22; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_22
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_23; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_23
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_24; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_24
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_25; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_25
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_26; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_26
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_3; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_3
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_30; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_30
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_4; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_4
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_5; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_5
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_7; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_7
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_8; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_8
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_event_participation_types_numbers_9; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_event_participation_types_numbers_9
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_sequence_recycled_numbers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE gapless_sequence_recycled_numbers (
    id bigint NOT NULL,
    sequence_name text NOT NULL,
    number bigint NOT NULL
);


--
-- Name: gapless_sequence_recycled_numbers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE gapless_sequence_recycled_numbers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gapless_sequence_recycled_numbers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE gapless_sequence_recycled_numbers_id_seq OWNED BY gapless_sequence_recycled_numbers.id;


--
-- Name: genders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE genders (
    id bigint NOT NULL,
    name text NOT NULL,
    abbreviation text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_genders_abbreviation_valid CHECK (((btrim(abbreviation) <> ''::text) AND (btrim(abbreviation) = abbreviation))),
    CONSTRAINT ck_genders_name_valid CHECK (((btrim(name) <> ''::text) AND (btrim(name) = name)))
);


--
-- Name: genders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE genders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: genders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE genders_id_seq OWNED BY genders.id;


--
-- Name: mobile_carriers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mobile_carriers (
    id bigint NOT NULL,
    name text NOT NULL,
    slug text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_mobile_carriers_name_valid CHECK (((btrim(name) <> ''::text) AND (btrim(name) = name))),
    CONSTRAINT ck_mobile_carriers_slug_valid CHECK (((btrim(slug) <> ''::text) AND (btrim(slug) = slug)))
);


--
-- Name: mobile_carriers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mobile_carriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobile_carriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mobile_carriers_id_seq OWNED BY mobile_carriers.id;


--
-- Name: participants; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE participants (
    id bigint NOT NULL,
    first_name character varying(256) NOT NULL,
    last_name character varying(256) NOT NULL,
    street character varying(256),
    city character varying(256),
    zipcode character varying(25),
    email character varying(256),
    home_phone character varying(25),
    mobile_phone character varying(25),
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    temporary boolean DEFAULT false,
    does_receive_notifications boolean DEFAULT false NOT NULL,
    gender_id bigint NOT NULL,
    shirt_size_id bigint,
    date_of_birth date,
    name_vector_index_col tsvector,
    CONSTRAINT ck_participants_date_of_birth_valid CHECK (((date_of_birth >= '1900-01-01'::date) AND (date_of_birth <= (now())::date)))
);


--
-- Name: participants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE participants_id_seq OWNED BY participants.id;


--
-- Name: participation_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE participation_types (
    id bigint NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_participation_types_name_valid CHECK (((name <> ''::text) AND (btrim(name) = name)))
);


--
-- Name: participation_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE participation_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participation_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE participation_types_id_seq OWNED BY participation_types.id;


--
-- Name: participations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE participations (
    id bigint NOT NULL,
    team_id bigint NOT NULL,
    event_id bigint NOT NULL,
    participant_id bigint NOT NULL,
    number bigint NOT NULL,
    event_participation_type_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    badge_file_name character varying,
    badge_content_type character varying,
    badge_file_size integer,
    badge_updated_at timestamp with time zone,
    CONSTRAINT ck_participations_number_valid CHECK ((number > 0))
);


--
-- Name: participations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE participations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE participations_id_seq OWNED BY participations.id;


--
-- Name: pending_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pending_jobs (
    id integer NOT NULL,
    target_id integer,
    target_type character varying,
    source_id integer,
    source_type character varying,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: pending_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pending_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pending_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pending_jobs_id_seq OWNED BY pending_jobs.id;


--
-- Name: premium_category_groupings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE premium_category_groupings (
    id integer NOT NULL,
    name text NOT NULL,
    price double precision DEFAULT 0.0,
    event_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: premium_category_groupings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE premium_category_groupings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: premium_category_groupings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE premium_category_groupings_id_seq OWNED BY premium_category_groupings.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255),
    resource_id integer,
    resource_type character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    decoration_text text NOT NULL,
    decoration_color text NOT NULL,
    display_name text NOT NULL,
    description text NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: shirt_sizes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE shirt_sizes (
    id bigint NOT NULL,
    name text NOT NULL,
    abbreviation text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_shirt_sizes_abbreviation_valid CHECK (((btrim(abbreviation) <> ''::text) AND (btrim(abbreviation) = abbreviation))),
    CONSTRAINT ck_shirt_sizes_name_valid CHECK (((btrim(name) <> ''::text) AND (btrim(name) = name)))
);


--
-- Name: shirt_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shirt_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shirt_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shirt_sizes_id_seq OWNED BY shirt_sizes.id;


--
-- Name: species; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE species (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    quantity integer DEFAULT 1 NOT NULL,
    base_species_id bigint,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    CONSTRAINT ck_species_quantity_is_positive CHECK ((quantity >= 1)),
    CONSTRAINT ck_species_require_base_species_id CHECK (((base_species_id <> id) AND (NOT ((quantity > 1) AND (base_species_id IS NULL)))))
);


--
-- Name: species_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE species_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: species_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE species_id_seq OWNED BY species.id;


--
-- Name: super_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE super_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: super_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE super_categories_id_seq OWNED BY super_categories.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    permanent boolean DEFAULT false NOT NULL
);


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: team_premium_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE team_premium_categories (
    id integer NOT NULL,
    team_id integer,
    category_id integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


--
-- Name: team_premium_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE team_premium_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: team_premium_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE team_premium_categories_id_seq OWNED BY team_premium_categories.id;


--
-- Name: team_premium_category_groupings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE team_premium_category_groupings (
    id integer NOT NULL,
    team_id bigint NOT NULL,
    premium_category_grouping_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


--
-- Name: team_premium_category_groupings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE team_premium_category_groupings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: team_premium_category_groupings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE team_premium_category_groupings_id_seq OWNED BY team_premium_category_groupings.id;


--
-- Name: teams; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teams (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    company character varying(256),
    description character varying(256),
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    captain_id bigint,
    slug character varying(255),
    summary_short_url character varying(255),
    event_id integer NOT NULL,
    contact_id integer,
    does_receive_notifications boolean DEFAULT true NOT NULL,
    checked_in boolean DEFAULT false,
    notes text,
    number bigint,
    qr_code_file_name character varying,
    qr_code_content_type character varying,
    qr_code_file_size integer,
    qr_code_updated_at timestamp with time zone
);


--
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teams_id_seq OWNED BY teams.id;


--
-- Name: tokens; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tokens (
    id integer NOT NULL,
    token character varying(255),
    expiration timestamp with time zone,
    tokenable_id integer,
    tokenable_type character varying(255)
);


--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tokens_id_seq OWNED BY tokens.id;


--
-- Name: units_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE units_id_seq OWNED BY units.id;


SET default_with_oids = true;

--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    user_name character varying(25) NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    participant_id bigint,
    encrypted_password character varying(128) DEFAULT ''::character varying NOT NULL,
    password_salt character varying(255),
    authentication_token character varying(255),
    confirmation_token character varying(255),
    confirmed_at timestamp with time zone,
    confirmation_sent_at timestamp with time zone,
    reset_password_token character varying(255),
    remember_token character varying(255),
    remember_created_at timestamp with time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp with time zone,
    last_sign_in_at timestamp with time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    failed_attempts integer DEFAULT 0,
    unlock_token character varying(255),
    locked_at timestamp with time zone,
    reset_password_sent_at timestamp with time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET default_with_oids = false;

--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users_roles (
    user_id integer,
    role_id integer,
    id bigint NOT NULL
);


--
-- Name: users_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_roles_id_seq OWNED BY users_roles.id;


--
-- Name: version_associations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE version_associations (
    id integer NOT NULL,
    version_id integer,
    foreign_key_name character varying NOT NULL,
    foreign_key_id integer
);


--
-- Name: version_associations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE version_associations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: version_associations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE version_associations_id_seq OWNED BY version_associations.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE versions (
    id integer NOT NULL,
    item_type character varying NOT NULL,
    item_id integer NOT NULL,
    event character varying NOT NULL,
    whodunnit character varying,
    object json,
    object_changes json,
    created_at timestamp with time zone,
    transaction_id integer
);


--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY capture_measurements ALTER COLUMN id SET DEFAULT nextval('capture_measurements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures ALTER COLUMN id SET DEFAULT nextval('captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_grouping_items ALTER COLUMN id SET DEFAULT nextval('category_grouping_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_grouping_items_super_categories ALTER COLUMN id SET DEFAULT nextval('category_grouping_items_super_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_capture_items ALTER COLUMN id SET DEFAULT nextval('category_species_capture_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_points_items ALTER COLUMN id SET DEFAULT nextval('category_species_points_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_unit_items ALTER COLUMN id SET DEFAULT nextval('category_species_unit_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY entity_tags ALTER COLUMN id SET DEFAULT nextval('entity_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_participation_types ALTER COLUMN id SET DEFAULT nextval('event_participation_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_scale_openings ALTER COLUMN id SET DEFAULT nextval('event_scale_openings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_users ALTER COLUMN id SET DEFAULT nextval('events_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY gapless_sequence_recycled_numbers ALTER COLUMN id SET DEFAULT nextval('gapless_sequence_recycled_numbers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY genders ALTER COLUMN id SET DEFAULT nextval('genders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mobile_carriers ALTER COLUMN id SET DEFAULT nextval('mobile_carriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants ALTER COLUMN id SET DEFAULT nextval('participants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY participation_types ALTER COLUMN id SET DEFAULT nextval('participation_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations ALTER COLUMN id SET DEFAULT nextval('participations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pending_jobs ALTER COLUMN id SET DEFAULT nextval('pending_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY premium_category_groupings ALTER COLUMN id SET DEFAULT nextval('premium_category_groupings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shirt_sizes ALTER COLUMN id SET DEFAULT nextval('shirt_sizes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY species ALTER COLUMN id SET DEFAULT nextval('species_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY super_categories ALTER COLUMN id SET DEFAULT nextval('super_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY team_premium_categories ALTER COLUMN id SET DEFAULT nextval('team_premium_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY team_premium_category_groupings ALTER COLUMN id SET DEFAULT nextval('team_premium_category_groupings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams ALTER COLUMN id SET DEFAULT nextval('teams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tokens ALTER COLUMN id SET DEFAULT nextval('tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY units ALTER COLUMN id SET DEFAULT nextval('units_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles ALTER COLUMN id SET DEFAULT nextval('users_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY version_associations ALTER COLUMN id SET DEFAULT nextval('version_associations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN id SET DEFAULT nextval('versions_id_seq'::regclass);


--
-- Name: capture_measurements_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY capture_measurements
    ADD CONSTRAINT capture_measurements_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categories_species_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY categories_species
    ADD CONSTRAINT categories_species_pk PRIMARY KEY (species_id, category_id);


--
-- Name: category_grouping_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_grouping_items
    ADD CONSTRAINT category_grouping_items_pkey PRIMARY KEY (id);


--
-- Name: category_grouping_items_super_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_grouping_items_super_categories
    ADD CONSTRAINT category_grouping_items_super_categories_pkey PRIMARY KEY (id);


--
-- Name: category_species_capture_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_species_capture_items
    ADD CONSTRAINT category_species_capture_items_pkey PRIMARY KEY (id);


--
-- Name: category_species_points_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_species_points_items
    ADD CONSTRAINT category_species_points_items_pkey PRIMARY KEY (id);


--
-- Name: category_species_unit_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_species_unit_items
    ADD CONSTRAINT category_species_unit_items_pkey PRIMARY KEY (id);


--
-- Name: entity_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY entity_tags
    ADD CONSTRAINT entity_tags_pkey PRIMARY KEY (id);


--
-- Name: event_participation_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_participation_types
    ADD CONSTRAINT event_participation_types_pkey PRIMARY KEY (id);


--
-- Name: event_scale_openings_overlapping_time_ranges; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_scale_openings
    ADD CONSTRAINT event_scale_openings_overlapping_time_ranges EXCLUDE USING gist (opening WITH &&, event_id WITH =);


--
-- Name: event_scale_openings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_scale_openings
    ADD CONSTRAINT event_scale_openings_pkey PRIMARY KEY (id);


--
-- Name: events_primary_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_primary_key PRIMARY KEY (id);


--
-- Name: events_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT events_users_pkey PRIMARY KEY (id);


--
-- Name: gapless_sequence_recycled_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gapless_sequence_recycled_numbers
    ADD CONSTRAINT gapless_sequence_recycled_numbers_pkey PRIMARY KEY (id);


--
-- Name: genders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY genders
    ADD CONSTRAINT genders_pkey PRIMARY KEY (id);


--
-- Name: mobile_carriers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mobile_carriers
    ADD CONSTRAINT mobile_carriers_pkey PRIMARY KEY (id);


--
-- Name: participant_60393_uq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT participant_60393_uq UNIQUE (participant_id);


--
-- Name: participation_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participation_types
    ADD CONSTRAINT participation_types_pkey PRIMARY KEY (id);


--
-- Name: participations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_pkey PRIMARY KEY (id);


--
-- Name: pending_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pending_jobs
    ADD CONSTRAINT pending_jobs_pkey PRIMARY KEY (id);


--
-- Name: premium_category_groupings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY premium_category_groupings
    ADD CONSTRAINT premium_category_groupings_pkey PRIMARY KEY (id);


--
-- Name: primary_key_capture_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT primary_key_capture_id PRIMARY KEY (id);


--
-- Name: primary_key_participants_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT primary_key_participants_id PRIMARY KEY (id);


--
-- Name: primary_key_team_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT primary_key_team_id PRIMARY KEY (id);


--
-- Name: primary_key_users_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT primary_key_users_id PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: shirt_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY shirt_sizes
    ADD CONSTRAINT shirt_sizes_pkey PRIMARY KEY (id);


--
-- Name: species_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY species
    ADD CONSTRAINT species_pkey PRIMARY KEY (id);


--
-- Name: species_uniq_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY species
    ADD CONSTRAINT species_uniq_id UNIQUE (id);


--
-- Name: super_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY super_categories
    ADD CONSTRAINT super_categories_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: team_premium_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY team_premium_categories
    ADD CONSTRAINT team_premium_categories_pkey PRIMARY KEY (id);


--
-- Name: team_premium_category_groupings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY team_premium_category_groupings
    ADD CONSTRAINT team_premium_category_groupings_pkey PRIMARY KEY (id);


--
-- Name: tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: uq_capture_measurements_capture_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY capture_measurements
    ADD CONSTRAINT uq_capture_measurements_capture_id UNIQUE (capture_id, unit_id);


--
-- Name: uq_category_grouping_items_all; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_grouping_items
    ADD CONSTRAINT uq_category_grouping_items_all UNIQUE (category_id, place);


--
-- Name: uq_category_grouping_items_super_categories_category_grouping_i; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_grouping_items_super_categories
    ADD CONSTRAINT uq_category_grouping_items_super_categories_category_grouping_i UNIQUE (category_grouping_item_id, super_category_id);


--
-- Name: uq_category_species_capture_items_species; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_species_capture_items
    ADD CONSTRAINT uq_category_species_capture_items_species UNIQUE (category_id, species_id);


--
-- Name: uq_category_species_unit_items; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY category_species_unit_items
    ADD CONSTRAINT uq_category_species_unit_items UNIQUE (category_id, species_id);


--
-- Name: uq_event_participation_type_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_participation_types
    ADD CONSTRAINT uq_event_participation_type_id UNIQUE (event_id, participation_type_id);


--
-- Name: uq_events_users_scope; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT uq_events_users_scope UNIQUE (event_id, user_id, role_id);


--
-- Name: uq_gapless_sequence_recycled_numbers_sequence_name_and_number; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY gapless_sequence_recycled_numbers
    ADD CONSTRAINT uq_gapless_sequence_recycled_numbers_sequence_name_and_number UNIQUE (sequence_name, number);


--
-- Name: uq_genders_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY genders
    ADD CONSTRAINT uq_genders_name UNIQUE (name);


--
-- Name: uq_id_and_event_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_participation_types
    ADD CONSTRAINT uq_id_and_event_id UNIQUE (id, event_id);


--
-- Name: uq_mobile_carriers_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mobile_carriers
    ADD CONSTRAINT uq_mobile_carriers_name UNIQUE (name);


--
-- Name: uq_mobile_carriers_slug; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mobile_carriers
    ADD CONSTRAINT uq_mobile_carriers_slug UNIQUE (slug);


--
-- Name: uq_participation_types_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participation_types
    ADD CONSTRAINT uq_participation_types_name UNIQUE (name);


--
-- Name: uq_participations_event_id_participation_type_and_number; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT uq_participations_event_id_participation_type_and_number UNIQUE (event_id, event_participation_type_id, number);


--
-- Name: uq_participations_participant_id_and_event_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT uq_participations_participant_id_and_event_id UNIQUE (event_id, participant_id);


--
-- Name: uq_participations_team_id_and_participant_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT uq_participations_team_id_and_participant_id UNIQUE (team_id, participant_id);


--
-- Name: uq_shirt_sizes_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY shirt_sizes
    ADD CONSTRAINT uq_shirt_sizes_name UNIQUE (name);


--
-- Name: uq_team_premium_categories_scope; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY team_premium_categories
    ADD CONSTRAINT uq_team_premium_categories_scope UNIQUE (team_id, category_id);


--
-- Name: uq_teams_id_and_event_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT uq_teams_id_and_event_id UNIQUE (id, event_id);


--
-- Name: uq_teams_number; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT uq_teams_number UNIQUE (event_id, number);


--
-- Name: uq_users_roles_user_id_and_role_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT uq_users_roles_user_id_and_role_id UNIQUE (user_id, role_id);


--
-- Name: users_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (id);


--
-- Name: version_associations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY version_associations
    ADD CONSTRAINT version_associations_pkey PRIMARY KEY (id);


--
-- Name: versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: idx_participants_name_vector; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_participants_name_vector ON participants USING gin (name_vector_index_col);


--
-- Name: idx_roles_lower_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_roles_lower_name ON roles USING btree (lower((name)::text));


--
-- Name: idx_team_prem_cat_group_team_cat_group; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_team_prem_cat_group_team_cat_group ON team_premium_category_groupings USING btree (team_id, premium_category_grouping_id);


--
-- Name: idx_users_user_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_users_user_name ON users USING btree (lower((user_name)::text));


--
-- Name: index_entity_tags_on_entity_type_and_entity_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_entity_tags_on_entity_type_and_entity_id ON entity_tags USING btree (entity_type, entity_id);


--
-- Name: index_events_on_slug; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_events_on_slug ON events USING btree (slug);


--
-- Name: index_pending_jobs_on_source_type_and_source_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_pending_jobs_on_source_type_and_source_id ON pending_jobs USING btree (source_type, source_id);


--
-- Name: index_pending_jobs_on_target_type_and_target_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_pending_jobs_on_target_type_and_target_id ON pending_jobs USING btree (target_type, target_id);


--
-- Name: index_premium_category_groupings_on_name_and_event_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_premium_category_groupings_on_name_and_event_id ON premium_category_groupings USING btree (name, event_id);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_roles_on_name ON roles USING btree (name);


--
-- Name: index_roles_on_name_and_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_roles_on_name_and_resource_type_and_resource_id ON roles USING btree (name, resource_type, resource_id);


--
-- Name: index_teams_on_event_id_and_slug; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_teams_on_event_id_and_slug ON teams USING btree (event_id, slug);


--
-- Name: index_users_roles_on_user_id_and_role_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_roles_on_user_id_and_role_id ON users_roles USING btree (user_id, role_id);


--
-- Name: index_version_associations_on_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_version_associations_on_foreign_key ON version_associations USING btree (foreign_key_name, foreign_key_id);


--
-- Name: index_version_associations_on_version_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_version_associations_on_version_id ON version_associations USING btree (version_id);


--
-- Name: index_versions_on_item_type_and_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_versions_on_item_type_and_item_id ON versions USING btree (item_type, item_id);


--
-- Name: index_versions_on_transaction_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_versions_on_transaction_id ON versions USING btree (transaction_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: _RETURN; Type: RULE; Schema: public; Owner: -
--

CREATE RULE "_RETURN" AS
    ON SELECT TO event_facts_view DO INSTEAD  WITH joined_captures AS (
         SELECT captures.id,
            captures.event_id,
            COALESCE(species_1.quantity, base_species.quantity) AS quantity,
            COALESCE(base_species.id, species_1.id) AS species_id
           FROM ((captures
             JOIN species species_1 ON ((species_1.id = captures.species_id)))
             LEFT JOIN species base_species ON ((species_1.base_species_id = base_species.id)))
          WHERE (captures.is_redundant = false)
        ), species_measurements AS (
         SELECT joined_captures_1.species_id,
            joined_captures_1.event_id,
            units.id AS unit_id,
            format_number(sum(capture_measurements.measurement)) AS total,
            concat(format_number(sum(capture_measurements.measurement)), ' ', units.abbreviation) AS label,
                CASE
                    WHEN (sum(joined_captures_1.quantity) = 0) THEN NULL::numeric
                    ELSE (sum(capture_measurements.measurement) / (sum(joined_captures_1.quantity))::numeric)
                END AS average
           FROM (((capture_measurements
             JOIN joined_captures joined_captures_1 ON ((joined_captures_1.id = capture_measurements.capture_id)))
             RIGHT JOIN event_all_units_view ON (((event_all_units_view.event_id = joined_captures_1.event_id) AND (event_all_units_view.unit_id = capture_measurements.unit_id))))
             LEFT JOIN units ON ((units.id = event_all_units_view.unit_id)))
          GROUP BY joined_captures_1.event_id, joined_captures_1.species_id, units.id
        )
 SELECT event_all_species_view.event_id,
    event_all_species_view.species_id,
    species.name AS species_name,
    COALESCE(sum(joined_captures.quantity), (0)::bigint) AS quantity,
    ( SELECT json_agg(t.*) AS json_agg
           FROM ( SELECT species_measurements.species_id,
                    species_measurements.event_id,
                    species_measurements.unit_id,
                    species_measurements.total,
                    species_measurements.label,
                    species_measurements.average
                   FROM species_measurements
                  WHERE ((species_measurements.species_id = event_all_species_view.species_id) AND (species_measurements.event_id = event_all_species_view.event_id))
                  ORDER BY species_measurements.unit_id) t) AS measurements
   FROM (((events
     LEFT JOIN event_all_species_view ON ((event_all_species_view.event_id = events.id)))
     JOIN species ON ((species.id = event_all_species_view.species_id)))
     LEFT JOIN joined_captures ON (((joined_captures.species_id = species.id) AND (joined_captures.event_id = event_all_species_view.event_id))))
  WHERE (species.base_species_id IS NULL)
  GROUP BY event_all_species_view.event_id, event_all_species_view.species_id, species.name;


--
-- Name: categories_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories_species
    ADD CONSTRAINT categories_fk FOREIGN KEY (category_id) REFERENCES categories(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: categories_gender_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_gender_id_fkey FOREIGN KEY (gender_id) REFERENCES genders(id);


--
-- Name: category_species_capture_items_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_capture_items
    ADD CONSTRAINT category_species_capture_items_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: category_species_capture_items_species_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_capture_items
    ADD CONSTRAINT category_species_capture_items_species_id_fkey FOREIGN KEY (species_id) REFERENCES species(id);


--
-- Name: cfk_participations_event_participation_type_id_and_event_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT cfk_participations_event_participation_type_id_and_event_id FOREIGN KEY (event_participation_type_id, event_id) REFERENCES event_participation_types(id, event_id) MATCH FULL;


--
-- Name: cfk_participations_team_id_and_event_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT cfk_participations_team_id_and_event_id FOREIGN KEY (team_id, event_id) REFERENCES teams(id, event_id) MATCH FULL;


--
-- Name: event_participation_types_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_participation_types
    ADD CONSTRAINT event_participation_types_event_id_fkey FOREIGN KEY (event_id) REFERENCES events(id);


--
-- Name: event_participation_types_participation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_participation_types
    ADD CONSTRAINT event_participation_types_participation_type_id_fkey FOREIGN KEY (participation_type_id) REFERENCES participation_types(id);


--
-- Name: events_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT events_fk FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: events_users_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT events_users_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: fk_capture_measurements_capture_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY capture_measurements
    ADD CONSTRAINT fk_capture_measurements_capture_id FOREIGN KEY (capture_id) REFERENCES captures(id) MATCH FULL;


--
-- Name: fk_capture_measurements_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY capture_measurements
    ADD CONSTRAINT fk_capture_measurements_unit_id FOREIGN KEY (unit_id) REFERENCES units(id) MATCH FULL;


--
-- Name: fk_categories_super_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_categories_super_category_id FOREIGN KEY (super_category_id) REFERENCES super_categories(id) MATCH FULL;


--
-- Name: fk_category_grouping_items_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_grouping_items
    ADD CONSTRAINT fk_category_grouping_items_category_id FOREIGN KEY (category_id) REFERENCES categories(id) MATCH FULL;


--
-- Name: fk_category_grouping_items_super_categories_category_grouping_i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_grouping_items_super_categories
    ADD CONSTRAINT fk_category_grouping_items_super_categories_category_grouping_i FOREIGN KEY (category_grouping_item_id) REFERENCES category_grouping_items(id) MATCH FULL;


--
-- Name: fk_category_grouping_items_super_categories_super_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_grouping_items_super_categories
    ADD CONSTRAINT fk_category_grouping_items_super_categories_super_category_id FOREIGN KEY (super_category_id) REFERENCES super_categories(id) MATCH FULL;


--
-- Name: fk_category_species_points_items_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_points_items
    ADD CONSTRAINT fk_category_species_points_items_category_id FOREIGN KEY (category_id) REFERENCES categories(id) MATCH FULL;


--
-- Name: fk_category_species_points_items_species_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_points_items
    ADD CONSTRAINT fk_category_species_points_items_species_id FOREIGN KEY (species_id) REFERENCES species(id) MATCH FULL;


--
-- Name: fk_category_species_unit_items_category_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_unit_items
    ADD CONSTRAINT fk_category_species_unit_items_category_id FOREIGN KEY (category_id) REFERENCES categories(id) MATCH FULL;


--
-- Name: fk_category_species_unit_items_species_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_unit_items
    ADD CONSTRAINT fk_category_species_unit_items_species_id FOREIGN KEY (species_id) REFERENCES species(id) MATCH FULL;


--
-- Name: fk_category_species_unit_items_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY category_species_unit_items
    ADD CONSTRAINT fk_category_species_unit_items_unit_id FOREIGN KEY (unit_id) REFERENCES units(id) MATCH FULL;


--
-- Name: fk_event_scale_openings_event; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_scale_openings
    ADD CONSTRAINT fk_event_scale_openings_event FOREIGN KEY (event_id) REFERENCES events(id);


--
-- Name: fk_rails_1eb1d82150; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT fk_rails_1eb1d82150 FOREIGN KEY (premium_category_grouping_id) REFERENCES premium_category_groupings(id);


--
-- Name: fk_rails_59bf6ddc89; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY entity_tags
    ADD CONSTRAINT fk_rails_59bf6ddc89 FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: fk_rails_7c7416bd8f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY team_premium_category_groupings
    ADD CONSTRAINT fk_rails_7c7416bd8f FOREIGN KEY (team_id) REFERENCES teams(id);


--
-- Name: fk_rails_bef2312bad; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY team_premium_category_groupings
    ADD CONSTRAINT fk_rails_bef2312bad FOREIGN KEY (premium_category_grouping_id) REFERENCES premium_category_groupings(id);


--
-- Name: fk_rails_ecb2d22ad1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT fk_rails_ecb2d22ad1 FOREIGN KEY (captain_id) REFERENCES participants(id);


--
-- Name: fk_rails_ee9b634dd5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY premium_category_groupings
    ADD CONSTRAINT fk_rails_ee9b634dd5 FOREIGN KEY (event_id) REFERENCES events(id);


--
-- Name: fk_super_categories_event_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY super_categories
    ADD CONSTRAINT fk_super_categories_event_id FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL;


--
-- Name: fk_teams_captain_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT fk_teams_captain_id FOREIGN KEY (captain_id) REFERENCES participants(id) MATCH FULL;


--
-- Name: fk_teams_event_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT fk_teams_event_id FOREIGN KEY (event_id) REFERENCES events(id) MATCH FULL;


--
-- Name: fk_users_roles_role_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT fk_users_roles_role_id FOREIGN KEY (role_id) REFERENCES roles(id) MATCH FULL;


--
-- Name: fk_users_roles_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT fk_users_roles_user_id FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT participant_fk FOREIGN KEY (participant_id) REFERENCES participants(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: participants_gender_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT participants_gender_id_fkey FOREIGN KEY (gender_id) REFERENCES genders(id);


--
-- Name: participants_shirt_size_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT participants_shirt_size_id_fkey FOREIGN KEY (shirt_size_id) REFERENCES shirt_sizes(id);


--
-- Name: participations_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_event_id_fkey FOREIGN KEY (event_id) REFERENCES events(id);


--
-- Name: participations_event_participation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_event_participation_type_id_fkey FOREIGN KEY (event_participation_type_id) REFERENCES event_participation_types(id);


--
-- Name: participations_participant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_participant_id_fkey FOREIGN KEY (participant_id) REFERENCES participants(id);


--
-- Name: participations_team_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY participations
    ADD CONSTRAINT participations_team_id_fkey FOREIGN KEY (team_id) REFERENCES teams(id);


--
-- Name: species_base_species_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY species
    ADD CONSTRAINT species_base_species_id_fkey FOREIGN KEY (base_species_id) REFERENCES species(id);


--
-- Name: species_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT species_fk FOREIGN KEY (species_id) REFERENCES species(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: species_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories_species
    ADD CONSTRAINT species_fk FOREIGN KEY (species_id) REFERENCES species(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: users_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: users_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_users
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20130418195132');

INSERT INTO schema_migrations (version) VALUES ('20130418200638');

INSERT INTO schema_migrations (version) VALUES ('20130418200811');

INSERT INTO schema_migrations (version) VALUES ('20130418200858');

INSERT INTO schema_migrations (version) VALUES ('20130418202057');

INSERT INTO schema_migrations (version) VALUES ('20130418203211');

INSERT INTO schema_migrations (version) VALUES ('20130418212914');

INSERT INTO schema_migrations (version) VALUES ('20130425171441');

INSERT INTO schema_migrations (version) VALUES ('20130426141920');

INSERT INTO schema_migrations (version) VALUES ('20130506194106');

INSERT INTO schema_migrations (version) VALUES ('20130507145117');

INSERT INTO schema_migrations (version) VALUES ('20130507192850');

INSERT INTO schema_migrations (version) VALUES ('20130509143142');

INSERT INTO schema_migrations (version) VALUES ('20130509155516');

INSERT INTO schema_migrations (version) VALUES ('20130509164853');

INSERT INTO schema_migrations (version) VALUES ('20130509202022');

INSERT INTO schema_migrations (version) VALUES ('20130514183000');

INSERT INTO schema_migrations (version) VALUES ('20130516183356');

INSERT INTO schema_migrations (version) VALUES ('20130520152850');

INSERT INTO schema_migrations (version) VALUES ('20130520163920');

INSERT INTO schema_migrations (version) VALUES ('20130520170529');

INSERT INTO schema_migrations (version) VALUES ('20130522174852');

INSERT INTO schema_migrations (version) VALUES ('20130522182944');

INSERT INTO schema_migrations (version) VALUES ('20130522183226');

INSERT INTO schema_migrations (version) VALUES ('20130524184606');

INSERT INTO schema_migrations (version) VALUES ('20130530163521');

INSERT INTO schema_migrations (version) VALUES ('20130530191021');

INSERT INTO schema_migrations (version) VALUES ('20130603195714');

INSERT INTO schema_migrations (version) VALUES ('20130606161535');

INSERT INTO schema_migrations (version) VALUES ('20130611175113');

INSERT INTO schema_migrations (version) VALUES ('20130611180506');

INSERT INTO schema_migrations (version) VALUES ('20130617162916');

INSERT INTO schema_migrations (version) VALUES ('20130617192947');

INSERT INTO schema_migrations (version) VALUES ('20130618172817');

INSERT INTO schema_migrations (version) VALUES ('20130626151819');

INSERT INTO schema_migrations (version) VALUES ('20130626152158');

INSERT INTO schema_migrations (version) VALUES ('20130626164625');

INSERT INTO schema_migrations (version) VALUES ('20130627203228');

INSERT INTO schema_migrations (version) VALUES ('20140502180226');

INSERT INTO schema_migrations (version) VALUES ('20140502200014');

INSERT INTO schema_migrations (version) VALUES ('20140514162804');

INSERT INTO schema_migrations (version) VALUES ('20140529183747');

INSERT INTO schema_migrations (version) VALUES ('20140610204659');

INSERT INTO schema_migrations (version) VALUES ('20140613155525');

INSERT INTO schema_migrations (version) VALUES ('20140613213110');

INSERT INTO schema_migrations (version) VALUES ('20140617185721');

INSERT INTO schema_migrations (version) VALUES ('20140618103600');

INSERT INTO schema_migrations (version) VALUES ('20140618151132');

INSERT INTO schema_migrations (version) VALUES ('20140618182448');

INSERT INTO schema_migrations (version) VALUES ('20140623150920');

INSERT INTO schema_migrations (version) VALUES ('20140623161014');

INSERT INTO schema_migrations (version) VALUES ('20140626150600');

INSERT INTO schema_migrations (version) VALUES ('20140626154100');

INSERT INTO schema_migrations (version) VALUES ('20140626160400');

INSERT INTO schema_migrations (version) VALUES ('20140630142400');

INSERT INTO schema_migrations (version) VALUES ('20140701120600');

INSERT INTO schema_migrations (version) VALUES ('20140707101000');

INSERT INTO schema_migrations (version) VALUES ('20140709210445');

INSERT INTO schema_migrations (version) VALUES ('20140709220000');

INSERT INTO schema_migrations (version) VALUES ('20140712043820');

INSERT INTO schema_migrations (version) VALUES ('20140714153909');

INSERT INTO schema_migrations (version) VALUES ('20140714155500');

INSERT INTO schema_migrations (version) VALUES ('20140714160100');

INSERT INTO schema_migrations (version) VALUES ('20140714170300');

INSERT INTO schema_migrations (version) VALUES ('20140714201838');

INSERT INTO schema_migrations (version) VALUES ('20140714202550');

INSERT INTO schema_migrations (version) VALUES ('20140715163800');

INSERT INTO schema_migrations (version) VALUES ('20140715165413');

INSERT INTO schema_migrations (version) VALUES ('20140717094300');

INSERT INTO schema_migrations (version) VALUES ('20140718201302');

INSERT INTO schema_migrations (version) VALUES ('20140721022918');

INSERT INTO schema_migrations (version) VALUES ('20140721125500');

INSERT INTO schema_migrations (version) VALUES ('20140721144800');

INSERT INTO schema_migrations (version) VALUES ('20140721151713');

INSERT INTO schema_migrations (version) VALUES ('20140721210613');

INSERT INTO schema_migrations (version) VALUES ('20140722214705');

INSERT INTO schema_migrations (version) VALUES ('20140725182654');

INSERT INTO schema_migrations (version) VALUES ('20140726203444');

INSERT INTO schema_migrations (version) VALUES ('20140729151851');

INSERT INTO schema_migrations (version) VALUES ('20150312144900');

INSERT INTO schema_migrations (version) VALUES ('20150312144990');

INSERT INTO schema_migrations (version) VALUES ('20150313142100');

INSERT INTO schema_migrations (version) VALUES ('20150316104500');

INSERT INTO schema_migrations (version) VALUES ('20150610152400');

INSERT INTO schema_migrations (version) VALUES ('20150615114700');

INSERT INTO schema_migrations (version) VALUES ('20150617194727');

INSERT INTO schema_migrations (version) VALUES ('20150618185106');

INSERT INTO schema_migrations (version) VALUES ('20150618194534');

INSERT INTO schema_migrations (version) VALUES ('20150618195900');

INSERT INTO schema_migrations (version) VALUES ('20150618214736');

INSERT INTO schema_migrations (version) VALUES ('20150622144907');

INSERT INTO schema_migrations (version) VALUES ('20150622152100');

INSERT INTO schema_migrations (version) VALUES ('20150622164900');

INSERT INTO schema_migrations (version) VALUES ('20150622185959');

INSERT INTO schema_migrations (version) VALUES ('20150623134900');

INSERT INTO schema_migrations (version) VALUES ('20150623142000');

INSERT INTO schema_migrations (version) VALUES ('20150623160100');

INSERT INTO schema_migrations (version) VALUES ('20150624115100');

INSERT INTO schema_migrations (version) VALUES ('20150624135400');

INSERT INTO schema_migrations (version) VALUES ('20150624143600');

INSERT INTO schema_migrations (version) VALUES ('20150624163800');

INSERT INTO schema_migrations (version) VALUES ('20150624174100');

INSERT INTO schema_migrations (version) VALUES ('20150625100000');

INSERT INTO schema_migrations (version) VALUES ('20150625174118');

INSERT INTO schema_migrations (version) VALUES ('20150629193952');

INSERT INTO schema_migrations (version) VALUES ('20150701204515');

INSERT INTO schema_migrations (version) VALUES ('20150702153600');

INSERT INTO schema_migrations (version) VALUES ('20150706121900');

INSERT INTO schema_migrations (version) VALUES ('20150710124421');

INSERT INTO schema_migrations (version) VALUES ('20150710130923');

INSERT INTO schema_migrations (version) VALUES ('20150710130924');

INSERT INTO schema_migrations (version) VALUES ('20150710213416');

INSERT INTO schema_migrations (version) VALUES ('20150710214458');

INSERT INTO schema_migrations (version) VALUES ('20150710214525');

INSERT INTO schema_migrations (version) VALUES ('20150711133646');

INSERT INTO schema_migrations (version) VALUES ('20150713185908');

INSERT INTO schema_migrations (version) VALUES ('20150715165000');

INSERT INTO schema_migrations (version) VALUES ('20150716104800');

INSERT INTO schema_migrations (version) VALUES ('20150717122500');

INSERT INTO schema_migrations (version) VALUES ('20150720120700');

INSERT INTO schema_migrations (version) VALUES ('20150721122400');

INSERT INTO schema_migrations (version) VALUES ('20150721202715');

INSERT INTO schema_migrations (version) VALUES ('20150722105900');

INSERT INTO schema_migrations (version) VALUES ('20150728153500');

INSERT INTO schema_migrations (version) VALUES ('20150728214544');

INSERT INTO schema_migrations (version) VALUES ('20150728215237');

INSERT INTO schema_migrations (version) VALUES ('20150729102300');

INSERT INTO schema_migrations (version) VALUES ('20150729143240');

INSERT INTO schema_migrations (version) VALUES ('20150729164700');

INSERT INTO schema_migrations (version) VALUES ('20150729202128');

INSERT INTO schema_migrations (version) VALUES ('20150729205253');

