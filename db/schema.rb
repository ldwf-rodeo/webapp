# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150729205253) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"
  enable_extension "btree_gist"

  create_table "capture_measurements", id: :bigserial, force: :cascade do |t|
    t.integer "unit_id",     limit: 8, null: false
    t.integer "capture_id",  limit: 8, null: false
    t.decimal "measurement",           null: false
  end

  add_index "capture_measurements", ["capture_id", "unit_id"], name: "uq_capture_measurements_capture_id", unique: true, using: :btree

  create_table "captures", id: :bigserial, force: :cascade do |t|
    t.datetime "updated_at",                               null: false
    t.datetime "created_at",                               null: false
    t.integer  "user_id",        limit: 8,                 null: false
    t.integer  "participant_id", limit: 8,                 null: false
    t.integer  "species_id",     limit: 8,                 null: false
    t.integer  "event_id",                                 null: false
    t.datetime "entered_at",                               null: false
    t.boolean  "is_redundant",             default: false, null: false
  end

# Could not dump table "categories" because of following StandardError
#   Unknown type 'score_entity' for column 'score_entity'

  create_table "categories_species", id: false, force: :cascade do |t|
    t.integer "species_id",  limit: 8, null: false
    t.integer "category_id", limit: 8, null: false
  end

  create_table "category_grouping_items", id: :bigserial, force: :cascade do |t|
    t.integer "category_id", limit: 8, null: false
    t.integer "place",       limit: 8, null: false
    t.integer "points",      limit: 8, null: false
  end

  add_index "category_grouping_items", ["category_id", "place"], name: "uq_category_grouping_items_all", unique: true, using: :btree

  create_table "category_grouping_items_super_categories", id: :bigserial, force: :cascade do |t|
    t.integer "category_grouping_item_id", limit: 8, null: false
    t.integer "super_category_id",         limit: 8, null: false
  end

  add_index "category_grouping_items_super_categories", ["category_grouping_item_id", "super_category_id"], name: "uq_category_grouping_items_super_categories_category_grouping_i", unique: true, using: :btree

  create_table "category_species_capture_items", id: :bigserial, force: :cascade do |t|
    t.integer  "category_id", limit: 8, null: false
    t.integer  "species_id",  limit: 8, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "category_species_capture_items", ["category_id", "species_id"], name: "uq_category_species_capture_items_species", unique: true, using: :btree

  create_table "category_species_points_items", id: :bigserial, force: :cascade do |t|
    t.integer "category_id", limit: 8, null: false
    t.integer "species_id",  limit: 8, null: false
    t.integer "points",      limit: 8, null: false
  end

  create_table "category_species_unit_items", id: :bigserial, force: :cascade do |t|
    t.integer "category_id", limit: 8, null: false
    t.integer "species_id",  limit: 8, null: false
    t.integer "unit_id",     limit: 8, null: false
  end

  add_index "category_species_unit_items", ["category_id", "species_id"], name: "uq_category_species_unit_items", unique: true, using: :btree

  create_table "entity_tags", force: :cascade do |t|
    t.integer  "entity_id"
    t.string   "entity_type"
    t.integer  "tag_id",      limit: 8, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "entity_tags", ["entity_type", "entity_id"], name: "index_entity_tags_on_entity_type_and_entity_id", using: :btree

  create_table "event_participation_types", id: :bigserial, force: :cascade do |t|
    t.integer  "event_id",              limit: 8,              null: false
    t.integer  "participation_type_id", limit: 8,              null: false
    t.integer  "number_padding",        limit: 8, default: 3,  null: false
    t.text     "number_prefix",                   default: "", null: false
    t.text     "number_suffix",                   default: "", null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "event_participation_types", ["event_id", "participation_type_id"], name: "uq_event_participation_type_id", unique: true, using: :btree
  add_index "event_participation_types", ["id", "event_id"], name: "uq_id_and_event_id", unique: true, using: :btree

  create_table "event_scale_openings", force: :cascade do |t|
    t.datetime  "start_time",           null: false
    t.datetime  "end_time",             null: false
    t.datetime  "created_at"
    t.datetime  "updated_at"
    t.integer   "event_id",   limit: 8, null: false
    t.tstzrange "opening",              null: false
  end

  add_index "event_scale_openings", ["opening", "event_id"], name: "event_scale_openings_overlapping_time_ranges", using: :gist

  create_table "events", id: :bigserial, force: :cascade do |t|
    t.string   "name",                                limit: 256,                                        null: false
    t.datetime "start_date",                                                                             null: false
    t.datetime "end_date",                                                                               null: false
    t.datetime "updated_at",                                                                             null: false
    t.datetime "created_at",                                                                             null: false
    t.string   "slug",                                limit: 255
    t.string   "icon_file_name",                      limit: 255
    t.string   "icon_content_type",                   limit: 255
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.string   "background_file_name",                limit: 255
    t.string   "background_content_type",             limit: 255
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.string   "short_leaderboard_url",               limit: 255
    t.string   "badge_template_file_name",            limit: 255
    t.string   "badge_template_content_type",         limit: 255
    t.integer  "badge_template_file_size"
    t.datetime "badge_template_updated_at"
    t.string   "ballot_template_file_name",           limit: 255
    t.string   "ballot_template_content_type",        limit: 255
    t.integer  "ballot_template_file_size"
    t.datetime "ballot_template_updated_at"
    t.string   "leaderboard_background_file_name",    limit: 255
    t.string   "leaderboard_background_content_type", limit: 255
    t.integer  "leaderboard_background_file_size"
    t.datetime "leaderboard_background_updated_at"
    t.string   "banner_color",                        limit: 255, default: "#7A5229"
    t.string   "twilio_phone",                        limit: 255
    t.boolean  "twilio_expired",                                  default: false
    t.string   "timezone",                            limit: 255, default: "Central Time (US & Canada)", null: false
    t.text     "category_text_color"
    t.text     "category_background_color"
    t.string   "qr_code_file_name"
    t.string   "qr_code_content_type"
    t.integer  "qr_code_file_size"
    t.datetime "qr_code_updated_at"
  end

  add_index "events", ["slug"], name: "index_events_on_slug", unique: true, using: :btree

  create_table "events_users", id: :bigserial, force: :cascade do |t|
    t.integer  "event_id",   limit: 8, null: false
    t.integer  "user_id",    limit: 8, null: false
    t.integer  "role_id",    limit: 8, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "events_users", ["event_id", "user_id", "role_id"], name: "uq_events_users_scope", unique: true, using: :btree

  create_table "gapless_sequence_recycled_numbers", id: :bigserial, force: :cascade do |t|
    t.text    "sequence_name",           null: false
    t.integer "number",        limit: 8, null: false
  end

  add_index "gapless_sequence_recycled_numbers", ["sequence_name", "number"], name: "uq_gapless_sequence_recycled_numbers_sequence_name_and_number", unique: true, using: :btree

  create_table "genders", id: :bigserial, force: :cascade do |t|
    t.text     "name",         null: false
    t.text     "abbreviation", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "genders", ["name"], name: "uq_genders_name", unique: true, using: :btree

  create_table "mobile_carriers", id: :bigserial, force: :cascade do |t|
    t.text     "name",       null: false
    t.text     "slug",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "mobile_carriers", ["name"], name: "uq_mobile_carriers_name", unique: true, using: :btree
  add_index "mobile_carriers", ["slug"], name: "uq_mobile_carriers_slug", unique: true, using: :btree

  create_table "participants", id: :bigserial, force: :cascade do |t|
    t.string   "first_name",                 limit: 256,                 null: false
    t.string   "last_name",                  limit: 256,                 null: false
    t.string   "street",                     limit: 256
    t.string   "city",                       limit: 256
    t.string   "zipcode",                    limit: 25
    t.string   "email",                      limit: 256
    t.string   "home_phone",                 limit: 25
    t.string   "mobile_phone",               limit: 25
    t.datetime "updated_at",                                             null: false
    t.datetime "created_at",                                             null: false
    t.boolean  "temporary",                              default: false
    t.boolean  "does_receive_notifications",             default: false, null: false
    t.integer  "gender_id",                  limit: 8,                   null: false
    t.integer  "shirt_size_id",              limit: 8
    t.date     "date_of_birth"
    t.tsvector "name_vector_index_col"
  end

  add_index "participants", ["name_vector_index_col"], name: "idx_participants_name_vector", using: :gin

  create_table "participation_types", id: :bigserial, force: :cascade do |t|
    t.text     "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "participation_types", ["name"], name: "uq_participation_types_name", unique: true, using: :btree

  create_table "participations", id: :bigserial, force: :cascade do |t|
    t.integer  "team_id",                     limit: 8, null: false
    t.integer  "event_id",                    limit: 8, null: false
    t.integer  "participant_id",              limit: 8, null: false
    t.integer  "number",                      limit: 8, null: false
    t.integer  "event_participation_type_id", limit: 8, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "badge_file_name"
    t.string   "badge_content_type"
    t.integer  "badge_file_size"
    t.datetime "badge_updated_at"
  end

  add_index "participations", ["event_id", "event_participation_type_id", "number"], name: "uq_participations_event_id_participation_type_and_number", unique: true, using: :btree
  add_index "participations", ["event_id", "participant_id"], name: "uq_participations_participant_id_and_event_id", unique: true, using: :btree
  add_index "participations", ["team_id", "participant_id"], name: "uq_participations_team_id_and_participant_id", unique: true, using: :btree

  create_table "pending_jobs", force: :cascade do |t|
    t.integer  "target_id"
    t.string   "target_type"
    t.integer  "source_id"
    t.string   "source_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "pending_jobs", ["source_type", "source_id"], name: "index_pending_jobs_on_source_type_and_source_id", using: :btree
  add_index "pending_jobs", ["target_type", "target_id"], name: "index_pending_jobs_on_target_type_and_target_id", using: :btree

  create_table "premium_category_groupings", force: :cascade do |t|
    t.text     "name",                               null: false
    t.float    "price",                default: 0.0
    t.integer  "event_id",   limit: 8,               null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "premium_category_groupings", ["name", "event_id"], name: "index_premium_category_groupings_on_name_and_event_id", unique: true, using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.integer  "resource_id"
    t.string   "resource_type",    limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "decoration_text",              null: false
    t.text     "decoration_color",             null: false
    t.text     "display_name",                 null: false
    t.text     "description",                  null: false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "shirt_sizes", id: :bigserial, force: :cascade do |t|
    t.text     "name",         null: false
    t.text     "abbreviation", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "shirt_sizes", ["name"], name: "uq_shirt_sizes_name", unique: true, using: :btree

  create_table "spatial_ref_sys", primary_key: "srid", force: :cascade do |t|
    t.string  "auth_name", limit: 256
    t.integer "auth_srid"
    t.string  "srtext",    limit: 2048
    t.string  "proj4text", limit: 2048
  end

  create_table "species", id: :bigserial, force: :cascade do |t|
    t.string   "name",            limit: 256,             null: false
    t.integer  "quantity",                    default: 1, null: false
    t.integer  "base_species_id", limit: 8
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "species", ["id"], name: "species_uniq_id", unique: true, using: :btree

  create_table "super_categories", force: :cascade do |t|
    t.string   "name",                      limit: 255, null: false
    t.integer  "event_id",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "page",                                  null: false
    t.integer  "position",                              null: false
    t.text     "category_text_color"
    t.text     "category_background_color"
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name",                       null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "permanent",  default: false, null: false
  end

  create_table "team_premium_categories", force: :cascade do |t|
    t.integer  "team_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "team_premium_categories", ["team_id", "category_id"], name: "uq_team_premium_categories_scope", unique: true, using: :btree

  create_table "team_premium_category_groupings", force: :cascade do |t|
    t.integer  "team_id",                      limit: 8, null: false
    t.integer  "premium_category_grouping_id", limit: 8, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "team_premium_category_groupings", ["team_id", "premium_category_grouping_id"], name: "idx_team_prem_cat_group_team_cat_group", unique: true, using: :btree

  create_table "teams", id: :bigserial, force: :cascade do |t|
    t.string   "name",                       limit: 256,                 null: false
    t.string   "company",                    limit: 256
    t.string   "description",                limit: 256
    t.datetime "updated_at",                                             null: false
    t.datetime "created_at",                                             null: false
    t.integer  "captain_id",                 limit: 8
    t.string   "slug",                       limit: 255
    t.string   "summary_short_url",          limit: 255
    t.integer  "event_id",                                               null: false
    t.integer  "contact_id"
    t.boolean  "does_receive_notifications",             default: true,  null: false
    t.boolean  "checked_in",                             default: false
    t.text     "notes"
    t.integer  "number",                     limit: 8
    t.string   "qr_code_file_name"
    t.string   "qr_code_content_type"
    t.integer  "qr_code_file_size"
    t.datetime "qr_code_updated_at"
  end

  add_index "teams", ["event_id", "number"], name: "uq_teams_number", unique: true, using: :btree
  add_index "teams", ["event_id", "slug"], name: "index_teams_on_event_id_and_slug", unique: true, using: :btree
  add_index "teams", ["id", "event_id"], name: "uq_teams_id_and_event_id", unique: true, using: :btree

  create_table "tokens", force: :cascade do |t|
    t.string   "token",          limit: 255
    t.datetime "expiration"
    t.integer  "tokenable_id"
    t.string   "tokenable_type", limit: 255
  end

  create_table "units", id: :bigserial, force: :cascade do |t|
    t.text     "description",          null: false
    t.text     "abbreviation",         null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.text     "dimension_name",       null: false
    t.text     "ascending_qualifier",  null: false
    t.text     "descending_qualifier", null: false
    t.text     "name",                 null: false
  end

  create_table "users", id: :bigserial, force: :cascade do |t|
    t.string   "user_name",              limit: 25,               null: false
    t.datetime "updated_at",                                      null: false
    t.datetime "created_at",                                      null: false
    t.integer  "participant_id",         limit: 8
    t.string   "encrypted_password",     limit: 128, default: "", null: false
    t.string   "password_salt",          limit: 255
    t.string   "authentication_token",   limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token",   limit: 255
    t.string   "remember_token",         limit: 255
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "failed_attempts",                    default: 0
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "reset_password_sent_at"
  end

  add_index "users", ["participant_id"], name: "participant_60393_uq", unique: true, using: :btree

  create_table "users_roles", id: :bigserial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  add_index "users_roles", ["user_id", "role_id"], name: "uq_users_roles_user_id_and_role_id", unique: true, using: :btree

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.json     "object"
    t.json     "object_changes"
    t.datetime "created_at"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

  add_foreign_key "capture_measurements", "captures", name: "fk_capture_measurements_capture_id"
  add_foreign_key "capture_measurements", "units", name: "fk_capture_measurements_unit_id"
  add_foreign_key "captures", "participants", name: "participant_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "captures", "species", name: "species_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "captures", "users", name: "users_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "categories", "genders", name: "categories_gender_id_fkey"
  add_foreign_key "categories", "premium_category_groupings"
  add_foreign_key "categories", "super_categories", name: "fk_categories_super_category_id"
  add_foreign_key "categories_species", "categories", name: "categories_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "categories_species", "species", name: "species_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "category_grouping_items", "categories", name: "fk_category_grouping_items_category_id"
  add_foreign_key "category_grouping_items_super_categories", "category_grouping_items", name: "fk_category_grouping_items_super_categories_category_grouping_i"
  add_foreign_key "category_grouping_items_super_categories", "super_categories", name: "fk_category_grouping_items_super_categories_super_category_id"
  add_foreign_key "category_species_capture_items", "categories", name: "category_species_capture_items_category_id_fkey"
  add_foreign_key "category_species_capture_items", "species", name: "category_species_capture_items_species_id_fkey"
  add_foreign_key "category_species_points_items", "categories", name: "fk_category_species_points_items_category_id"
  add_foreign_key "category_species_points_items", "species", name: "fk_category_species_points_items_species_id"
  add_foreign_key "category_species_unit_items", "categories", name: "fk_category_species_unit_items_category_id"
  add_foreign_key "category_species_unit_items", "species", name: "fk_category_species_unit_items_species_id"
  add_foreign_key "category_species_unit_items", "units", name: "fk_category_species_unit_items_unit_id"
  add_foreign_key "entity_tags", "tags"
  add_foreign_key "event_participation_types", "events", name: "event_participation_types_event_id_fkey"
  add_foreign_key "event_participation_types", "participation_types", name: "event_participation_types_participation_type_id_fkey"
  add_foreign_key "event_scale_openings", "events", name: "fk_event_scale_openings_event"
  add_foreign_key "events_users", "events", name: "events_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "events_users", "roles", name: "events_users_role_id_fkey"
  add_foreign_key "events_users", "users", name: "users_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "participants", "genders", name: "participants_gender_id_fkey"
  add_foreign_key "participants", "shirt_sizes", name: "participants_shirt_size_id_fkey"
  add_foreign_key "participations", "event_participation_types", name: "cfk_participations_event_participation_type_id_and_event_id"
  add_foreign_key "participations", "event_participation_types", name: "participations_event_participation_type_id_fkey"
  add_foreign_key "participations", "events", name: "participations_event_id_fkey"
  add_foreign_key "participations", "participants", name: "participations_participant_id_fkey"
  add_foreign_key "participations", "teams", name: "cfk_participations_team_id_and_event_id"
  add_foreign_key "participations", "teams", name: "participations_team_id_fkey"
  add_foreign_key "premium_category_groupings", "events"
  add_foreign_key "species", "species", column: "base_species_id", name: "species_base_species_id_fkey"
  add_foreign_key "super_categories", "events", name: "fk_super_categories_event_id"
  add_foreign_key "team_premium_category_groupings", "premium_category_groupings"
  add_foreign_key "team_premium_category_groupings", "teams"
  add_foreign_key "teams", "events", name: "fk_teams_event_id"
  add_foreign_key "teams", "participants", column: "captain_id"
  add_foreign_key "teams", "participants", column: "captain_id", name: "fk_teams_captain_id"
  add_foreign_key "users", "participants", name: "participant_fk", on_update: :cascade, on_delete: :restrict
  add_foreign_key "users_roles", "roles", name: "fk_users_roles_role_id"
  add_foreign_key "users_roles", "users", name: "fk_users_roles_user_id"
end
