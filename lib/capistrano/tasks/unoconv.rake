namespace :load do
  task :defaults do
    set :unoconv_pid, -> { File.join(current_path, "tmp", "pids", "unoconv.pid") }
    set :unoconv_restart_sleep_time, 3
  end
end

namespace :unoconv do

  desc 'Stop the unoconv listener process'
  task :stop do
    on roles(:app) do
      if test("[ -e #{fetch(:unoconv_pid)} ]")
        if test("kill -0 #{unoconv_pid}")
          info "stopping unoconv..."
          execute :kill, "-s QUIT", unoconv_pid
        else
          info "cleaning up dead unoconv pid..."
          execute :rm, fetch(:unoconv_pid)
        end
      else
        info "unoconv is not running..."
      end
    end
  end

  desc 'Start the websocket process'
  task :start do
    on roles(:app) do
      if test("[ -e #{fetch(:unoconv_pid)} ] && kill -0 #{unoconv_pid}")
        info "unoconv is running..."
      else
        execute :nohup, :unoconv, "--listener > /dev/null 2>&1 & echo $! > #{fetch(:unoconv_pid)}"
      end
    end
  end

  desc 'Restart the delayed_job process'
  task :restart do
    on roles(:app) do
      invoke 'unoconv:stop'
      execute :sleep, fetch(:unoconv_restart_sleep_time)
      invoke 'unoconv:start'
    end
  end

end

def unoconv_pid
  "`cat #{fetch(:unoconv_pid)}`"
end