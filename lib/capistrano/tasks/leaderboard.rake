namespace :leaderboard do
  desc "reload leaderboard sql"
  task :reload_sql do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :rake, 'db:update_sql_functions'
        end
      end
    end
  end
end