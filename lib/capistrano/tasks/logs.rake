namespace :logs do
  desc "tail rails logs"
  task :tail_rails do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/#{fetch(:rails_env)}.log"
    end
  end

  desc "tail unicorn stdout logs"
  task :tail_unicorn_stdout do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/unicorn.stdout.log"
    end
  end

  desc "tail unicorn stderr logs"
  task :tail_unicorn_stderr do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/unicorn.stderr.log"
    end
  end

  desc "tail sms logs"
  task :tail_sms do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/sms.log"
    end
  end

  desc "tail active_job logs"
  task :tail_sms do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/active_job.log"
    end
  end

  desc "tail skynet logs"
  task :tail_skynet do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/skynet.log"
    end
  end


  desc "tail websocket rails logs"
  task :tail_websocket_rails do
    on roles(:app) do
      execute "tail -f -n 500 #{shared_path}/log/websocket_rails.log"
    end
  end
end