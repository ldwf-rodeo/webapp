module LeaderboardUpdateModule

  class Leaderboard

    class << self

      def initiate_leaderboard_update(event, capture_id, send_sms)
          LeaderboardController::trigger_websockets(event)
          if send_sms === true
            capture = Capture.find(capture_id)
            Rails.logger.sms.info "Sending SMS leaderboard bumps for capture #{capture.id} (#{capture.species.name} by #{capture.participant.full_name})."
            send_sms_messages(event,capture)
          else
            capture = Capture.where(:id => capture_id).first
            if capture.nil?
              Rails.logger.sms.info "Suppressing SMS leaderboard bumps for now-deleted capture #{capture_id}."
            else
              Rails.logger.sms.info "Suppressing SMS leaderboard bumps for capture #{capture.id} (#{capture.species.name} by #{capture.participant.full_name})."
            end
          end
      end

      private

      def send_sms_messages(event,capture)
        #Compute the bumps and send them over.
        if event != nil and capture != nil
          (query = '') <<  <<-SQL
            SELECT * FROM get_leaderboard_bumps_for_event(#{event.id},'#{capture.entered_at}')
          SQL
          bumps = ActiveRecord::Base.connection.execute(query)
          send_leaderboard_bump_sms(bumps)
        end
      end

      def send_leaderboard_bump_sms(bumps)
        #Send each bump.
        bumps.each do |bump|
          category = Category.find(bump['category_id'])
          bump_location = (bump['new_position'] != nil) ? ('to position ' + bump['new_position']) : 'and off the leaderboard'
          team = Team.find(bump['team_id'])
          if !bump['participant_id'].nil?
            #A Participant bump.
            participant = Participant.find(bump['participant_id'])
            participation = participant.participation(team.event)
            SMSSender.send_generic_message_to_participation(participation,'You have fallen from position ' + bump['old_position'].to_s + ' ' + bump_location + ' in the "' + category.super_category.name + ' / ' + category.name + '" category. ' + category.summary_url)
            #Also send to the team captain.
            if (team.captain != participant) and (bump['usurping_team_id'] != bump['team_id'])
              SMSSender.send_generic_message_to_team_captain(team,'Your team has fallen from position ' + bump['old_position'].to_s + ' ' + bump_location + ' in the "' + category.super_category.name + ' / ' + category.name + '" category. ' + category.summary_url)
            end
          else
            #A Team bump. Send to all participants.
            team.participations.each do |participation|
              SMSSender.send_generic_message_to_participation(participation,'Your team has fallen from position ' + bump['old_position'].to_s + ' ' + bump_location + ' in the "' + category.super_category.name + ' / ' + category.name + '" category. ' + category.summary_url)
            end
          end
        end
      end

    end

  end
end