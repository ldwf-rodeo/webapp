require 'rqrcode_png'

module BadgeModule
  class Badge
    def self.generate(participants, event)


      #Create a temp directory to store all the files created and get rid of them later
      result_file = Tempfile.new(['result', '.pdf']) # leave as regular temp

      ballot_path = event.ballot_template.path.nil? ? "#{Rails.root}/stuff/badges/ballot_template.odt" : event.ballot_template.path


      #this part here generates the "11th" page which is just some basic info about the participants and functions as door
      #prize ballots

      ballots = Tempfile.new(['zzzzzzzzz','.odt'])
      ballot_pdf = Tempfile.new(['ballot','.pdf'])
      bal_report = ODFReport::Report.new(ballot_path) do |r|

        parsed_1 = []
        parsed_2 = []

        n = participants.each_slice(5).to_a

        parts1 = n[0]
        parts2 = n[1] || []  # add 2nd column if there are more then 5 participants

        parts1.each do |person|
          participation = person.participation(event)
          value = "#{person.first_name} #{person.last_name} #{person.shirt_size.nil? ? '' : '(' + person.shirt_size.abbreviation + ')'}\n#{participation.team.name} #{participation.team.company}\n\n#{participation.display_number}"
          parsed_1 << OpenStruct.new({value: value})
        end

        parts2.each do |person|
          participation = person.participation(event)
          value = "#{person.first_name} #{person.last_name} #{person.shirt_size.nil? ? '' : '(' + person.shirt_size.abbreviation + ')'}\n#{participation.team.name} #{participation.team.company}\n\n#{participation.display_number}"
          parsed_2 << OpenStruct.new({value: value})
        end


        r.add_table('TABLE_1', parsed_1) do |t|
          t.add_column(:person, :value)
        end

        r.add_table('TABLE_2', parsed_2) do |t|
          t.add_column(:person, :value)
        end


      end
      bal_report.generate(ballots.path)

      ##################################################################################################################

      # call libreoffice to batch convert the templates to PDFs


      `#{LIBREOFFICE_BINARY["path"]} -f pdf -o "#{ballot_pdf.path}" #{ballots.path}`


      paths = participants.collect { |p| p.participation(event).badge.path }.flatten.unshift(ballot_pdf.path).map {|b| b }.join(' ')


      `pdftk #{paths} cat output "#{result_file.path}"`

      return result_file
    end
  end
end
