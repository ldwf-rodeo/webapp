namespace :butt do
  desc 'Reset and reseed the database'
  task :rub => :environment do
    Rails.application.eager_load!

    Capture.destroy_all
    Category.destroy_all
    Team.destroy_all
    Event.destroy_all
    Categoryitem.destroy_all
    Category.destroy_all
    SuperCategory.destroy_all
    Vessel.destroy_all
    Participant.destroy_all

    Rake::Task['db:migrate'].invoke
    Rake::Task['db:seed'].invoke
  end
end