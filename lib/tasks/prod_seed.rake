namespace :db do
  desc "Seed the prod stuff"
  task :prod_seed => :environment do
    %w(admin organizer).each do |role|
      Role.where(:name => role).first_or_create!
    end

    # add a user to the system as an admin
    if User.where(:user_name => 'testadmin').first.nil?
      User.create(:user_name => 'testadmin', :password => 'thelocalFishmonger1', :password_confirmation => 'thelocalFishmonger1')
      User.where(:user_name => 'testadmin').first.add_role :admin
    end

    # add a non admin user to the database
    if User.where(:user_name => 'test').first.nil?
      User.create(:user_name => 'test', :password => 'thelocalFishmonger1', :password_confirmation => 'thelocalFishmonger1')
    end

    @user1 =  User.where(:user_name => 'testadmin').first
    @u =  User.where(:user_name => 'testadmin').first
    @user2 =  User.where(:user_name => 'test').first

    # create species items
    Species.where( :name => 'Amberjack' ).first_or_create!
    Species.where( :name => 'American Shad' ).first_or_create!
    Species.where( :name => 'Atlantic Croaker' ).first_or_create!
    Species.where( :name => 'Black Drum' ).first_or_create!
    Species.where( :name => 'Black Snapper' ).first_or_create!
    Species.where( :name => 'Blackfin Tuna' ).first_or_create!
    Species.where( :name => 'Blacktip Shark' ).first_or_create!
    Species.where( :name => 'Blue Marlin' ).first_or_create!
    Species.where( :name => 'Bluefish' ).first_or_create!
    Species.where( :name => 'Bonefish' ).first_or_create!
    Species.where( :name => 'Bonnethead Shark' ).first_or_create!
    Species.where( :name => 'Cobia' ).first_or_create!
    Species.where( :name => 'Cod' ).first_or_create!
    Species.where( :name => 'Dolphin').first_or_create!
    Species.where( :name => 'Drum').first_or_create!
    Species.where( :name => 'Flounder' ).first_or_create!
    Species.where( :name => 'Gag' ).first_or_create!
    Species.where( :name => 'Gray Snapper' ).first_or_create!
    Species.where( :name => 'Gray Triggerfish' ).first_or_create!
    Species.where( :name => 'Grouper' ).first_or_create!
    Species.where( :name => 'Great Hammerhead Shark' ).first_or_create!
    Species.where( :name => 'Hardhead Catfish' ).first_or_create!
    Species.where( :name => 'Goliath Grouper' ).first_or_create!
    Species.where( :name => 'Jack Crevalle').first_or_create!
    Species.where( :name => 'King Mackerel' ).first_or_create!
    Species.where( :name => 'Lane Snapper' ).first_or_create!
    Species.where( :name => 'Largemouth Bass' ).first_or_create!
    Species.where( :name => 'Little Tunny' ).first_or_create!
    Species.where( :name => 'Mangrove Snapper').first_or_create!
    Species.where( :name => 'Permit' ).first_or_create!
    Species.where( :name => 'Porgy' ).first_or_create!
    Species.where( :name => 'Red Drum' ).first_or_create!
    Species.where( :name => 'Bullred Drum' ).first_or_create!
    Species.where( :name => 'Red Drum - 5' ).first_or_create!
    Species.where( :name => 'Red Grouper' ).first_or_create!
    Species.where( :name => 'Red Snapper' ).first_or_create!
    Species.where( :name => 'Rock Hind' ).first_or_create!
    Species.where( :name => 'Scamp' ).first_or_create!
    Species.where( :name => 'Shark').first_or_create!
    Species.where( :name => 'Sheepshead' ).first_or_create!
    Species.where( :name => 'Snapper' ).first_or_create!
    Species.where( :name => 'Snook' ).first_or_create!
    Species.where( :name => 'Snowy Grouper' ).first_or_create!
    Species.where( :name => 'Spanish Mackerel' ).first_or_create!
    Species.where( :name => 'Speckled Trout' ).first_or_create!
    Species.where( :name => 'Speckled Trout - 5' ).first_or_create!
    Species.where( :name => 'Striped Mullet' ).first_or_create!
    Species.where( :name => 'Swordfish').first_or_create!
    Species.where( :name => 'Sailfish').first_or_create!
    Species.where( :name => 'Tarpon' ).first_or_create!
    Species.where( :name => 'Tiger Shark' ).first_or_create!
    Species.where( :name => 'Tripletail' ).first_or_create!
    Species.where( :name => 'Vermilion Snapper' ).first_or_create!
    Species.where( :name => 'Wahoo' ).first_or_create!
    Species.where( :name => 'Warsaw Grouper' ).first_or_create!
    Species.where( :name => 'Weakfish' ).first_or_create!
    Species.where( :name => 'White Marlin' ).first_or_create!
    Species.where( :name => 'White Trout' ).first_or_create!
    Species.where( :name => 'Yellowfin Tuna' ).first_or_create!
    Species.where(:name => "Black Drum").first_or_create
    Species.where(:name => "Blackfin Tuna").first_or_create
    Species.where(:name => "Bullred Drum").first_or_create
    Species.where(:name => "Cobia").first_or_create
    Species.where(:name => "Dolphin").first_or_create
    Species.where(:name => "Grouper").first_or_create
    Species.where(:name => "Jack Crevalle").first_or_create
    Species.where(:name => "King Mackerel").first_or_create
    Species.where(:name => "Mangrove Snapper").first_or_create
    Species.where(:name => "Red Drum").first_or_create
    Species.where(:name => "Red Snapper").first_or_create
    Species.where(:name => "Shark").first_or_create
    Species.where(:name => "Speckled Trout").first_or_create
    Species.where(:name => "Wahoo").first_or_create
    Species.where(:name => "Yellowfin Tuna").first_or_create

    @bullred = Species.where( :name => 'Bullred Drum' ).first
    @redfish = Species.where( :name => 'Red Drum' ).first
    @redfish_5 = Species.where( :name => 'Red Drum - 5' ).first
    @trout = Species.where( :name => 'Speckled Trout' ).first
    @trout_5 = Species.where( :name => 'Speckled Trout - 5' ).first
    @black_drum = Species.where(:name => 'Black Drum').first
    @sheep_head = Species.where(:name => 'Sheepshead').first
    @catfish = Species.where(:name => 'Catfish').first_or_create!
    @goliath_grouper = Species.where( :name => 'Grouper' ).first
    @snapper = Species.where(:name => 'Mangrove Snapper').first
    @shark = Species.where(:name => 'Shark').first
    @cobia = Species.where(:name => 'Cobia').first
    @jack = Species.where(:name => 'Jack Crevalle').first
    @king = Species.where(:name => 'King Mackerel').first
    @tarpon = Species.where( :name => 'Tarpon' ).first
    @flounder = Species.where(:name => 'Flounder').first
    @yellow_tuna = Species.where(:name => 'Yellowfin Tuna').first
    @black_tuna = Species.where(:name => 'Blackfin Tuna').first
    @flipper = Species.where(:name => 'Dolphin').first
    @wahoo = Species.where(:name => 'Wahoo').first
    @blue = Species.where(:name => 'Blue Marlin').first
    @white = Species.where(:name => 'White Marlin').first
    @sword = Species.where(:name => 'Swordfish').first
    @sail = Species.where(:name => 'Sailfish').first

    @fourchon = Event.where(:name => "Fourchon Oilman's Invitational", :start_date => '2013-07-11', :end_date => '2013-07-13').first_or_create!

    @super_cat1 = SuperCategory.where(:name => 'Inshore', :page => 1, :position => 1).first_or_create!
    @super_cat2 = SuperCategory.where(:name => 'Offshore', :page => 2, :position => 1).first_or_create!
    @super_cat3 = SuperCategory.where(:name => 'Bluewater', :page => 2, :position => 2).first_or_create!
    @super_cat4 = SuperCategory.where(:name => 'BillFish Release', :page => 2, :position => 3).first_or_create!
    @super_cat5 = SuperCategory.where(:name => 'Overall Standings', :page => 1, :position => 3).first_or_create!
    @super_cat6 = SuperCategory.where(:name => 'Inshore - Ladies', :page => 1, :position => 2).first_or_create!

#inshore - men categories
    @real_cat1 = Category.where(:name => 'Speckled Trout', :gender => :male, :positions => 10, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat2 = Category.where(:name => 'Bullred', :gender => :male, :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat3 = Category.where(:name => 'Redfish', :gender => :male, :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat4 = Category.where(:name => 'Black Drum', :gender => :male, :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat5 = Category.where(:name => 'Sheepshead', :gender => :male, :positions => 10, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat6 = Category.where(:name => 'Catfish', :gender => :male, :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!

#inshore - ladies
    @real_cat9 = Category.where(:name => 'Speckled Trout', :gender => :female, :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat10 = Category.where(:name => 'Bullred', :gender => :female, :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat11 = Category.where(:name => 'Redfish', :gender => :female, :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat12 = Category.where(:name => 'Black Drum', :gender => :female, :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat13 = Category.where(:name => 'Sheepshead', :gender => :female, :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat14 = Category.where(:name => 'Catfish', :gender => :female, :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!

#offshore categories
    @real_cat16 = Category.where(:name => 'Mangrove Snapper', :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat17 = Category.where(:name => 'Shark', :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat18 = Category.where(:name => 'Cobia', :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat19 = Category.where(:name => 'Grouper', :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat20 = Category.where(:name => 'Jack Crevalle', :positions => 10, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat21 = Category.where(:name => 'King Mackerel', :positions => 5, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat22 = Category.where(:name => 'Tarpon', :positions => 3, :score_by => :weight, :category_type => :species).first_or_create!

#inshore special species
    @real_cat23 = Category.where(:name => 'Speckled Trout - 5 Fish Stringer', :positions => 1, :score_by => :weight, :category_type => :species).first_or_create!
    @real_cat24 = Category.where(:name => 'Redfish - 5 Fish Stringer', :positions => 1, :score_by => :weight, :category_type => :species).first_or_create!

#overall standings
    @real_cat25 = Category.where(:name => 'Inshore - Jimmy Davis', :positions => 1, :category_type => :aggregate, :score_on => :participant).first_or_create!
    @real_cat26 = Category.where(:name => 'Offshore - Jimmy Davis', :positions => 1, :category_type => :aggregate, :score_on => :participant).first_or_create!
    @real_cat27 = Category.where(:name => 'Blue Water - Jimmy Davis', :positions => 1, :category_type => :aggregate, :score_on => :participant).first_or_create!
    @real_cat28 = Category.where(:name => 'Inshore - team', :positions => 1, :category_type => :aggregate, :score_on => :team).first_or_create!
    @real_cat29 = Category.where(:name => 'Offshore - team', :positions => 1, :category_type => :aggregate, :score_on => :team).first_or_create!
    @real_cat30 = Category.where(:name => 'Blue Water - team', :positions => 1, :category_type => :aggregate, :score_on => :team).first_or_create!
    @real_cat31 = Category.where(:name => 'All Around Best Angler', :positions => 1, :category_type => :aggregate, :score_on => :participant).first_or_create!

#bluewater species
    @real_cat32 = Category.where(:name => 'Yellowfin Tuna', :positions => 5, :score_by => :weight, :category_type => :species, :score_on => :participant).first_or_create!
    @real_cat33 = Category.where(:name => 'Blackfin Tuna', :positions => 10, :score_by => :weight, :category_type => :species, :score_on => :participant).first_or_create!
    @real_cat34 = Category.where(:name => 'Dolphin', :positions => 5, :score_by => :weight, :category_type => :species, :score_on => :participant).first_or_create!
    @real_cat35 = Category.where(:name => 'Wahoo', :positions => 3, :score_by => :weight, :category_type => :species, :score_on => :participant).first_or_create!

#billfish release
    @bil_cat = Category.where(:name => 'Billfish Release', :positions => 3, :score_by => :points, :category_type => :species, :score_on => :team).first_or_create!

#inshore category items
    @real_cat_item1 = Categoryitem.where(:species_id => @trout).first_or_create!
    @real_cat_item2 = Categoryitem.where(:species_id => @bullred, :min_length => 27).first_or_create!
    @real_cat_item3 = Categoryitem.where(:species_id => @redfish, :max_length => 27).first_or_create!
    @real_cat_item4 = Categoryitem.where(:species_id => @black_drum).first_or_create!
    @real_cat_item5 = Categoryitem.where(:species_id => @sheep_head).first_or_create!
    @real_cat_item6 = Categoryitem.where(:species_id => @catfish).first_or_create!

    @real_cat_item1_1 = Categoryitem.create(:species_id => @trout.id)
    @real_cat_item2_1 = Categoryitem.create(:species_id => @bullred.id, :min_length => 27)
    @real_cat_item3_1 = Categoryitem.create(:species_id => @redfish.id, :max_length => 27)
    @real_cat_item4_1 = Categoryitem.create(:species_id => @black_drum.id)
    @real_cat_item5_1 = Categoryitem.create(:species_id => @sheep_head.id)
    @real_cat_item6_1 = Categoryitem.create(:species_id => @catfish.id)

#offshore category items
    @real_cat_item7 = Categoryitem.where(:species_id => @snapper).first_or_create!
    @real_cat_item8 = Categoryitem.where(:species_id => @shark).first_or_create!
    @real_cat_item9 = Categoryitem.where(:species_id => @cobia).first_or_create!
    @real_cat_item10 = Categoryitem.where(:species_id => @goliath_grouper).first_or_create!
    @real_cat_item11 = Categoryitem.where(:species_id => @jack).first_or_create!
    @real_cat_item12 = Categoryitem.where(:species_id => @king).first_or_create!
    @real_cat_item13 = Categoryitem.where(:species_id => @tarpon).first_or_create!

#bluewater category items
    @real_cat_item14 = Categoryitem.where(:species_id => @yellow_tuna).first_or_create!
    @real_cat_item15 = Categoryitem.where(:species_id => @black_tuna).first_or_create!
    @real_cat_item16 = Categoryitem.where(:species_id => @flipper).first_or_create!
    @real_cat_item17 = Categoryitem.where(:species_id => @wahoo).first_or_create!

#special fish for inshore
    @real_cat_item18 = Categoryitem.where(:species_id => @trout_5).first_or_create!
    @real_cat_item19 = Categoryitem.where(:species_id => @redfish_5).first_or_create!

#some test items for the jimmy davis award - inshore
    @real_cat_item20 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item21 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item22 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item23 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item24 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item25 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item26 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item27 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item28 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item29 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item30 = Categoryitem.create(:super_category_id => @super_cat1.id)

    @real_cat_item31 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item32 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item33 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item34 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item35 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item36 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item37 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item38 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item39 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item40 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item41 = Categoryitem.create(:super_category_id => @super_cat2.id)

    @real_cat_item200 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item210 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item220 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item230 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item240 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item250 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item260 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item270 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item280 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item290 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item300 = Categoryitem.create(:super_category_id => @super_cat3.id)


# overall best angler
    @real_cat_item42 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item43 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item44 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item45 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item46 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item47 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item48 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item49 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item50 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item51 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item52 = Categoryitem.create(:super_category_id => @super_cat1.id)
    @real_cat_item53 = Categoryitem.create(:super_category_id => @super_cat2.id)

# inshore team
    @real_cat_item54 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item55 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item56 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item57 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item58 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item59 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item60 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item61 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item62 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item63 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item64 = Categoryitem.create(:super_category_id => @super_cat1.id)

    @real_cat_item65 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item66 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item67 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item68 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item69 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item70 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item71 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item72 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item73 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item74 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item75 = Categoryitem.create(:super_category_id => @super_cat2.id)

    @real_cat_item76 = Categoryitem.create(:place => 1,  :points => 100)
    @real_cat_item77 = Categoryitem.create(:place => 2,  :points => 90)
    @real_cat_item78 = Categoryitem.create(:place => 3,  :points => 80)
    @real_cat_item79 = Categoryitem.create(:place => 4,  :points => 70)
    @real_cat_item80 = Categoryitem.create(:place => 5,  :points => 60)
    @real_cat_item81 = Categoryitem.create(:place => 6,  :points => 50)
    @real_cat_item82 = Categoryitem.create(:place => 7,  :points => 40)
    @real_cat_item83 = Categoryitem.create(:place => 8,  :points => 30)
    @real_cat_item84 = Categoryitem.create(:place => 9,  :points => 20)
    @real_cat_item85 = Categoryitem.create(:place => 10, :points => 10)
    @real_cat_item86 = Categoryitem.create(:super_category_id => @super_cat3.id)

    @bill_fish_item_1 = Categoryitem.create(:species_id => @blue.id, :points => 1000)
    @bill_fish_item_2 = Categoryitem.create(:species_id => @white.id, :points => 500)
    @bill_fish_item_3 = Categoryitem.create(:species_id => @sword.id, :points => 300)
    @bill_fish_item_4 = Categoryitem.create(:species_id => @sail.id, :points => 250)

#jimmy davis inshore
    @real_cat25.categoryitems << @real_cat_item20
    @real_cat25.categoryitems << @real_cat_item21
    @real_cat25.categoryitems << @real_cat_item22
    @real_cat25.categoryitems << @real_cat_item23
    @real_cat25.categoryitems << @real_cat_item24
    @real_cat25.categoryitems << @real_cat_item25
    @real_cat25.categoryitems << @real_cat_item26
    @real_cat25.categoryitems << @real_cat_item27
    @real_cat25.categoryitems << @real_cat_item28
    @real_cat25.categoryitems << @real_cat_item29
    @real_cat25.categoryitems << @real_cat_item30

#offshore
    @real_cat26.categoryitems << @real_cat_item31
    @real_cat26.categoryitems << @real_cat_item32
    @real_cat26.categoryitems << @real_cat_item33
    @real_cat26.categoryitems << @real_cat_item34
    @real_cat26.categoryitems << @real_cat_item35
    @real_cat26.categoryitems << @real_cat_item36
    @real_cat26.categoryitems << @real_cat_item37
    @real_cat26.categoryitems << @real_cat_item38
    @real_cat26.categoryitems << @real_cat_item39
    @real_cat26.categoryitems << @real_cat_item40
    @real_cat26.categoryitems << @real_cat_item41
#bluewater
    @real_cat27.categoryitems << @real_cat_item200
    @real_cat27.categoryitems << @real_cat_item210
    @real_cat27.categoryitems << @real_cat_item220
    @real_cat27.categoryitems << @real_cat_item230
    @real_cat27.categoryitems << @real_cat_item240
    @real_cat27.categoryitems << @real_cat_item250
    @real_cat27.categoryitems << @real_cat_item260
    @real_cat27.categoryitems << @real_cat_item270
    @real_cat27.categoryitems << @real_cat_item280
    @real_cat27.categoryitems << @real_cat_item290
    @real_cat27.categoryitems << @real_cat_item300

# inshore team
    @real_cat28.categoryitems << @real_cat_item54
    @real_cat28.categoryitems << @real_cat_item55
    @real_cat28.categoryitems << @real_cat_item56
    @real_cat28.categoryitems << @real_cat_item57
    @real_cat28.categoryitems << @real_cat_item58
    @real_cat28.categoryitems << @real_cat_item59
    @real_cat28.categoryitems << @real_cat_item60
    @real_cat28.categoryitems << @real_cat_item61
    @real_cat28.categoryitems << @real_cat_item62
    @real_cat28.categoryitems << @real_cat_item63
    @real_cat28.categoryitems << @real_cat_item64

#offshore team
    @real_cat29.categoryitems << @real_cat_item65
    @real_cat29.categoryitems << @real_cat_item66
    @real_cat29.categoryitems << @real_cat_item67
    @real_cat29.categoryitems << @real_cat_item68
    @real_cat29.categoryitems << @real_cat_item69
    @real_cat29.categoryitems << @real_cat_item70
    @real_cat29.categoryitems << @real_cat_item71
    @real_cat29.categoryitems << @real_cat_item72
    @real_cat29.categoryitems << @real_cat_item73
    @real_cat29.categoryitems << @real_cat_item74
    @real_cat29.categoryitems << @real_cat_item75

#bluewater team
    @real_cat30.categoryitems << @real_cat_item76
    @real_cat30.categoryitems << @real_cat_item77
    @real_cat30.categoryitems << @real_cat_item78
    @real_cat30.categoryitems << @real_cat_item79
    @real_cat30.categoryitems << @real_cat_item80
    @real_cat30.categoryitems << @real_cat_item81
    @real_cat30.categoryitems << @real_cat_item82
    @real_cat30.categoryitems << @real_cat_item83
    @real_cat30.categoryitems << @real_cat_item84
    @real_cat30.categoryitems << @real_cat_item85
    @real_cat30.categoryitems << @real_cat_item86

# overall best angler
    @real_cat31.categoryitems << @real_cat_item42
    @real_cat31.categoryitems << @real_cat_item43
    @real_cat31.categoryitems << @real_cat_item44
    @real_cat31.categoryitems << @real_cat_item45
    @real_cat31.categoryitems << @real_cat_item46
    @real_cat31.categoryitems << @real_cat_item47
    @real_cat31.categoryitems << @real_cat_item48
    @real_cat31.categoryitems << @real_cat_item49
    @real_cat31.categoryitems << @real_cat_item50
    @real_cat31.categoryitems << @real_cat_item51
    @real_cat31.categoryitems << @real_cat_item52
    @real_cat31.categoryitems << @real_cat_item53

#Inshore men
    @real_cat1.categoryitems << @real_cat_item1_1
    @real_cat2.categoryitems << @real_cat_item2_1
    @real_cat3.categoryitems << @real_cat_item3_1
    @real_cat4.categoryitems << @real_cat_item4_1
    @real_cat5.categoryitems << @real_cat_item5_1
    @real_cat6.categoryitems << @real_cat_item6_1
    @real_cat23.categoryitems << @real_cat_item18
    @real_cat24.categoryitems << @real_cat_item19
    @super_cat1.categories << @real_cat1
    @super_cat1.categories << @real_cat2
    @super_cat1.categories << @real_cat3
    @super_cat1.categories << @real_cat4
    @super_cat1.categories << @real_cat5
    @super_cat1.categories << @real_cat6
    @super_cat1.categories << @real_cat23
    @super_cat1.categories << @real_cat24

#inshore women
    @real_cat9.categoryitems << @real_cat_item1
    @real_cat10.categoryitems << @real_cat_item2
    @real_cat11.categoryitems << @real_cat_item3
    @real_cat12.categoryitems << @real_cat_item4
    @real_cat13.categoryitems << @real_cat_item5
    @real_cat14.categoryitems << @real_cat_item6
    @super_cat6.categories << @real_cat9
    @super_cat6.categories << @real_cat10
    @super_cat6.categories << @real_cat11
    @super_cat6.categories << @real_cat12
    @super_cat6.categories << @real_cat13
    @super_cat6.categories << @real_cat14



#offshore
    @real_cat16.categoryitems << @real_cat_item7
    @real_cat17.categoryitems << @real_cat_item8
    @real_cat18.categoryitems << @real_cat_item9
    @real_cat19.categoryitems << @real_cat_item10
    @real_cat20.categoryitems << @real_cat_item11
    @real_cat21.categoryitems << @real_cat_item12
    @real_cat22.categoryitems << @real_cat_item13

    @super_cat2.categories << @real_cat16
    @super_cat2.categories << @real_cat17
    @super_cat2.categories << @real_cat18
    @super_cat2.categories << @real_cat19
    @super_cat2.categories << @real_cat20
    @super_cat2.categories << @real_cat21
    @super_cat2.categories << @real_cat22


# overall standins
    @super_cat5.categories << @real_cat25
    @super_cat5.categories << @real_cat26
    @super_cat5.categories << @real_cat31
    @super_cat5.categories << @real_cat28
    @super_cat5.categories << @real_cat27
    @super_cat5.categories << @real_cat29
    @super_cat5.categories << @real_cat30



#blue water
    @real_cat32.categoryitems << @real_cat_item14
    @real_cat33.categoryitems << @real_cat_item15
    @real_cat34.categoryitems << @real_cat_item16
    @real_cat35.categoryitems << @real_cat_item17

    @super_cat3.categories << @real_cat32
    @super_cat3.categories << @real_cat33
    @super_cat3.categories << @real_cat34
    @super_cat3.categories << @real_cat35

#billfish release
    @bil_cat.categoryitems << @bill_fish_item_1
    @bil_cat.categoryitems << @bill_fish_item_2
    @bil_cat.categoryitems << @bill_fish_item_3
    @bil_cat.categoryitems << @bill_fish_item_4

    @super_cat4.categories << @bil_cat



    @fourchon.super_categories << @super_cat1
    @fourchon.super_categories << @super_cat6
    @fourchon.super_categories << @super_cat2
    @fourchon.super_categories << @super_cat3
    @fourchon.super_categories << @super_cat4
    @fourchon.super_categories << @super_cat5


    @event     =    @fourchon

    part1 = Participant.create(:first_name => "John ", :last_name => "Aucoin", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part2 = Participant.create(:first_name => "Eddie ", :last_name => "Davis", :participant_type => :angler, :gender => :male, :mobile_phone => "504-975-0167", :email => "edavis@axisoilfieldrentals.com", :street => "", :city => "", :zipcode => ""  )
    part3 = Participant.create(:first_name => "Chad ", :last_name => "Reinhardt", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "200 Woodland Drive", :city => "Broussard", :zipcode => "70518"  )
    part4 = Participant.create(:first_name => "George ", :last_name => "Bent", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "nolaboater@gmail.com", :street => "8001 Airline Drive", :city => "Metairie", :zipcode => "70003"  )
    part5 = Participant.create(:first_name => "Ben ", :last_name => "Todd", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "ben@beierradio.com", :street => "", :city => "", :zipcode => ""  )
    part6 = Participant.create(:first_name => "Eric ", :last_name => "Hellbach", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "eh@bissomarine.com", :street => "11311 Neeshaw Drive", :city => "Houston", :zipcode => "77065"  )
    part7 = Participant.create(:first_name => "Roy ", :last_name => "Buchler", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part8 = Participant.create(:first_name => "Beau ", :last_name => "Bisso", :participant_type => :angler, :gender => :male, :mobile_phone => "504-858-9535", :email => "", :street => "", :city => "", :zipcode => ""  )
    part9 = Participant.create(:first_name => "Toby ", :last_name => "Blanchard", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "tblanchard@blanchardcontractors.com", :street => "15444 Hwy 3235", :city => "Cut Off", :zipcode => "70345"  )
    part10 = Participant.create(:first_name => "Ryan ", :last_name => "Dicharry", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "PO Box 250", :city => "Lockport", :zipcode => "70374"  )
    part11 = Participant.create(:first_name => "T-Kip ", :last_name => "Plaisance", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "PO Box 250", :city => "Lockport", :zipcode => "70374"  )
    part12 = Participant.create(:first_name => "Lance ", :last_name => "Lejeune", :participant_type => :angler, :gender => :male, :mobile_phone => "985-665-0873", :email => "llejeune@byronetalbot.com", :street => "PO Box 5658", :city => "Thibodaux", :zipcode => "70302"  )
    part13 = Participant.create(:first_name => "Travis ", :last_name => "Cusimano", :participant_type => :angler, :gender => :male, :mobile_phone => " 337-893-3686", :email => "travis.cusimano@cajunusa.com", :street => "1940 Cheryl Drive", :city => "Abbeville", :zipcode => "70510"  )
    part14 = Participant.create(:first_name => "Thomas", :last_name => "Biernatzki", :participant_type => :angler, :gender => :male, :mobile_phone => "985-709-3433", :email => "Thomas.biernatzki@cetco.com", :street => "", :city => "", :zipcode => ""  )
    part15 = Participant.create(:first_name => "Dino", :last_name => "Cheramie", :participant_type => :angler, :gender => :male, :mobile_phone => "985-691-2525", :email => "dino@cheramiemarine.com", :street => "", :city => "", :zipcode => ""  )
    part16 = Participant.create(:first_name => "John ", :last_name => "Deblieux", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "PO Box 3301", :city => "Houma", :zipcode => "70361"  )
    part17 = Participant.create(:first_name => "Warner ", :last_name => "Williams", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "100 Northpark Blvd", :city => "Covington", :zipcode => "70433"  )
    part18 = Participant.create(:first_name => "Glynn ", :last_name => "Haines", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "glynn@hainescompanies.com", :street => "PO Box 1550", :city => "Amelia", :zipcode => "70340"  )
    part19 = Participant.create(:first_name => "Craig ", :last_name => "Roddy", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-0131", :email => "", :street => "", :city => "", :zipcode => ""  )
    part20 = Participant.create(:first_name => "Brad ", :last_name => "Prejean", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "PO Drawer 490", :city => "Galliano", :zipcode => "70354"  )
    part21 = Participant.create(:first_name => "Rockelle ", :last_name => "Prejean", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part22 = Participant.create(:first_name => "Micheal ", :last_name => "Lee", :participant_type => :angler, :gender => :male, :mobile_phone => "504-881-9148", :email => "mike.lee@cummins.com", :street => "110 East Airline Hwy", :city => "Kenner", :zipcode => "70062"  )
    part23 = Participant.create(:first_name => "Todd ", :last_name => "Thibodeaux", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-5507", :email => "toddt@thecrossgroup.com", :street => "", :city => "", :zipcode => ""  )
    part24 = Participant.create(:first_name => "Jamie ", :last_name => "Rodrigue", :participant_type => :angler, :gender => :male, :mobile_phone => "985-859-4927", :email => "jrodrigue@thecrossgroup.com", :street => "", :city => "", :zipcode => ""  )
    part25 = Participant.create(:first_name => "Reed ", :last_name => "Pere", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "PO Box 1460", :city => "Larose", :zipcode => "70373"  )
    part26 = Participant.create(:first_name => "Shawn ", :last_name => "Olivier", :participant_type => :angler, :gender => :male, :mobile_phone => "985-278-1637", :email => "shawno@getdatacom.com", :street => "Lafayette", :city => "LA", :zipcode => ""  )
    part27 = Participant.create(:first_name => "Poochie ", :last_name => "Suttoon", :participant_type => :angler, :gender => :male, :mobile_phone => "985-518-8311", :email => "", :street => "", :city => "", :zipcode => ""  )
    part28 = Participant.create(:first_name => "Brent ", :last_name => "Guidry ", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-1532", :email => "bguidry18337@hotmail.com", :street => "120 Airport Rd", :city => "Galliano", :zipcode => "70354"  )
    part29 = Participant.create(:first_name => "Albert ", :last_name => "Miller", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "16201 East Main St", :city => "Galliano", :zipcode => "70354"  )
    part30 = Participant.create(:first_name => "David ", :last_name => "Dalfo", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part31 = Participant.create(:first_name => "Dominic ", :last_name => "LaCombe", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part32 = Participant.create(:first_name => "Scott ", :last_name => "Dufrene", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part33 = Participant.create(:first_name => "Murty ", :last_name => "Hoey", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part34 = Participant.create(:first_name => "Lauren ", :last_name => "Adams", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "ladams@epsteam.com ", :street => "PO Box 80644", :city => "Lafayette", :zipcode => "70353"  )
    part35 = Participant.create(:first_name => "Lee", :last_name => "Arcement", :participant_type => :angler, :gender => :male, :mobile_phone => "985-665-4319", :email => "", :street => "PO Box 9217", :city => "Houma", :zipcode => "70361"  )
    part36 = Participant.create(:first_name => "Stuart ", :last_name => "Faucheux", :participant_type => :angler, :gender => :male, :mobile_phone => "985-258-0044", :email => "stuart@expresssteel.net", :street => "", :city => "", :zipcode => ""  )
    part37 = Participant.create(:first_name => "Bucky ", :last_name => "Angelette", :participant_type => :angler, :gender => :male, :mobile_phone => "985-232-0456", :email => "bangelette@forcepowersystems.com", :street => "", :city => "", :zipcode => ""  )
    part38 = Participant.create(:first_name => "Vick ", :last_name => "Lafont", :participant_type => :angler, :gender => :male, :mobile_phone => "504-382-3515", :email => "", :street => "18838 Hwy 3235", :city => "Galliano", :zipcode => "70354"  )
    part39 = Participant.create(:first_name => "Manuel ", :last_name => "Merlos", :participant_type => :angler, :gender => :male, :mobile_phone => "985-475-5238", :email => "mmerlos@gisy.com", :street => "", :city => "", :zipcode => ""  )
    part40 = Participant.create(:first_name => "Bryan ", :last_name => "Pregant", :participant_type => :angler, :gender => :male, :mobile_phone => "985-677-0572", :email => "bryan@gisy.com", :street => "", :city => "", :zipcode => ""  )
    part41 = Participant.create(:first_name => "Chris ", :last_name => "Vincent", :participant_type => :angler, :gender => :male, :mobile_phone => "337-291-6524", :email => "chrisv@getgds.com", :street => "537 Cajun Dome Blvd.", :city => "Lafayette", :zipcode => "70506"  )
    part42 = Participant.create(:first_name => "Nathan ", :last_name => "Guice", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "nathan.guice@guiceoffshore.com", :street => "101 Ashland Way", :city => "Madisonville", :zipcode => "70447"  )
    part43 = Participant.create(:first_name => "Noonie", :last_name => "Guilbeau", :participant_type => :angler, :gender => :male, :mobile_phone => "985-632-6633", :email => "chadg@viscom.net", :street => "", :city => "", :zipcode => ""  )
    part44 = Participant.create(:first_name => "Danny ", :last_name => "Angeron", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "danny.angeron@gulfmark.com", :street => "", :city => "", :zipcode => ""  )
    part45 = Participant.create(:first_name => "Bambi ", :last_name => "Roper", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "bambi@gulf-log.com", :street => "P.O. Box 433", :city => "Mathews", :zipcode => "70375"  )
    part46 = Participant.create(:first_name => "Lance ", :last_name => "Reynolds", :participant_type => :angler, :gender => :male, :mobile_phone => "985-278-2207", :email => "lance@harveygulf.com", :street => "", :city => "", :zipcode => ""  )
    part47 = Participant.create(:first_name => "Andy ", :last_name => "Bruzdinski", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "andy.bruzdinski@hornbeckoffshore.com", :street => "103 Northpark Blvd Suite 300", :city => "Covington", :zipcode => "70433"  )
    part48 = Participant.create(:first_name => "Peter ", :last_name => "Fortier", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "peter.fortier@hornbeckoffshore.com", :street => "", :city => "", :zipcode => ""  )
    part49 = Participant.create(:first_name => "Randy ", :last_name => "Tredinich", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "randy.tredinich@hornbeckoffshore.com", :street => "", :city => "", :zipcode => ""  )
    part50 = Participant.create(:first_name => "Chad ", :last_name => "Boudreaux", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "joesseptic@viscom.net", :street => "", :city => "", :zipcode => ""  )
    part51 = Participant.create(:first_name => "Andy ", :last_name => "Naquin", :participant_type => :angler, :gender => :male, :mobile_phone => "985-209-3983", :email => "andy@kilgoremarine.com", :street => "1819 West Pinhook Ste. 109", :city => "Lafayette", :zipcode => "70508"  )
    part52 = Participant.create(:first_name => "Van ", :last_name => "McNeil", :participant_type => :angler, :gender => :male, :mobile_phone => "985-696-0777", :email => "van@kimsusan.com", :street => "PO Box 1428 ", :city => "Larose", :zipcode => "70373"  )
    part53 = Participant.create(:first_name => "Grady ", :last_name => "Fagan", :participant_type => :angler, :gender => :male, :mobile_phone => "985-696-2409", :email => "grady@kimsusan.com", :street => "", :city => "", :zipcode => ""  )
    part54 = Participant.create(:first_name => "Nicholas ", :last_name => "Knight", :participant_type => :angler, :gender => :male, :mobile_phone => "337-257-3223", :email => "nknight@knightoiltools.com", :street => "402 Worth Ave.", :city => "Lafayette", :zipcode => "70508"  )
    part55 = Participant.create(:first_name => "Ross ", :last_name => "Laris", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-2024", :email => "ross@larisinsurance.com", :street => "810 Crescent Ave", :city => "Lockport", :zipcode => "70374"  )
    part56 = Participant.create(:first_name => "Jacob ", :last_name => "Pitre", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-9699", :email => "jacobp@botruc.com", :street => "18692 West Main St", :city => "Galliano", :zipcode => "70354"  )
    part57 = Participant.create(:first_name => "Jimmy ", :last_name => "Gele", :participant_type => :angler, :gender => :male, :mobile_phone => "504-400-3771", :email => "", :street => "", :city => "", :zipcode => ""  )
    part58 = Participant.create(:first_name => "Brad ", :last_name => "Matte", :participant_type => :angler, :gender => :male, :mobile_phone => "985-384-2928", :email => "bmatte@leevac.com", :street => "", :city => "", :zipcode => ""  )
    part59 = Participant.create(:first_name => "Greg ", :last_name => "Galliano", :participant_type => :angler, :gender => :male, :mobile_phone => "504-416-2682", :email => "gggalliano@loopllc.com", :street => "", :city => "", :zipcode => ""  )
    part60 = Participant.create(:first_name => "Ray ", :last_name => "Mayet", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "1423 Whitney Ave. ", :city => "Gretna", :zipcode => "70056"  )
    part61 = Participant.create(:first_name => "Ray ", :last_name => "Rebstock", :participant_type => :angler, :gender => :male, :mobile_phone => "504-392-8670", :email => "", :street => "", :city => "", :zipcode => ""  )
    part62 = Participant.create(:first_name => "Bryan ", :last_name => "Paille", :participant_type => :angler, :gender => :male, :mobile_phone => "225-323-1649", :email => "bpaille@louisianamachinery.com", :street => "", :city => "", :zipcode => ""  )
    part63 = Participant.create(:first_name => "Bryan ", :last_name => "Arceneaux ", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-1862", :email => "", :street => "P.O. Box 1390", :city => "Larose", :zipcode => "70373"  )
    part64 = Participant.create(:first_name => "Heath ", :last_name => "Matherne", :participant_type => :angler, :gender => :male, :mobile_phone => "985-870-4555", :email => "", :street => "", :city => "", :zipcode => ""  )
    part65 = Participant.create(:first_name => "Glenn ", :last_name => "Kourik", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "glenn.kourik@martinmlp.com", :street => "", :city => "", :zipcode => ""  )
    part66 = Participant.create(:first_name => "Gary ", :last_name => "Daigle", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "garyd@magnummud.com", :street => "", :city => "", :zipcode => ""  )
    part67 = Participant.create(:first_name => "Glenn ", :last_name => "Plaisance", :participant_type => :angler, :gender => :male, :mobile_phone => "504-915-3794", :email => "glenn@marinespecialties-inc.com", :street => "", :city => "", :zipcode => ""  )
    part68 = Participant.create(:first_name => "Dale ", :last_name => "Mitchell", :participant_type => :angler, :gender => :male, :mobile_phone => "337-380-4111", :email => "dale@mitchellliftboats.com", :street => "", :city => "", :zipcode => ""  )
    part69 = Participant.create(:first_name => "Dale ", :last_name => "Bergeron", :participant_type => :angler, :gender => :male, :mobile_phone => "985-665-7192", :email => "", :street => "", :city => "", :zipcode => ""  )
    part70 = Participant.create(:first_name => "Grady ", :last_name => "Saucier", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "gsaucier@mmrgrp.com", :street => "", :city => "", :zipcode => ""  )
    part71 = Participant.create(:first_name => "Grady ", :last_name => "Galiano", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-9177", :email => "grady.galiano@montco.com", :street => "P.O. Box 850 ", :city => "Galliano", :zipcode => "70354"  )
    part72 = Participant.create(:first_name => "Eddie ", :last_name => "Bousegard", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-9185", :email => "eddie.bousegard@montco.com", :street => "", :city => "", :zipcode => ""  )
    part73 = Participant.create(:first_name => "Chris ", :last_name => "Moran", :participant_type => :angler, :gender => :male, :mobile_phone => "225-931-7306", :email => "cmoransmarina@gmail.com", :street => "", :city => "", :zipcode => ""  )
    part74 = Participant.create(:first_name => "Joe ", :last_name => "Gregory", :participant_type => :angler, :gender => :male, :mobile_phone => "985-448-0958", :email => "joegregory@ngmarine.com", :street => "PO Box 937", :city => "Bourg", :zipcode => "70343"  )
    part75 = Participant.create(:first_name => "James ", :last_name => "Cloutier", :participant_type => :angler, :gender => :male, :mobile_phone => "985-381-3307", :email => "", :street => "", :city => "", :zipcode => ""  )
    part76 = Participant.create(:first_name => "Ryan ", :last_name => "Cheramie", :participant_type => :angler, :gender => :male, :mobile_phone => "985-312-9391", :email => "", :street => "", :city => "", :zipcode => ""  )
    part77 = Participant.create(:first_name => "Chad ", :last_name => "Harvey", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part78 = Participant.create(:first_name => "Mike ", :last_name => "Melancon", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "mike@offshoreliftboats.com", :street => "P.O. Box 398", :city => "Cut Off", :zipcode => "70345"  )
    part79 = Participant.create(:first_name => "Gary ", :last_name => "Callais", :participant_type => :angler, :gender => :male, :mobile_phone => "985-688-0842", :email => "gary@offshoreliftboats.com", :street => "", :city => "", :zipcode => ""  )
    part80 = Participant.create(:first_name => "Jordan ", :last_name => "Orgeron", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "jordano@omci.biz", :street => "P.O. Box 1463", :city => "Larose", :zipcode => "70373"  )
    part81 = Participant.create(:first_name => "Bryce ", :last_name => "Michel", :participant_type => :angler, :gender => :male, :mobile_phone => "504-273-3010", :email => "", :street => "PO Box 25 ", :city => "Des Allemands", :zipcode => "70030"  )
    part82 = Participant.create(:first_name => "Joe ", :last_name => "Picciola", :participant_type => :angler, :gender => :male, :mobile_phone => "985-291-5334", :email => "joe@picciola.com", :street => "", :city => "", :zipcode => ""  )
    part83 = Participant.create(:first_name => "John ", :last_name => "Picciola", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-5322", :email => "john@picciolaconstruction.com", :street => "", :city => "", :zipcode => ""  )
    part84 = Participant.create(:first_name => "Mark", :last_name => "Cheramie", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "18708 West Main St", :city => "Galliano", :zipcode => "70354"  )
    part85 = Participant.create(:first_name => "Brian ", :last_name => "Barthelemy", :participant_type => :angler, :gender => :male, :mobile_phone => "985-665-7833", :email => "brian@recmarine.net", :street => "P.O. Box 774", :city => "Galliano", :zipcode => "70354"  )
    part86 = Participant.create(:first_name => "Paul ", :last_name => "Hebert", :participant_type => :angler, :gender => :male, :mobile_phone => "985-637-6383", :email => "paul@rcstankcleaning.com", :street => "PO Box 474", :city => "Crowley", :zipcode => "70527"  )
    part87 = Participant.create(:first_name => "Randy ", :last_name => "Adams", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "randy@sea-support.com", :street => "104 ABC Lane", :city => "Cut Off", :zipcode => "70345"  )
    part88 = Participant.create(:first_name => "Buddy ", :last_name => "Curole", :participant_type => :angler, :gender => :male, :mobile_phone => "985-632-6000", :email => "", :street => "", :city => "", :zipcode => ""  )
    part89 = Participant.create(:first_name => "Glynn ", :last_name => "Haines", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "glynn@hainescompanies.com", :street => "PO Box 1551", :city => "Amelia", :zipcode => "70341"  )
    part90 = Participant.create(:first_name => "Clayton ", :last_name => "Breaux", :participant_type => :angler, :gender => :male, :mobile_phone => "985-226-1506", :email => "clayton@ckor.com", :street => "7910 Main Street ", :city => "Houma", :zipcode => "70360"  )
    part91 = Participant.create(:first_name => "Alan ", :last_name => "Power", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "alan.power@shell.com", :street => "", :city => "", :zipcode => ""  )
    part92 = Participant.create(:first_name => "Ralph ", :last_name => "Mcingvale", :participant_type => :angler, :gender => :male, :mobile_phone => "281-209-2871", :email => "ralphm@southernstatesboats.com", :street => "19101 Oil Center Blvd. ", :city => "Houston", :zipcode => "77073"  )
    part93 = Participant.create(:first_name => "Penny", :last_name => "Robichaux", :participant_type => :angler, :gender => :male, :mobile_phone => "985-677-6048", :email => "pennyr@southernstatesboats.com", :street => "19101 Oil Center Blvd. ", :city => "Houston", :zipcode => "77073"  )
    part94 = Participant.create(:first_name => "Roger", :last_name => "Braud", :participant_type => :angler, :gender => :male, :mobile_phone => "985-580-2404", :email => "roger@istranco.com", :street => "", :city => "", :zipcode => ""  )
    part95 = Participant.create(:first_name => "Dave ", :last_name => "Duhe", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "", :street => "", :city => "", :zipcode => ""  )
    part96 = Participant.create(:first_name => "Neil ", :last_name => "Loupe", :participant_type => :angler, :gender => :male, :mobile_phone => "985-992-9544", :email => "nloupe@synergyresllc.com", :street => "8422 Hwy 182 E", :city => "Morgan City", :zipcode => "70380"  )
    part97 = Participant.create(:first_name => "Deanna ", :last_name => "Stansbury", :participant_type => :angler, :gender => :male, :mobile_phone => "985-385-1913", :email => "", :street => "", :city => "", :zipcode => ""  )
    part98 = Participant.create(:first_name => "Dale ", :last_name => "Martin", :participant_type => :angler, :gender => :male, :mobile_phone => "985-518-0384", :email => "dale@dalemartinent.com", :street => "", :city => "", :zipcode => ""  )
    part99 = Participant.create(:first_name => "Kris ", :last_name => "Callais ", :participant_type => :angler, :gender => :male, :mobile_phone => "", :email => "kris@vaccomarine.com", :street => "PO Box 8032 ", :city => "Houma", :zipcode => "70361"  )
    part100 = Participant.create(:first_name => "Al ", :last_name => "Waguespack", :participant_type => :angler, :gender => :male, :mobile_phone => "985-665-1766", :email => "al@wagoil.com", :street => "PO Box 326", :city => "Thibodaux", :zipcode => "70302"  )
    team1= Team.create(:name => "Angelette-Picciola", :company => "Angeltte-Picciola (1)", :captain_id => part1.id )
    team2= Team.create(:name => "Axis", :company => "Axis Oilfield Rentals", :captain_id => part2.id )
    team3= Team.create(:name => "BeeMar", :company => "Bee Mar LLC (1)", :captain_id => part3.id )
    team4= Team.create(:name => "Team Bent Marine", :company => "Bent Marine(1)", :captain_id => part4.id )
    team5= Team.create(:name => "Beier Offshore Fishing Team", :company => "Beier Radio", :captain_id => part5.id )
    team6= Team.create(:name => "Bisso Marine 1", :company => "Bisso Marine (1)", :captain_id => part6.id )
    team7= Team.create(:name => "Bisso Marine 2", :company => "Bisso Marine (2)", :captain_id => part7.id )
    team8= Team.create(:name => "Bisso Marine 3", :company => "Bisso Marine (3)", :captain_id => part8.id )
    team9= Team.create(:name => "Team Blanchard", :company => "Blanchard Contractors (1)", :captain_id => part9.id )
    team10= Team.create(:name => "Bollinger 1", :company => "Bollinger Shipyards (1)", :captain_id => part10.id )
    team11= Team.create(:name => "Bollinger 2", :company => "Bollinger Shipyards (2)", :captain_id => part11.id )
    team12= Team.create(:name => "Byron Talbot", :company => "Byron Talbot Contractors (1)", :captain_id => part12.id )
    team13= Team.create(:name => "Cajun Maritime", :company => "Cajun Maritime, LLC", :captain_id => part13.id )
    team14= Team.create(:name => "Cetco Oilfield Services", :company => "Cetco ", :captain_id => part14.id )
    team15= Team.create(:name => "Salops", :company => "Cheramie Marine", :captain_id => part15.id )
    team16= Team.create(:name => "Chet Morrison Contractors", :company => "Chet Morrison Contractors (1)", :captain_id => part16.id )
    team17= Team.create(:name => "Chevron", :company => "Chevron (1)", :captain_id => part17.id )
    team18= Team.create(:name => "Comar Marine", :company => "Comar (1)", :captain_id => part18.id )
    team19= Team.create(:name => "Team Conrad", :company => "Conrad Shipyards", :captain_id => part19.id )
    team20= Team.create(:name => "Team Amnesia 1", :company => "CWP Family LLC (1)", :captain_id => part20.id )
    team21= Team.create(:name => "Team Amnesia 2", :company => "CWP Family LLC (2)", :captain_id => part21.id )
    team22= Team.create(:name => "Call Me Gone", :company => "Cummins Mid South (1)", :captain_id => part22.id )
    team23= Team.create(:name => "Cross Group 1", :company => "Cross Group(1)", :captain_id => part23.id )
    team24= Team.create(:name => "Cross Group 2", :company => "Cross Group (2)", :captain_id => part24.id )
    team25= Team.create(:name => "Team Danos", :company => "Danos (1)", :captain_id => part25.id )
    team26= Team.create(:name => "Datacom", :company => "Datacom(1)", :captain_id => part26.id )
    team27= Team.create(:name => "Delmar", :company => "Delmar Systems, Inc (1)", :captain_id => part27.id )
    team28= Team.create(:name => "Gon Pacon", :company => "E&L (1)", :captain_id => part28.id )
    team29= Team.create(:name => "C'est la Vie", :company => "Edison Chouest Offshore(1)", :captain_id => part29.id )
    team30= Team.create(:name => "Chach", :company => "Edison Chouest Offshore(2)", :captain_id => part30.id )
    team31= Team.create(:name => "Patriot", :company => "Edison Chouest Offshore(3)", :captain_id => part31.id )
    team32= Team.create(:name => "Freedom", :company => "Edison Chouest Offshore(4)", :captain_id => part32.id )
    team33= Team.create(:name => "Ellsworth Corperation", :company => "Ellsworth Corporation(1)", :captain_id => part33.id )
    team34= Team.create(:name => "EPS Logistics", :company => "EPS Logistics(1)", :captain_id => part34.id )
    team35= Team.create(:name => "ES&H", :company => "ES&H Consulting(1)", :captain_id => part35.id )
    team36= Team.create(:name => "Team Express", :company => "Express Supply & Steel(1)", :captain_id => part36.id )
    team37= Team.create(:name => "Force Power Systems", :company => "Force Power Systems", :captain_id => part37.id )
    team38= Team.create(:name => "Deez Nutsacks", :company => "GIS Marine (1)", :captain_id => part38.id )
    team39= Team.create(:name => "GIS", :company => "GIS (1)", :captain_id => part39.id )
    team40= Team.create(:name => "Bad Company", :company => "GIS (2)", :captain_id => part40.id )
    team41= Team.create(:name => "Global Data Systems", :company => "Global Data Systems Inc. (1)", :captain_id => part41.id )
    team42= Team.create(:name => "Guice Offshore", :company => "Guice Offshore(1)", :captain_id => part42.id )
    team43= Team.create(:name => "Team Dangereuse", :company => "Guilbeau Marine Inc./C&G Boats", :captain_id => part43.id )
    team44= Team.create(:name => "Gulf Mark", :company => "Gulf Mark Americas Inc.", :captain_id => part44.id )
    team45= Team.create(:name => "GOL", :company => "Gulf Offshore Logistics(1)", :captain_id => part45.id )
    team46= Team.create(:name => "Team Harvey", :company => "Harvey Gulf (1)", :captain_id => part46.id )
    team47= Team.create(:name => "HOSMAX 320", :company => "HOS(1)", :captain_id => part47.id )
    team48= Team.create(:name => "HOSMAX 310", :company => "HOS(2)", :captain_id => part48.id )
    team49= Team.create(:name => "HOSMAX 300", :company => "HOS (3)", :captain_id => part49.id )
    team50= Team.create(:name => "Joe Septic", :company => "Joe's Septic.", :captain_id => part50.id )
    team51= Team.create(:name => "Team Kilgore", :company => "Kilgore Marine (1)", :captain_id => part51.id )
    team52= Team.create(:name => "Team KSI ", :company => "Kim Susan (1)", :captain_id => part52.id )
    team53= Team.create(:name => "KSI Outlaws", :company => "Kim Susan (2)", :captain_id => part53.id )
    team54= Team.create(:name => "Knight Life", :company => "Knight Oil Tools (1)", :captain_id => part54.id )
    team55= Team.create(:name => "Team Laris", :company => "Laris Insurance(1)", :captain_id => part55.id )
    team56= Team.create(:name => "Botruc", :company => "L&M Botruc (1)", :captain_id => part56.id )
    team57= Team.create(:name => "Lady Lab", :company => "Laborde Marine (1)", :captain_id => part57.id )
    team58= Team.create(:name => "Team Leevac", :company => "Leevac Shipyards", :captain_id => part58.id )
    team59= Team.create(:name => "Team LOOP", :company => "LOOP(1)", :captain_id => part59.id )
    team60= Team.create(:name => "Team Greg", :company => "LOOP(2)", :captain_id => part60.id )
    team61= Team.create(:name => "LIM", :company => "Louisiana International Marine", :captain_id => part61.id )
    team62= Team.create(:name => "Louisiana CAT 1", :company => "Louisiana Machinery", :captain_id => part62.id )
    team63= Team.create(:name => "Colby Jean", :company => "Louisiana Tank Specialties(1)", :captain_id => part63.id )
    team64= Team.create(:name => "Team Skanktury", :company => "Machine Tech", :captain_id => part64.id )
    team65= Team.create(:name => "Martine Energy Services", :company => "Martin Energy Services, LLC", :captain_id => part65.id )
    team66= Team.create(:name => "Team Magnum", :company => "Magnum Mud Equipment(1)", :captain_id => part66.id )
    team67= Team.create(:name => "Team Obsessed Koonass", :company => "Marine Specialties Inc(1)", :captain_id => part67.id )
    team68= Team.create(:name => "Big Bang", :company => "Mitchell Liftboats(1)", :captain_id => part68.id )
    team69= Team.create(:name => "MMR Group 1", :company => "MMR Group  1", :captain_id => part69.id )
    team70= Team.create(:name => "MMR Group 2", :company => "MMR Group Inc 2", :captain_id => part70.id )
    team71= Team.create(:name => "Montco Team 1", :company => "Montco Offshore(1)", :captain_id => part71.id )
    team72= Team.create(:name => "Montco Team 2", :company => "Montco Offshore(2)", :captain_id => part72.id )
    team73= Team.create(:name => "Team Moran's/Slo Melt Ice", :company => "Moran's Marina/Slo-Melt Ice(1)", :captain_id => part73.id )
    team74= Team.create(:name => "New Generation Marine", :company => "New Generation Marine Services(1)", :captain_id => part74.id )
    team75= Team.create(:name => "NVI", :company => "NVI, LLC(1)", :captain_id => part75.id )
    team76= Team.create(:name => "Ocean Marine Contractors 1", :company => "Ocean Marine Contractors (1)", :captain_id => part76.id )
    team77= Team.create(:name => "Ocean Marine Contractors 2", :company => "Ocean Marine Contractors (2)", :captain_id => part77.id )
    team78= Team.create(:name => "Offshore Liftboats 1", :company => "Offshore Liftboats(1)", :captain_id => part78.id )
    team79= Team.create(:name => "Offshore Liftboats 2", :company => "Offshore Liftboats(2)", :captain_id => part79.id )
    team80= Team.create(:name => "Team OMC", :company => "Offshore Marine Contractors (1)", :captain_id => part80.id )
    team81= Team.create(:name => "Otto Candies", :company => "Otto Candies(1)", :captain_id => part81.id )
    team82= Team.create(:name => "Team P&A", :company => "Picciola & Associates(1)", :captain_id => part82.id )
    team83= Team.create(:name => "Trea'", :company => "Picciola Construction(1)", :captain_id => part83.id )
    team84= Team.create(:name => "C-Quest", :company => "Rebstock Supply(1)", :captain_id => part84.id )
    team85= Team.create(:name => "REC Marine", :company => "REC Marine(1)", :captain_id => part85.id )
    team86= Team.create(:name => "Team RCS", :company => "RCS LLC(1)", :captain_id => part86.id )
    team87= Team.create(:name => "No Fear", :company => "Sea Support (1)", :captain_id => part87.id )
    team88= Team.create(:name => "Lil Fear", :company => "Sea Support (2)", :captain_id => part88.id )
    team89= Team.create(:name => "Seacraft Shipyards", :company => "Seacraft Shipyards (1)", :captain_id => part89.id )
    team90= Team.create(:name => "Team Seacor", :company => "Seacor(1)", :captain_id => part90.id )
    team91= Team.create(:name => "Shell", :company => "Shell Offshore", :captain_id => part91.id )
    team92= Team.create(:name => "Texas Tigers 1", :company => "Southern States Offshore(1)", :captain_id => part92.id )
    team93= Team.create(:name => "Texas Tigers 2", :company => "Southern States Offshore/Strata Oil(2)", :captain_id => part93.id )
    team94= Team.create(:name => "Stranco-Tailings Services", :company => "Stranco-Tailings ", :captain_id => part94.id )
    team95= Team.create(:name => "High Gear", :company => "Sewart Supply, Inc.", :captain_id => part95.id )
    team96= Team.create(:name => "Team Synergy", :company => "Synergy Resources LLC(1)", :captain_id => part96.id )
    team97= Team.create(:name => "Tanks-a-Lot", :company => "Tanks-a-Lot ", :captain_id => part97.id )
    team98= Team.create(:name => "Reelax", :company => "Turn Key Cleaning Services GOM", :captain_id => part98.id )
    team99= Team.create(:name => "Team Vacco", :company => "Vacco Marine(1)", :captain_id => part99.id )
    team100= Team.create(:name => "Fueled Up", :company => "Waguespack Oil(1)", :captain_id => part100.id )
    team1.participants << part1
    team2.participants << part2
    team3.participants << part3
    team4.participants << part4
    team5.participants << part5
    team6.participants << part6
    team7.participants << part7
    team8.participants << part8
    team9.participants << part9
    team10.participants << part10
    team11.participants << part11
    team12.participants << part12
    team13.participants << part13
    team14.participants << part14
    team15.participants << part15
    team16.participants << part16
    team17.participants << part17
    team18.participants << part18
    team19.participants << part19
    team20.participants << part20
    team21.participants << part21
    team22.participants << part22
    team23.participants << part23
    team24.participants << part24
    team25.participants << part25
    team26.participants << part26
    team27.participants << part27
    team28.participants << part28
    team29.participants << part29
    team30.participants << part30
    team31.participants << part31
    team32.participants << part32
    team33.participants << part33
    team34.participants << part34
    team35.participants << part35
    team36.participants << part36
    team37.participants << part37
    team38.participants << part38
    team39.participants << part39
    team40.participants << part40
    team41.participants << part41
    team42.participants << part42
    team43.participants << part43
    team44.participants << part44
    team45.participants << part45
    team46.participants << part46
    team47.participants << part47
    team48.participants << part48
    team49.participants << part49
    team50.participants << part50
    team51.participants << part51
    team52.participants << part52
    team53.participants << part53
    team54.participants << part54
    team55.participants << part55
    team56.participants << part56
    team57.participants << part57
    team58.participants << part58
    team59.participants << part59
    team60.participants << part60
    team61.participants << part61
    team62.participants << part62
    team63.participants << part63
    team64.participants << part64
    team65.participants << part65
    team66.participants << part66
    team67.participants << part67
    team68.participants << part68
    team69.participants << part69
    team70.participants << part70
    team71.participants << part71
    team72.participants << part72
    team73.participants << part73
    team74.participants << part74
    team75.participants << part75
    team76.participants << part76
    team77.participants << part77
    team78.participants << part78
    team79.participants << part79
    team80.participants << part80
    team81.participants << part81
    team82.participants << part82
    team83.participants << part83
    team84.participants << part84
    team85.participants << part85
    team86.participants << part86
    team87.participants << part87
    team88.participants << part88
    team89.participants << part89
    team90.participants << part90
    team91.participants << part91
    team92.participants << part92
    team93.participants << part93
    team94.participants << part94
    team95.participants << part95
    team96.participants << part96
    team97.participants << part97
    team98.participants << part98
    team99.participants << part99
    team100.participants << part100

    @event.teams << team1
    @event.teams << team2
    @event.teams << team3
    @event.teams << team4
    @event.teams << team5
    @event.teams << team6
    @event.teams << team7
    @event.teams << team8
    @event.teams << team9
    @event.teams << team10
    @event.teams << team11
    @event.teams << team12
    @event.teams << team13
    @event.teams << team14
    @event.teams << team15
    @event.teams << team16
    @event.teams << team17
    @event.teams << team18
    @event.teams << team19
    @event.teams << team20
    @event.teams << team21
    @event.teams << team22
    @event.teams << team23
    @event.teams << team24
    @event.teams << team25
    @event.teams << team26
    @event.teams << team27
    @event.teams << team28
    @event.teams << team29
    @event.teams << team30
    @event.teams << team31
    @event.teams << team32
    @event.teams << team33
    @event.teams << team34
    @event.teams << team35
    @event.teams << team36
    @event.teams << team37
    @event.teams << team38
    @event.teams << team39
    @event.teams << team40
    @event.teams << team41
    @event.teams << team42
    @event.teams << team43
    @event.teams << team44
    @event.teams << team45
    @event.teams << team46
    @event.teams << team47
    @event.teams << team48
    @event.teams << team49
    @event.teams << team50
    @event.teams << team51
    @event.teams << team52
    @event.teams << team53
    @event.teams << team54
    @event.teams << team55
    @event.teams << team56
    @event.teams << team57
    @event.teams << team58
    @event.teams << team59
    @event.teams << team60
    @event.teams << team61
    @event.teams << team62
    @event.teams << team63
    @event.teams << team64
    @event.teams << team65
    @event.teams << team66
    @event.teams << team67
    @event.teams << team68
    @event.teams << team69
    @event.teams << team70
    @event.teams << team71
    @event.teams << team72
    @event.teams << team73
    @event.teams << team74
    @event.teams << team75
    @event.teams << team76
    @event.teams << team77
    @event.teams << team78
    @event.teams << team79
    @event.teams << team80
    @event.teams << team81
    @event.teams << team82
    @event.teams << team83
    @event.teams << team84
    @event.teams << team85
    @event.teams << team86
    @event.teams << team87
    @event.teams << team88
    @event.teams << team89
    @event.teams << team90
    @event.teams << team91
    @event.teams << team92
    @event.teams << team93
    @event.teams << team94
    @event.teams << team95
    @event.teams << team96
    @event.teams << team97
    @event.teams << team98
    @event.teams << team99
    @event.teams << team100

  end
end