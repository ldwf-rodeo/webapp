namespace :db do
  desc 'initialize database'
  task :initialize => :environment do
    config   = Rails.configuration.database_configuration
    database = config[Rails.env]['database']

    sh "export PGPASSWORD=#{config[Rails.env]['password']}"

    sh "psql -d #{database} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/schema.sql"
  end
end