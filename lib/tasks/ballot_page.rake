desc 'Test the odt replacement for badge creation'
task :badge => :environment do

  working_dir = Dir.mktmpdir
  result_file = Tempfile.new(['result', '.pdf'])


  event = Event.find(34)

  handles = []

  # the satellite teams
  event.teams.each do |team|
    participants = team.participants

    ballot_path = "#{Rails.root}/stuff/badges/ballot_template.odt"

    ballots = Tempfile.new([team.id.to_s,'.odt'], working_dir)

    bal_report = ODFReport::Report.new(ballot_path) do |r|
      parsed_1 = []
      parsed_2 = []

      n = participants.each_slice(5).to_a

      parts1 = n[0]
      parts2 = n[1] || []


      parts1.each do |person|
        team = person.event_team(event).first
        part_num = person.participant_numbers.where(event: event).first

        value = "#{person.first_name} #{person.last_name}\n#{team.name} #{team.company}\n\n#{part_num.value if part_num}"
        parsed_1 << OpenStruct.new({value: value})
      end

      parts2.each do |person|
        team = person.event_team(event).first
        part_num = person.participant_numbers.where(event: event).first

        value = "#{person.first_name} #{person.last_name}\n#{team.name} #{team.company}\n\n#{part_num.value if part_num}"
        parsed_2 << OpenStruct.new({value: value})
      end

      r.add_table('TABLE_1', parsed_1) do |t|
        t.add_column(:person, :value)
      end

      r.add_table('TABLE_2', parsed_2) do |t|
        t.add_column(:person, :value)
      end


    end

    bal_report.generate(ballots.path)

    handles << ballots
  end

  # call libreoffice to batch convert the templates to PDFs
  `#{LIBREOFFICE_BINARY["path"]} --outdir "#{working_dir}" "#{working_dir}"/*.odt`
  `pdftk "#{working_dir}/"*.pdf cat output "/home/webapps/result.pdf"`

end