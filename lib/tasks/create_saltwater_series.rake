namespace :db do
  desc 'create saltwater series'
  task :create_saltwater_series => :environment do

    lass = Event.where(:name => "Louisiana Saltwater Series 2015").first_or_create! do |event|
      event.slug = 'lass15'
      event.start_date = '2015-08-21'
      event.end_date = '2015-11-10'
    end

    pounds = Unit.where(:description => 'Weight (lbs.)').first

    northshore = SuperCategory.where(
        :event => lass,
        :name => 'Northshore Boat-N-Fishing Show'
    ).first_or_create! do |sc|
      sc.page = 1
    end

    youth = Category.where(
        :super_category => northshore,
        :name => 'Youth'
    ).first_or_create! do |c|
      c.position = 2
      c.category_type = 'species_unit'
      c.positions = 10
      c.score_entity = 'team'
      c.score_grouping = 'top_n'
      c.score_grouping_limit = 2
      c.start_age = nil
      c.end_age = 15
      c.valid_start_time = '2015-03-20'
      c.valid_end_time = '2015-03-22'
      c.has_distinct_places = true
      c.score_1_source = 'score'
      c.score_1_order = 'desc'
      c.score_2_source = 'largest_subscore'
      c.score_2_order = 'desc'
      c.score_3_source = 'score'
      c.score_3_order = 'desc'
      c.display_mode = 'smallest_and_largest'
    end

    CategorySpeciesUnitItem.where(
        :category => youth
    ).first_or_create! do |i|
      i.species = Species.where(:name => 'Red Drum').first
      i.unit = pounds
    end

    regular = Category.where(
        :super_category => northshore,
        :name => 'Regular'
    ).first_or_create! do |c|
      c.position = 1
      c.category_type = 'species_unit'
      c.positions = 10
      c.score_entity = 'team'
      c.score_grouping = 'top_n'
      c.score_grouping_limit = 2
      c.start_age = 16
      c.end_age = nil
      c.valid_start_time = '2015-03-20'
      c.valid_end_time = '2015-03-22'
      c.has_distinct_places = true
      c.score_1_source = 'score'
      c.score_1_order = 'desc'
      c.score_2_source = 'largest_subscore'
      c.score_2_order = 'desc'
      c.score_3_source = 'score'
      c.score_3_order = 'desc'
      c.display_mode = 'smallest_and_largest'
    end

    CategorySpeciesUnitItem.where(
        :category => regular
    ).first_or_create! do |i|
      i.species = Species.where(:name => 'Red Drum').first
      i.unit = pounds
    end

    #Add participant types.
    ParticipationType.where(:name => 'Developers').first_or_create!
    ParticipationType.all.each do |participation_type|
      EventParticipationType.where(
          :event => lass,
          :participation_type => participation_type
      ).first_or_create! do |ept|
        ept.number_padding = 3
        ept.number_prefix = ''
        ept.number_suffix = ''
      end
    end

    if !lass.units.include?(pounds)
      lass.units << pounds
    end

    developer = EventParticipationType.where(:participation_type => ParticipationType.where(:name => 'Developers').first, :event => lass).first

    #Add teams.
    uno = Team.where(:event => lass, :name => 'UNO Saltwater Developers').first_or_create! do |uno|

    end

    #Add participations
    Participation.where(:team => uno, :event => lass, :event_participation_type => developer, :participant => Participant.where(:first_name => 'Nathan', :last_name => 'Cooper').first, :number => 88).first_or_create!
    Participation.where(:team => uno, :event => lass, :event_participation_type => developer, :participant => Participant.where(:first_name => 'Daniel', :last_name => 'Ward').first, :number => 90).first_or_create!
    Participation.where(:team => uno, :event => lass, :event_participation_type => developer, :participant => Participant.where(:first_name => 'Devin', :last_name => 'Frey').first, :number => 1).first_or_create!
    Participation.where(:team => uno, :event => lass, :event_participation_type => developer, :participant => Participant.where(:first_name => 'Dustin', :last_name => 'Peabody').first, :number => 27).first_or_create!

  end

end