require 'open-uri'
desc 'Test the odt replacement for badge creation'
task :odt_test => :environment do

  TEMPLATE_PATH = '/Users/dpeabody/Development/rodeo_rails/stuff/test_replace.otg'

  event = Event.all.first
  participant = event.get_participants.first

  file = Tempfile.new('qr_code_participant.png')
  outfile = Tempfile.new('out.otg')

  open(file.path, 'wb') do |temp_file|
    uri = URI.parse(URI.encode(participant.qr_code(200,event).strip))
    temp_file << open(uri).read
  end

  report = ODFReport::Report.new(TEMPLATE_PATH) do |r|
    r.add_field :help, 'test worked'
    r.add_image :derp , file.path #"#{Rails.root}/#{file.path}"
  end

  report.generate(outfile.path)

  sh "#{LIBREOFFICE_BINARY["path"]} --outdir #{File.dirname(outfile.path)} #{outfile.path}"
  sh "mv #{File.dirname(outfile.path)}/out.pdf ~/"

  outfile.delete
  file.delete
end