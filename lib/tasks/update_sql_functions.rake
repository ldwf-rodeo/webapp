namespace :db do
  desc 'update SQL functions'
  task :update_sql_functions => :environment do
    config   = Rails.configuration.database_configuration
    database = config[Rails.env]['database']

    sh "PGPASSWORD=#{config[Rails.env]['password']} psql -d #{database} -h #{config[Rails.env]['host']} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/sql/leaderboard_compute.sql"
    sh "PGPASSWORD=#{config[Rails.env]['password']} psql -d #{database} -h #{config[Rails.env]['host']} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/sql/leaderboard_display.sql"
    sh "PGPASSWORD=#{config[Rails.env]['password']} psql -d #{database} -h #{config[Rails.env]['host']} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/sql/gapless_sequence.sql"
    sh "PGPASSWORD=#{config[Rails.env]['password']} psql -d #{database} -h #{config[Rails.env]['host']} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/sql/utilities.sql"
    sh "PGPASSWORD=#{config[Rails.env]['password']} psql -d #{database} -h #{config[Rails.env]['host']} -U #{config[Rails.env]['username']} -f #{Rails.root}/db/sql/event_views.sql"
  end
end