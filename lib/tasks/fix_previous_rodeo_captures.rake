namespace :db do

  desc 'fix previous rodeo captures'
  task :fix_previous_rodeo_captures => :environment do

    ActiveRecord::Base.transaction do

      redfish_stringer = Species.where(:name => 'Red Fish (5) Stringer').first
      trout_stringer = Species.where(:name => 'Speckled Trout (5) Stringer').first

      fourchon_2015 = Event.find(36)

      # ----------------------------------
      # Fourchon 2015
      # Currently using deprecated species, but rules changed to be inclusive. Replace.
      # ----------------------------------

      #Convert captures

      weight = Unit.first

      fourchon_2015.captures.each do |capture|
        if capture.species == redfish_stringer || capture.species == trout_stringer
          # We also used lbs* for stringers when there was no need to.
          measurement = capture.capture_measurements.first
          measurement.unit = weight
          measurement.save!
        end

        if capture.species == redfish_stringer
          capture.is_redundant = true
          capture.save!
        elsif capture.species == trout_stringer
          capture.is_redundant = true
          capture.save!
        end
      end

      #Convert categories

      redfish_2015_friday = fourchon_2015.categories.where(:name => 'Redfish (Friday)').first
      redfish_2015_saturday = fourchon_2015.categories.where(:name => 'Redfish (Saturday)').first

      trout_2015_friday = fourchon_2015.categories.where(:name => 'Speckled Trout (Friday)').first
      trout_2015_saturday = fourchon_2015.categories.where(:name => 'Speckled Trout (Saturday)').first

      [redfish_2015_friday, redfish_2015_saturday].each do |category|
        category.category_species_unit_items.first.destroy
        CategorySpeciesUnitItem.create!(:category => category, :species => redfish_stringer, :unit => weight)
      end

      [trout_2015_friday, trout_2015_saturday].each do |category|
        category.category_species_unit_items.first.destroy
        CategorySpeciesUnitItem.create!(:category => category, :species => trout_stringer, :unit => weight)
      end

      # ----------------------------------
      # Faux Pas Rodeos
      # Blue Marlin is still using the count weight. This should be the new species_capture type.
      # ----------------------------------

      count = Unit.find(5)

      #Remove all categories that are referencing count and replace it with the right item.
      CategorySpeciesUnitItem.where(:unit => count).each do |item|
        CategorySpeciesCaptureItem.create!(:category => item.category, :species => item.species)
        category = item.category
        category.category_type = :species_capture
        category.save!
        item.destroy!
      end

      #Destroy all capture measurements for count.
      count.capture_measurements.each do |cm|
        cm.destroy!
      end

      count.destroy

    end

  end

end