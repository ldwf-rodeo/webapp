require 'twilio-ruby'

module SMSModule

  class TwilioClient

    def self.build_client
      Twilio::REST::Client.new(TWILIO_CONFIG['sid'], TWILIO_CONFIG['token'])
    end

    def self.get_new_number
      client =  build_client
      numbers = client.account.available_phone_numbers.get('US').local.list(:area_code => '504')
      numbers[0].phone_number #return the first availble number
    end

    def self.buy_new_number
      client = build_client
      number = get_new_number
      client.account.incoming_phone_numbers.create(:phone_number => number)
      return number
    end

  end

  class SMSSender

    def self.log_message_out(participation,message)
      participant = participation.participant
      team = participation.team
      from_name = "#{participant.full_name} (#{participant.mobile_phone || participant.home_phone})"
      event_name = "#{team.event.name} (#{team.event.twilio_phone})"
      Rails.logger.sms.info "[SENDING] [#{event_name}] [#{from_name}] #{message}"
    end

    #
    # SEND TO A NUMBER
    #
    def self.send_generic_message_to_participation(participation,message)

      if participation.nil?
        Rails.logger.sms.info 'Not sending to nil participation: ' + message
        return
      end

      participant = participation.participant

      if !participant.does_receive_notifications
        Rails.logger.sms.info '[NO SEND] Could not send to opted-out participant ' + participant.full_name + ': ' + message.to_s
        return
      end

      number = participant.mobile_phone || participant.home_phone
      if number.nil?
        Rails.logger.sms.info "[NO SEND] Not sending to participation #{participant.full_name} with no valid phone numbers: " + message.to_s
        return
      end

      from_number = participation.team.event.twilio_phone
      if from_number.nil?
        Rails.logger.sms.info "[NO SEND] Not sending to participation #{participant.full_name} with no valid event phone:" + message.to_s
        return
      end

      #Check parameters.
      if number.nil? or number.empty?
        raise SMSModule::SMSError.new '[NO SEND] Could not send text to nil or empty number: ' + message.to_s
      elsif message.nil? or message.empty?
        raise SMSModule::SMSError.new '[NO SEND] Could not send text with nil or empty message: ' + message.to_s
      end

      message = SMSSender.format_message(message, participation)

      log_message_out(participation,message)

      begin

        to = TWILIO_CONFIG['to'] || ('+1' + number.gsub(/^\+1/,'').gsub(/[^0-9]/,''))
        client = TwilioClient.build_client
        sms_message = client.account.messages.create(
            from: TWILIO_CONFIG['from'] || from_number,
            to: TWILIO_CONFIG['to'] || to,
            body: message
        )

        Rails.logger.sms.info 'TWILIO SMS STATUS TO ' + to + ': ' + ' => ' + sms_message.status

      # catch twilio errors
      rescue Twilio::REST::RequestError => e
        code = e.code
        Rails.logger.sms.info 'TWILIO ERROR CODE: ' + e.code.to_s

        case code
          when TWILIO_BLACKLIST_ERROR
            # remove from the list of recipients

          when TWILIO_CANNOT_ROUTE_ERROR
            # probably a number that we cannot send to
            unsubscribe_participant number

          when TWILIO_INVALID_NUMBER_ERROR
            # not a real phone number
            unsubscribe_participant number

          when TWILIO_INCAPABLE_OF_SMS_ERROR
            # this is probably a home phone
            unsubscribe_participant number

          else
            # message successful
        end

      rescue Exception => e
        Rails.logger.sms.info 'ERROR SENDING TEXT TO ' + number + ': ' + message + ' => ' + e.message
      end

    end

    #
    # SEND TO TEAM CAPTAIN
    #
    def self.send_generic_message_to_team_captain(team, message)

      if team.nil?
        Rails.logger.sms.info 'Could not send text to nil team: ' + message
        return
      end

      Rails.logger.sms.info "[TEAM CAPTAIN OUTGOING] [#{team.name}] #{message}"

      if !team.does_receive_notifications
        Rails.logger.sms.info 'Could not send to opted-out team ' + team.name + ': ' + message
        return
      end

      participation = Participation.where(:event => team.event, :participant => team.captain).first

      message = SMSSender.format_message(message, participation)

      SMSSender.send_generic_message_to_participation(participation,message)
    end

    #######################################
    #######################################

    def self.send_test_twilio(team)
      # Instantiate a Twilio client
      client = Twilio::REST::Client.new(TWILIO_CONFIG['sid'], TWILIO_CONFIG['token'])

      team.participants.each do |participant|

        # Create and send an SMS message
        begin
          client.account.sms.messages.create(
              from: TWILIO_CONFIG['from'] || team.event.twilio_phone,
              to: '+1' + participant.mobile_phone.gsub(/[^0-9]/,''),
              body: 'Thanks for signing up. Reply with STOP to stop messages'
          )
        rescue Exception => e
          Rails.logger.sms.notice 'Could not send text to ' + participant.full_name + ' (' + participant.mobile_phone + ').'
        end

      end

    end

    #####################################################################
    #MESSAGE COMPOSITION
    #####################################################################

    def self.get_standings_messages(event)
      query = <<-SQL
        SELECT
          board.participant_id,
          board.team_id,
          super_categories.name as super_category_name,
          categories.name as category_name,
          board.position as position,
          board.is_empty as is_empty,
          board.entity_label as entity_label,
          CASE WHEN is_empty THEN NULL ELSE concat(score_label,' ',unit_label) END as score_and_unit_label
        FROM
          get_event_leaderboard_display_for_event(#{event.id},NULL) as board
          INNER JOIN categories ON categories.id = board.category_id
          INNER JOIN super_categories on super_categories.id = categories.super_category_id
        WHERE
          board.position = 1
        ORDER BY
          super_categories.page ASC,
          super_categories.position ASC,
          super_categories.name ASC,
          super_categories.id ASC,
          categories.grouping_number ASC,
          categories.position ASC,
          categories.name ASC,
          categories.id ASC,
          board.position
      SQL

      results = ActiveRecord::Base.connection.execute(query)
      return leaderboard_result_parser(results, event)

    end

    def self.get_specific_categories(name, event, places = nil)
      places.nil?

      query =  <<-SQL
        SELECT
          board.participant_id,
          board.team_id,
          super_categories.name as super_category_name,
          categories.name as category_name,
          board.position as position,
          board.is_empty as is_empty,
          board.entity_label as entity_label,
          CASE WHEN is_empty THEN NULL ELSE concat(score_label,' ',unit_label) END as score_and_unit_label
        FROM
          get_event_leaderboard_display_for_event(#{event.id},NULL) as board
          INNER JOIN categories ON categories.id = board.category_id
          INNER JOIN super_categories on super_categories.id = categories.super_category_id
        WHERE
          board.position <@ int8range(1, COALESCE(?, categories.positions), '[]')
          AND
          (
            super_categories.name ILIKE COALESCE(?,super_categories.name)
            OR
            categories.name ILIKE COALESCE(?,categories.name)
          )
        ORDER BY
          super_categories.page ASC,
          super_categories.position ASC,
          super_categories.name ASC,
          super_categories.id ASC,
          categories.grouping_number ASC,
          categories.position ASC,
          categories.name ASC,
          categories.id ASC,
          board.position
      SQL

      name = "%#{name}%" unless name.nil?

      sql = ActiveRecord::Base.send(:sanitize_sql_array, [query, places, name, name])
      results = ActiveRecord::Base.connection.execute(sql)

      return leaderboard_result_parser(results, event)
    end

    def self.unsubscribe_participant(number)
      received_number = number.gsub(/^\+1/,'').gsub(/[^0-9]/, '')
      p = Participant.where("regexp_replace(home_phone, '[^0-9], '') = ? or regexp_replace(mobile_phone, '[^0-9], '') = ?", received_number, received_number)
      p.each { |p| p.update_column(:does_receive_notifications, false) }
    end

    def self.split_string_to_sms_chunks(str, delimeter = /\ /)
      msgs = []
      msg = ''
      words = str.split(delimeter)
      words.each do |word|
        if (msg + ' ' + word).length > 1590
          msgs << msg
          msg = word
        else
          msg = (msg + ' ' + word);
        end
      end
      msgs << msg
      return msgs
    end

    def self.format_message(message, participation)
=begin
      [[t_name]]        # team name
      [[t_number]]      # team number
      [[e_name]]        # event name
      [[e_leaderboard]] # event url
      [[p_full_name]]   # participant full name
      [[p_first_name]]  # participant first name
      [[p_last_name]]   # participant last name
      [[p_number]]      # participant number
=end
      if message.match /\[\[.+\]\]/

        event = participation.event
        team = participation.team
        participant = participation.participant

        message
            .gsub(/\[\[t_name\]\]/, team.name || '')
            .gsub(/\[\[t_number\]\]/, team.number.try(:to_s) || '')
            .gsub(/\[\[e_name\]\]/, event.name || '')
            .gsub(/\[\[e_leaderboard\]\]/, event.short_leaderboard_url || '')
            .gsub(/\[\[p_full_name\]\]/, participant.full_name || '')
            .gsub(/\[\[p_first_name\]\]/, participant.first_name || '')
            .gsub(/\[\[p_last_name\]\]/, participant.last_name || '')
            .gsub(/\[\[p_number\]\]/, participation.display_number || '')

      else
        message
      end
    end

    private

    def self.leaderboard_result_parser(results, event)
      msg = 'Current ' + event.name + " Top Standings:\n"
      super_category = nil
      category = nil

      # holds the message chunks
      msgs_result = []

      results.each do |result|

        result = result.symbolize_keys.delete_if {|k,v| v.nil? or v.empty?}

        if result[:super_category_name] != super_category
          super_category = result[:super_category_name]
          msg = msg + '--- ' + result[:super_category_name] + " ---\n"
        end

        if result[:category_name] != category
          category = result[:category_name]
          msg = msg + result[:category_name] + "\n"
        end

        if result[:participant_id].present?
          participant = Participant.find(result[:participant_id])
          label = participant.full_name
        elsif result[:team_id].present?
          team = Team.find(result[:team_id])
          label = team.number.nil? ? team.name : ("#" + team.number.to_s)
        end

        msg = msg + ' ' + (
        (result[:is_empty] == 't')  ? '(Empty)' : ( label + ' -> ' + result[:score_and_unit_label])
        )

        msg = msg + "\n"

        # if the accumulated message length is over 1200, go ahead and create a chunk
        if msg.length > 1200
          msgs_result << msg
          msg = ''
        end
      end

      # add the last chuck of messages
      msgs_result << msg

      # clean up the array: remove nils and empty strings
      msgs_result = msgs_result.compact.reject {|m| m.strip.blank? }

      # if there are more than 1 chunks, then add a message count header
      if msgs_result.count > 1
        msgs_result = msgs_result.each_with_index.map {|m, i| "Message #{i+1} of #{msgs_result.count}\n\n" + m }
      end

      # the messages arrive in revers order when left in logical order.
      # reversing the result so they will more likely arrive in the proper order
      return msgs_result.reverse

    end

  end

  class SMSError < StandardError
    # attr_reader :code

    def initialize(message)
      super message
      # @code = code
    end
  end

end