source 'https://rubygems.org'

gem 'rails', '4.2.5'

# webserver gems
gem 'websocket-rails', '0.7.0', git: 'https://github.com/dwa012/websocket-rails.git' # adds websockets

# database gems
gem 'pg' # PostgreSQL
gem 'enum_type' # ends active record extension for enums
gem 'enum_simulator', :git => 'git://github.com/dwa012/enum_simulator.git' # creates virtual enums
gem 'activerecord-native_db_types_override' # override active record types with native ones

# authentication gems
gem 'devise' # authentication
gem 'rolify' # user roles
gem 'cancancan', :git =>'https://github.com/ncooper123/cancancan.git', :branch => 'develop'
gem 'paper_trail', '~> 4.0.0.rc' # keeps versions of models, and audit history

# site adming tools
# gem 'rails_admin', git: 'https://github.com/dwa012/rails_admin.git'

# adds the haml compiler
gem 'haml-rails' # haml compilerf

# sued for parsing imported spreadsheets
gem 'rubyzip', '< 1.0.0' # needed for ROO
gem 'roo' # spreadsheet parsing
gem 'people', git: 'https://github.com/dwa012/people.git' # string to name component parsing

# model enhancing gems
gem 'bcrypt-ruby', '~> 3.0.0' # used for enabling secure passwords on acive model
gem 'geocoder' # normalizes addresss info
gem 'annotate' # create schema annotations for models
gem 'paperclip' # used for uploading files from a client
gem 'rails-jquery-autocomplete', github: 'dwa012/rails-jquery-autocomplete', branch: 'dwa012-patch-1' # allows for autocompletion of fields
gem 'nilify_blanks' # converts empty strings to nils
gem 'axlsx_rails' # rails responder
gem 'jc-validates_timeliness' # date time validator

# utility gems
gem 'numbers_and_words' # has tools for convert words to numbers and vice versa
gem 'bitly' # creates bityl links
gem 'chronic' # date time parsing
gem 'twilio-ruby' # used for sending SMS messages
gem 'jbuilder', '~> 1.0.1' # used for making json strings less painful
gem 'multi_logger' # for splitting logs.
gem 'yajl-ruby', require: 'yajl' # faster json building
gem 'colorize'

# Gems for badge generation
gem 'odf-report' # can read ODF files and write values to them
gem 'rqrcode_png' # can create QRCode PNGs

# front end tools
gem 'jquery-rails' # JQuery
gem 'turbolinks' # used for faster page loads
gem 'browser-timezone-rails' # gets the timezone of a clients browser
gem 'font-awesome-rails' # addes the font awesome library to the assets pipleline
gem 'recaptcha', :require => 'recaptcha/rails' # adds recaptcha helpers
gem 'route_downcaser' #allows route urls to be case insensitive

# logging
gem 'lograge' # use a different logging tool
gem 'logstash-event' # the logstash JSON formatter helper
gem 'device_detector' # user agent parser

# assets
gem 'sass-rails'
gem 'coffee-rails'
gem 'uglifier', '>= 1.0.3'

gem 'hiredis', '~> 0.6.0'
gem 'redis', '>= 3.2.0', :require => %w(redis redis/connection/hiredis)
gem 'resque' # for queuing jobs
gem 'resque-pool' # for queuing jobs
gem 'resque-web', require: 'resque_web'
gem 'active_scheduler'
gem 'resque-scheduler-web'

gem 'puma'
gem 'figaro'

group :development do
  gem 'byebug'
  gem 'spring'
  gem 'web-console', '~> 2.0'
  gem 'yard'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
end

group :development do
  gem 'capistrano' # used to help automate deployments
  gem 'capistrano-rails-console'
  gem 'capistrano-rails' # rails specific stuff
  gem 'rvm1-capistrano3', require: false # adds RVM to capistrano
  gem 'capistrano3-unicorn' # allows us to start and stop the unicorn process
  gem 'capistrano-resque', require: false # can control resque
  gem 'capistrano3-puma'
  gem 'capistrano3-postgres', require: false
  gem 'capistrano-db-tasks', require: false
  gem 'capistrano-resque-pool', require: false
  gem 'capistrano-eye', git: 'https://github.com/dwa012/capistrano-eye.git', branch: 'dwa012-patch-1'
  gem 'capistrano-figaro-yml'
  gem 'capistrano-scm-copy', git: 'https://github.com/dwa012/capistrano-scm-copy.git', branch: 'patch-1'
end