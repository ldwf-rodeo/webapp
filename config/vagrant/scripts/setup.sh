#!/bin/bash

# Load RVM into a shell session *as a function*
# Loading RVM *as a function* is mandatory
# so that we can use 'rvm use <specific version>'
if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then
  # First try to load from a user install
  source "$HOME/.rvm/scripts/rvm"
  echo "using user install $HOME/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
  # Then try to load from a root install
  source "/usr/local/rvm/scripts/rvm"
  echo "using root install /usr/local/rvm/scripts/rvm"
else
  echo "ERROR: An RVM installation was not found.\n"
fi

rvm use ruby-$VAGRANT_RUBY_VERSION@vagrant --create

## Check for TMP directory
if [ ! -d "$HOME/workspace" ]
then
    echo "Create workspace directory."
	ln -s /vagrant $HOME/workspace
fi

## Check for TMP directory
if [ ! -d "/vagrant/log" ]
then
    echo "Create log directory."
	mkdir -p /vagrant/log
fi

## Check for pids directory
if [ ! -d "/vagrant/tmp/pids" ]
then
	echo "Creating pids in ~/workspace/tmp/pids"
	mkdir -p /vagrant/tmp/pids
fi

## move into the workspace directory
cd $HOME/workspace

gem install foreman

# install the gems
bundle install

if [ ! -z `psql -lqt | cut -d \| -f 1 | sed 's/\s//g' | grep $DEVELOPMENT_DATABASE_NAME` ];then
    dropdb $DEVELOPMENT_DATABASE_NAME
fi

# build the database if needed
echo "Setting up the database"
export RAILS_ENV='development'
bundle exec rake db:create
bundle exec rake db:rebuild_from_remote SKIP_DOWNLOAD=true IF_NEEDED=TRUE
bundle exec rake db:migrate

echo "Setting up the staging database"

if [ ! -z `psql -lqt | cut -d \| -f 1 | sed 's/\s//g' | grep $STAGING_DATABASE_NAME` ];then
    dropdb $STAGING_DATABASE_NAME
fi

# create the needed staging database
createdb $STAGING_DATABASE_NAME -O vagrant -T $DEVELOPMENT_DATABASE_NAME