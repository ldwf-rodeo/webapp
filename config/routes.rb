require 'resque_web'

RodeoRails4::Application.routes.draw do

  scope '/twilio', as: 'twilio' do
    post '/sms',    to: 'twilio#sms'
    post '/voice',  to: 'twilio#voice'
  end

  #error routes
  get '/404', :to => 'errors#not_found'
  get '/422', :to => 'errors#unacceptable'
  get '/500', :to => 'errors#internal_error'
  get '/401', :to => 'errors#unauthorized'

  devise_for :users

  scope '/admin' do
    get '/', to: 'admin#landing'

    scope '/users/:id/' do
      get '/passwords/edit' => 'users#edit_password', as: 'admin_edit_password'
      patch '/passwords/update' => 'users#update_password', as: 'admin_update_password'
    end

    resources :users

    resources :events do
      post '/clone' => 'events#clone'
      post '/import' => 'events#import', as: 'admin_events_import'
      post '/upload' => 'events#upload'
      resources :users, :controller => 'event_users', :as => 'users', :only => [:index, :edit, :update]
    end

    resque_web_constraint = lambda do |request|
      current_user = request.env['warden'].user
      current_user.present? && current_user.has_role?(:admin)
    end

    constraints resque_web_constraint do
      mount ResqueWeb::Engine => "/resque_web"
      # mount RailsAdmin::Engine => '/rails'
    end
  end

  root :to => 'welcome#index'
end

RodeoRails4::Application.routes.draw do

  match "/websocket", :to => WebsocketRails::ConnectionManager.new, via: [:get, :post]

  scope '/:event_slug' do
    get '/' => 'event_public#landing', as: 'event_landing'
    get '/facts' => 'event_public#facts_show'

    scope 'leaderboard', as: 'leaderboard' do
      get '/' => 'event_public#leaderboard'
      get '/categories/:id' => 'leaderboard#category', as: 'category'
      get '/teams/:id' => 'leaderboard#team', as: 'team'

      scope 'api', as: 'api', :defaults => {:format => 'json'} do
        get '/event' => 'leaderboard#event_leaderboard', as: 'event'
        get '/unlimited' => 'leaderboard#unlimited_leaderboard', as: 'unlimited'
      end

    end

    get '/leaderboard_at_capture' => 'leaderboard#leaderboard_at_capture'
    get '/get_empty_leaderboard_html' => 'leaderboard#get_empty_leaderboard_html'

    get '/participants/summary/:participant_id' => 'participants#summary'

    scope '/teams' do
      get '/' => 'teams#teams'
      get '/registration' => 'teams#registration'
      post '/registration' => 'teams#registration_create'
      get '/:team_slug' => 'teams#leaderboard'
      get '/:team_slug/confirmation' => 'teams#captain_edit_confirmation', as: 'captain_edit_confirmation_dos'
      get '/:team_slug/:token' => 'teams#captain_edit'
      patch '/:team_slug/:token' => 'teams#captain_edit_commit', as: 'captain_edit'
    end

    scope '/admin' do
      get '/' => 'events#admin'

      scope :reports, as: 'reports' do
        get '/results' => 'event_reports#results'
        get '/categories' => 'event_reports#show_categories'
        get '/team_categories' => 'event_reports#team_categories'
      end

      scope 'sms' do
        get '/' => 'events_sms#index', as: 'sms'
        put '/send_standings' => 'events_sms#send_standings_sms', as: 'sms_send_standings'
        put '/send_custom' => 'events_sms#send_custom_sms', as: 'sms_send_custom'
      end

      resources :captures

      get '/teams/team_lookup', to: 'teams#team_lookup'

      resources :teams do
        member do
          get 'badges', defaults: { format: 'pdf' }
        end

        collection do
          get '/check_badge_status', to: 'teams#check_badge_status'
        end

        get '/check_in' => 'teams#check_in'
        get '/check_out' => 'teams#check_out'
      end

      resources :categories do
        resources :category_items
      end

      resources :super_categories do
        get '/categories' => 'super_categories#categories_index'
      end

      resources :premium_category_groupings

      resources :event_scale_openings

      resources :participants do
        member do
          get 'badge', defaults: { format: 'pdf'}
        end

        get '/get_info', action: :get_info, on: :collection
      end

      resources :participations, except: [:new, :create, :edit, :update, :destroy] do
        get :autocomplete_participation_capture, on: :collection
        get :autocomplete_participation_form, on: :collection
        get '/get_info', action: :get_info, on: :collection
      end

    end
  end
end