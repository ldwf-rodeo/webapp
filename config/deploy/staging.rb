set :stage, :staging

set :host_ip, 'localhost'
set :port, 22
set :user, 'webapps'
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, :copy
set :exclude_dir, ['.vagrant', 'tmp', 'test', 'config/application.yml','Vagrantfile', 'log', 'config/ansible', 'storage', 'db/current.db.backup.enc']

set :rails_env, :staging
set :conditionally_migrate, true

set :rvm1_ruby_version, 'ruby-2.3.0@rails_staging'

set :bundle_without, %w{development test production}.join(' ')
set :bundle_env_variables, { nokogiri_use_system_libraries: 1 }

role :resque_worker, fetch(:host_ip)
role :resque_scheduler, fetch(:host_ip)

set :resque_log_file, "log/resque.log"

set :keep_releases, 1

server fetch(:host_ip),
       roles: [:web, :app, :db],
       port: fetch(:port),
       user: fetch(:user),
       primary: true

set :deploy_to, "/home/#{fetch(:user)}/apps/rails_server"

set :ssh_options, {
                    forward_agent: true,
                    user: fetch(:user),
                    password: 'webapps'
                }

# configure the eye components
set :eye_env, -> { {rails_env: fetch(:rails_env)} }
set :eye_config, -> { "#{current_path}/config/eye/#{fetch :rails_env}.rb" }

### Puma options
set :puma_threads, [0, 2]