set :application, 'rodeo'
set :repo_url, 'git@gitlab.com:ldwf-rodeo/webapp.git'

set :workers, { sms: 1, badges: 2, leaderboard: 1 }

ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :use_sudo, false
set :bundle_binstubs, nil
# set :keep_releases, 10
# set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

after 'deploy:finished', 'deploy:restart'

namespace :deploy do
  task :restart do
    invoke 'unoconv:start'
    invoke 'leaderboard:reload_sql'
    invoke 'resque:restart'
    invoke 'websocket_rails:restart'
    invoke 'unicorn:restart'
  end
end
