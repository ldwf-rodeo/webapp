# Load the rails application.
require File.expand_path('../application', __FILE__)

# Initialize the rails application.
RodeoRails4::Application.initialize!

#Removes redundant whitespace from a string and converts it to nil if it winds up empty.
def manual_nilify(string)
  if (string == nil)
    return nil
  else
    trimmed = string.to_s.split.join(' ')
    if (trimmed.empty?)
      return nil
    else
      return trimmed
    end
  end
end
