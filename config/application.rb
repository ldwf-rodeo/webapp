require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)


module RodeoRails4
  class Application < Rails::Application
    #auto load everything in lib, Modules mostly
    config.exceptions_app = self.routes
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{Rails.root}/lib)

    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.active_job.queue_adapter = :resque
    config.active_job.logger = MultiLogger.add_logger('active_job')

    config.active_record.raise_in_transactional_callbacks = true
    config.active_record.raise_in_transactional_callbacks = true

    # Enable escaping HTML in JSON.
    # config.active_support.escape_html_entities_in_json = true

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    config.active_record.schema_format = :sql

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '2.0'

    config.lograge.enabled = true
    config.lograge.keep_original_rails_log = true
    config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/lograge_#{Rails.env}.log"
    config.lograge.formatter = Lograge::Formatters::Logstash.new
    config.lograge.custom_options = lambda do |event|
      params = event.payload[:params].reject do |k|
        ['controller', 'action', 'format', 'utf8'].include? k
      end

      {
          remote_ip: event.payload[:remote_ip],
          user_id: event.payload[:user_id],
          device:  event.payload[:device],
          params: params
      }
    end
  end
end
