TWILIO_TEST_NUMBERS = {
    invalid: '+15005550001',
    non_routable: '+15005550002',
    blacklist: '+15005550004',
    non_sms: '+15005550009',
    valid: '+15005550006'
}

TWILIO_INVALID_NUMBER_ERROR = 21211
TWILIO_BLACKLIST_ERROR = 21610
TWILIO_INCAPABLE_OF_SMS_ERROR = 21614
TWILIO_CANNOT_ROUTE_ERROR = 21612

path = File.join(Rails.root, "config/twilio.yml")
TWILIO_CONFIG = YAML.load(File.read(path))[Rails.env] || {'sid' => '', 'from' => '', 'token' => ''}

