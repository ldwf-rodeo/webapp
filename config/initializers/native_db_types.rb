NativeDbTypesOverride.configure({
                                    postgres: {
                                        integer: { name: 'bigint' },
                                        datetime: { name: "timestamptz" },
                                        timestamp: { name: "timestamptz" }
                                    }
                                })