MultiLogger.add_logger('sms')

MultiLogger.add_logger('skynet')

def log_skynet_incoming(message)
  from_name = (!defined?(@participant) or @participant.nil?) ? "Unknown Participant (#{@from_number})" : "#{@participant.full_name} (#{@from_number})"
  event_name = (!defined?(@event) or @event.nil?) ? 'NO EVENT' : "#{@event.name} (#{@from_number})"
  logger.skynet.info "[RECEIVING] [#{event_name}] [#{from_name}] #{message}"
end

def log_skynet_outgoing(message)
  from_name = (!defined?(@participant) or @participant.nil?) ? "Unknown Participant (#{@from_number})" : "#{@participant.full_name} (#{@from_number})"
  event_name = (!defined?(@event) or @event.nil?) ? 'NO EVENT' : "#{@event.name} (#{@from_number})"
  logger.skynet.info "[SENDING] [#{event_name}] [#{from_name}] #{message}"
end