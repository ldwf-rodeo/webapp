#!/bin/bash

#create species

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $1 }' | sort | uniq | awk 'BEGIN { FS=":"}; { print "Species.where(:name => \"" $1 "\").first_or_create" }' > import.rb

echo "" >> import.rb
echo "" >> import.rb
echo "" >> import.rb

# create participants

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $3 }' | sort | uniq | sed 's/^\([^ ]*\) /\1:/' |  awk 'BEGIN { FS=":"}; { print "Participant.create(:first_name => \"" $1 "\", :last_name => \"" $2 "\", :participant_type => :angler, :gender => :male)" }' >> import.rb

echo "" >> import.rb
echo "" >> import.rb
echo "" >> import.rb

# create teams

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $4 ":" $3 }' | sort -u -t: -k1,1 | sed 's/^\([^:]*[^ ]*\) /\1:/' | awk 'BEGIN { FS=":"}; { print "Team.create(:name => \""$1"\", :captain_id => Participant.where(:first_name => \""$2"\", :last_name => \""$3"\").first.id )" }' >> import.rb

echo "" >> import.rb
echo "" >> import.rb
echo "" >> import.rb

# associate participants to teams 

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $4 ":" $3 }' | sort | uniq | sed 's/^\([^:]*[^ ]*\) /\1:/' | awk 'BEGIN { FS=":"}; { print "Team.where(:name => \""$1"\").first.participants <<  Participant.where(:first_name => \""$2"\", :last_name => \""$3"\").first" }' >> import.rb

echo "" >> import.rb
echo "" >> import.rb
echo "" >> import.rb

# associate teams

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $4 ":" $3 }' | sort -u -t: -k1,1 | sed 's/^\([^:]*[^ ]*\) /\1:/' | awk 'BEGIN { FS=":"}; { print "@event_fourchon.teams << Team.where(:name => \""$1"\").first " }' >> import.rb

# create captures

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $1 ":" $3 ":" $7  }' | sed 's/^\([^:]*[^ ]*\) /\1:/' | awk 'BEGIN { FS=":"}; { print "@capture" NR " = Capture.create(:participant_id => Participant.where(:first_name => \""$2"\", :last_name => \"" $3 "\").first.id, :species_id => Species.where(:name => \""$1"\").first.id, :weight => "$4", :user_id => @u.id, :time_entered => Time.now() )"}' >> import.rb

echo "" >> import.rb
echo "" >> import.rb
echo "" >> import.rb


# associate captures

cat rodeo_data.csv | awk 'BEGIN { FS=":"}; { print $3 ":" $1 ":" $7  }' | sed 's/^\([^ ]*\) /\1:/' | awk 'BEGIN { FS=":"}; { print "@event_fourchon.captures << @capture" NR }' >> import.rb